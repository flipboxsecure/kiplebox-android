package com.kiple.kipleBox.database;

import android.content.Context;

import com.kiple.kipleBox.model.responses.TicketListsResponse;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LocalCacheManager {

    private Context context;
    private static LocalCacheManager _instance;
    private AppDatabase db;

    public static LocalCacheManager getInstance(Context context) {
        if (_instance == null) {
            _instance = new LocalCacheManager(context);
        }
        return _instance;
    }

    private LocalCacheManager(Context context) {
        this.context = context;
        db = AppDatabase.getAppDatabase(context);
    }

    public void getAllTicket(final DBInterface dbInterface) {
        db.ticketDao().getAllTicket().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<TicketListsResponse>>() {
            @Override
            public void accept(List<TicketListsResponse> tickets) throws Exception {
                dbInterface.onDBTicketLoaded(tickets);
            }
        });
    }

    public void getNotUpdatedList(final DBInterface dbInterface) {
        db.ticketDao().getNotUpdatedList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<TicketListsResponse>>() {
            @Override
            public void accept(List<TicketListsResponse> tickets) throws Exception {
                dbInterface.onDBTicketLoaded(tickets);
            }
        });
    }

    public void getTicket(final DBInterface dbInterface, String ticketId, String status) {
        db.ticketDao().getTicket(ticketId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<TicketListsResponse>>() {
            @Override
            public void accept(List<TicketListsResponse> tickets) throws Exception {
                dbInterface.onDBTicketLoaded(tickets);
            }
        });
    }

    public void getTicketListByStatus(final DBInterface dbInterface, String status) {
        db.ticketDao().getTicketListByStatus(status).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<TicketListsResponse>>() {
            @Override
            public void accept(List<TicketListsResponse> tickets) throws Exception {
                dbInterface.onDBTicketLoaded(tickets);
            }
        });
    }

    public void addTickets(final DBInterface dbInterface, final List tickets) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.ticketDao().insertAll(tickets);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                dbInterface.onDBTicketUpdated();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                dbInterface.onDBTaskFailed();
            }
        });
    }

    public void addTicket(DBInterface dbInterface, TicketListsResponse ticket) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.ticketDao().insert(ticket);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                dbInterface.onDBTicketUpdated();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                dbInterface.onDBTaskFailed();
            }
        });
    }

    public void deleteTicket(DBInterface dbInterface, String ticket_id) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.ticketDao().deleteTicket(ticket_id);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                dbInterface.onDBTicketUpdated();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        });
    }

}
