package com.kiple.kipleBox.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.kiple.kipleBox.model.responses.TicketListsResponse;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface TicketDao {
    @Query("SELECT * FROM ticket WHERE ticketId=:ticketId ")
    Maybe<List<TicketListsResponse>> getTicket(String ticketId);

    @Query("SELECT * FROM ticket ")
    Maybe<List<TicketListsResponse>> getAllTicket();

    @Query("SELECT * FROM ticket WHERE status=:status ")
    Maybe<List<TicketListsResponse>> getTicketListByStatus(String status);

    @Query("UPDATE ticket SET status = :status WHERE ticketId = :ticketId")
    int updateTicketStatus(String ticketId, String status);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TicketListsResponse> ticket);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TicketListsResponse... ticket);

    @Query("DELETE FROM ticket")
    void deleteTableData();

    @Query("DELETE FROM ticket WHERE ticketId=:ticketId")
    void deleteTicket(String ticketId);

    //  credit card payment update storage
    @Query("SELECT * FROM ticket WHERE card_payment_response IS NOT NULL AND card_payment_response != ''")
    Maybe<List<TicketListsResponse>> getNotUpdatedList();
}
