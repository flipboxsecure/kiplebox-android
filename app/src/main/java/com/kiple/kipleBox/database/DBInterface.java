package com.kiple.kipleBox.database;

import com.kiple.kipleBox.model.responses.TicketListsResponse;

import java.util.ArrayList;
import java.util.List;

public interface DBInterface {

    void onDBTicketLoaded(List<TicketListsResponse> tickets);

    void onDBTicketUpdated();

    void onDBTaskFailed();


}
