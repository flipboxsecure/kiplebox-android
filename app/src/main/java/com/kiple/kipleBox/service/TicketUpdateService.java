package com.kiple.kipleBox.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.database.DBInterface;
import com.kiple.kipleBox.database.LocalCacheManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.CashPaymentResponse;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.core.lists.TicketListsFragment;
import com.kiple.kipleBox.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Response;

public class TicketUpdateService extends Service implements CallBackInterface {
    static Handler handler = new Handler();
    static int delay = 1000 * 15; //15 seconds in milliseconds
    static Context context;
    // Define the code block to be executed
    static final Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Intent serviceIntent = new Intent(context, TicketUpdateService.class);
            context.startService(serviceIntent);
            handler.postDelayed(this, delay);
        }
    };
    boolean isBroadcastSent = false;

    public static void startRepeatingService(Context ctx) {
        context = ctx;
        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);
    }

    public static void stopRepeatingService() {
        handler.removeCallbacks(runnableCode);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        LocalCacheManager localCacheManager = LocalCacheManager.getInstance(this);

        //  check and update not updated transactions
        localCacheManager.getNotUpdatedList(new DBInterface() {
            @Override
            public void onDBTicketLoaded(List<TicketListsResponse> tickets) {
                //  go through list items and attempt to update payment status
                Log.e("DB LIST SIZE", "not updated list size: " + tickets.size());
                updatePayment((ArrayList<TicketListsResponse>) tickets);
            }

            @Override
            public void onDBTicketUpdated() {

            }

            @Override
            public void onDBTaskFailed() {

            }
        });

        isBroadcastSent = false;
        APICall.fullTicketList(false, this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParams(Constants.TICKET_PAID_INT));

        return Service.START_NOT_STICKY;
    }

    private HashMap<String, Object> getParams(int listType) {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail()); //change this to signed in user
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("status", String.valueOf(listType));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void updateTicketPaymentStatus(TicketListsResponse ticket) {
        LocalCacheManager.getInstance(this).addTicket(new DBInterface() {
            @Override
            public void onDBTicketLoaded(List<TicketListsResponse> tickets) {
                Log.e("Update Payment Status", "Ticket payment status updated");
            }

            @Override
            public void onDBTicketUpdated() {

            }

            @Override
            public void onDBTaskFailed() {

            }
        }, ticket);
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        assert apiFlags != null;
        switch (apiFlags) {
            case GET_FULL_TICKET_LISTS_SERVICE:
                ArrayList<TicketListsResponse> list = (ArrayList<TicketListsResponse>) response.body();
                if (list != null && list.size() > 0) {
                    Log.d(TicketUpdateService.class.getSimpleName(), "onSuccessCallBack: 111 " + list.get(0).getValet_status_desc());
                    compareDataWithDB((ArrayList<TicketListsResponse>) response.body());
                }
                break;

            case UPDATE_PAYMENT_SERVICE:
                CashPaymentResponse data = (CashPaymentResponse) response.body();
                LocalCacheManager localCacheManager = LocalCacheManager.getInstance(this);
                localCacheManager.getAllTicket(new DBInterface() {
                    @Override
                    public void onDBTicketLoaded(List<TicketListsResponse> ticketlist) {
                        for (TicketListsResponse ticket : ticketlist) {
                            if (data.getReceipt().contains(ticket.getTicketId())) {
                                ticket.setCard_payment_response("");
                                updateTicketPaymentStatus(ticket);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onDBTicketUpdated() {

                    }

                    @Override
                    public void onDBTaskFailed() {

                    }
                });
                break;
        }
    }


    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        assert apiFlags != null;
        switch (apiFlags) {
            case UPDATE_PAYMENT_SERVICE:
                Log.e("Payment Update", "Payment update failed");
                break;
        }
    }

    private void updatePayment(ArrayList<TicketListsResponse> list) {
        for (TicketListsResponse ticketListsResponse : list) {
            //  update payment API call
            APICall.updatePayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), compileParams(ticketListsResponse));
        }
    }

    private HashMap<String, Object> compileParams(TicketListsResponse info) {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("ticketId", info.getTicketId());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("pos_payer", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("service", SessionManager.getInstance(this).getActivationInfo().getIdentify());
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("staff_payer", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
            params.put("paymentAmount", info.getAmount());
            params.put("paymentReceived", info.getAmount());
            params.put("paymentBalance", Utils.getBalance(info.getAmount(), info.getAmount()));
            params.put("entryTime", info.getEntryTime());
            long currentUnix = Utils.getCurrentTimeUnix();
            params.put("reqTime", String.valueOf(currentUnix));
            params.put("duration", String.valueOf(currentUnix - Long.parseLong(info.getEntryTime())));
            params.put("gracePeriod", SessionManager.getInstance(this).getActivationInfo().getGrace_period().toString());
            params.put("datePayUnix", String.valueOf(currentUnix));

            //  credit card -> 3
            //  debit card  -> 4
            JSONObject jsonObject;
            int chargeType;
            try {
                jsonObject = new JSONObject(info.getCard_payment_response());

                if (jsonObject.getString("pay_resp_card_auth_code").equalsIgnoreCase(Constants.DEBIT_CARD_PAYMENT)) {
                    chargeType = 4;
                } else {
                    chargeType = 3;
                }

                params.put("payment_pos_method", chargeType + 1);

                JsonParser jsonParser = new JsonParser();
                JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
                params.put("card_payment_response", gsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //  credit card payment response

            params.put("receipt", SessionManager.getInstance(this).getActivationInfo().getSite_id() + info.getTicketId());
            //  which status to proceed to
            ActivationResponse.Service_customize services = SessionManager.getInstance(this).getActivationInfo().getService_customize();
            if (!services.getCollect().getActive() && services.getComplete().getActive()) {
                params.put("next_status", "4");
            } else if (!services.getCollect().getActive() && !services.getComplete().getActive()) {
                params.put("next_status", "5");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void compareDataWithDB(ArrayList<TicketListsResponse> list) {
        LocalCacheManager localCacheManager = LocalCacheManager.getInstance(this);
        for (TicketListsResponse ticket : list) {
            localCacheManager.getTicket(new DBInterface() {
                @Override
                public void onDBTicketLoaded(List<TicketListsResponse> list2) {
                    if (list2.size() == 0) {
                        Utils.sendNotification(TicketUpdateService.this, ticket);
                        if (!isBroadcastSent) {
                            sendUpdateBroadCast();
                            isBroadcastSent = true;
                        }
                        addTicket(ticket);
                    }
                }

                @Override
                public void onDBTicketUpdated() {

                }

                @Override
                public void onDBTaskFailed() {

                }
            }, ticket.getTicketId(), ticket.getStatus());
        }
    }

    private void addTicket(TicketListsResponse ticket) {
        LocalCacheManager.getInstance(this).addTicket(new DBInterface() {
            @Override
            public void onDBTicketLoaded(List<TicketListsResponse> tickets) {

            }

            @Override
            public void onDBTicketUpdated() {
                Log.d(TicketUpdateService.class.getSimpleName(), "onDBTicketLoaded: TicketAdded");
            }

            @Override
            public void onDBTaskFailed() {

            }
        }, ticket);
    }

    private void sendUpdateBroadCast() {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(TicketListsFragment.BROADCAST_ACTION);
            sendBroadcast(broadCastIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
