package com.kiple.kipleBox.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.kiple.kipleBox.R;

public class ConnectivityListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case ConnectivityManager.CONNECTIVITY_ACTION:
                    new NetworkConnectionCheck(hasInternet -> {
                        if (hasInternet) {
                            //  has internet connection
//                            Toast.makeText(context, "device is online", Toast.LENGTH_SHORT).show();
                        } else{
                            //  no internet connection
//                            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
            }
        }
    }
}
