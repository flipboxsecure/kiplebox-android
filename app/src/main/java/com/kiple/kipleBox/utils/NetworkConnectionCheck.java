package com.kiple.kipleBox.utils;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class NetworkConnectionCheck extends AsyncTask<Void, Void, Boolean> {

    private Consumer mConsumer;
    public  interface Consumer { void accept(Boolean hasInternet); }

    public NetworkConnectionCheck(Consumer consumer) { mConsumer = consumer; execute(); }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
            sock.close();
            return true;
        } catch (IOException e) { return false; } }

    @Override protected void onPostExecute(Boolean hasInternet) { mConsumer.accept(hasInternet); }
}
