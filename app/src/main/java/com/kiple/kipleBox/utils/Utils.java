package com.kiple.kipleBox.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.ui.core.landing.AppActivity;

import org.threeten.bp.Duration;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by xenhao.yap on 30/01/2018.
 */

public class Utils {
    private static final String TAG = "Utils";

    public static String getDeviceUDID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String myNumberHandler(String number, String countryCode){
        //  used only if mobile number is used as account username
        //  to handle ONLY malaysia country code scenario
        if(countryCode.equalsIgnoreCase("my")){
            if(number.startsWith("6")){
                return number;
            }else{
                StringBuilder temp = new StringBuilder(number);
                return temp.insert(0, "6").toString();
            }
        }else{
            return number;
        }
    }

    public static String singleDigitCheck(Long amount){
        if(amount < 10)
            return "0" + String.valueOf(amount);
        else
            return String.valueOf(amount);
    }

    public static String getCorrectBalance(String wallet, String payment){
        return formatBalance(Double.parseDouble(wallet) - Double.parseDouble(payment));
    }

    public static String getCorrectBalanceTopup(String wallet, String payment){
        return formatBalance(Double.parseDouble(wallet) + Double.parseDouble(payment));
    }

    public static String formatBalance(Double value){
        DecimalFormat df = new DecimalFormat("####0.00");
        return df.format(value);
    }

    public static String formatBalance(String value){
        if(value == null)
            return "Not Available";
        else {
            DecimalFormat df = new DecimalFormat("####0.00");
            return df.format(Double.valueOf(value));
        }
    }

    public static String formatBalance(String value, String defaultValue){
        if(value == null)
            return defaultValue;
        else {
            DecimalFormat df = new DecimalFormat("####0.00");
            return df.format(Double.valueOf(value));
        }
    }

    public static boolean isUserSupervisor(List<String> accessLevelList){
        for(int i = 0; i < accessLevelList.size(); i++){
            if("supervisor".equalsIgnoreCase(accessLevelList.get(i)))
                return true;
        }
        return false;
    }

    public static String getParkingType(String normal, String season, String valet, String onStreet){
        StringBuilder type = new StringBuilder();
        if(normal.equalsIgnoreCase("1"))
            type.append("normal");
        if(season.equalsIgnoreCase("1"))
            type.append(", season");
        if(valet.equalsIgnoreCase("1"))
            type.append(", valet");
        if(onStreet != null && onStreet.equalsIgnoreCase("1"))
            type.append(", on street");

        return type.toString();
    }

    public static long getRemainingGrace(String payTimeInUnix, String graceInMins, long serverUnix, long startElapsed, long currentElapsed){
        long remaining;
        long currentTime = Long.parseLong(realCurrentUnix(serverUnix, startElapsed, currentElapsed)) * 1000L;
        long paymentTime = Long.parseLong(payTimeInUnix) * 1000;
        long gracePeriod = (long) Double.parseDouble(graceInMins) * 60 * 1000;

        remaining = currentTime - paymentTime;
        remaining = gracePeriod - remaining;

        return remaining < Constants.ZERO_SECONDS ? Constants.ZERO_SECONDS : remaining;
    }

    public static long getRemainingGraceWithNegative(String payTime, String grace){
        long remaining;
        long currentTime = System.currentTimeMillis();
        long paymentTime = Long.parseLong(payTime) * 1000;
        long gracePeriod = (long) Double.parseDouble(grace) * 60 * 1000;

        remaining = currentTime - paymentTime;
        remaining = gracePeriod - remaining;

        return remaining;
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static String getCurrentDateOrTime(String format){
        Date currentDateTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(currentDateTime);
    }

    public static String historyTimeFormatter(String date){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//          SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
            SimpleDateFormat output = new SimpleDateFormat("MMM dd, yyyy HH:mm aa");
            Date d = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.add(Calendar.HOUR_OF_DAY, 8);  //  add 8 hours for malaysian timezone
            return output.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String appendDelimeter(String content, String delimiter, int length){
        if(content == null)
            content = "";

        if(content.length() < length){
            StringBuilder contentBuilder = new StringBuilder(content);
            while(contentBuilder.length() < length){
                contentBuilder.append(delimiter);
            }
            return contentBuilder.toString();
        }else{
            return content;
        }
    }

    public static String getNumberPlateFromTicketdata(String ticketdata){
        if(ticketdata.length() < Constants.TICKETDATA_LENGTH)
            return "Ticketdata Corrupted";
        else {
            final int NPstart = 16, NPend = 30;
            String NP = ticketdata.substring(NPstart, NPend);
            return NP.replaceAll("\\^", "");
        }
    }

    public static String getRunningIdFromTicketdata(String ticketdata){
        if(ticketdata.length() < Constants.TICKETDATA_LENGTH)
            return "Ticketdata Corrupted";
        else {
            final int NPstart = 61, NPend = 68;
            String NP = ticketdata.substring(NPstart, NPend);
            return NP.replaceAll("\\^", "");
        }
    }

    public static byte[] getImageInputStreamToByteArray(InputStream inputStream) throws IOException{
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String getUnix(String format, String time){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            Date date = formatter.parse(time);
            return String.valueOf(date.getTime() / 1000);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getCurrentTimeUnix(){
        return System.currentTimeMillis() / 1000;
    }

    public static String getCurrentTimeUnixString(){
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    public static String getCurrentDateWithFormat(String format){
        return new SimpleDateFormat(format, Locale.getDefault()).format(new Date());
    }

    public static String unixFormatter(String unixTime, String format, String timezone){
        if(unixTime == null){
            return "Not Available";
        }else {
            long entryTime = Long.parseLong(unixTime);
//            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//            cal.setTimeInMillis(entryTime * 1000L);
//            return DateFormat.format(format, cal).toString();
            // convert seconds to milliseconds
            Date date = new Date(entryTime * 1000L);
            // the format of your date
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            // give a timezone reference for formatting (see comment at the bottom)
            sdf.setTimeZone(TimeZone.getTimeZone(timezone));
            return sdf.format(date);
        }
    }

    public static String realCurrentUnix(long serverUnix, long startElapsed, long currentElapsed){
        return String.valueOf(serverUnix + (currentElapsed - startElapsed));
    }

    public static String getEntryTimeFromUnix(String unixTime, String timezone){
        Long entryTime = Long.parseLong(unixTime);
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(entryTime * 1000L);
//        return DateFormat.format("dd MMM, yyyy HH:mm aa", cal).toString();

        // convert seconds to milliseconds
        Date date = new Date(entryTime * 1000L);
        // the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy HH:mm aa");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        return sdf.format(date);
    }

    public static String getElapsedTimeFromUnix(String entryTime, String requestTime){
        if(null == entryTime || null == requestTime)
            return "-";
        Long elapsedTime = Long.parseLong(requestTime) - Long.parseLong(entryTime);

        long months = elapsedTime/(60 * 60 * 24 * 7 * 4);
        elapsedTime = elapsedTime%(60 * 60 * 24 * 7 * 4);

        //  exclude weeks
//        long weeks = elapsedTime/(60 * 60 * 24 * 7);
//        elapsedTime = elapsedTime%(60 * 60 * 24 * 7);

        long days = elapsedTime/(60 * 60 * 24);
        elapsedTime = elapsedTime%(60 * 60 * 24);

        long hours = elapsedTime/(60 * 60);
        elapsedTime = elapsedTime%(60 * 60);

        long mins = (elapsedTime/(60)) % 60;
        elapsedTime = elapsedTime%(60);

        //  add minute if time elapsed more than 30 seconds
        if(elapsedTime > 30)
            mins++;

        StringBuilder finalElapsed = new StringBuilder();

        if(months > 0)
            return finalElapsed
                    .append(String.valueOf(months)).append("M")
//                    .append(String.valueOf(weeks)).append("w")
                    .append(String.valueOf(days)).append("d")
                    .append(String.valueOf(hours)).append("h")
                    .append(String.valueOf(mins)).append("m")
                    .toString();

        //  exclude weeks
//        else if(weeks > 0)
//            return finalElapsed
//                    .append(String.valueOf(weeks)).append("w")
//                    .append(String.valueOf(days)).append("d")
//                    .append(String.valueOf(hours)).append("h")
//                    .append(String.valueOf(mins)).append("m")
//                    .toString();

        else if(days > 0)
            return finalElapsed
                    .append(String.valueOf(days)).append("d")
                    .append(String.valueOf(hours)).append("h")
                    .append(String.valueOf(mins)).append("m")
                    .toString();

        else if(hours > 0)
            return finalElapsed
                    .append(String.valueOf(hours)).append("h")
                    .append(String.valueOf(mins)).append("m")
                    .toString();

        else
            return finalElapsed
                    .append(String.valueOf(mins)).append("m")
                    .toString();
    }

    public static String getBalance(String finalCost, String amountPaid){
        finalCost = finalCost.replaceAll("[^\\d.]", "");
        amountPaid = amountPaid.replaceAll("[^\\d.]", "");
        if(Double.parseDouble(finalCost) > Double.parseDouble(amountPaid)){
            return "-";
        }else{
            return formatBalance(Double.parseDouble(amountPaid) - Double.parseDouble(finalCost));
        }
    }

    public static String coloredString(String msg, String color){
        String input = "<font color=" + color + "><i>" + msg + "</i></font>";
        return input;
    }

    public static String timeFormatter(String time){
        if(time == null || time.equalsIgnoreCase(""))
            return "";

        Long entryTime = Long.parseLong(time);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(entryTime * 1000L);
        return DateFormat.format("MMM dd, yyyy HH:mm aa", cal).toString();
    }

    public static String bracketText(String text){
        return "(" + text + ")";

    }

    public static String durationFormatter(String data){
        Duration duration = Duration.parse(data);
        long hours = duration.getSeconds()/(60 * 60);
        long mins = (duration.getSeconds()/(60)) % 60;

        return String.valueOf(hours) + ":" + singleDigitCheck(mins);
    }

    public static String paymentMethodFilter(String data){
        switch (data){
            case "OnlinePayment":
                return Constants.WALLET;

                default:
                    return data;
        }
    }

    public static File getOutputMediaFile(Activity activity) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return new File(activity.getFilesDir() + File.separator + "IMG_" + timeStamp + ".jpg");
            }
        }
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    public static void closeSilently(Closeable c) {
        if (c == null) return;
        try {
            c.close();
        } catch (IOException t) {
            Log.w(TAG, "close fail ", t);
        }
    }

    public static void closeSilently(ParcelFileDescriptor fd) {
        try {
            if (fd != null) fd.close();
        } catch (Throwable t) {
            Log.w(TAG, "fail to close", t);
        }
    }

    public static void closeSilently(Cursor cursor) {
        try {
            if (cursor != null) cursor.close();
        } catch (Throwable t) {
            Log.w(TAG, "fail to close", t);
        }
    }

    public static int nextPowerOf2(int n) {
        if (n <= 0 || n > (1 << 30)) throw new IllegalArgumentException("n is invalid: " + n);
        n -= 1;
        n |= n >> 16;
        n |= n >> 8;
        n |= n >> 4;
        n |= n >> 2;
        n |= n >> 1;
        return n + 1;
    }
    public static int prevPowerOf2(int n) {
        if (n <= 0) throw new IllegalArgumentException();
        return Integer.highestOneBit(n);
    }

    // Throws AssertionError if the input is false.
    public static void assertTrue(boolean cond) {
        if (!cond) {
            throw new AssertionError();
        }
    }

    public static String convertDateToStringWithFormat(Date date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String convertStringDateToNewFormat(String strDate, String oldFormat, String newFormat) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(oldFormat);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            Date date = simpleDateFormat.parse(strDate);
            return new SimpleDateFormat(newFormat).format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String addCharAtIndex(String originalStr, int index, String charToAdd){
        if(null == originalStr || originalStr.equalsIgnoreCase("") || index < 0)
            return "-";
        else
            return new StringBuilder(originalStr).insert(index, charToAdd).toString();
    }

    public static String objNullCheck(Object object){
        return object == null ? "" : object.toString();
    }

    public static boolean isNumerical(String string){
        if(string.startsWith("-"))
            return string.replace("-", "").matches("\\d+(?:\\.\\d+)?");
        else
            return string.matches("\\d+(?:\\.\\d+)?");
    }

    public static int getViewHeight(LinearLayout v) {
        v.measure(View.MeasureSpec.makeMeasureSpec(v.getMeasuredWidth(), View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(v.getMeasuredHeight(), View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(false);
        return (int) (((v.getHeight() / Resources.getSystem().getDisplayMetrics().density)) * 2);
    }

    private final static AtomicInteger c = new AtomicInteger(0);

    public static void sendNotification(Context context, TicketListsResponse ticket1) {
        final String CHANNEL_ID = "my_channel_01";

        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(context, AppActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.payment_notification_heading))
                .setTicker(Utils.getNumberPlateFromTicketdata(ticket1.getTicketData()) + " " + context.getString(R.string.payment_notification_text))
                .setContentText(Utils.getNumberPlateFromTicketdata(ticket1.getTicketData()) + " " + context.getString(R.string.payment_notification_text))
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText("Much longer text that cannot fit one line..."))
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        Notification notification = mBuilder.build();
        notification.contentIntent = pendingIntent;

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(getID(), notification);
    }

    public static int getID() {
        return c.incrementAndGet();
    }


}
