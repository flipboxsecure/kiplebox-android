package com.kiple.kipleBox.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.kiple.kipleBox.R;

/**
 * Created by xenhao.yap on 30/01/2018.
 *
 *  usage in xml:
 *      * same as normal TextView
 *      * textSize to determine icon size
 *      * @attr customFont to set font file
 *      * @attr iconId to get icon from within font file
 */

public class CustomFontView extends TextView {

    public CustomFontView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public CustomFontView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public CustomFontView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }
    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load attributes
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewPlusFont, 0, 0);
        try {
            String fontInAssets = ta.getString(R.styleable.TextViewPlusFont_customFont);
            setTypeface(Typefaces.get(context, fontInAssets));
        } finally {
            ta.recycle();
        }
    }
}
