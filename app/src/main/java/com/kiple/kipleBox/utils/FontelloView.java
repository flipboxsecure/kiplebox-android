package com.kiple.kipleBox.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.kiple.kipleBox.R;

/**
 * Created by xenhao.yap on 30/01/2018.
 *
 *  usage in xml:
 *      * same as normal TextView
 *      * textSize to determine icon size
 *      * font is hardcoded to "fontello.ttf"
 *      * @attr iconId to get icon from within font file
 */

public class FontelloView extends TextView {

    public FontelloView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public FontelloView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public FontelloView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }
    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load attributes
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewPlusFont, 0, 0);
        try {
            this.setText(String.valueOf((char) ta.getInteger(R.styleable.TextViewPlusFont_iconId, 0)));
            String fontInAssets = "fontello.ttf";
            setTypeface(Typefaces.get(context, fontInAssets));
        } finally {
            ta.recycle();
        }
    }
}
