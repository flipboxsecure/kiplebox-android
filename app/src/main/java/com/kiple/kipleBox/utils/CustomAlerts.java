package com.kiple.kipleBox.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APIErrorBody;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by xenhao.yap on 15/02/2018.
 */

public class CustomAlerts {

//    public static void showResponseAlert(OnFragmentChangeListener listener, Context context, String title, String message)
//    {
//        if(!((AppCompatActivity) context).isFinishing()) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//            builder.setCancelable(false);
//            builder.setTitle(title);
//            builder.setMessage(message);
//            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> listener.triggerBack());
//            builder.setNegativeButton(android.R.string.cancel, null);
//            builder.show();
//        }
//    }

    public static void showResponseAlert(Context context, DialogInterface.OnClickListener listener, String title, String message) {
        if(!((AppCompatActivity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(android.R.string.ok, listener);
            builder.setNegativeButton(android.R.string.cancel, listener);
            builder.show();
        }
    }

    public static void showResponseAlert(Context context, DialogInterface.OnClickListener listener, String title, String message, String positiveLabel, String negativeLabel) {
        if(!((AppCompatActivity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(positiveLabel, listener);
            builder.setNegativeButton(negativeLabel, listener);
            builder.show();
        }
    }

    public static void showOKAlert(Context context, DialogInterface.OnClickListener listener, String title, String message)
    {
        if(!((AppCompatActivity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(android.R.string.ok, listener);
            builder.show();
        }
    }

    public static void showOKAlert(Context context, String title, String message)
    {
        if(!((AppCompatActivity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(android.R.string.ok, null);
            builder.show();
        }
    }

    public static void displayError(Activity activity, Response response){
        if(activity !=null) {
            try {

                ResponseBody body = response.errorBody();
                if (body != null) {

                    APIErrorBody apiErrorBody = new Gson().fromJson(body.string(), APIErrorBody.class);

                    if (apiErrorBody != null) {
                        if ("PHONE_NUMBER_WRONG_FORMAT".equals(apiErrorBody.getCode())) {
                            CustomAlerts.showOKAlert(activity, activity.getString(R.string.error), apiErrorBody.getMessage());
                        } else {
                            CustomAlerts.showOKAlert(activity, activity.getString(R.string.error), apiErrorBody.getMessage());
                        }
                    } else {

                        CustomAlerts.showOKAlert(activity, activity.getString(R.string.error), activity.getString(R.string.cannot_connect_to_server));
                    }


                } else {

                    CustomAlerts.showOKAlert(activity, activity.getString(R.string.error), activity.getString(R.string.cannot_connect_to_server));
                }
            } catch (Exception e) {

            }
        }
    }
}
