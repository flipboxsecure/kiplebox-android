package com.kiple.kipleBox.utils;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

public class PanViewAdjuster {

    public static void AdjustView(View rootView, View parentView, View focusTarget, double offset){

        //  parentView is the Viewgroup layout which is inside a ScrollView.
        //  rootView is the parent View Group of my XML layout
        //  focusTarget is the edit text / view you want to be visible with extra scrolling
        //  offset is how much more you want to adjust, 0 is original amount

        final int[] edtHeight = {0};

        ViewTreeObserver viewTreeObserver1 = parentView.getViewTreeObserver();
        viewTreeObserver1.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                // Here edtHeight will get the height of edittext as i wanted my view to scroll that much bit more
                edtHeight[0] = focusTarget.getHeight();

                ViewTreeObserver obs = parentView.getViewTreeObserver();

                obs.removeOnGlobalLayoutListener(this);
            }
        });


        rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            // r will be populated with the coordinates of your view
            // that area still visible.
            rootView.getWindowVisibleDisplayFrame(r);

            int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 100)
            { // if more than 100 pixels, its probably a keyboard...
                if(focusTarget.hasFocus()){
                    parentView.scrollTo(0, (int) (edtHeight[0] * offset));
                }else{
                    parentView.scrollTo(0, 0);
                }
            }
            else
            {
                parentView.scrollTo(0, 0);
            }
        });
    }
}
