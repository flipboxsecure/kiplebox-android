/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kiple.kipleBox.utils;

import android.os.Bundle;
import android.util.Log;

public class LogUtils {
    private static final String LOG_PREFIX      = "kiplePark";
    private static final int LOG_PREFIX_LENGTH  = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;
    public static boolean LOGGING_ENABLED       = true;

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }
        return LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    public static void LOGD(final String tag, String message) {
        if (LOGGING_ENABLED){
            if (Log.isLoggable(tag, Log.DEBUG)) {
                Log.d(tag, message);
            }
        }
    }

    public static void LOGD(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED){
            if (Log.isLoggable(tag, Log.DEBUG)) {
                Log.d(tag, message, cause);
            }
        }
    }

    public static void LOGV(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.VERBOSE)) {
                Log.v(tag, message);
            }
        }
    }

    public static void LOGV(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.VERBOSE)) {
                Log.v(tag, message, cause);
            }
        }
    }

    public static void LOGI(final Class cls, String message) {
        try {
            if (LOGGING_ENABLED) {
                Log.i(makeLogTag(cls), message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void LOGI(final String tag, String message) {
        try {
            if (LOGGING_ENABLED) {
                Log.i(tag, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void LOGI(final String tag, String message, Throwable cause) {
        try {
            if (LOGGING_ENABLED) {
                Log.i(tag, message, cause);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void LOGW(final String tag, String message) {
        if (LOGGING_ENABLED) {
            Log.w(tag, message);
        }
    }

    public static void LOGW(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            Log.w(tag, message, cause);
        }
    }

    public static void LOGE(final String tag, String message) {
        if (LOGGING_ENABLED){
            Log.e(tag, message);
        }
    }

    public static void LOGE(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            Log.e(tag, message, cause);
        }
    }

    public static void LOG_BUNDLE(final String tag, Bundle bundle) {
        if (!LOGGING_ENABLED){
            return;
        }
        for (String key : bundle.keySet()) {
            LOGI(tag, key + ": " +
                    bundle.get(key).toString() + ", " +
                    bundle.get(key).getClass().getName());
        }
    }

    private LogUtils() {
    }
}
