package com.kiple.kipleBox.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.sunmi.impl.V1Printer;

/**
 * Created by Bala on 10/13/2017.
 */

public class PrintReceipts extends AsyncTask<String, Void, String> {

    private V1Printer printer;
    private IssueTicketModel mIssueTicketModel;
    private PaymentReceiptModel mPaymentReceiptModel;
    private SummaryModel mSummaryModel;
    private String mTicketType = "";

    private Context context;

    public PrintReceipts(V1Printer printer, Context context, IssueTicketModel issueTicketModel, PaymentReceiptModel paymentReceiptModel, SummaryModel summaryModel, String ticketType) {

        this.printer = printer;
        this.context = context;
        this.mIssueTicketModel = issueTicketModel;
        this.mPaymentReceiptModel = paymentReceiptModel;
        this.mSummaryModel = summaryModel;
        this.mTicketType = ticketType;

        LogUtils.LOGI(this.getClass().getSimpleName(), "printer method called");
    }

    @Override
    protected String doInBackground(String... params) {

        if (Looper.myLooper() == null)
            Looper.prepare();

        LogUtils.LOGI(this.getClass().getSimpleName(), "do in background executed");

        printer.beginTransaction();
        printer.setAlignment(1);
        printer.setFontSize(32);

        if (mIssueTicketModel != null) {
            if (mTicketType.equalsIgnoreCase("ticket") || mTicketType.equalsIgnoreCase("")) {
                ticketPrint(mIssueTicketModel);
            } else if (mTicketType.equalsIgnoreCase("dashboard")) {
                ticketDashboardPrint(mIssueTicketModel);
            } else if (mTicketType.equalsIgnoreCase("valet")) {
                ticketValetPrint(mIssueTicketModel);
            }
        }
        else if (mPaymentReceiptModel != null)
            receiptPrint(mPaymentReceiptModel);
        else if (mSummaryModel != null)
            summaryPrint(mSummaryModel);

//        Looper.loop();

        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

    private void summaryPrint(SummaryModel data) {
        try {

            printer.setFontSize(16);
            printer.printOriginalText("Welcome to\n");
            printer.setFontSize(28);
            printer.printText(data.getSite_name() + "\n");
            printer.setFontSize(16);
            printer.printText(data.getSite_address() + "\n");

            printer.setFontSize(32);
            printer.printText("\n" + data.getStaff_name() + "\n\n");

            //  set date & time
            printer.setFontSize(20);
            String[] text = new String[2];
            int[] width = new int[]{10, 25};
            int[] align = new int[]{0, 0}; // 左齐,左齐

            text[0] = "START ";
            text[1] = ": " + data.getStartDateTime();
            printer.printColumnsText(text, width, align);

            text[0] = "END ";
            text[1] = ": " + data.getEndDateTime();
            printer.printColumnsText(text, width, align);

            printer.printHorizontalLine(2, 0);

            //  transaction & amount
            printer.setFontSize(20);

            text[0] = "Transaction";
            text[1] = "Amount";
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.printHorizontalLine(2, 0);

            //  list of cash transaction
            for (int i = 0; i < data.getCashArrayList().size(); i++) {
                //  transaction & amount
                printer.setFontSize(20);

                String tempTicketInfo;
                tempTicketInfo = data.getCashArrayList().get(i).getTicketId() + " - " + data.getCashArrayList().get(i).getType();

                text[0] = tempTicketInfo;

                if (data.getCashArrayList().get(i).getType().toLowerCase().contains("void"))
                    text[1] = "- RM " + Utils.formatBalance(data.getCashArrayList().get(i).getAmount());
                else
                    text[1] = "RM " + Utils.formatBalance(data.getCashArrayList().get(i).getAmount());

                printer.printColumnsText(text, new int[]{27, 10}, new int[]{0, 2});

            }

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            //  cash total & amount
            printer.setFontSize(20);

            text[0] = "Cash Total";
            text[1] = "RM " + Utils.formatBalance(data.getCashTotal());
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            //  list of guest room transaction
            for (int i = 0; i < data.getGuestRoomArrayListArrayList().size(); i++) {
                //  transaction & amount
                printer.setFontSize(20);

                String tempTicketInfo;
                tempTicketInfo = data.getGuestRoomArrayListArrayList().get(i).getTicketId() + " - " + data.getGuestRoomArrayListArrayList().get(i).getType();

                text[0] = tempTicketInfo;

                if (data.getGuestRoomArrayListArrayList().get(i).getType().toLowerCase().contains("void"))
                    text[1] = "- RM " + Utils.formatBalance(data.getGuestRoomArrayListArrayList().get(i).getAmount());
                else
                    text[1] = "RM " + Utils.formatBalance(data.getGuestRoomArrayListArrayList().get(i).getAmount());

                printer.printColumnsText(text, new int[]{27, 10}, new int[]{0, 2});

            }

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            //  guest room total & amount
            printer.setFontSize(20);

            text[0] = "Guest Room Total";
            text[1] = "RM " + Utils.formatBalance(data.getGuestRoomTotal());
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            //  list of master account transaction
            for (int i = 0; i < data.getMasterAccountArrayListArrayList().size(); i++) {
                //  transaction & amount
                printer.setFontSize(20);

                String tempTicketInfo;
                if (data.getMasterAccountArrayListArrayList().get(i).getType().toLowerCase().contains("void"))
                    tempTicketInfo = data.getMasterAccountArrayListArrayList().get(i).getTicketId() + " - master acc [void]";
                else
                    tempTicketInfo = data.getMasterAccountArrayListArrayList().get(i).getTicketId() + " - master acc";

                text[0] = tempTicketInfo;

                if (data.getMasterAccountArrayListArrayList().get(i).getType().toLowerCase().contains("void"))
                    text[1] = "- RM " + Utils.formatBalance(data.getMasterAccountArrayListArrayList().get(i).getAmount());
                else
                    text[1] = "RM " + Utils.formatBalance(data.getMasterAccountArrayListArrayList().get(i).getAmount());

//                text[1] = "RM " + data.getMasterAccountArrayListArrayList().get(i).getAmount();
                printer.printColumnsText(text, new int[]{27, 10}, new int[]{0, 2});

            }

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            //  master account total & amount
            printer.setFontSize(20);

            text[0] = "Master Acc Total";
            text[1] = "RM " + Utils.formatBalance(data.getMasterAccTotal());
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

         //  list of debit/credit transaction
            for (int i = 0; i < data.getDebitCreditArrayListArrayList().size(); i++) {
                //  transaction & amount
                printer.setFontSize(20);

                String tempTicketInfo;
                tempTicketInfo = data.getDebitCreditArrayListArrayList().get(i).getTicketId() + " "+data.getDebitCreditArrayListArrayList().get(i).getType();

                text[0] = tempTicketInfo;

                if (data.getDebitCreditArrayListArrayList().get(i).getType().toLowerCase().contains("void"))
                    text[1] = "- RM " + Utils.formatBalance(data.getDebitCreditArrayListArrayList().get(i).getAmount());
                else
                    text[1] = "RM " + Utils.formatBalance(data.getDebitCreditArrayListArrayList().get(i).getAmount());

//                text[1] = "RM " + data.getMasterAccountArrayListArrayList().get(i).getAmount();
                printer.printColumnsText(text, new int[]{27, 10}, new int[]{0, 2});

            }

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);


            //  debit credit total & amount
            printer.setFontSize(20);

            text[0] = "Debit/Credit Total";
            text[1] = "RM " + Utils.formatBalance(data.getDebitCreditTotal());
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.setAlignment(1);
            printer.setFontSize(18);
            printer.printText("------------------------------------------\n");
//            printer.printHorizontalLine(1,0);

            printer.setAlignment(2);
            printer.setFontSize(20);
            printer.printText("\n");

            //  grand total & amount
            printer.setFontSize(20);

            text[0] = "Grand Total";
            text[1] = "RM " + Utils.formatBalance(data.getGrandTotal());
            printer.printColumnsText(text, new int[]{18, 19}, new int[]{0, 2});

            printer.setAlignment(2);
            printer.setFontSize(23);
            printer.printText("================================\n");

            printer.setAlignment(1);
            printer.setFontSize(16);
            printer.printOriginalText("*****THANK YOU*****");
            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintSummary", e.getMessage());
            e.printStackTrace();

        }
    }

    private void receiptPrint(PaymentReceiptModel data) {
        try {

            if(null != data.getBitmap()) {
                printer.printBitmap(data.getBitmap());
                printer.printText("\n");
            }
            printer.setFontSize(20);
            printer.printText("Welcome to\n");
            printer.setFontSize(28);
            printer.printText(data.getSite_name() + "\n");
            printer.setFontSize(16);
            printer.printText(data.getSite_address() + "\n");

            if (data.getSite_phone_number() != null)
                printer.printText(data.getSite_phone_number() + "\n");

            printer.setFontSize(23);
            if(data.isDuplicated_receipt_flag())
                printer.printText("\n[DUPLICATED RECEIPT]" + "\n\n");
            else
                printer.printText("\n[RECEIPT]" + "\n\n");

            String[] text = new String[2];
            int[] width = new int[]{15, 25};
            int[] align = new int[]{0, 0}; // 左齐,右齐

            //  set date
//            printer.setFontSize(20);
//
//                text[0] = "DATE";
//                text[1] = ": " + data.getDate();
//            printer.printColumnsText(text, width, align);

            //  set time
//            printer.setFontSize(20);
//
//                text[0] = "TIME";
//                text[1] = ": " + data.getTime();
//            printer.printColumnsText(text, width, align);

            //  set car number plate
//            printer.setFontSize(20);
//
//                text[0] = "VEHICLE REG NO.";
//                text[1] = ": " + data.getNumber_plate();
//            printer.printColumnsText(text, width, align);

            //  set receipt number
            printer.setFontSize(20);

            text[0] = "RECEIPT NO.";
            text[1] = ": " + data.getReceipt_id();
            printer.printColumnsText(text, width, align);

            //  set ticket ID
            printer.setFontSize(20);

            text[0] = "TICKET ID";
            text[1] = ": " + data.getTicket_id();
            printer.printColumnsText(text, width, align);

            //  set car number plate
            if(!"Ticketdata Corrupted".equalsIgnoreCase(data.getNumber_plate()) && !"".equalsIgnoreCase(data.getNumber_plate())) {
                printer.setFontSize(20);

                text[0] = "CAR PLATE NO.";
                text[1] = ": " + data.getNumber_plate();
                printer.printColumnsText(text, width, align);
            }

            //  set entry time
            printer.setFontSize(20);

            text[0] = "ENTRY TIME";
            text[1] = ": " + data.getEntry_time();
            printer.printColumnsText(text, width, align);

            //  set payment time
            printer.setFontSize(20);

            text[0] = "PAYMENT TIME";
            text[1] = ": " + data.getPayment_time();
            printer.printColumnsText(text, width, align);

            //  set parking duration
            printer.setFontSize(20);

            text[0] = "DURATION";
            text[1] = ": " + data.getDuration();
            printer.printColumnsText(text, width, align);

            //  set valet staff name
//            printer.setFontSize(20);
//
//                text[0] = "VALET";
//                text[1] = ": " + data.getStaff_name();
//            printer.printColumnsText(text, width, align);

            //  set payment method
            printer.setFontSize(20);

            if(null != data.getPayment_method() && !"".equalsIgnoreCase(data.getPayment_method()) && data.getPayment_method().contains("+")){
                //  add upfront section
                String[] splitInfo;
                splitInfo = data.getPayment_method().split("\\+");

                text[0] = "UPFRONT PAYMENT";
                text[1] = ": " + splitInfo[1].trim();
                printer.printColumnsText(text, width, align);

                String paymentString = splitInfo[0].trim();
                String[] optionalPayment;
                String optionalMethod;
                String optionalAmount;
                if(paymentString.contains(Constants.MAXIMUM_CHARGE)) {
                    paymentString = paymentString.replace(Constants.MAXIMUM_CHARGE, "Max Charge");
                    optionalPayment = paymentString.split("\\(");
                    optionalMethod = optionalPayment[0].trim();
                    optionalAmount = "(" + optionalPayment[1].trim();

                    text[0] = "PAYMENT METHOD";
                    text[1] = ": " + optionalMethod;
                    printer.printColumnsText(text, width, align);

                    text[0] = "";
                    text[1] = "  " + optionalAmount;
                    printer.printColumnsText(text, width, align);

                } else if (paymentString.contains(Constants.LOST_TICKET)) {
                    optionalPayment = paymentString.split("\\(");
                    optionalMethod = optionalPayment[0].trim();
                    optionalAmount = "(" + optionalPayment[1].trim();

                    text[0] = "PAYMENT METHOD";
                    text[1] = ": " + optionalMethod;
                    printer.printColumnsText(text, width, align);

                    text[0] = "";
                    text[1] = "  " + optionalAmount;
                    printer.printColumnsText(text, width, align);
                } else {

                    text[0] = "PAYMENT METHOD";
                    text[1] = ": " + paymentString;
                    printer.printColumnsText(text, width, align);
                }

            }else if(null != data.getPayment_method() && !"".equalsIgnoreCase(data.getPayment_method()) && !data.getPayment_method().contains("+")){
                //  show usual payment method only
                text[0] = "PAYMENT METHOD";
                text[1] = ": " + data.getPayment_method();
                printer.printColumnsText(text, width, align);
            }else{
                //  payment method not available
                text[0] = "PAYMENT METHOD";
                text[1] = ": Not Available";
                printer.printColumnsText(text, width, align);
            }


            //  set parking fee
            printer.setFontSize(20);

            text[0] = "PARKING FEE";
            text[1] = ": " + data.getValet_amount();
            printer.printColumnsText(text, width, align);

            //  horizontal line
//            printer.printOriginalText("\n");
            printer.printHorizontalLine(2, 0);

            //  item & amount
//            printer.setFontSize(20);
//
//                text[0] = " ITEM";
//                text[1] = "AMOUNT";
//            printer.printColumnsText(text, new int[] { 18, 19 }, new int[] {0,2});

//            printer.printHorizontalLine(2,0);

            //  item & amount
//            printer.setFontSize(20);
//
//                text[0] = " Valet Parking Service";
//                text[1] = data.getValet_amount();
//            printer.printColumnsText(text, new int[] { 24, 13 }, new int[] {0,2});

            printer.printOriginalText("\n");

            //  subtotal
//            printer.setFontSize(20);
//
//            text[0] = "SUBTOTAL";
//            text[1] = ": " + data.getSubtotal();
//            printer.printColumnsText(text, new int[]{19, 19}, new int[]{2, 0});
//
//            //  discount
//            if (null != data.getDiscount()) {
//                printer.setFontSize(20);
//
//                text[0] = "DISCOUNT";
//                text[1] = ": " + data.getDiscount();
//                printer.printColumnsText(text, new int[]{19, 19}, new int[]{2, 0});
//            }
//
//            //  rounding adjustment
//            printer.setFontSize(20);
//
//            text[0] = "ROUNDING ADJ";
//            text[1] = ": " + data.getRoundingAdj();
//            printer.printColumnsText(text, new int[]{19, 19}, new int[]{2, 0});
//
//            printer.setAlignment(1);
//            printer.setFontSize(23);
//            printer.printText("------------------------------\n");

            //  total
            printer.setFontSize(32);

            text[0] = "TOTAL:";
            text[1] = data.getValet_amount();
            printer.printColumnsText(text, new int[]{10, 15}, new int[]{2, 2});

//            if ("cash".equalsIgnoreCase(data.getPayment_method())) {
//                //  set paid
//                printer.setFontSize(16);
//
//                text[0] = "PAID: ";
//                text[1] = data.getPaid();
//                printer.printColumnsText(text, new int[]{20, 28}, new int[]{2, 2});
//
//                //  set change
//                printer.setFontSize(16);
//
//                text[0] = "CHANGE: ";
//                text[1] = data.getChange();
//                printer.printColumnsText(text, new int[]{20, 28}, new int[]{2, 2});
//
//            }

            //  horizontal line
            printer.printHorizontalLine(2, 0);

            //  GST message
            if(data.isShow_tax_info()) {
                printer.setAlignment(0);
                printer.setFontSize(20);
                printer.printText("Inclusive of 6% SST");
            }

//            printer.setAlignment(1);
//            printer.setFontSize(23);
//            printer.printText("==============================\n");

            //  parking operator name & tax ID
            printer.setAlignment(0);
            printer.setFontSize(20);
            printer.printText(data.getParking_operator() /*+ "\nGST ID: " + data.getGst_id()*/ + "\n");

//            printer.printHorizontalLine(2,0);

            //  remove promotional items
//            if(null != data.getPos_info()) {
//                printer.setAlignment(0);
//                printer.setFontSize(20);
//                printer.printOriginalText(data.getPos_info());
//            }else{
//                printer.setAlignment(0);
//                printer.setFontSize(20);
//                printer.printOriginalText("kiplePark download instructions");
//            }
//
//            printer.printOriginalText("\n");
//            printer.printHorizontalLine(2,0);
//
//            if(null != data.getQr_info()) {
//                printer.setAlignment(1);
//                printer.printQRCode(data.getQr_info(), 200);
//            }else{
//                printer.setAlignment(1);
//                printer.setFontSize(20);
//                printer.printOriginalText("Parking Site QR code");
//            }

            printer.setAlignment(0);
            printer.setFontSize(20);
            printer.printText("Powered by kiplePark\n");
//            printer.printBitmap(data.getBitmap());

            printer.printText("\n");

//            printer.setAlignment(1);
//            printer.setFontSize(23);
//            printer.printText("==============================\n");

            //  horizontal line
            printer.printHorizontalLine(2, 0);

            printer.setAlignment(1);
            printer.setFontSize(16);
            printer.printText("*****THANK YOU*****");
            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintReceipt", e.getMessage());
            e.printStackTrace();

        }
    }

    private String newLinePaymentMethod(String info){
        if(info.contains("+")){
            String[] splitInfo;
            splitInfo = info.split("\\+");
            return splitInfo[0] + "\n" + splitInfo[1];
        }else{
            return info;
        }
    }

    private void ticketPrint(IssueTicketModel data) {
        try {
//            printer.setFontSize(20);
//            printer.printText("Welcome to\n");
            printer.setFontSize(28);
            printer.printText(data.getSite_name() + "\n");
            printer.setFontSize(28);
            printer.printText(data.getDate_issued() + " - ");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            printer.printText(data.getRunning_id());
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.printText(" - " + data.getTime_issued() + "\n");
//            printer.setFontSize(16);
//            printer.printText(data.getSite_address() + "\n");
            printer.setFontSize(23);
            printer.printText("--------------------------------");
            printer.setAlignment(1);
            if(data.isCustom_qr_size())
                printer.printQRCode(data.getTicket_data(), data.getQr_size());
            else
                printer.printQRCode(data.getTicket_data(), 200);
            printer.setAlignment(1);
            printer.setFontSize(26);
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            printer.printText("\n" + data.getNumber_plate() + "\n");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.setFontSize(23);
            printer.printText("--------------------------------\n");
            printer.setAlignment(0);
            printer.setFontSize(20);

            if (data.isUpfront_flag()) {
                printer.printText(data.getTicket_info() + " " + data.getUpfront_amount());
            } else {
                printer.printText(data.getTicket_info());
            }
//            printer.printOriginalText("Would you like to summon your\ncar with your phone?\n\n");
//            printer.printOriginalText("1. Download kiplePark App\n");
//            printer.printOriginalText("2. Scan the QR code here\n");
//            printer.printOriginalText("3. Pay and request for car pick up\n");
            printer.printText("\n");
//            printer.printOriginalText("Your car will arrive in a few\nminutes\n");
            printer.setFontSize(23);
            printer.printText("--------------------------------\n");
//            printer.setAlignment(1);
//            printer.setFontSize(16);
//            printer.printText("*****THANK YOU*****");
            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintTicket", e.getMessage());
            e.printStackTrace();

        }
    }

    private void ticketDashboardPrint(IssueTicketModel data) {
        try {
//            printer.setFontSize(20);
//            printer.printText("Welcome to\n");
            printer.setFontSize(28);
            printer.printText(data.getSite_name() + "\n");
            printer.setFontSize(28);
            printer.printText(data.getDate_issued() + " - ");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            printer.printText(data.getRunning_id());
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.printText(" - " + data.getTime_issued() + "\n");
//            printer.setFontSize(16);
//            printer.printText(data.getSite_address() + "\n");
            printer.setFontSize(23);
            printer.printText("--------------------------------");
            printer.setAlignment(1);
            printer.setFontSize(170);
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            String numberPlate = data.getNumber_plate().replaceAll("[^0-9]", "");
            printer.printText("\n" + numberPlate + "\n");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.setFontSize(23);
            printer.printText("--------------------------------\n");
            printer.setAlignment(0);
            printer.setFontSize(20);

            if (data.isUpfront_flag()) {
                printer.printText(context.getString(R.string.upfront_payment) + " : " + data.getUpfront_amount());

                printer.printText("\n");
//            printer.printOriginalText("Your car will arrive in a few\nminutes\n");
                printer.setFontSize(23);
                printer.printText("--------------------------------\n");
            }
//            printer.printOriginalText("Would you like to summon your\ncar with your phone?\n\n");
//            printer.printOriginalText("1. Download kiplePark App\n");
//            printer.printOriginalText("2. Scan the QR code here\n");
//            printer.printOriginalText("3. Pay and request for car pick up\n");

//            printer.setAlignment(1);
//            printer.setFontSize(16);
//            printer.printText("*****THANK YOU*****");
            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintTicket", e.getMessage());
            e.printStackTrace();

        }
    }


    private void ticketValetPrint(IssueTicketModel data) {
        try {
//            printer.setFontSize(20);
//            printer.printText("Welcome to\n");
            printer.setFontSize(28);
            printer.printText(data.getSite_name() + "\n");
            printer.setFontSize(28);
            printer.printText(data.getDate_issued() + " - ");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            printer.printText(data.getRunning_id());
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.printText(" - " + data.getTime_issued() + "\n");
//            printer.setFontSize(16);
//            printer.printText(data.getSite_address() + "\n");

            printer.setFontSize(23);
            printer.printText("--------------------------------");
            printer.setAlignment(1);
            printer.setFontSize(26);
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x1});
            printer.printText("\n" + data.getNumber_plate() + "\n");
            printer.sendRAWData(new byte [] {0x1B, 0x45, 0x0});
            printer.setFontSize(23);
            printer.printText("--------------------------------\n");

            printer.setAlignment(0);
            printer.setFontSize(20);

            if (data.isUpfront_flag()) {
                printer.printText(context.getString(R.string.upfront_payment) + " : " + data.getUpfront_amount());
            }
//            printer.printOriginalText("Would you like to summon your\ncar with your phone?\n\n");
//            printer.printOriginalText("1. Download kiplePark App\n");
//            printer.printOriginalText("2. Scan the QR code here\n");
//            printer.printOriginalText("3. Pay and request for car pick up\n");
            printer.printText("\n");

            printer.printText("Issued staff:" + " - " + data.getStaff_issuer());

            printer.printText("\n");
//            printer.printOriginalText("Your car will arrive in a few\nminutes\n");
            printer.setFontSize(23);
            printer.printText("--------------------------------\n");
//            printer.setAlignment(1);
//            printer.setFontSize(16);
//            printer.printText("*****THANK YOU*****");
            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintTicket", e.getMessage());
            e.printStackTrace();

        }
    }

}
