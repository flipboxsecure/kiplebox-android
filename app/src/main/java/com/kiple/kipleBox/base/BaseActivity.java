package com.kiple.kipleBox.base;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.ui.core.history.HistoryTransactionFragment;
import com.kiple.kipleBox.ui.core.homepage.HomepageFragment;

/**
 * Created by xenhao.yap on 19/01/2018.
 */

public class BaseActivity extends AppCompatActivity{

    protected Toolbar mToolbar;

    protected Context mContext;
    //  for customised progress bar
//    protected LoadingDialogFragment mLoadingDialog;
    private boolean isStopped = false;
    protected FragmentManager fm;
    protected LayoutInflater inflater;

    protected int grey = R.color.text_grey,
            blue = R.color.kiple_blue;

    private long mBackPressed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        fm = getSupportFragmentManager();
        inflater = this.getLayoutInflater();

        //  Appsee  (trial expired, disabled for now)
        //  only activate if app is live version
//        if(BuildConfig.FLAVOR.equalsIgnoreCase("live")) {
//            Appsee.start(Constants.APPSEE_API_KEY);
//
//            //  Appsee user tagging  (trial expired, disabled for now)
//            if(BuildConfig.FLAVOR.equalsIgnoreCase("live")
//                    && SessionManager.getInstance(this).getUserInfo() != null) {
//                if (SessionManager.getInstance(this).getUserName() != null
//                        && !SessionManager.getInstance(this).getUserName().equalsIgnoreCase("")) {
//                    Appsee.setUserId(SessionManager.getInstance(this).getUserName());
//                    Crashlytics.setUserName(SessionManager.getInstance(this).getUserName());
//                }else {
//                    if (SessionManager.getInstance(this).getUserInfo().getHasPhoneNumber()) {
//                        Appsee.setUserId(SessionManager.getInstance(this).getUserInfo().getMobileNumber());
//                        Crashlytics.setUserName(SessionManager.getInstance(this).getUserInfo().getMobileNumber());
//                    }
//                }
//                Crashlytics.setUserIdentifier(SessionManager.getInstance(this).getUserId());
//            }
//        }

//        new Thread(onCreateThread).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        isStopped = false;
    }

    @Override
    public void onStop() {
        isStopped = true;
        super.onStop();
    }

    protected void initToolbar() {
//        mToolbar = (Toolbar) findViewById(<Insert Toolbar here>);
        if (mToolbar != null)
            setSupportActionBar(mToolbar);
    }

    //  system back key handling
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){

            //  detect which fragment is currently on display
            if(false/*fm.findFragmentById(R.id.content_frame) instanceof ContactUsFragment*/){
                //  show show confirmation before leaving page
//                BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                fragment.promptAlert();
//                CustomAlerts.showResponseAlert(this, getString(R.string.reminder_title), getString(R.string.alert_msg));
            }else if(fm.findFragmentById(R.id.content_frame) instanceof HomepageFragment
                    || fm.findFragmentById(R.id.content_frame) instanceof HistoryTransactionFragment){
                        //  detect, confirm, and close app
                if (mBackPressed + Constants.TIME_INTERVAL > System.currentTimeMillis()) {
                    //  exit app here
                    this.finish();
                    return super.onKeyDown(keyCode, event);
                }
                else {
                    Toast.makeText(getBaseContext(), getString(R.string.exit_msg), Toast.LENGTH_SHORT).show();
                }

                mBackPressed = System.currentTimeMillis();
            }else {
                //  back to previous page in backstack
                getSupportFragmentManager().popBackStackImmediate();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void replaceFragment(String animationFlag, int id, Fragment f, boolean history, String title){
        if(fm!=null && !isStopped){
//            Toast.makeText(mContext, "replace fragment", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft 	= fm.beginTransaction();

            switch (animationFlag){
                case Constants.SLIDE_ANIMATION:
                    ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.stay, R.anim.slide_out_right);
                    break;

                case Constants.REVERSE_SLIDE_ANIMATION:
                    ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.stay, R.anim.slide_out_right);
                    break;

                case Constants.ACTIVITY_ANIMATION:
                    ft.setCustomAnimations(R.anim.activity_open_enter, R.anim.activity_open_exit, R.anim.activity_close_enter, R.anim.activity_close_exit);
                    break;

                case Constants.VERTICAL_ANIMATION:
                    ft.setCustomAnimations(R.anim.slide_in_up, R.anim.stay, R.anim.stay, R.anim.slide_out_down);
                    break;

                default:
                    //  no animations
            }

            if(!title.isEmpty())
                ft.replace(id, f, title);
            else
                ft.replace(id, f);

            if(history)
                ft.addToBackStack(null);

            ft.commit();

        }
    }

    public void replaceFragment(boolean doAnimation, int id, Fragment f, boolean history, String title){
        if(fm!=null && !isStopped){
//            Toast.makeText(mContext, "replace fragment", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft 	= fm.beginTransaction();

            if(doAnimation)
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.stay, R.anim.slide_out_right);

            if(!title.isEmpty())
                ft.replace(id, f, title);
            else
                ft.replace(id, f);

            if(history)
                ft.addToBackStack(null);

            ft.commit();

        }
    }

    public void replaceFragment(boolean doAnimation, int id,Fragment f){
        if(fm!=null && !isStopped){
//            Toast.makeText(mContext, "replace fragment", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft 	= fm.beginTransaction();
            if(doAnimation)
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.stay, R.anim.slide_out_right);
            ft.replace(id, f);
            ft.addToBackStack(null);
            ft.commit();

        }
    }

    public void replaceFragment(int id,Fragment f){
        if(fm!=null && !isStopped){
//            Toast.makeText(mContext, "replace fragment", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft 	= fm.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.stay, R.anim.slide_out_right);
            ft.replace(id, f);
            ft.addToBackStack(null);
            ft.commit();

        }
    }

    public void replaceFragment(int id, Fragment f, boolean history){
        if(fm!=null && !isStopped){
//            Toast.makeText(mContext, "replace fragment fragment_history", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft 	= fm.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.stay, R.anim.slide_out_right);
            ft.replace(id, f);
            if(history) ft.addToBackStack(null);
            ft.commit();

        }
    }

    public void triggerBack(){
        this.getSupportFragmentManager().popBackStack();
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected int applyColor(int color){
        return getResources().getColor(color);
    }

    private Runnable onCreateThread = new Runnable() {
//        @Override
        public void run() {
//            if (mContext != null && mContext instanceof SplashActivity) {
//                registerFCM();
//                AppUpdateActivity.shouldDisplay(mContext);
//            }
        }
    };
}
