package com.kiple.kipleBox.base;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.kiple.kipleBox.common.OnFragmentChangeListener;
import com.kiple.kipleBox.network.APIInterface;
import com.kiple.kipleBox.utils.LogUtils;
import com.kiple.kipleBox.R;

/**
 * Created by xenhao.yap on 22/01/2018.
 */

public class BaseFragment extends Fragment implements View.OnClickListener {

    public OnFragmentChangeListener listener;
    protected FragmentManager fm;
    public LayoutInflater inflater;

    protected APIInterface connectAuthInterface;

    protected APIInterface authInterface;
    protected APIInterface profileInterface;
    protected APIInterface walletInterface;
    protected APIInterface parkingInterface;
    protected APIInterface kipleInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentChangeListener) context;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            LogUtils.LOGI(this.getClass().getSimpleName(), getString(R.string.listener_not_implemented));
        }
    }

    public void hideKeyboard(View view) {
        if(isAdded()) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

//        connectAuthInterface = APIClient.getKipleConnect().create(APIInterface.class);
//
//        authInterface = APIClient.getAuth().create(APIInterface.class);
//        profileInterface = APIClient.getProfile().create(APIInterface.class);
//        walletInterface = APIClient.getWallet().create(APIInterface.class);
//        parkingInterface = APIClient.getParking().create(APIInterface.class);
//        kipleInterface = APIClient.getKipleUrl().create(APIInterface.class);

        inflater = getActivity().getLayoutInflater();
        fm = getChildFragmentManager();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public void makeToast(String msg){
        if(isAdded()) {
            Toast toast = Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT);
            TextView tv = toast.getView().findViewById(android.R.id.message);
            if(tv != null)
                tv.setGravity(Gravity.CENTER);
            toast.show();
        }
    }

    public void makeToastLong(String msg){
        if(isAdded()) {
            Toast toast =Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
            TextView tv = toast.getView().findViewById(android.R.id.message);
            if(tv != null)
                tv.setGravity(Gravity.CENTER);
            toast.show();

        }
    }

    @Override
    public void onClick(View v) {

    }
}
