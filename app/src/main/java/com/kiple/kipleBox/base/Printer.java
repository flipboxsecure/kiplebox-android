package com.kiple.kipleBox.base;

import android.content.Context;

import com.sunmi.impl.V1Printer;

public class Printer extends V1Printer {
    public Printer(Context context) {
        super(context);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public void disconnectPrinter(){
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
