package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class ActivationResponse implements Parcelable
{

    @SerializedName("kpCode")
    @Expose
    private String kpCode;
    @SerializedName("kipleCodeDesc")
    @Expose
    private String kipleCodeDesc;
    @SerializedName("manager_name")
    @Expose
    private String manager_name;
    @SerializedName("manager_email")
    @Expose
    private String manager_email;
    @SerializedName("manager_token")
    @Expose
    private String manager_token;
    @SerializedName("site_id")
    @Expose
    private String site_id;
    @SerializedName("site_name")
    @Expose
    private String site_name;
    @SerializedName("phone_number")
    @Expose
    private Object phone_number;
    @SerializedName("street1")
    @Expose
    private String street1;
    @SerializedName("street2")
    @Expose
    private String street2;
    @SerializedName("postcode")
    @Expose
    private Integer postcode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("pos_id")
    @Expose
    private String pos_id;
    @SerializedName("pos_info1")
    @Expose
    private String pos_info1;
    @SerializedName("pos_info2")
    @Expose
    private String pos_info2;
    @SerializedName("identify")
    @Expose
    private String identify;
    @SerializedName("running_no_start")
    @Expose
    private Integer running_no_start;
    @SerializedName("old_deletion_after")
    @Expose
    private String old_deletion_after;
    @SerializedName("inactivity_allow")
    @Expose
    private String inactivity_allow;
    @SerializedName("validity_for_use")
    @Expose
    private String validity_for_use;
    @SerializedName("dateActivated")
    @Expose
    private String dateActivated;
    @SerializedName("dateExpired")
    @Expose
    private String dateExpired;
    @SerializedName("delimeter")
    @Expose
    private String delimeter;
    @SerializedName("grace_period")
    @Expose
    private Integer grace_period;
    @SerializedName("rate_formula")
    @Expose
    private Rate_formula rate_formula;
    @SerializedName("partner_name")
    @Expose
    private String partner_name;
    @SerializedName("gst_id")
    @Expose
    private String gst_id;
    @SerializedName("service_customize")
    @Expose
    private Service_customize service_customize;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("manager_id")
    @Expose
    private String managerId;
    @SerializedName("running_no_lenght")
    @Expose
    private String runningNoLenght;
    @SerializedName("pos_no")
    @Expose
    private Integer posNo;
    @SerializedName("parking_validation_flag")
    @Expose
    private Integer parkingValidationFlag;

    public final static Parcelable.Creator<ActivationResponse> CREATOR = new Creator<ActivationResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ActivationResponse createFromParcel(Parcel in) {
            return new ActivationResponse(in);
        }

        public ActivationResponse[] newArray(int size) {
            return (new ActivationResponse[size]);
        }

    }
            ;

    protected ActivationResponse(Parcel in) {
        this.kpCode = ((String) in.readValue((String.class.getClassLoader())));
        this.kipleCodeDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.manager_name = ((String) in.readValue((String.class.getClassLoader())));
        this.manager_email = ((String) in.readValue((String.class.getClassLoader())));
        this.manager_token = ((String) in.readValue((String.class.getClassLoader())));
        this.site_id = ((String) in.readValue((String.class.getClassLoader())));
        this.site_name = ((String) in.readValue((String.class.getClassLoader())));
        this.phone_number = ((Object) in.readValue((Object.class.getClassLoader())));
        this.street1 = ((String) in.readValue((String.class.getClassLoader())));
        this.street2 = ((String) in.readValue((String.class.getClassLoader())));
        this.postcode = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.pos_id = ((String) in.readValue((String.class.getClassLoader())));
        this.pos_info1 = ((String) in.readValue((String.class.getClassLoader())));
        this.pos_info2 = ((String) in.readValue((String.class.getClassLoader())));
        this.identify = ((String) in.readValue((String.class.getClassLoader())));
        this.running_no_start = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.old_deletion_after = ((String) in.readValue((String.class.getClassLoader())));
        this.inactivity_allow = ((String) in.readValue((String.class.getClassLoader())));
        this.validity_for_use = ((String) in.readValue((String.class.getClassLoader())));
        this.dateActivated = ((String) in.readValue((String.class.getClassLoader())));
        this.dateExpired = ((String) in.readValue((String.class.getClassLoader())));
        this.delimeter = ((String) in.readValue((String.class.getClassLoader())));
        this.grace_period = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.rate_formula = ((Rate_formula) in.readValue((Rate_formula.class.getClassLoader())));
        this.partner_name = ((String) in.readValue((String.class.getClassLoader())));
        this.gst_id = ((String) in.readValue((String.class.getClassLoader())));
        this.service_customize = ((Service_customize) in.readValue((Service_customize.class.getClassLoader())));
        this.timezone = ((String) in.readValue((String.class.getClassLoader())));
        this.managerId = ((String) in.readValue((String.class.getClassLoader())));
        this.runningNoLenght = ((String) in.readValue((String.class.getClassLoader())));
        this.posNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.parkingValidationFlag = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ActivationResponse() {
    }

    public String getKpCode() {
        return kpCode;
    }

    public void setKpCode(String kpCode) {
        this.kpCode = kpCode;
    }

    public String getKipleCodeDesc() {
        return kipleCodeDesc;
    }

    public void setKipleCodeDesc(String kipleCodeDesc) {
        this.kipleCodeDesc = kipleCodeDesc;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_email() {
        return manager_email;
    }

    public void setManager_email(String manager_email) {
        this.manager_email = manager_email;
    }

    public String getManager_token() {
        return manager_token;
    }

    public void setManager_token(String manager_token) {
        this.manager_token = manager_token;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public Object getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(Object phone_number) {
        this.phone_number = phone_number;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPos_id() {
        return pos_id;
    }

    public void setPos_id(String pos_id) {
        this.pos_id = pos_id;
    }

    public String getPos_info1() {
        return pos_info1;
    }

    public void setPos_info1(String pos_info1) {
        this.pos_info1 = pos_info1;
    }

    public String getPos_info2() {
        return pos_info2;
    }

    public void setPos_info2(String pos_info2) {
        this.pos_info2 = pos_info2;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public Integer getRunning_no_start() {
        return running_no_start;
    }

    public void setRunning_no_start(Integer running_no_start) {
        this.running_no_start = running_no_start;
    }

    public String getOld_deletion_after() {
        return old_deletion_after;
    }

    public void setOld_deletion_after(String old_deletion_after) {
        this.old_deletion_after = old_deletion_after;
    }

    public String getInactivity_allow() {
        return inactivity_allow;
    }

    public void setInactivity_allow(String inactivity_allow) {
        this.inactivity_allow = inactivity_allow;
    }

    public String getValidity_for_use() {
        return validity_for_use;
    }

    public void setValidity_for_use(String validity_for_use) {
        this.validity_for_use = validity_for_use;
    }

    public String getDateActivated() {
        return dateActivated;
    }

    public void setDateActivated(String dateActivated) {
        this.dateActivated = dateActivated;
    }

    public String getDateExpired() {
        return dateExpired;
    }

    public void setDateExpired(String dateExpired) {
        this.dateExpired = dateExpired;
    }

    public String getDelimeter() {
        return delimeter;
    }

    public void setDelimeter(String delimeter) {
        this.delimeter = delimeter;
    }

    public Integer getGrace_period() {
        return grace_period;
    }

    public void setGrace_period(Integer grace_period) {
        this.grace_period = grace_period;
    }

    public Rate_formula getRate_formula() {
        return rate_formula;
    }

    public void setRate_formula(Rate_formula rate_formula) {
        this.rate_formula = rate_formula;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getGst_id() {
        return gst_id;
    }

    public void setGst_id(String gst_id) {
        this.gst_id = gst_id;
    }

    public Service_customize getService_customize() {
        return service_customize;
    }

    public void setService_customize(Service_customize service_customize) {
        this.service_customize = service_customize;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getRunningNoLenght() {
        return runningNoLenght;
    }

    public void setRunningNoLenght(String runningNoLenght) {
        this.runningNoLenght = runningNoLenght;
    }

    public Integer getPosNo() {
        return posNo;
    }

    public void setPosNo(Integer posNo) {
        this.posNo = posNo;
    }

    public Integer getParkingValidationFlag() {
        return parkingValidationFlag;
    }

    public void setParkingValidationFlag(Integer parkingValidationFlag) {
        this.parkingValidationFlag = parkingValidationFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("kpCode", kpCode).append("kipleCodeDesc", kipleCodeDesc).append("manager_name", manager_name).append("manager_email", manager_email).append("manager_token", manager_token).append("site_id", site_id).append("site_name", site_name).append("phone_number", phone_number).append("street1", street1).append("street2", street2).append("postcode", postcode).append("city", city).append("state", state).append("pos_id", pos_id).append("pos_info1", pos_info1).append("pos_info2", pos_info2).append("identify", identify).append("running_no_start", running_no_start).append("old_deletion_after", old_deletion_after).append("inactivity_allow", inactivity_allow).append("validity_for_use", validity_for_use).append("dateActivated", dateActivated).append("dateExpired", dateExpired).append("delimeter", delimeter).append("grace_period", grace_period).append("parking_validation_flag", parkingValidationFlag).append("rate_formula", rate_formula).append("partner_name", partner_name).append("gst_id", gst_id).append("service_customize", service_customize).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(kpCode);
        dest.writeValue(kipleCodeDesc);
        dest.writeValue(manager_name);
        dest.writeValue(manager_email);
        dest.writeValue(manager_token);
        dest.writeValue(site_id);
        dest.writeValue(site_name);
        dest.writeValue(phone_number);
        dest.writeValue(street1);
        dest.writeValue(street2);
        dest.writeValue(postcode);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(pos_id);
        dest.writeValue(pos_info1);
        dest.writeValue(pos_info2);
        dest.writeValue(identify);
        dest.writeValue(running_no_start);
        dest.writeValue(old_deletion_after);
        dest.writeValue(inactivity_allow);
        dest.writeValue(validity_for_use);
        dest.writeValue(dateActivated);
        dest.writeValue(dateExpired);
        dest.writeValue(delimeter);
        dest.writeValue(grace_period);
        dest.writeValue(rate_formula);
        dest.writeValue(partner_name);
        dest.writeValue(gst_id);
        dest.writeValue(service_customize);
        dest.writeValue(timezone);
        dest.writeValue(managerId);
        dest.writeValue(runningNoLenght);
        dest.writeValue(posNo);
        dest.writeValue(parkingValidationFlag);
    }

    public int describeContents() {
        return 0;
    }

    public static class Datum implements Parcelable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("package_id")
        @Expose
        private Object package_id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("location")
        @Expose
        private Object location;
        @SerializedName("site_id")
        @Expose
        private String site_id;
//        @SerializedName("rate_per_hour")
//        @Expose
//        private Rate_per_hour rate_per_hour;
        @SerializedName("subsequent_block")
        @Expose
        private String subsequent_block;
        @SerializedName("subsequent_rates")
        @Expose
        private String subsequent_rates;
//        @SerializedName("rate_per_entry")
//        @Expose
//        private String rate_per_entry;
        @SerializedName("start_rate_per_entry")
        @Expose
        private String start_rate_per_entry;
        @SerializedName("end_rate_per_entry")
        @Expose
        private String end_rate_per_entry;
        @SerializedName("max_rate")
        @Expose
        private String max_rate;
        @SerializedName("lost_ticket")
        @Expose
        private String lost_ticket;
        public final static Parcelable.Creator<Datum> CREATOR = new Creator<Datum>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            public Datum[] newArray(int size) {
                return (new Datum[size]);
            }

        }
                ;

        protected Datum(Parcel in) {
            this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.package_id = ((Object) in.readValue((Object.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
            this.location = ((Object) in.readValue((Object.class.getClassLoader())));
            this.site_id = ((String) in.readValue((String.class.getClassLoader())));
//            this.rate_per_hour = ((Rate_per_hour) in.readValue((Rate_per_hour.class.getClassLoader())));
            this.subsequent_block = ((String) in.readValue((String.class.getClassLoader())));
            this.subsequent_rates = ((String) in.readValue((String.class.getClassLoader())));
//            this.rate_per_entry = ((String) in.readValue((String.class.getClassLoader())));
            this.start_rate_per_entry = ((String) in.readValue((String.class.getClassLoader())));
            this.end_rate_per_entry = ((String) in.readValue((String.class.getClassLoader())));
            this.max_rate = ((String) in.readValue((String.class.getClassLoader())));
            this.lost_ticket = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Datum() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Object getPackage_id() {
            return package_id;
        }

        public void setPackage_id(Object package_id) {
            this.package_id = package_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Object getLocation() {
            return location;
        }

        public void setLocation(Object location) {
            this.location = location;
        }

        public String getSite_id() {
            return site_id;
        }

        public void setSite_id(String site_id) {
            this.site_id = site_id;
        }

//        public Rate_per_hour getRate_per_hour() {
//            return rate_per_hour;
//        }
//
//        public void setRate_per_hour(Rate_per_hour rate_per_hour) {
//            this.rate_per_hour = rate_per_hour;
//        }

        public String getSubsequent_block() {
            return subsequent_block;
        }

        public void setSubsequent_block(String subsequent_block) {
            this.subsequent_block = subsequent_block;
        }

        public String getSubsequent_rates() {
            return subsequent_rates;
        }

        public void setSubsequent_rates(String subsequent_rates) {
            this.subsequent_rates = subsequent_rates;
        }

//        public String getRate_per_entry() {
//            return rate_per_entry;
//        }
//
//        public void setRate_per_entry(String rate_per_entry) {
//            this.rate_per_entry = rate_per_entry;
//        }

        public String getStart_rate_per_entry() {
            return start_rate_per_entry;
        }

        public void setStart_rate_per_entry(String start_rate_per_entry) {
            this.start_rate_per_entry = start_rate_per_entry;
        }

        public String getEnd_rate_per_entry() {
            return end_rate_per_entry;
        }

        public void setEnd_rate_per_entry(String end_rate_per_entry) {
            this.end_rate_per_entry = end_rate_per_entry;
        }

        public String getMax_rate() {
            return max_rate;
        }

        public void setMax_rate(String max_rate) {
            this.max_rate = max_rate;
        }

        public String getLost_ticket() {
            return lost_ticket;
        }

        public void setLost_ticket(String lost_ticket) {
            this.lost_ticket = lost_ticket;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("id", id).append("package_id", package_id).append("type", type).append("location", location).append("site_id", site_id)/*.append("rate_per_hour", rate_per_hour)*/.append("subsequent_block", subsequent_block).append("subsequent_rates", subsequent_rates)/*.append("rate_per_entry", rate_per_entry)*/.append("start_rate_per_entry", start_rate_per_entry).append("end_rate_per_entry", end_rate_per_entry).append("max_rate", max_rate).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(package_id);
            dest.writeValue(type);
            dest.writeValue(location);
            dest.writeValue(site_id);
//            dest.writeValue(rate_per_hour);
            dest.writeValue(subsequent_block);
            dest.writeValue(subsequent_rates);
//            dest.writeValue(rate_per_entry);
            dest.writeValue(start_rate_per_entry);
            dest.writeValue(end_rate_per_entry);
            dest.writeValue(max_rate);
            dest.writeValue(lost_ticket);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Issue implements Parcelable
    {

        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("service_features")
        @Expose
        private List<Service_feature> service_features = new ArrayList<>();
        public final static Parcelable.Creator<Issue> CREATOR = new Creator<Issue>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Issue createFromParcel(Parcel in) {
                return new Issue(in);
            }

            public Issue[] newArray(int size) {
                return (new Issue[size]);
            }

        }
                ;

        protected Issue(Parcel in) {
            this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(this.service_features, (Service_feature.class.getClassLoader()));
        }

        public Issue() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public List<Service_feature> getService_features() {
            return service_features;
        }

        public void setService_features(List<Service_feature> service_features) {
            this.service_features = service_features;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("service_features", service_features).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(active);
            dest.writeList(service_features);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Park implements Parcelable
    {

        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("service_features")
        @Expose
        private List<Service_feature> service_features = new ArrayList<>();
        public final static Parcelable.Creator<Park> CREATOR = new Creator<Park>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Park createFromParcel(Parcel in) {
                return new Park(in);
            }

            public Park[] newArray(int size) {
                return (new Park[size]);
            }

        }
                ;

        protected Park(Parcel in) {
            this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(this.service_features, (Service_feature.class.getClassLoader()));
        }

        public Park() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public List<Service_feature> getService_features() {
            return service_features;
        }

        public void setService_features(List<Service_feature> service_features) {
            this.service_features = service_features;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("service_features", service_features).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(active);
            dest.writeList(service_features);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Pay implements Parcelable
    {

        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("service_features")
        @Expose
        private List<Service_feature> service_features = new ArrayList<>();
        public final static Parcelable.Creator<Pay> CREATOR = new Creator<Pay>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Pay createFromParcel(Parcel in) {
                return new Pay(in);
            }

            public Pay[] newArray(int size) {
                return (new Pay[size]);
            }

        }
                ;

        protected Pay(Parcel in) {
            this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(this.service_features, (Service_feature.class.getClassLoader()));
        }

        public Pay() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public List<Service_feature> getService_features() {
            return service_features;
        }

        public void setService_features(List<Service_feature> service_features) {
            this.service_features = service_features;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("service_features", service_features).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(active);
            dest.writeList(service_features);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Collect implements Parcelable
    {

        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("service_features")
        @Expose
        private List<Service_feature> service_features = new ArrayList<>();
        public final static Parcelable.Creator<Collect> CREATOR = new Creator<Collect>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Collect createFromParcel(Parcel in) {
                return new Collect(in);
            }

            public Collect[] newArray(int size) {
                return (new Collect[size]);
            }

        }
                ;

        protected Collect(Parcel in) {
            this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(this.service_features, (Service_feature.class.getClassLoader()));
        }

        public Collect() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public List<Service_feature> getService_features() {
            return service_features;
        }

        public void setService_features(List<Service_feature> service_features) {
            this.service_features = service_features;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("service_features", service_features).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(active);
            dest.writeList(service_features);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Complete implements Parcelable
    {

        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("service_features")
        @Expose
        private List<Service_feature> service_features = new ArrayList<>();
        public final static Parcelable.Creator<Complete> CREATOR = new Creator<Complete>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Complete createFromParcel(Parcel in) {
                return new Complete(in);
            }

            public Complete[] newArray(int size) {
                return (new Complete[size]);
            }

        }
                ;

        protected Complete(Parcel in) {
            this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            in.readList(this.service_features, (Service_feature.class.getClassLoader()));
        }

        public Complete() {
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public List<Service_feature> getService_features() {
            return service_features;
        }

        public void setService_features(List<Service_feature> service_features) {
            this.service_features = service_features;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("service_features", service_features).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(active);
            dest.writeList(service_features);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Rate_formula implements Parcelable
    {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<Datum> data = new ArrayList<>();
        public final static Parcelable.Creator<Rate_formula> CREATOR = new Creator<Rate_formula>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Rate_formula createFromParcel(Parcel in) {
                return new Rate_formula(in);
            }

            public Rate_formula[] newArray(int size) {
                return (new Rate_formula[size]);
            }

        }
                ;

        protected Rate_formula(Parcel in) {
            this.status = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(this.data, (Datum.class.getClassLoader()));
        }

        public Rate_formula() {
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("status", status).append("data", data).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(status);
            dest.writeList(data);
        }

        public int describeContents() {
            return 0;
        }

    }

//    public static class Rate_per_hour implements Parcelable
//    {
//
//        @SerializedName("30")
//        @Expose
//        private String _30;
//        @SerializedName("10000000000")
//        @Expose
//        private String _10000000000;
//        public final static Parcelable.Creator<Rate_per_hour> CREATOR = new Creator<Rate_per_hour>() {
//
//
//            @SuppressWarnings({
//                    "unchecked"
//            })
//            public Rate_per_hour createFromParcel(Parcel in) {
//                return new Rate_per_hour(in);
//            }
//
//            public Rate_per_hour[] newArray(int size) {
//                return (new Rate_per_hour[size]);
//            }
//
//        }
//                ;
//
//        protected Rate_per_hour(Parcel in) {
//            this._30 = ((String) in.readValue((String.class.getClassLoader())));
//            this._10000000000 = ((String) in.readValue((String.class.getClassLoader())));
//        }
//
//        public Rate_per_hour() {
//        }
//
//        public String get30() {
//            return _30;
//        }
//
//        public void set30(String _30) {
//            this._30 = _30;
//        }
//
//        public String get10000000000() {
//            return _10000000000;
//        }
//
//        public void set10000000000(String _10000000000) {
//            this._10000000000 = _10000000000;
//        }
//
//        @Override
//        public String toString() {
//            return new ToStringBuilder(this).append("_30", _30).append("_10000000000", _10000000000).toString();
//        }
//
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeValue(_30);
//            dest.writeValue(_10000000000);
//        }
//
//        public int describeContents() {
//            return 0;
//        }
//
//    }

    public static class Service_customize implements Parcelable
    {

        @SerializedName("issue")
        @Expose
        private Issue issue;
        @SerializedName("park")
        @Expose
        private Park park;
        @SerializedName("pay")
        @Expose
        private Pay pay;
        @SerializedName("collect")
        @Expose
        private Collect collect;
        @SerializedName("complete")
        @Expose
        private Complete complete;
        public final static Parcelable.Creator<Service_customize> CREATOR = new Creator<Service_customize>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Service_customize createFromParcel(Parcel in) {
                return new Service_customize(in);
            }

            public Service_customize[] newArray(int size) {
                return (new Service_customize[size]);
            }

        }
                ;

        protected Service_customize(Parcel in) {
            this.issue = ((Issue) in.readValue((Issue.class.getClassLoader())));
            this.park = ((Park) in.readValue((Park.class.getClassLoader())));
            this.pay = ((Pay) in.readValue((Pay.class.getClassLoader())));
            this.collect = ((Collect) in.readValue((Collect.class.getClassLoader())));
            this.complete = ((Complete) in.readValue((Complete.class.getClassLoader())));
        }

        public Service_customize() {
        }

        public Issue getIssue() {
            return issue;
        }

        public void setIssue(Issue issue) {
            this.issue = issue;
        }

        public Park getPark() {
            return park;
        }

        public void setPark(Park park) {
            this.park = park;
        }

        public Pay getPay() {
            return pay;
        }

        public void setPay(Pay pay) {
            this.pay = pay;
        }

        public Collect getCollect() {
            return collect;
        }

        public void setCollect(Collect collect) {
            this.collect = collect;
        }

        public Complete getComplete() {
            return complete;
        }

        public void setComplete(Complete complete) {
            this.complete = complete;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("issue", issue).append("park", park).append("pay", pay).append("collect", collect).append("complete", complete).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(issue);
            dest.writeValue(park);
            dest.writeValue(pay);
            dest.writeValue(collect);
            dest.writeValue(complete);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Service_feature implements Parcelable
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("flag_value")
        @Expose
        private String flag_value;
        @SerializedName("default_values")
        @Expose
        private String default_values;
        @SerializedName("effective_flag_value")
        @Expose
        private String effective_flag_value;
        @SerializedName("attributes")
        @Expose
        private List<Attribute> attributes = new ArrayList<>();
        public final static Parcelable.Creator<Service_feature> CREATOR = new Creator<Service_feature>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Service_feature createFromParcel(Parcel in) {
                return new Service_feature(in);
            }

            public Service_feature[] newArray(int size) {
                return (new Service_feature[size]);
            }

        }
                ;

        protected Service_feature(Parcel in) {
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.flag_value = ((String) in.readValue((String.class.getClassLoader())));
            this.default_values = ((String) in.readValue((String.class.getClassLoader())));
            this.effective_flag_value = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(this.attributes, (Attribute.class.getClassLoader()));
        }

        public Service_feature() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFlag_value() {
            return flag_value;
        }

        public void setFlag_value(String flag_value) {
            this.flag_value = flag_value;
        }

        public String getDefault_values() {
            return default_values;
        }

        public void setDefault_values(String default_values) {
            this.default_values = default_values;
        }

        public String getEffective_flag_value() {
            return effective_flag_value;
        }

        public void setEffective_flag_value(String effective_flag_value) {
            this.effective_flag_value = effective_flag_value;
        }

        public List<Attribute> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<Attribute> attributes) {
            this.attributes = attributes;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("name", name).append("flag_value", flag_value).append("default_values", default_values).append("effective_flag_value", effective_flag_value).append("attributes", attributes).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(name);
            dest.writeValue(flag_value);
            dest.writeValue(default_values);
            dest.writeValue(effective_flag_value);
            dest.writeList(attributes);
        }

        public int describeContents() {
            return 0;
        }

    }

//    public static class Service_feature_ implements Parcelable
//    {
//
//        @SerializedName("name")
//        @Expose
//        private String name;
//        @SerializedName("flag_value")
//        @Expose
//        private String flag_value;
//        @SerializedName("default_values")
//        @Expose
//        private String default_values;
//        @SerializedName("effective_flag_value")
//        @Expose
//        private String effective_flag_value;
//        @SerializedName("attributes")
//        @Expose
//        private List<Attribute_> attributes = null;
//        public final static Parcelable.Creator<Service_feature_> CREATOR = new Creator<Service_feature_>() {
//
//
//            @SuppressWarnings({
//                    "unchecked"
//            })
//            public Service_feature_ createFromParcel(Parcel in) {
//                return new Service_feature_(in);
//            }
//
//            public Service_feature_[] newArray(int size) {
//                return (new Service_feature_[size]);
//            }
//
//        }
//                ;
//
//        protected Service_feature_(Parcel in) {
//            this.name = ((String) in.readValue((String.class.getClassLoader())));
//            this.flag_value = ((String) in.readValue((String.class.getClassLoader())));
//            this.default_values = ((String) in.readValue((String.class.getClassLoader())));
//            this.effective_flag_value = ((String) in.readValue((String.class.getClassLoader())));
//            in.readList(this.attributes, (Attribute_.class.getClassLoader()));
//        }
//
//        public Service_feature_() {
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getFlag_value() {
//            return flag_value;
//        }
//
//        public void setFlag_value(String flag_value) {
//            this.flag_value = flag_value;
//        }
//
//        public String getDefault_values() {
//            return default_values;
//        }
//
//        public void setDefault_values(String default_values) {
//            this.default_values = default_values;
//        }
//
//        public String getEffective_flag_value() {
//            return effective_flag_value;
//        }
//
//        public void setEffective_flag_value(String effective_flag_value) {
//            this.effective_flag_value = effective_flag_value;
//        }
//
//        public List<Attribute_> getAttributes() {
//            return attributes;
//        }
//
//        public void setAttributes(List<Attribute_> attributes) {
//            this.attributes = attributes;
//        }
//
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeValue(name);
//            dest.writeValue(flag_value);
//            dest.writeValue(default_values);
//            dest.writeValue(effective_flag_value);
//            dest.writeList(attributes);
//        }
//
//        public int describeContents() {
//            return 0;
//        }
//
//    }

//    public static class Service_feature__ implements Parcelable
//    {
//
//        @SerializedName("name")
//        @Expose
//        private String name;
//        @SerializedName("flag_value")
//        @Expose
//        private String flag_value;
//        @SerializedName("default_values")
//        @Expose
//        private String default_values;
//        @SerializedName("effective_flag_value")
//        @Expose
//        private String effective_flag_value;
//        @SerializedName("attributes")
//        @Expose
//        private List<Attribute__> attributes = null;
//        public final static Parcelable.Creator<Service_feature__> CREATOR = new Creator<Service_feature__>() {
//
//
//            @SuppressWarnings({
//                    "unchecked"
//            })
//            public Service_feature__ createFromParcel(Parcel in) {
//                return new Service_feature__(in);
//            }
//
//            public Service_feature__[] newArray(int size) {
//                return (new Service_feature__[size]);
//            }
//
//        }
//                ;
//
//        protected Service_feature__(Parcel in) {
//            this.name = ((String) in.readValue((String.class.getClassLoader())));
//            this.flag_value = ((String) in.readValue((String.class.getClassLoader())));
//            this.default_values = ((String) in.readValue((String.class.getClassLoader())));
//            this.effective_flag_value = ((String) in.readValue((String.class.getClassLoader())));
//            in.readList(this.attributes, (Attribute__.class.getClassLoader()));
//        }
//
//        public Service_feature__() {
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getFlag_value() {
//            return flag_value;
//        }
//
//        public void setFlag_value(String flag_value) {
//            this.flag_value = flag_value;
//        }
//
//        public String getDefault_values() {
//            return default_values;
//        }
//
//        public void setDefault_values(String default_values) {
//            this.default_values = default_values;
//        }
//
//        public String getEffective_flag_value() {
//            return effective_flag_value;
//        }
//
//        public void setEffective_flag_value(String effective_flag_value) {
//            this.effective_flag_value = effective_flag_value;
//        }
//
//        public List<Attribute__> getAttributes() {
//            return attributes;
//        }
//
//        public void setAttributes(List<Attribute__> attributes) {
//            this.attributes = attributes;
//        }
//
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeValue(name);
//            dest.writeValue(flag_value);
//            dest.writeValue(default_values);
//            dest.writeValue(effective_flag_value);
//            dest.writeList(attributes);
//        }
//
//        public int describeContents() {
//            return 0;
//        }
//
//    }

    public static class Attribute implements Parcelable
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("values")
        @Expose
        private String values;
        @SerializedName("default")
        @Expose
        private String _default;
        public final static Parcelable.Creator<Attribute> CREATOR = new Creator<Attribute>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Attribute createFromParcel(Parcel in) {
                return new Attribute(in);
            }

            public Attribute[] newArray(int size) {
                return (new Attribute[size]);
            }

        }
                ;

        protected Attribute(Parcel in) {
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.values = ((String) in.readValue((String.class.getClassLoader())));
            this._default = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Attribute() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValues() {
            return values;
        }

        public void setValues(String values) {
            this.values = values;
        }

        public String getDefault() {
            return _default;
        }

        public void setDefault(String _default) {
            this._default = _default;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("name", name).append("values", values).append("_default", _default).toString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(name);
            dest.writeValue(values);
            dest.writeValue(_default);
        }

        public int describeContents() {
            return 0;
        }

    }

//    public static class Attribute_ implements Parcelable
//    {
//
//        @SerializedName("name")
//        @Expose
//        private String name;
//        @SerializedName("values")
//        @Expose
//        private String values;
//        public final static Parcelable.Creator<Attribute_> CREATOR = new Creator<Attribute_>() {
//
//
//            @SuppressWarnings({
//                    "unchecked"
//            })
//            public Attribute_ createFromParcel(Parcel in) {
//                return new Attribute_(in);
//            }
//
//            public Attribute_[] newArray(int size) {
//                return (new Attribute_[size]);
//            }
//
//        }
//                ;
//
//        protected Attribute_(Parcel in) {
//            this.name = ((String) in.readValue((String.class.getClassLoader())));
//            this.values = ((String) in.readValue((String.class.getClassLoader())));
//        }
//
//        public Attribute_() {
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getValues() {
//            return values;
//        }
//
//        public void setValues(String values) {
//            this.values = values;
//        }
//
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeValue(name);
//            dest.writeValue(values);
//        }
//
//        public int describeContents() {
//            return 0;
//        }
//
//    }

//    public static class Attribute__ implements Parcelable
//    {
//
//        @SerializedName("name")
//        @Expose
//        private String name;
//        @SerializedName("values")
//        @Expose
//        private String values;
//        @SerializedName("default")
//        @Expose
//        private String _default;
//        public final static Parcelable.Creator<Attribute__> CREATOR = new Creator<Attribute__>() {
//
//
//            @SuppressWarnings({
//                    "unchecked"
//            })
//            public Attribute__ createFromParcel(Parcel in) {
//                return new Attribute__(in);
//            }
//
//            public Attribute__[] newArray(int size) {
//                return (new Attribute__[size]);
//            }
//
//        }
//                ;
//
//        protected Attribute__(Parcel in) {
//            this.name = ((String) in.readValue((String.class.getClassLoader())));
//            this.values = ((String) in.readValue((String.class.getClassLoader())));
//            this._default = ((String) in.readValue((String.class.getClassLoader())));
//        }
//
//        public Attribute__() {
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getValues() {
//            return values;
//        }
//
//        public void setValues(String values) {
//            this.values = values;
//        }
//
//        public String getDefault() {
//            return _default;
//        }
//
//        public void setDefault(String _default) {
//            this._default = _default;
//        }
//
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeValue(name);
//            dest.writeValue(values);
//            dest.writeValue(_default);
//        }
//
//        public int describeContents() {
//            return 0;
//        }
//
//    }

}