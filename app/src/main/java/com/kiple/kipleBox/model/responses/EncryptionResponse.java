package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EncryptionResponse implements Parcelable
{

    @SerializedName("encoded_ticket")
    @Expose
    private String encoded_ticket;
    @SerializedName("decoded_ticket")
    @Expose
    private String decoded_ticket;
    public final static Parcelable.Creator<EncryptionResponse> CREATOR = new Creator<EncryptionResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public EncryptionResponse createFromParcel(Parcel in) {
            return new EncryptionResponse(in);
        }

        public EncryptionResponse[] newArray(int size) {
            return (new EncryptionResponse[size]);
        }

    }
            ;

    protected EncryptionResponse(Parcel in) {
        this.encoded_ticket = ((String) in.readValue((String.class.getClassLoader())));
        this.decoded_ticket = ((String) in.readValue((String.class.getClassLoader())));
    }

    public EncryptionResponse() {
    }

    public String getEncoded_ticket() {
        return encoded_ticket;
    }

    public void setEncoded_ticket(String encoded_ticket) {
        this.encoded_ticket = encoded_ticket;
    }

    public String getDecoded_ticket() {
        return decoded_ticket;
    }

    public void setDecoded_ticket(String decoded_ticket) {
        this.decoded_ticket = decoded_ticket;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(encoded_ticket);
        dest.writeValue(decoded_ticket);
    }

    public int describeContents() {
        return 0;
    }

}