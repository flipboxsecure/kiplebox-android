package com.kiple.kipleBox.model.printer_models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.kiple.kipleBox.model.histroy.SummaryResponse;

import java.util.ArrayList;
import java.util.List;

public class SummaryModel implements Parcelable {

    private String site_name;
    private String site_address;
    private String startDateTime;
    private String endDateTime;
    private String ticket_id;
    private String cashTotal;
    private String guestRoomTotal;
    private String masterAccTotal;
    private String debitCreditTotal;
    private String transaction_type;
    private String staff_name;
    private String pos_info;
    private String qr_info;
    private List<SummaryResponse.Cash> cashArrayList = new ArrayList<>();
    private List<SummaryResponse.Guest_room> guestRoomArrayListArrayList = new ArrayList<>();
    private List<SummaryResponse.Master_account> masterAccountArrayListArrayList = new ArrayList<>();
    private List<SummaryResponse.Debit_credit> debitCreditArrayListArrayList = new ArrayList<>();
    private String grandTotal;
    private Bitmap bitmap;

    public SummaryModel() {
    }

    protected SummaryModel(Parcel in) {
        site_name = in.readString();
        site_address = in.readString();
        startDateTime = in.readString();
        endDateTime = in.readString();
        staff_name = in.readString();
        ticket_id = in.readString();
        cashTotal = in.readString();
        guestRoomTotal = in.readString();
        masterAccTotal = in.readString();
        transaction_type = in.readString();
        pos_info = in.readString();
        qr_info = in.readString();
        in.readList(this.cashArrayList, (java.lang.String.class.getClassLoader()));
        in.readList(this.guestRoomArrayListArrayList, (java.lang.String.class.getClassLoader()));
        in.readList(this.masterAccountArrayListArrayList, (java.lang.String.class.getClassLoader()));
        in.readList(this.debitCreditArrayListArrayList, (java.lang.String.class.getClassLoader()));
        grandTotal = in.readString();
        this.bitmap = ((Bitmap) in.readValue((String.class.getClassLoader())));
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getSite_address() {
        return site_address;
    }

    public void setSite_address(String site_address) {
        this.site_address = site_address;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getCashTotal() {
        return cashTotal;
    }

    public void setCashTotal(String amount) {
        this.cashTotal = amount;
    }

    public String getGuestRoomTotal() {
        return guestRoomTotal;
    }

    public void setGuestRoomTotal(String guestRoomTotal) {
        this.guestRoomTotal = guestRoomTotal;
    }

    public String getMasterAccTotal() {
        return masterAccTotal;
    }

    public void setMasterAccTotal(String masterAccTotal) {
        this.masterAccTotal = masterAccTotal;
    }

    public String getDebitCreditTotal() {
        return debitCreditTotal;
    }

    public void setDebitCreditTotal(String debitCreditTotal) {
        this.debitCreditTotal = debitCreditTotal;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getPos_info() {
        return pos_info;
    }

    public void setPos_info(String pos_info) {
        this.pos_info = pos_info;
    }

    public String getQr_info() {
        return qr_info;
    }

    public void setQr_info(String qr_info) {
        this.qr_info = qr_info;
    }

    public List<SummaryResponse.Cash> getCashArrayList() {
        return cashArrayList;
    }

    public void setCashArrayList(List<SummaryResponse.Cash> cashArrayList) {
        this.cashArrayList = cashArrayList;
    }

    public List<SummaryResponse.Guest_room> getGuestRoomArrayListArrayList() {
        return guestRoomArrayListArrayList;
    }

    public void setGuestRoomArrayListArrayList(List<SummaryResponse.Guest_room> guestRoomArrayListArrayList) {
        this.guestRoomArrayListArrayList = guestRoomArrayListArrayList;
    }

    public List<SummaryResponse.Master_account> getMasterAccountArrayListArrayList() {
        return masterAccountArrayListArrayList;
    }

    public void setMasterAccountArrayListArrayList(List<SummaryResponse.Master_account> masterAccountArrayListArrayList) {
        this.masterAccountArrayListArrayList = masterAccountArrayListArrayList;
    }

    public List<SummaryResponse.Debit_credit> getDebitCreditArrayListArrayList() {
        return debitCreditArrayListArrayList;
    }

    public void setDebitCreditArrayListArrayList(List<SummaryResponse.Debit_credit> debitCreditArrayListArrayList) {
        this.debitCreditArrayListArrayList = debitCreditArrayListArrayList;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public static final Creator<SummaryModel> CREATOR = new Creator<SummaryModel>() {
        @Override
        public SummaryModel createFromParcel(Parcel in) {
            return new SummaryModel(in);
        }

        @Override
        public SummaryModel[] newArray(int size) {
            return new SummaryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(site_name);
        dest.writeString(site_address);
        dest.writeString(startDateTime);
        dest.writeString(endDateTime);
        dest.writeString(staff_name);
        dest.writeString(ticket_id);
        dest.writeString(cashTotal);
        dest.writeString(guestRoomTotal);
        dest.writeString(masterAccTotal);
        dest.writeString(transaction_type);
        dest.writeString(pos_info);
        dest.writeString(qr_info);
        dest.writeList(cashArrayList);
        dest.writeList(guestRoomArrayListArrayList);
        dest.writeList(masterAccountArrayListArrayList);
        dest.writeList(debitCreditArrayListArrayList);
        dest.writeString(grandTotal);
        dest.writeValue(bitmap);
    }
}
