package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetValidatedTicketResponse implements Parcelable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<GetValidatedTicketResponse> CREATOR = new Creator<GetValidatedTicketResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GetValidatedTicketResponse createFromParcel(Parcel in) {
            return new GetValidatedTicketResponse(in);
        }

        public GetValidatedTicketResponse[] newArray(int size) {
            return (new GetValidatedTicketResponse[size]);
        }

    }
            ;

    protected GetValidatedTicketResponse(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public GetValidatedTicketResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable
    {

        @SerializedName("siteId")
        @Expose
        private String siteId;
        @SerializedName("ticketData")
        @Expose
        private String ticketData;
        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("plate_no")
        @Expose
        private String plateNo;
        @SerializedName("valet_status_id")
        @Expose
        private Integer valetStatusId;
        @SerializedName("valet_car_key_collected")
        @Expose
        private String valetCarKeyCollected;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("fee")
        @Expose
        private String fee;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("discount_amount")
        @Expose
        private String discountAmount;
        @SerializedName("upfront_payment")
        @Expose
        private String upfrontPayment;
        @SerializedName("gst")
        @Expose
        private String gst;
        @SerializedName("entryTime")
        @Expose
        private String entryTime;
        @SerializedName("entryTimeReadable")
        @Expose
        private String entryTimeReadable;
        @SerializedName("reqTime")
        @Expose
        private String reqTime;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("duration_format_full")
        @Expose
        private String durationFormatFull;
        @SerializedName("gracePeriod")
        @Expose
        private Integer gracePeriod;
        @SerializedName("dateGraceEnd")
        @Expose
        private String dateGraceEnd;
        @SerializedName("grace_exceed")
        @Expose
        private String graceExceed;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("dateCreate")
        @Expose
        private String dateCreate;
        @SerializedName("dateUpdate")
        @Expose
        private String dateUpdate;
        @SerializedName("datePark")
        @Expose
        private String datePark;
        @SerializedName("datePickup")
        @Expose
        private String datePickup;
        @SerializedName("dateCollected")
        @Expose
        private String dateCollected;
        @SerializedName("datePay")
        @Expose
        private String datePay;
        @SerializedName("datePayUnix")
        @Expose
        private String datePayUnix;
        @SerializedName("requestId")
        @Expose
        private String requestId;
        @SerializedName("paymentAuthCode")
        @Expose
        private String paymentAuthCode;
        @SerializedName("receipt")
        @Expose
        private String receipt;
        @SerializedName("flag")
        @Expose
        private Integer flag;
        @SerializedName("valet_status_desc")
        @Expose
        private String valetStatusDesc;
        @SerializedName("valet_park_loc")
        @Expose
        private String valetParkLoc;
        @SerializedName("staff_issuer")
        @Expose
        private String staffIssuer;
        @SerializedName("staff_parker")
        @Expose
        private String staffParker;
        @SerializedName("staff_pickup")
        @Expose
        private String staffPickup;
        @SerializedName("staff_collected")
        @Expose
        private String staffCollected;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.siteId = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketData = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.service = ((String) in.readValue((String.class.getClassLoader())));
            this.plateNo = ((String) in.readValue((String.class.getClassLoader())));
            this.valetStatusId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.valetCarKeyCollected = ((String) in.readValue((String.class.getClassLoader())));
            this.userId = ((String) in.readValue((String.class.getClassLoader())));
            this.mobileNo = ((String) in.readValue((String.class.getClassLoader())));
            this.fee = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((Integer.class.getClassLoader())));
            this.discountAmount = ((String) in.readValue((Integer.class.getClassLoader())));
            this.upfrontPayment = ((String) in.readValue((String.class.getClassLoader())));
            this.gst = ((String) in.readValue((String.class.getClassLoader())));
            this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
            this.entryTimeReadable = ((String) in.readValue((String.class.getClassLoader())));
            this.reqTime = ((String) in.readValue((String.class.getClassLoader())));
            this.duration = ((String) in.readValue((String.class.getClassLoader())));
            this.durationFormatFull = ((String) in.readValue((String.class.getClassLoader())));
            this.gracePeriod = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.dateGraceEnd = ((String) in.readValue((String.class.getClassLoader())));
            this.graceExceed = ((String) in.readValue((String.class.getClassLoader())));
            this.status = ((String) in.readValue((String.class.getClassLoader())));
            this.dateCreate = ((String) in.readValue((String.class.getClassLoader())));
            this.dateUpdate = ((String) in.readValue((String.class.getClassLoader())));
            this.datePark = ((String) in.readValue((String.class.getClassLoader())));
            this.datePickup = ((String) in.readValue((String.class.getClassLoader())));
            this.dateCollected = ((String) in.readValue((String.class.getClassLoader())));
            this.datePay = ((String) in.readValue((String.class.getClassLoader())));
            this.datePayUnix = ((String) in.readValue((String.class.getClassLoader())));
            this.requestId = ((String) in.readValue((String.class.getClassLoader())));
            this.paymentAuthCode = ((String) in.readValue((String.class.getClassLoader())));
            this.receipt = ((String) in.readValue((String.class.getClassLoader())));
            this.flag = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.valetStatusDesc = ((String) in.readValue((String.class.getClassLoader())));
            this.valetParkLoc = ((String) in.readValue((String.class.getClassLoader())));
            this.staffIssuer = ((String) in.readValue((String.class.getClassLoader())));
            this.staffParker = ((String) in.readValue((String.class.getClassLoader())));
            this.staffPickup = ((String) in.readValue((String.class.getClassLoader())));
            this.staffCollected = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public String getSiteId() {
            return siteId;
        }

        public void setSiteId(String siteId) {
            this.siteId = siteId;
        }

        public String getTicketData() {
            return ticketData;
        }

        public void setTicketData(String ticketData) {
            this.ticketData = ticketData;
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public Integer getValetStatusId() {
            return valetStatusId;
        }

        public void setValetStatusId(Integer valetStatusId) {
            this.valetStatusId = valetStatusId;
        }

        public String getValetCarKeyCollected() {
            return valetCarKeyCollected;
        }

        public void setValetCarKeyCollected(String valetCarKeyCollected) {
            this.valetCarKeyCollected = valetCarKeyCollected;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(String discountAmount) {
            this.discountAmount = discountAmount;
        }

        public String getUpfrontPayment() {
            return upfrontPayment;
        }

        public void setUpfrontPayment(String upfrontPayment) {
            this.upfrontPayment = upfrontPayment;
        }

        public String getGst() {
            return gst;
        }

        public void setGst(String gst) {
            this.gst = gst;
        }

        public String getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(String entryTime) {
            this.entryTime = entryTime;
        }

        public String getEntryTimeReadable() {
            return entryTimeReadable;
        }

        public void setEntryTimeReadable(String entryTimeReadable) {
            this.entryTimeReadable = entryTimeReadable;
        }

        public String getReqTime() {
            return reqTime;
        }

        public void setReqTime(String reqTime) {
            this.reqTime = reqTime;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getDurationFormatFull() {
            return durationFormatFull;
        }

        public void setDurationFormatFull(String durationFormatFull) {
            this.durationFormatFull = durationFormatFull;
        }

        public Integer getGracePeriod() {
            return gracePeriod;
        }

        public void setGracePeriod(Integer gracePeriod) {
            this.gracePeriod = gracePeriod;
        }

        public String getDateGraceEnd() {
            return dateGraceEnd;
        }

        public void setDateGraceEnd(String dateGraceEnd) {
            this.dateGraceEnd = dateGraceEnd;
        }

        public String getGraceExceed() {
            return graceExceed;
        }

        public void setGraceExceed(String graceExceed) {
            this.graceExceed = graceExceed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDateCreate() {
            return dateCreate;
        }

        public void setDateCreate(String dateCreate) {
            this.dateCreate = dateCreate;
        }

        public String getDateUpdate() {
            return dateUpdate;
        }

        public void setDateUpdate(String dateUpdate) {
            this.dateUpdate = dateUpdate;
        }

        public String getDatePark() {
            return datePark;
        }

        public void setDatePark(String datePark) {
            this.datePark = datePark;
        }

        public String getDatePickup() {
            return datePickup;
        }

        public void setDatePickup(String datePickup) {
            this.datePickup = datePickup;
        }

        public String getDateCollected() {
            return dateCollected;
        }

        public void setDateCollected(String dateCollected) {
            this.dateCollected = dateCollected;
        }

        public String getDatePay() {
            return datePay;
        }

        public void setDatePay(String datePay) {
            this.datePay = datePay;
        }

        public String getDatePayUnix() {
            return datePayUnix;
        }

        public void setDatePayUnix(String datePayUnix) {
            this.datePayUnix = datePayUnix;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getPaymentAuthCode() {
            return paymentAuthCode;
        }

        public void setPaymentAuthCode(String paymentAuthCode) {
            this.paymentAuthCode = paymentAuthCode;
        }

        public String getReceipt() {
            return receipt;
        }

        public void setReceipt(String receipt) {
            this.receipt = receipt;
        }

        public Integer getFlag() {
            return flag;
        }

        public void setFlag(Integer flag) {
            this.flag = flag;
        }

        public String getValetStatusDesc() {
            return valetStatusDesc;
        }

        public void setValetStatusDesc(String valetStatusDesc) {
            this.valetStatusDesc = valetStatusDesc;
        }

        public String getValetParkLoc() {
            return valetParkLoc;
        }

        public void setValetParkLoc(String valetParkLoc) {
            this.valetParkLoc = valetParkLoc;
        }

        public String getStaffIssuer() {
            return staffIssuer;
        }

        public void setStaffIssuer(String staffIssuer) {
            this.staffIssuer = staffIssuer;
        }

        public String getStaffParker() {
            return staffParker;
        }

        public void setStaffParker(String staffParker) {
            this.staffParker = staffParker;
        }

        public String getStaffPickup() {
            return staffPickup;
        }

        public void setStaffPickup(String staffPickup) {
            this.staffPickup = staffPickup;
        }

        public String getStaffCollected() {
            return staffCollected;
        }

        public void setStaffCollected(String staffCollected) {
            this.staffCollected = staffCollected;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(siteId);
            dest.writeValue(ticketData);
            dest.writeValue(ticketId);
            dest.writeValue(service);
            dest.writeValue(plateNo);
            dest.writeValue(valetStatusId);
            dest.writeValue(valetCarKeyCollected);
            dest.writeValue(userId);
            dest.writeValue(mobileNo);
            dest.writeValue(fee);
            dest.writeValue(amount);
            dest.writeValue(discountAmount);
            dest.writeValue(upfrontPayment);
            dest.writeValue(gst);
            dest.writeValue(entryTime);
            dest.writeValue(entryTimeReadable);
            dest.writeValue(reqTime);
            dest.writeValue(duration);
            dest.writeValue(durationFormatFull);
            dest.writeValue(gracePeriod);
            dest.writeValue(dateGraceEnd);
            dest.writeValue(graceExceed);
            dest.writeValue(status);
            dest.writeValue(dateCreate);
            dest.writeValue(dateUpdate);
            dest.writeValue(datePark);
            dest.writeValue(datePickup);
            dest.writeValue(dateCollected);
            dest.writeValue(datePay);
            dest.writeValue(datePayUnix);
            dest.writeValue(requestId);
            dest.writeValue(paymentAuthCode);
            dest.writeValue(receipt);
            dest.writeValue(flag);
            dest.writeValue(valetStatusDesc);
            dest.writeValue(valetParkLoc);
            dest.writeValue(staffIssuer);
            dest.writeValue(staffParker);
            dest.writeValue(staffPickup);
            dest.writeValue(staffCollected);
        }

        public int describeContents() {
            return 0;
        }

    }

}