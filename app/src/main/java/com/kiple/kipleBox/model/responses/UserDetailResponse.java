package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserDetailResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<UserDetailResponse> CREATOR = new Creator<UserDetailResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UserDetailResponse createFromParcel(Parcel in) {
            return new UserDetailResponse(in);
        }

        public UserDetailResponse[] newArray(int size) {
            return (new UserDetailResponse[size]);
        }

    }
            ;

    protected UserDetailResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public UserDetailResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("site_id")
        @Expose
        private String site_id;
        @SerializedName("activated")
        @Expose
        private Boolean activated;
        @SerializedName("created_at")
        @Expose
        private String created_at;
        @SerializedName("updated_at")
        @Expose
        private String updated_at;
        @SerializedName("auth_history")
        @Expose
        private Auth_history auth_history;
        @SerializedName("access_level")
        @Expose
        private List<String> access_level = new ArrayList<>();
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.email = ((String) in.readValue((String.class.getClassLoader())));
            this.avatar = ((String) in.readValue((String.class.getClassLoader())));
            this.site_id = ((String) in.readValue((String.class.getClassLoader())));
            this.activated = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            this.created_at = ((String) in.readValue((String.class.getClassLoader())));
            this.updated_at = ((String) in.readValue((String.class.getClassLoader())));
            this.auth_history = ((Auth_history) in.readValue((Auth_history.class.getClassLoader())));
            in.readList(this.access_level, (java.lang.String.class.getClassLoader()));
        }

        public Data() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getSite_id() {
            return site_id;
        }

        public void setSite_id(String site_id) {
            this.site_id = site_id;
        }

        public Boolean getActivated() {
            return activated;
        }

        public void setActivated(Boolean activated) {
            this.activated = activated;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Auth_history getAuth_history() {
            return auth_history;
        }

        public void setAuth_history(Auth_history auth_history) {
            this.auth_history = auth_history;
        }

        public List<String> getAccess_level() {
            return access_level;
        }

        public void setAccess_level(List<String> access_level) {
            this.access_level = access_level;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(name);
            dest.writeValue(email);
            dest.writeValue(avatar);
            dest.writeValue(site_id);
            dest.writeValue(activated);
            dest.writeValue(created_at);
            dest.writeValue(updated_at);
            dest.writeValue(auth_history);
            dest.writeList(access_level);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Auth_history implements Parcelable
    {

        @SerializedName("login_at")
        @Expose
        private String login_at;
        @SerializedName("logout_at")
        @Expose
        private Object logout_at;
        public final static Parcelable.Creator<Auth_history> CREATOR = new Creator<Auth_history>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Auth_history createFromParcel(Parcel in) {
                return new Auth_history(in);
            }

            public Auth_history[] newArray(int size) {
                return (new Auth_history[size]);
            }

        }
                ;

        protected Auth_history(Parcel in) {
            this.login_at = ((String) in.readValue((String.class.getClassLoader())));
            this.logout_at = ((Object) in.readValue((Object.class.getClassLoader())));
        }

        public Auth_history() {
        }

        public String getLogin_at() {
            return login_at;
        }

        public void setLogin_at(String login_at) {
            this.login_at = login_at;
        }

        public Object getLogout_at() {
            return logout_at;
        }

        public void setLogout_at(Object logout_at) {
            this.logout_at = logout_at;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(login_at);
            dest.writeValue(logout_at);
        }

        public int describeContents() {
            return 0;
        }

    }

}