package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashPaymentResponse implements Parcelable
{

    @SerializedName("kpCode")
    @Expose
    private String kpCode;
    @SerializedName("kipleCodeDesc")
    @Expose
    private String kipleCodeDesc;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("receipt")
    @Expose
    private String receipt;
    @SerializedName("entrytime")
    @Expose
    private String entrytime;
    @SerializedName("paytime")
    @Expose
    private String paytime;
    @SerializedName("duration_format_full")
    @Expose
    private String duration_format_full;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("new_amount")
    @Expose
    private String new_amount;
    public final static Parcelable.Creator<CashPaymentResponse> CREATOR = new Creator<CashPaymentResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CashPaymentResponse createFromParcel(Parcel in) {
            return new CashPaymentResponse(in);
        }

        public CashPaymentResponse[] newArray(int size) {
            return (new CashPaymentResponse[size]);
        }

    }
            ;

    protected CashPaymentResponse(Parcel in) {
        this.kpCode = ((String) in.readValue((String.class.getClassLoader())));
        this.kipleCodeDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.receipt = ((String) in.readValue((String.class.getClassLoader())));
        this.entrytime = ((String) in.readValue((String.class.getClassLoader())));
        this.paytime = ((String) in.readValue((String.class.getClassLoader())));
        this.duration_format_full = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.new_amount = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CashPaymentResponse() {
    }

    public String getKpCode() {
        return kpCode;
    }

    public void setKpCode(String kpCode) {
        this.kpCode = kpCode;
    }

    public String getKipleCodeDesc() {
        return kipleCodeDesc;
    }

    public void setKipleCodeDesc(String kipleCodeDesc) {
        this.kipleCodeDesc = kipleCodeDesc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getEntrytime() {
        return entrytime;
    }

    public void setEntrytime(String entrytime) {
        this.entrytime = entrytime;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getDuration_format_full() {
        return duration_format_full;
    }

    public void setDuration_format_full(String duration_format_full) {
        this.duration_format_full = duration_format_full;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNew_amount() {
        return new_amount;
    }

    public void setNew_amount(String new_amount) {
        this.new_amount = new_amount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(kpCode);
        dest.writeValue(kipleCodeDesc);
        dest.writeValue(message);
        dest.writeValue(receipt);
        dest.writeValue(entrytime);
        dest.writeValue(paytime);
        dest.writeValue(duration_format_full);
        dest.writeValue(duration);
        dest.writeValue(new_amount);
    }

    public int describeContents() {
        return 0;
    }

}