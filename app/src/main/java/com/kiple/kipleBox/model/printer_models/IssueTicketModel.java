package com.kiple.kipleBox.model.printer_models;

import android.os.Parcel;
import android.os.Parcelable;

public class IssueTicketModel implements Parcelable {

    private String site_name;
    private String site_address;
    private String ticket_data;
    private String number_plate;
    private String ticket_info;
    private String running_id;
    private String date_issued;
    private String time_issued;
    private boolean upfront_flag;
    private String upfront_amount;
    private String staff_issuer;
    private int qr_size;
    private boolean custom_qr_size;


    public final static Parcelable.Creator<IssueTicketModel> CREATOR = new Creator<IssueTicketModel>() {
        @SuppressWarnings({
                "unchecked"
        })
        public IssueTicketModel createFromParcel(Parcel source) {
            return new IssueTicketModel(source);
        }

        public IssueTicketModel[] newArray(int size) {
            return new IssueTicketModel[size];
        }
    };

    protected IssueTicketModel(Parcel in) {
        this.site_name = ((String) in.readValue((String.class.getClassLoader())));
        this.site_address = ((String) in.readValue((String.class.getClassLoader())));
        this.ticket_data = ((String) in.readValue((String.class.getClassLoader())));
        this.number_plate = ((String) in.readValue((String.class.getClassLoader())));
        this.ticket_info = ((String) in.readValue((String.class.getClassLoader())));
        this.running_id = ((String) in.readValue((String.class.getClassLoader())));
        this.date_issued = ((String) in.readValue((String.class.getClassLoader())));
        this.time_issued = ((String) in.readValue((String.class.getClassLoader())));
        this.upfront_flag = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.upfront_amount = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_issuer = ((String) in.readValue((String.class.getClassLoader())));
        this.qr_size = ((int) in.readValue((String.class.getClassLoader())));
        this.custom_qr_size = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public IssueTicketModel() {
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getSite_address() {
        return site_address;
    }

    public void setSite_address(String site_address) {
        this.site_address = site_address;
    }

    public String getTicket_data() {
        return ticket_data;
    }

    public void setTicket_data(String ticket_data) {
        this.ticket_data = ticket_data;
    }

    public String getNumber_plate() {
        return number_plate;
    }

    public void setNumber_plate(String number_plate) {
        this.number_plate = number_plate;
    }

    public String getTicket_info() {
        return ticket_info;
    }

    public void setTicket_info(String ticket_info) {
        this.ticket_info = ticket_info;
    }

    public String getRunning_id() {
        return running_id;
    }

    public void setRunning_id(String running_id) {
        this.running_id = running_id;
    }

    public String getDate_issued() {
        return date_issued;
    }

    public void setDate_issued(String date_issued) {
        this.date_issued = date_issued;
    }

    public String getTime_issued() {
        return time_issued;
    }

    public void setTime_issued(String time_issued) {
        this.time_issued = time_issued;
    }

    public boolean isUpfront_flag() {
        return upfront_flag;
    }

    public void setUpfront_flag(boolean upfront_flag) {
        this.upfront_flag = upfront_flag;
    }

    public String getUpfront_amount() {
        return upfront_amount;
    }

    public void setUpfront_amount(String upfront_amount) {
        this.upfront_amount = upfront_amount;
    }

    public String getStaff_issuer() {
        return staff_issuer;
    }

    public void setStaff_issuer(String staff_issuer) {
        this.staff_issuer = staff_issuer;
    }

    public int getQr_size() {
        return qr_size;
    }

    public void setQr_size(int qr_size) {
        this.qr_size = qr_size;
    }

    public boolean isCustom_qr_size() {
        return custom_qr_size;
    }

    public void setCustom_qr_size(boolean custom_qr_size) {
        this.custom_qr_size = custom_qr_size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(site_name);
        dest.writeValue(site_address);
        dest.writeValue(ticket_data);
        dest.writeValue(ticket_info);
        dest.writeValue(number_plate);
        dest.writeValue(running_id);
        dest.writeValue(date_issued);
        dest.writeValue(time_issued);
        dest.writeValue(upfront_flag);
        dest.writeValue(upfront_amount);
        dest.writeValue(staff_issuer);
        dest.writeValue(qr_size);
        dest.writeValue(custom_qr_size);
    }
}
