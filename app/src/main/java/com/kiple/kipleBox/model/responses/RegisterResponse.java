package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<RegisterResponse> CREATOR = new Creator<RegisterResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RegisterResponse createFromParcel(Parcel in) {
            return new RegisterResponse(in);
        }

        public RegisterResponse[] newArray(int size) {
            return (new RegisterResponse[size]);
        }

    }
            ;

    protected RegisterResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public RegisterResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable
    {

        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("user")
        @Expose
        private User user;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.token = ((String) in.readValue((String.class.getClassLoader())));
            this.user = ((User) in.readValue((User.class.getClassLoader())));
        }

        public Data() {
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(token);
            dest.writeValue(user);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class User implements Parcelable
    {

        @SerializedName("activated ")
        @Expose
        private Boolean activated_;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("user_id")
        @Expose
        private Integer user_id;
        public final static Parcelable.Creator<User> CREATOR = new Creator<User>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            public User[] newArray(int size) {
                return (new User[size]);
            }

        }
                ;

        protected User(Parcel in) {
            this.activated_ = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            this.username = ((String) in.readValue((String.class.getClassLoader())));
            this.user_id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        }

        public User() {
        }

        public Boolean getActivated_() {
            return activated_;
        }

        public void setActivated_(Boolean activated_) {
            this.activated_ = activated_;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(activated_);
            dest.writeValue(username);
            dest.writeValue(user_id);
        }

        public int describeContents() {
            return 0;
        }

    }

}