package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApplyPackageResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<ApplyPackageResponse> CREATOR = new Creator<ApplyPackageResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ApplyPackageResponse createFromParcel(Parcel in) {
            return new ApplyPackageResponse(in);
        }

        public ApplyPackageResponse[] newArray(int size) {
            return (new ApplyPackageResponse[size]);
        }

    }
            ;

    protected ApplyPackageResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public ApplyPackageResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("vendorId")
        @Expose
        private String vendorId;
        @SerializedName("siteId")
        @Expose
        private String siteId;
        @SerializedName("ticketData")
        @Expose
        private String ticketData;
        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("plate_no")
        @Expose
        private String plate_no;
        @SerializedName("mobile_no")
        @Expose
        private String mobile_no;
        @SerializedName("valet_status_id")
        @Expose
        private Integer valet_status_id;
        @SerializedName("valet_car_key_collected")
        @Expose
        private String valet_car_key_collected;
        @SerializedName("valet_park_loc")
        @Expose
        private String valet_park_loc;
        @SerializedName("userId")
        @Expose
        private Object userId;
        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("pos_issuer")
        @Expose
        private String pos_issuer;
        @SerializedName("pos_parker")
        @Expose
        private String pos_parker;
        @SerializedName("pos_payer")
        @Expose
        private String pos_payer;
        @SerializedName("pos_pickup")
        @Expose
        private String pos_pickup;
        @SerializedName("pos_collected")
        @Expose
        private String pos_collected;
        @SerializedName("staff_issuer")
        @Expose
        private String staff_issuer;
        @SerializedName("staff_parker")
        @Expose
        private String staff_parker;
        @SerializedName("staff_payer_id")
        @Expose
        private String staff_payer_id;
        @SerializedName("staff_payer")
        @Expose
        private String staff_payer;
        @SerializedName("staff_pickup")
        @Expose
        private String staff_pickup;
        @SerializedName("staff_collected")
        @Expose
        private String staff_collected;
        @SerializedName("rate_package_id")
        @Expose
        private String rate_package_id;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("cash_received_from_user")
        @Expose
        private String cash_received_from_user;
        @SerializedName("gst")
        @Expose
        private Object gst;
        @SerializedName("entryTime")
        @Expose
        private String entryTime;
        @SerializedName("reqTime")
        @Expose
        private String reqTime;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("gracePeriod")
        @Expose
        private Integer gracePeriod;
        @SerializedName("dateGraceEnd")
        @Expose
        private String dateGraceEnd;
        @SerializedName("grace_exceed")
        @Expose
        private Integer grace_exceed;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("payment_pos_method")
        @Expose
        private Integer payment_pos_method;
        @SerializedName("payment_remarks")
        @Expose
        private Object payment_remarks;
        @SerializedName("img1")
        @Expose
        private Object img1;
        @SerializedName("img2")
        @Expose
        private Object img2;
        @SerializedName("img3")
        @Expose
        private Object img3;
        @SerializedName("img4")
        @Expose
        private Object img4;
        @SerializedName("img5")
        @Expose
        private Object img5;
        @SerializedName("img6")
        @Expose
        private Object img6;
        @SerializedName("dateCreate")
        @Expose
        private String dateCreate;
        @SerializedName("dateUpdate")
        @Expose
        private Object dateUpdate;
        @SerializedName("datePark")
        @Expose
        private String datePark;
        @SerializedName("datePickup")
        @Expose
        private String datePickup;
        @SerializedName("dateCollect")
        @Expose
        private String dateCollect;
        @SerializedName("datePay")
        @Expose
        private String datePay;
        @SerializedName("datePayUnix")
        @Expose
        private String datePayUnix;
        @SerializedName("requestId")
        @Expose
        private Object requestId;
        @SerializedName("paymentAuthCode")
        @Expose
        private Object paymentAuthCode;
        @SerializedName("receipt")
        @Expose
        private String receipt;
        @SerializedName("fcmToken")
        @Expose
        private Object fcmToken;
        @SerializedName("remarks")
        @Expose
        private String remarks;
        @SerializedName("flag")
        @Expose
        private Integer flag;
        @SerializedName("staff_issuer_id")
        @Expose
        private Object staff_issuer_id;
        @SerializedName("staff_parker_id")
        @Expose
        private Object staff_parker_id;
        @SerializedName("staff_pickup_id")
        @Expose
        private String staff_pickup_id;
        @SerializedName("staff_collected_id")
        @Expose
        private String staff_collected_id;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.vendorId = ((String) in.readValue((String.class.getClassLoader())));
            this.siteId = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketData = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.plate_no = ((String) in.readValue((String.class.getClassLoader())));
            this.mobile_no = ((String) in.readValue((String.class.getClassLoader())));
            this.valet_status_id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.valet_car_key_collected = ((String) in.readValue((String.class.getClassLoader())));
            this.valet_park_loc = ((String) in.readValue((String.class.getClassLoader())));
            this.userId = ((Object) in.readValue((Object.class.getClassLoader())));
            this.service = ((String) in.readValue((String.class.getClassLoader())));
            this.pos_issuer = ((String) in.readValue((String.class.getClassLoader())));
            this.pos_parker = ((String) in.readValue((String.class.getClassLoader())));
            this.pos_payer = ((String) in.readValue((String.class.getClassLoader())));
            this.pos_pickup = ((String) in.readValue((String.class.getClassLoader())));
            this.pos_collected = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_issuer = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_parker = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_payer_id = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_payer = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_pickup = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_collected = ((String) in.readValue((String.class.getClassLoader())));
            this.rate_package_id = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.cash_received_from_user = ((String) in.readValue((String.class.getClassLoader())));
            this.gst = ((Object) in.readValue((Object.class.getClassLoader())));
            this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
            this.reqTime = ((String) in.readValue((String.class.getClassLoader())));
            this.duration = ((String) in.readValue((String.class.getClassLoader())));
            this.gracePeriod = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.dateGraceEnd = ((String) in.readValue((String.class.getClassLoader())));
            this.grace_exceed = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.status = ((String) in.readValue((String.class.getClassLoader())));
            this.payment_pos_method = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.payment_remarks = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img1 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img2 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img3 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img4 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img5 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.img6 = ((Object) in.readValue((Object.class.getClassLoader())));
            this.dateCreate = ((String) in.readValue((String.class.getClassLoader())));
            this.dateUpdate = ((Object) in.readValue((Object.class.getClassLoader())));
            this.datePark = ((String) in.readValue((String.class.getClassLoader())));
            this.datePickup = ((String) in.readValue((String.class.getClassLoader())));
            this.dateCollect = ((String) in.readValue((String.class.getClassLoader())));
            this.datePay = ((String) in.readValue((String.class.getClassLoader())));
            this.datePayUnix = ((String) in.readValue((String.class.getClassLoader())));
            this.requestId = ((Object) in.readValue((Object.class.getClassLoader())));
            this.paymentAuthCode = ((Object) in.readValue((Object.class.getClassLoader())));
            this.receipt = ((String) in.readValue((String.class.getClassLoader())));
            this.fcmToken = ((Object) in.readValue((Object.class.getClassLoader())));
            this.remarks = ((String) in.readValue((String.class.getClassLoader())));
            this.flag = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.staff_issuer_id = ((Object) in.readValue((Object.class.getClassLoader())));
            this.staff_parker_id = ((Object) in.readValue((Object.class.getClassLoader())));
            this.staff_pickup_id = ((String) in.readValue((String.class.getClassLoader())));
            this.staff_collected_id = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getVendorId() {
            return vendorId;
        }

        public void setVendorId(String vendorId) {
            this.vendorId = vendorId;
        }

        public String getSiteId() {
            return siteId;
        }

        public void setSiteId(String siteId) {
            this.siteId = siteId;
        }

        public String getTicketData() {
            return ticketData;
        }

        public void setTicketData(String ticketData) {
            this.ticketData = ticketData;
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getPlate_no() {
            return plate_no;
        }

        public void setPlate_no(String plate_no) {
            this.plate_no = plate_no;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public Integer getValet_status_id() {
            return valet_status_id;
        }

        public void setValet_status_id(Integer valet_status_id) {
            this.valet_status_id = valet_status_id;
        }

        public String getValet_car_key_collected() {
            return valet_car_key_collected;
        }

        public void setValet_car_key_collected(String valet_car_key_collected) {
            this.valet_car_key_collected = valet_car_key_collected;
        }

        public String getValet_park_loc() {
            return valet_park_loc;
        }

        public void setValet_park_loc(String valet_park_loc) {
            this.valet_park_loc = valet_park_loc;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getPos_issuer() {
            return pos_issuer;
        }

        public void setPos_issuer(String pos_issuer) {
            this.pos_issuer = pos_issuer;
        }

        public String getPos_parker() {
            return pos_parker;
        }

        public void setPos_parker(String pos_parker) {
            this.pos_parker = pos_parker;
        }

        public String getPos_payer() {
            return pos_payer;
        }

        public void setPos_payer(String pos_payer) {
            this.pos_payer = pos_payer;
        }

        public String getPos_pickup() {
            return pos_pickup;
        }

        public void setPos_pickup(String pos_pickup) {
            this.pos_pickup = pos_pickup;
        }

        public String getPos_collected() {
            return pos_collected;
        }

        public void setPos_collected(String pos_collected) {
            this.pos_collected = pos_collected;
        }

        public String getStaff_issuer() {
            return staff_issuer;
        }

        public void setStaff_issuer(String staff_issuer) {
            this.staff_issuer = staff_issuer;
        }

        public String getStaff_parker() {
            return staff_parker;
        }

        public void setStaff_parker(String staff_parker) {
            this.staff_parker = staff_parker;
        }

        public String getStaff_payer_id() {
            return staff_payer_id;
        }

        public void setStaff_payer_id(String staff_payer_id) {
            this.staff_payer_id = staff_payer_id;
        }

        public String getStaff_payer() {
            return staff_payer;
        }

        public void setStaff_payer(String staff_payer) {
            this.staff_payer = staff_payer;
        }

        public String getStaff_pickup() {
            return staff_pickup;
        }

        public void setStaff_pickup(String staff_pickup) {
            this.staff_pickup = staff_pickup;
        }

        public String getStaff_collected() {
            return staff_collected;
        }

        public void setStaff_collected(String staff_collected) {
            this.staff_collected = staff_collected;
        }

        public String getRate_package_id() {
            return rate_package_id;
        }

        public void setRate_package_id(String rate_package_id) {
            this.rate_package_id = rate_package_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCash_received_from_user() {
            return cash_received_from_user;
        }

        public void setCash_received_from_user(String cash_received_from_user) {
            this.cash_received_from_user = cash_received_from_user;
        }

        public Object getGst() {
            return gst;
        }

        public void setGst(Object gst) {
            this.gst = gst;
        }

        public String getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(String entryTime) {
            this.entryTime = entryTime;
        }

        public String getReqTime() {
            return reqTime;
        }

        public void setReqTime(String reqTime) {
            this.reqTime = reqTime;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public Integer getGracePeriod() {
            return gracePeriod;
        }

        public void setGracePeriod(Integer gracePeriod) {
            this.gracePeriod = gracePeriod;
        }

        public String getDateGraceEnd() {
            return dateGraceEnd;
        }

        public void setDateGraceEnd(String dateGraceEnd) {
            this.dateGraceEnd = dateGraceEnd;
        }

        public Integer getGrace_exceed() {
            return grace_exceed;
        }

        public void setGrace_exceed(Integer grace_exceed) {
            this.grace_exceed = grace_exceed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getPayment_pos_method() {
            return payment_pos_method;
        }

        public void setPayment_pos_method(Integer payment_pos_method) {
            this.payment_pos_method = payment_pos_method;
        }

        public Object getPayment_remarks() {
            return payment_remarks;
        }

        public void setPayment_remarks(Object payment_remarks) {
            this.payment_remarks = payment_remarks;
        }

        public Object getImg1() {
            return img1;
        }

        public void setImg1(Object img1) {
            this.img1 = img1;
        }

        public Object getImg2() {
            return img2;
        }

        public void setImg2(Object img2) {
            this.img2 = img2;
        }

        public Object getImg3() {
            return img3;
        }

        public void setImg3(Object img3) {
            this.img3 = img3;
        }

        public Object getImg4() {
            return img4;
        }

        public void setImg4(Object img4) {
            this.img4 = img4;
        }

        public Object getImg5() {
            return img5;
        }

        public void setImg5(Object img5) {
            this.img5 = img5;
        }

        public Object getImg6() {
            return img6;
        }

        public void setImg6(Object img6) {
            this.img6 = img6;
        }

        public String getDateCreate() {
            return dateCreate;
        }

        public void setDateCreate(String dateCreate) {
            this.dateCreate = dateCreate;
        }

        public Object getDateUpdate() {
            return dateUpdate;
        }

        public void setDateUpdate(Object dateUpdate) {
            this.dateUpdate = dateUpdate;
        }

        public String getDatePark() {
            return datePark;
        }

        public void setDatePark(String datePark) {
            this.datePark = datePark;
        }

        public String getDatePickup() {
            return datePickup;
        }

        public void setDatePickup(String datePickup) {
            this.datePickup = datePickup;
        }

        public String getDateCollect() {
            return dateCollect;
        }

        public void setDateCollect(String dateCollect) {
            this.dateCollect = dateCollect;
        }

        public String getDatePay() {
            return datePay;
        }

        public void setDatePay(String datePay) {
            this.datePay = datePay;
        }

        public String getDatePayUnix() {
            return datePayUnix;
        }

        public void setDatePayUnix(String datePayUnix) {
            this.datePayUnix = datePayUnix;
        }

        public Object getRequestId() {
            return requestId;
        }

        public void setRequestId(Object requestId) {
            this.requestId = requestId;
        }

        public Object getPaymentAuthCode() {
            return paymentAuthCode;
        }

        public void setPaymentAuthCode(Object paymentAuthCode) {
            this.paymentAuthCode = paymentAuthCode;
        }

        public String getReceipt() {
            return receipt;
        }

        public void setReceipt(String receipt) {
            this.receipt = receipt;
        }

        public Object getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(Object fcmToken) {
            this.fcmToken = fcmToken;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public Integer getFlag() {
            return flag;
        }

        public void setFlag(Integer flag) {
            this.flag = flag;
        }

        public Object getStaff_issuer_id() {
            return staff_issuer_id;
        }

        public void setStaff_issuer_id(Object staff_issuer_id) {
            this.staff_issuer_id = staff_issuer_id;
        }

        public Object getStaff_parker_id() {
            return staff_parker_id;
        }

        public void setStaff_parker_id(Object staff_parker_id) {
            this.staff_parker_id = staff_parker_id;
        }

        public String getStaff_pickup_id() {
            return staff_pickup_id;
        }

        public void setStaff_pickup_id(String staff_pickup_id) {
            this.staff_pickup_id = staff_pickup_id;
        }

        public String getStaff_collected_id() {
            return staff_collected_id;
        }

        public void setStaff_collected_id(String staff_collected_id) {
            this.staff_collected_id = staff_collected_id;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(vendorId);
            dest.writeValue(siteId);
            dest.writeValue(ticketData);
            dest.writeValue(ticketId);
            dest.writeValue(plate_no);
            dest.writeValue(mobile_no);
            dest.writeValue(valet_status_id);
            dest.writeValue(valet_car_key_collected);
            dest.writeValue(valet_park_loc);
            dest.writeValue(userId);
            dest.writeValue(service);
            dest.writeValue(pos_issuer);
            dest.writeValue(pos_parker);
            dest.writeValue(pos_payer);
            dest.writeValue(pos_pickup);
            dest.writeValue(pos_collected);
            dest.writeValue(staff_issuer);
            dest.writeValue(staff_parker);
            dest.writeValue(staff_payer_id);
            dest.writeValue(staff_payer);
            dest.writeValue(staff_pickup);
            dest.writeValue(staff_collected);
            dest.writeValue(rate_package_id);
            dest.writeValue(amount);
            dest.writeValue(cash_received_from_user);
            dest.writeValue(gst);
            dest.writeValue(entryTime);
            dest.writeValue(reqTime);
            dest.writeValue(duration);
            dest.writeValue(gracePeriod);
            dest.writeValue(dateGraceEnd);
            dest.writeValue(grace_exceed);
            dest.writeValue(status);
            dest.writeValue(payment_pos_method);
            dest.writeValue(payment_remarks);
            dest.writeValue(img1);
            dest.writeValue(img2);
            dest.writeValue(img3);
            dest.writeValue(img4);
            dest.writeValue(img5);
            dest.writeValue(img6);
            dest.writeValue(dateCreate);
            dest.writeValue(dateUpdate);
            dest.writeValue(datePark);
            dest.writeValue(datePickup);
            dest.writeValue(dateCollect);
            dest.writeValue(datePay);
            dest.writeValue(datePayUnix);
            dest.writeValue(requestId);
            dest.writeValue(paymentAuthCode);
            dest.writeValue(receipt);
            dest.writeValue(fcmToken);
            dest.writeValue(remarks);
            dest.writeValue(flag);
            dest.writeValue(staff_issuer_id);
            dest.writeValue(staff_parker_id);
            dest.writeValue(staff_pickup_id);
            dest.writeValue(staff_collected_id);
        }

        public int describeContents() {
            return 0;
        }

    }

}