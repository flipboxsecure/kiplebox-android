package com.kiple.kipleBox.model.histroy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SummaryResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<SummaryResponse> CREATOR = new Creator<SummaryResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SummaryResponse createFromParcel(Parcel in) {
            return new SummaryResponse(in);
        }

        public SummaryResponse[] newArray(int size) {
            return (new SummaryResponse[size]);
        }

    }
            ;

    protected SummaryResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public SummaryResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Transactions implements Parcelable
    {

        @SerializedName("guest_room")
        @Expose
        private List<Guest_room> guest_room = new ArrayList<>();
        @SerializedName("master_account")
        @Expose
        private List<Master_account> master_account = new ArrayList<>();
        @SerializedName("cash")
        @Expose
        private List<Cash> cash = new ArrayList<>();
        @SerializedName("debit_credit")
        @Expose
        private List<Debit_credit> debit_credit = new ArrayList<>();
        public final static Parcelable.Creator<Transactions> CREATOR = new Creator<Transactions>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Transactions createFromParcel(Parcel in) {
                return new Transactions(in);
            }

            public Transactions[] newArray(int size) {
                return (new Transactions[size]);
            }

        }
                ;

        protected Transactions(Parcel in) {
            in.readList(this.guest_room, (Guest_room.class.getClassLoader()));
            in.readList(this.master_account, (Master_account.class.getClassLoader()));
            in.readList(this.cash, (Cash.class.getClassLoader()));
            in.readList(this.debit_credit, (Debit_credit.class.getClassLoader()));
        }

        public Transactions() {
        }

        public List<Guest_room> getGuest_room() {
            return guest_room;
        }

        public void setGuest_room(List<Guest_room> guest_room) {
            this.guest_room = guest_room;
        }

        public List<Master_account> getMaster_account() {
            return master_account;
        }

        public void setMaster_account(List<Master_account> master_account) {
            this.master_account = master_account;
        }

        public List<Debit_credit> getDebit_credit() {
            return debit_credit;
        }

        public void setDebit_credit(List<Debit_credit> debit_credit) {
            this.debit_credit = debit_credit;
        }

        public List<Cash> getCash() {
            return cash;
        }

        public void setCash(List<Cash> cash) {
            this.cash = cash;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(guest_room);
            dest.writeList(master_account);
            dest.writeList(cash);
            dest.writeList(debit_credit);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Site_information implements Parcelable
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("street1")
        @Expose
        private String street1;
        @SerializedName("street2")
        @Expose
        private String street2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        public final static Parcelable.Creator<Site_information> CREATOR = new Creator<Site_information>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Site_information createFromParcel(Parcel in) {
                return new Site_information(in);
            }

            public Site_information[] newArray(int size) {
                return (new Site_information[size]);
            }

        }
                ;

        protected Site_information(Parcel in) {
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.street1 = ((String) in.readValue((String.class.getClassLoader())));
            this.street2 = ((String) in.readValue((String.class.getClassLoader())));
            this.city = ((String) in.readValue((String.class.getClassLoader())));
            this.state = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Site_information() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStreet1() {
            return street1;
        }

        public void setStreet1(String street1) {
            this.street1 = street1;
        }

        public String getStreet2() {
            return street2;
        }

        public void setStreet2(String street2) {
            this.street2 = street2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(name);
            dest.writeValue(street1);
            dest.writeValue(street2);
            dest.writeValue(city);
            dest.writeValue(state);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Master_account implements Parcelable
    {

        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("type")
        @Expose
        private String type;
        public final static Parcelable.Creator<Master_account> CREATOR = new Creator<Master_account>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Master_account createFromParcel(Parcel in) {
                return new Master_account(in);
            }

            public Master_account[] newArray(int size) {
                return (new Master_account[size]);
            }

        }
                ;

        protected Master_account(Parcel in) {
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Master_account() {
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(ticketId);
            dest.writeValue(amount);
            dest.writeValue(type);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Guest_room implements Parcelable
    {

        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("type")
        @Expose
        private String type;
        public final static Parcelable.Creator<Guest_room> CREATOR = new Creator<Guest_room>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Guest_room createFromParcel(Parcel in) {
                return new Guest_room(in);
            }

            public Guest_room[] newArray(int size) {
                return (new Guest_room[size]);
            }

        }
                ;

        protected Guest_room(Parcel in) {
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Guest_room() {
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(ticketId);
            dest.writeValue(amount);
            dest.writeValue(type);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Data implements Parcelable
    {
        @SerializedName("auth_history")
        @Expose
        private String auth_history;
        @SerializedName("site_information")
        @Expose
        private Site_information site_information;
        @SerializedName("transactions")
        @Expose
        private Transactions transactions;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        };

        protected Data(Parcel in) {
            this.auth_history = ((String) in.readValue((String.class.getClassLoader())));
            this.site_information = ((Site_information) in.readValue((Site_information.class.getClassLoader())));
            this.transactions = ((Transactions) in.readValue((Transactions.class.getClassLoader())));
        }

        public Data() {
        }

        public String getAuth_history() {
            return auth_history;
        }

        public void setAuth_history(String auth_history) {
            this.auth_history = auth_history;
        }

        public Site_information getSite_information() {
            return site_information;
        }

        public void setSite_information(Site_information site_information) {
            this.site_information = site_information;
        }

        public Transactions getTransactions() {
            return transactions;
        }

        public void setTransactions(Transactions transactions) {
            this.transactions = transactions;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(auth_history);
            dest.writeValue(site_information);
            dest.writeValue(transactions);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Cash implements Parcelable
    {

        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("type")
        @Expose
        private String type;
        public final static Parcelable.Creator<Cash> CREATOR = new Creator<Cash>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Cash createFromParcel(Parcel in) {
                return new Cash(in);
            }

            public Cash[] newArray(int size) {
                return (new Cash[size]);
            }

        }
                ;

        protected Cash(Parcel in) {
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Cash() {
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(ticketId);
            dest.writeValue(amount);
            dest.writeValue(type);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class Debit_credit implements Parcelable
    {

        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("type")
        @Expose
        private String type;
        public final static Parcelable.Creator<Debit_credit> CREATOR = new Creator<Debit_credit>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Debit_credit createFromParcel(Parcel in) {
                return new Debit_credit(in);
            }

            public Debit_credit[] newArray(int size) {
                return (new Debit_credit[size]);
            }

        }
                ;

        protected Debit_credit(Parcel in) {
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Debit_credit() {
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(ticketId);
            dest.writeValue(amount);
            dest.writeValue(type);
        }

        public int describeContents() {
            return 0;
        }

    }


}

//import java.util.ArrayList;
//
//public class SummaryResponse {
//
//    private String status;
//
//    private Data data;
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//    @Override
//    public String toString() {
//        return "ClassPojo [status = " + status + ", data = " + data + "]";
//    }
//
//    /**
//     * data
//     */
//
//
//    public class Data {
//        private ArrayList<Transactions> transactions;
//
//        private Site_information site_information;
//
//        public ArrayList<Transactions> getTransactions() {
//            return transactions;
//        }
//
//        public void setTransactions(ArrayList<Transactions> transactions) {
//            this.transactions = transactions;
//        }
//
//        public Site_information getSite_information() {
//            return site_information;
//        }
//
//        public void setSite_information(Site_information site_information) {
//            this.site_information = site_information;
//        }
//
//        @Override
//        public String toString() {
//            return "ClassPojo [transactions = " + transactions + ", site_information = " + site_information + "]";
//        }
//    }
//
//    /**
//     * Transaction
//     */
//
//
//    public class Transactions {
//        private int amount;
//
//        private String ticketId;
//
//        private String type;
//
//        public int getAmount() {
//            return amount;
//        }
//
//        public void setAmount(int amount) {
//            this.amount = amount;
//        }
//
//        public String getTicketId() {
//            return ticketId;
//        }
//
//        public void setTicketId(String ticketId) {
//            this.ticketId = ticketId;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        @Override
//        public String toString() {
//            return "ClassPojo [amount = " + amount + ", ticketId = " + ticketId + ", type = " + type + "]";
//        }
//    }
//
//    /**
//     * Site info
//     */
//
//
//    public class Site_information {
//        private String street2;
//
//        private String street1;
//
//        private String name;
//
//        private String state;
//
//        private String city;
//
//        public String getStreet2() {
//            return street2;
//        }
//
//        public void setStreet2(String street2) {
//            this.street2 = street2;
//        }
//
//        public String getStreet1() {
//            return street1;
//        }
//
//        public void setStreet1(String street1) {
//            this.street1 = street1;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getState() {
//            return state;
//        }
//
//        public void setState(String state) {
//            this.state = state;
//        }
//
//        public String getCity() {
//            return city;
//        }
//
//        public void setCity(String city) {
//            this.city = city;
//        }
//
//        @Override
//        public String toString() {
//            return "ClassPojo [street2 = " + street2 + ", street1 = " + street1 + ", name = " + name + ", state = " + state + ", city = " + city + "]";
//        }
//    }
//
//
//}
