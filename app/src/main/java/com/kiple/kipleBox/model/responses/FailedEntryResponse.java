package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FailedEntryResponse implements Parcelable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<FailedEntryResponse> CREATOR = new Creator<FailedEntryResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FailedEntryResponse createFromParcel(Parcel in) {
            return new FailedEntryResponse(in);
        }

        public FailedEntryResponse[] newArray(int size) {
            return (new FailedEntryResponse[size]);
        }

    }
            ;

    protected FailedEntryResponse(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public FailedEntryResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable
    {

        @SerializedName("entry_log_id")
        @Expose
        private Integer entryLogId;
        @SerializedName("image")
        @Expose
        private String image;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.entryLogId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.image = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public Integer getEntryLogId() {
            return entryLogId;
        }

        public void setEntryLogId(Integer entryLogId) {
            this.entryLogId = entryLogId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(entryLogId);
            dest.writeValue(image);
        }

        public int describeContents() {
            return 0;
        }

    }

}