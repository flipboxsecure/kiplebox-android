package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MiniDashboardResponse implements Parcelable
{

    @SerializedName("valet_status_id")
    @Expose
    private String valet_status_id;
    @SerializedName("valet_status_desc")
    @Expose
    private String valet_status_desc;
    @SerializedName("total")
    @Expose
    private Integer total;
    public final static Parcelable.Creator<MiniDashboardResponse> CREATOR = new Creator<MiniDashboardResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MiniDashboardResponse createFromParcel(Parcel in) {
            return new MiniDashboardResponse(in);
        }

        public MiniDashboardResponse[] newArray(int size) {
            return (new MiniDashboardResponse[size]);
        }

    }
            ;

    protected MiniDashboardResponse(Parcel in) {
        this.valet_status_id = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_status_desc = ((String) in.readValue((String.class.getClassLoader())));
        this.total = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public MiniDashboardResponse() {
    }

    public String getValet_status_id() {
        return valet_status_id;
    }

    public void setValet_status_id(String valet_status_id) {
        this.valet_status_id = valet_status_id;
    }

    public String getValet_status_desc() {
        return valet_status_desc;
    }

    public void setValet_status_desc(String valet_status_desc) {
        this.valet_status_desc = valet_status_desc;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(valet_status_id);
        dest.writeValue(valet_status_desc);
        dest.writeValue(total);
    }

    public int describeContents() {
        return 0;
    }

}