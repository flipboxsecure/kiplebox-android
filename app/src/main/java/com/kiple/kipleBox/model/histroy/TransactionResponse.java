package com.kiple.kipleBox.model.histroy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TransactionResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Data> data = new ArrayList<>();
    public final static Parcelable.Creator<TransactionResponse> CREATOR = new Creator<TransactionResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TransactionResponse createFromParcel(Parcel in) {
            return new TransactionResponse(in);
        }

        public TransactionResponse[] newArray(int size) {
            return (new TransactionResponse[size]);
        }

    }
            ;

    protected TransactionResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (Data.class.getClassLoader()));
    }

    public TransactionResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Data implements Parcelable {

        @SerializedName("receipt")
        @Expose
        private String receipt;
        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("entryTime")
        @Expose
        private String entryTime;
        @SerializedName("datePay")
        @Expose
        private String datePay;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("paidAmount")
        @Expose
        private String paidAmount;
        @SerializedName("paymentBalance")
        @Expose
        private double paymentBalance;
        @SerializedName("payTime")
        @Expose
        private String payTime;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("car_plate")
        @Expose
        private String car_plate;
        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
                ;

        protected Data(Parcel in) {
            this.receipt = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
            this.datePay = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
            this.paidAmount = ((String) in.readValue((String.class.getClassLoader())));
            this.paymentBalance = ((double) in.readValue((Integer.class.getClassLoader())));
            this.payTime = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((String) in.readValue((String.class.getClassLoader())));
            this.car_plate = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public String getReceipt() {
            return receipt;
        }

        public void setReceipt(String receipt) {
            this.receipt = receipt;
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(String entryTime) {
            this.entryTime = entryTime;
        }

        public String getDatePay() {
            return datePay;
        }

        public void setDatePay(String datePay) {
            this.datePay = datePay;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getPaidAmount() {
            return paidAmount;
        }

        public void setPaidAmount(String paidAmount) {
            this.paidAmount = paidAmount;
        }

        public double getPaymentBalance() {
            return paymentBalance;
        }

        public void setPaymentBalance(double paymentBalance) {
            this.paymentBalance = paymentBalance;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCar_plate() {
            return car_plate;
        }

        public void setCar_plate(String car_plate) {
            this.car_plate = car_plate;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(receipt);
            dest.writeValue(ticketId);
            dest.writeValue(entryTime);
            dest.writeValue(datePay);
            dest.writeValue(amount);
            dest.writeValue(paidAmount);
            dest.writeValue(paymentBalance);
            dest.writeValue(payTime);
            dest.writeValue(type);
            dest.writeValue(car_plate);
        }

        public int describeContents() {
            return 0;
        }

    }

}

//public class TransactionResponse {
//
//    private String status;
//
//    private ArrayList<Data> data;
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//
//    public ArrayList<Data> getData() {
//        return data;
//    }
//
//    public void setData(ArrayList<Data> data) {
//        this.data = data;
//    }
//
//    @Override
//    public String toString() {
//        return "ClassPojo [status = " + status + ", data = " + data + "]";
//    }
//
//
//    public class Data {
//        private String amount;
//
//        private String ticketId;
//
//        private String datePay;
//
//        private String car_plate;
//
//        private String type;
//
//        public String getAmount() {
//            return amount;
//        }
//
//        public void setAmount(String amount) {
//            this.amount = amount;
//        }
//
//        public String getTicketId() {
//            return ticketId;
//        }
//
//        public void setTicketId(String ticketId) {
//            this.ticketId = ticketId;
//        }
//
//        public String getDatePay() {
//            return datePay;
//        }
//
//        public void setDatePay(String datePay) {
//            this.datePay = datePay;
//        }
//
//        public String getCar_plate() {
//            return car_plate;
//        }
//
//        public void setCar_plate(String car_plate) {
//            this.car_plate = car_plate;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        @Override
//        public String toString() {
//            return "ClassPojo [amount = " + amount + ", ticketId = " + ticketId + ", datePay = " + datePay + ", car_plate = " + car_plate + ", type = " + type + "]";
//        }
//    }
//
//
//}
