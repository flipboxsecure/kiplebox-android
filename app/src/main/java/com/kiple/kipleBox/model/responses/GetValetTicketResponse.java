package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetValetTicketResponse implements Parcelable
{

    @SerializedName("kpCode")
    @Expose
    private String kpCode;
    @SerializedName("kipleCodeDesc")
    @Expose
    private String kipleCodeDesc;
    @SerializedName("siteId")
    @Expose
    private String siteId;
    @SerializedName("ticketData")
    @Expose
    private String ticketData;
    @SerializedName("ticketId")
    @Expose
    private String ticketId;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("valet_status_id")
    @Expose
    private Integer valet_status_id;
    @SerializedName("valet_car_key_collected")
    @Expose
    private String valet_car_key_collected;
    @SerializedName("userId")
    @Expose
    private Object userId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("gst")
    @Expose
    private Object gst;
    @SerializedName("entryTime")
    @Expose
    private String entryTime;
    @SerializedName("reqTime")
    @Expose
    private String reqTime;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("gracePeriod")
    @Expose
    private Integer gracePeriod;
    @SerializedName("dateGraceEnd")
    @Expose
    private String dateGraceEnd;
    @SerializedName("grace_exceed")
    @Expose
    private Integer grace_exceed;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dateCreate")
    @Expose
    private String dateCreate;
    @SerializedName("dateUpdate")
    @Expose
    private Object dateUpdate;
    @SerializedName("datePark")
    @Expose
    private String datePark;
    @SerializedName("datePickup")
    @Expose
    private Object datePickup;
    @SerializedName("dateCollected")
    @Expose
    private Object dateCollected;
    @SerializedName("datePay")
    @Expose
    private Object datePay;
    @SerializedName("datePayUnix")
    @Expose
    private Object datePayUnix;
    @SerializedName("requestId")
    @Expose
    private Object requestId;
    @SerializedName("paymentAuthCode")
    @Expose
    private Object paymentAuthCode;
    @SerializedName("receipt")
    @Expose
    private Object receipt;
    @SerializedName("flag")
    @Expose
    private Integer flag;
    @SerializedName("valet_status_desc")
    @Expose
    private String valet_status_desc;
    @SerializedName("valet_park_loc")
    @Expose
    private String valet_park_loc;
    @SerializedName("staff_issuer")
    @Expose
    private String staff_issuer;
    @SerializedName("staff_parker")
    @Expose
    private String staff_parker;
    @SerializedName("staff_pickup")
    @Expose
    private Object staff_pickup;
    @SerializedName("staff_collected")
    @Expose
    private Object staff_collected;
    @SerializedName("img")
    @Expose
    private List<String> img = null;
    @SerializedName("upfront_payment")
    @Expose
    private String upfront_payment;
    @SerializedName("fee")
    @Expose
    private String fee;
    public final static Parcelable.Creator<GetValetTicketResponse> CREATOR = new Creator<GetValetTicketResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GetValetTicketResponse createFromParcel(Parcel in) {
            return new GetValetTicketResponse(in);
        }

        public GetValetTicketResponse[] newArray(int size) {
            return (new GetValetTicketResponse[size]);
        }

    }
            ;

    protected GetValetTicketResponse(Parcel in) {
        this.kpCode = ((String) in.readValue((String.class.getClassLoader())));
        this.kipleCodeDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.siteId = ((String) in.readValue((String.class.getClassLoader())));
        this.ticketData = ((String) in.readValue((String.class.getClassLoader())));
        this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
        this.service = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_status_id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.valet_car_key_collected = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.gst = ((Object) in.readValue((Object.class.getClassLoader())));
        this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
        this.reqTime = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.gracePeriod = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateGraceEnd = ((String) in.readValue((String.class.getClassLoader())));
        this.grace_exceed = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.dateCreate = ((String) in.readValue((String.class.getClassLoader())));
        this.dateUpdate = ((Object) in.readValue((Object.class.getClassLoader())));
        this.datePark = ((String) in.readValue((String.class.getClassLoader())));
        this.datePickup = ((Object) in.readValue((Object.class.getClassLoader())));
        this.dateCollected = ((Object) in.readValue((Object.class.getClassLoader())));
        this.datePay = ((Object) in.readValue((Object.class.getClassLoader())));
        this.datePayUnix = ((Object) in.readValue((Object.class.getClassLoader())));
        this.requestId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.paymentAuthCode = ((Object) in.readValue((Object.class.getClassLoader())));
        this.receipt = ((Object) in.readValue((Object.class.getClassLoader())));
        this.flag = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.valet_status_desc = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_park_loc = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_issuer = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_parker = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_pickup = ((Object) in.readValue((Object.class.getClassLoader())));
        this.staff_collected = ((Object) in.readValue((Object.class.getClassLoader())));
        in.readList(this.img, (java.lang.String.class.getClassLoader()));
        this.upfront_payment = ((String) in.readValue((String.class.getClassLoader())));
        this.fee = ((String) in.readValue((String.class.getClassLoader())));
    }

    public GetValetTicketResponse() {
    }

    public String getKpCode() {
        return kpCode;
    }

    public void setKpCode(String kpCode) {
        this.kpCode = kpCode;
    }

    public String getKipleCodeDesc() {
        return kipleCodeDesc;
    }

    public void setKipleCodeDesc(String kipleCodeDesc) {
        this.kipleCodeDesc = kipleCodeDesc;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getTicketData() {
        return ticketData;
    }

    public void setTicketData(String ticketData) {
        this.ticketData = ticketData;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getValet_status_id() {
        return valet_status_id;
    }

    public void setValet_status_id(Integer valet_status_id) {
        this.valet_status_id = valet_status_id;
    }

    public String getValet_car_key_collected() {
        return valet_car_key_collected;
    }

    public void setValet_car_key_collected(String valet_car_key_collected) {
        this.valet_car_key_collected = valet_car_key_collected;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Object getGst() {
        return gst;
    }

    public void setGst(Object gst) {
        this.gst = gst;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(Integer gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getDateGraceEnd() {
        return dateGraceEnd;
    }

    public void setDateGraceEnd(String dateGraceEnd) {
        this.dateGraceEnd = dateGraceEnd;
    }

    public Integer getGrace_exceed() {
        return grace_exceed;
    }

    public void setGrace_exceed(Integer grace_exceed) {
        this.grace_exceed = grace_exceed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Object getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Object dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getDatePark() {
        return datePark;
    }

    public void setDatePark(String datePark) {
        this.datePark = datePark;
    }

    public Object getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Object datePickup) {
        this.datePickup = datePickup;
    }

    public Object getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Object dateCollected) {
        this.dateCollected = dateCollected;
    }

    public Object getDatePay() {
        return datePay;
    }

    public void setDatePay(Object datePay) {
        this.datePay = datePay;
    }

    public Object getDatePayUnix() {
        return datePayUnix;
    }

    public void setDatePayUnix(Object datePayUnix) {
        this.datePayUnix = datePayUnix;
    }

    public Object getRequestId() {
        return requestId;
    }

    public void setRequestId(Object requestId) {
        this.requestId = requestId;
    }

    public Object getPaymentAuthCode() {
        return paymentAuthCode;
    }

    public void setPaymentAuthCode(Object paymentAuthCode) {
        this.paymentAuthCode = paymentAuthCode;
    }

    public Object getReceipt() {
        return receipt;
    }

    public void setReceipt(Object receipt) {
        this.receipt = receipt;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getValet_status_desc() {
        return valet_status_desc;
    }

    public void setValet_status_desc(String valet_status_desc) {
        this.valet_status_desc = valet_status_desc;
    }

    public String getValet_park_loc() {
        return valet_park_loc;
    }

    public void setValet_park_loc(String valet_park_loc) {
        this.valet_park_loc = valet_park_loc;
    }

    public String getStaff_issuer() {
        return staff_issuer;
    }

    public void setStaff_issuer(String staff_issuer) {
        this.staff_issuer = staff_issuer;
    }

    public String getStaff_parker() {
        return staff_parker;
    }

    public void setStaff_parker(String staff_parker) {
        this.staff_parker = staff_parker;
    }

    public Object getStaff_pickup() {
        return staff_pickup;
    }

    public void setStaff_pickup(Object staff_pickup) {
        this.staff_pickup = staff_pickup;
    }

    public Object getStaff_collected() {
        return staff_collected;
    }

    public void setStaff_collected(Object staff_collected) {
        this.staff_collected = staff_collected;
    }

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }

    public String getUpfront_payment() {
        return upfront_payment;
    }

    public void setUpfront_payment(String upfront_payment) {
        this.upfront_payment = upfront_payment;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(kpCode);
        dest.writeValue(kipleCodeDesc);
        dest.writeValue(siteId);
        dest.writeValue(ticketData);
        dest.writeValue(ticketId);
        dest.writeValue(service);
        dest.writeValue(valet_status_id);
        dest.writeValue(valet_car_key_collected);
        dest.writeValue(userId);
        dest.writeValue(amount);
        dest.writeValue(gst);
        dest.writeValue(entryTime);
        dest.writeValue(reqTime);
        dest.writeValue(duration);
        dest.writeValue(gracePeriod);
        dest.writeValue(dateGraceEnd);
        dest.writeValue(grace_exceed);
        dest.writeValue(status);
        dest.writeValue(dateCreate);
        dest.writeValue(dateUpdate);
        dest.writeValue(datePark);
        dest.writeValue(datePickup);
        dest.writeValue(dateCollected);
        dest.writeValue(datePay);
        dest.writeValue(datePayUnix);
        dest.writeValue(requestId);
        dest.writeValue(paymentAuthCode);
        dest.writeValue(receipt);
        dest.writeValue(flag);
        dest.writeValue(valet_status_desc);
        dest.writeValue(valet_park_loc);
        dest.writeValue(staff_issuer);
        dest.writeValue(staff_parker);
        dest.writeValue(staff_pickup);
        dest.writeValue(staff_collected);
        dest.writeList(img);
        dest.writeValue(upfront_payment);
        dest.writeValue(fee);
    }

    public int describeContents() {
        return 0;
    }

}