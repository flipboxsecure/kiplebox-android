package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UpfrontAmountResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<>();
    public final static Parcelable.Creator<UpfrontAmountResponse> CREATOR = new Creator<UpfrontAmountResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UpfrontAmountResponse createFromParcel(Parcel in) {
            return new UpfrontAmountResponse(in);
        }

        public UpfrontAmountResponse[] newArray(int size) {
            return (new UpfrontAmountResponse[size]);
        }

    }
            ;

    protected UpfrontAmountResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (Datum.class.getClassLoader()));
    }

    public UpfrontAmountResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

    public static class Datum implements Parcelable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("site_id")
        @Expose
        private String site_id;
        @SerializedName("amount")
        @Expose
        private String amount;
        public final static Parcelable.Creator<Datum> CREATOR = new Creator<Datum>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            public Datum[] newArray(int size) {
                return (new Datum[size]);
            }

        }
                ;

        protected Datum(Parcel in) {
            this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.site_id = ((String) in.readValue((String.class.getClassLoader())));
            this.amount = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Datum() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getSite_id() {
            return site_id;
        }

        public void setSite_id(String site_id) {
            this.site_id = site_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(site_id);
            dest.writeValue(amount);
        }

        public int describeContents() {
            return 0;
        }

    }

}