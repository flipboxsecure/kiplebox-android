package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kiple.kipleBox.database.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "ticket")
public class TicketListsResponse implements Parcelable
{


    @ColumnInfo(name = "siteId")
    @SerializedName("siteId")
    @Expose
    private String siteId;

    @ColumnInfo(name = "ticketData")
    @SerializedName("ticketData")
    @Expose
    private String ticketData;

    @PrimaryKey @NotNull
    @ColumnInfo(name = "ticketId")
    @SerializedName("ticketId")
    @Expose
    private String ticketId;

    @ColumnInfo(name = "service")
    @SerializedName("service")
    @Expose
    private String service;

    @ColumnInfo(name = "valet_status_id")
    @SerializedName("valet_status_id")
    @Expose
    private Integer valet_status_id;

    @ColumnInfo(name = "valet_car_key_collected")
    @SerializedName("valet_car_key_collected")
    @Expose
    private String valet_car_key_collected;

    @ColumnInfo(name = "userId")
    @SerializedName("userId")
    @Expose
    private String userId;

    @ColumnInfo(name = "mobile_no")
    @SerializedName("mobile_no")
    @Expose
    private String mobile_no;

    @ColumnInfo(name = "amount")
    @SerializedName("amount")
    @Expose
    private String amount;

    @ColumnInfo(name = "fee")
    @SerializedName("fee")
    @Expose
    private String fee;

    @ColumnInfo(name = "upfront_payment")
    @SerializedName("upfront_payment")
    @Expose
    private String upfront_payment;

    @ColumnInfo(name = "gst")
    @SerializedName("gst")
    @Expose
    private String gst;

    @ColumnInfo(name = "entryTime")
    @SerializedName("entryTime")
    @Expose
    private String entryTime;

    @ColumnInfo(name = "entryTimeReadable")
    @SerializedName("entryTimeReadable")
    @Expose
    private String entryTimeReadable;

    @ColumnInfo(name = "reqTime")
    @SerializedName("reqTime")
    @Expose
    private String reqTime;

    @ColumnInfo(name = "duration")
    @SerializedName("duration")
    @Expose
    private String duration;

    @ColumnInfo(name = "gracePeriod")
    @SerializedName("gracePeriod")
    @Expose
    private Integer gracePeriod;

    @ColumnInfo(name = "dateGraceEnd")
    @SerializedName("dateGraceEnd")
    @Expose
    private String dateGraceEnd;

    @ColumnInfo(name = "status")
    @SerializedName("status")
    @Expose
    private String status;

    @ColumnInfo(name = "dateCreate")
    @SerializedName("dateCreate")
    @Expose
    private String dateCreate;

    @ColumnInfo(name = "dateUpdate")
    @SerializedName("dateUpdate")
    @Expose
    private String dateUpdate;

    @Ignore
    @TypeConverters(Converters.class)
    @SerializedName("datePark")
    @Expose
    private Object datePark;

    @Ignore
    @TypeConverters(Converters.class)
    @SerializedName("datePickup")
    @Expose
    private Object datePickup;

    @Ignore
    @TypeConverters(Converters.class)
    @SerializedName("dateCollected")
    @Expose
    private Object dateCollected;

    @ColumnInfo(name = "datePay")
    @SerializedName("datePay")
    @Expose
    private String datePay;

    @ColumnInfo(name = "datePayUnix")
    @TypeConverters(Converters.class)
    @SerializedName("datePayUnix")
    @Expose
    private String datePayUnix;

    @Ignore
    @SerializedName("requestId")
    @Expose
    private Object requestId;

    @ColumnInfo(name = "paymentAuthCode")
    @SerializedName("paymentAuthCode")
    @Expose
    private String paymentAuthCode;

    @ColumnInfo(name = "receipt")
    @SerializedName("receipt")
    @Expose
    private String receipt;

    @ColumnInfo(name = "flag")
    @SerializedName("flag")
    @Expose
    private Integer flag;

    @ColumnInfo(name = "valet_status_desc")
    @SerializedName("valet_status_desc")
    @Expose
    private String valet_status_desc;

    @ColumnInfo(name = "valet_park_loc")
    @SerializedName("valet_park_loc")
    @Expose
    private String valet_park_loc;

    @ColumnInfo(name = "staff_issuer")
    @SerializedName("staff_issuer")
    @Expose
    private String staff_issuer;

    @ColumnInfo(name = "staff_parker")
    @SerializedName("staff_parker")
    @Expose
    private String staff_parker;

    @Ignore
    @SerializedName("staff_pickup")
    @TypeConverters(Converters.class)
    @Expose
    private Object staff_pickup;

    @Ignore
    @SerializedName("staff_collected")
    @Expose
    private Object staff_collected;

    @ColumnInfo(name = "img")
    @SerializedName("img")
    @Expose
    private List<String> img = new ArrayList<>();

    @ColumnInfo(name = "card_payment_response", defaultValue = "")
    @NonNull
    @SerializedName("card_payment_response")
    @Expose
    private String card_payment_response;

    public final static Parcelable.Creator<TicketListsResponse> CREATOR = new Creator<TicketListsResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TicketListsResponse createFromParcel(Parcel in) {
            return new TicketListsResponse(in);
        }

        public TicketListsResponse[] newArray(int size) {
            return (new TicketListsResponse[size]);
        }

    }
            ;

    protected TicketListsResponse(Parcel in) {
        this.siteId = ((String) in.readValue((String.class.getClassLoader())));
        this.ticketData = ((String) in.readValue((String.class.getClassLoader())));
        this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
        this.service = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_status_id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.valet_car_key_collected = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.mobile_no = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.fee = ((String) in.readValue((String.class.getClassLoader())));
        this.upfront_payment = ((String) in.readValue((String.class.getClassLoader())));
        this.gst = ((String) in.readValue((String.class.getClassLoader())));
        this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
        this.entryTimeReadable = ((String) in.readValue((String.class.getClassLoader())));
        this.reqTime = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.gracePeriod = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateGraceEnd = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.dateCreate = ((String) in.readValue((String.class.getClassLoader())));
        this.dateUpdate = ((String) in.readValue((String.class.getClassLoader())));
        this.datePark = ((Object) in.readValue((Object.class.getClassLoader())));
        this.datePickup = ((Object) in.readValue((Object.class.getClassLoader())));
        this.dateCollected = ((Object) in.readValue((Object.class.getClassLoader())));
        this.datePay = ((String) in.readValue((String.class.getClassLoader())));
        this.datePayUnix = ((String) in.readValue((String.class.getClassLoader())));
        this.requestId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.paymentAuthCode = ((String) in.readValue((String.class.getClassLoader())));
        this.receipt = ((String) in.readValue((String.class.getClassLoader())));
        this.flag = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.valet_status_desc = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_park_loc = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_issuer = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_parker = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_pickup = ((Object) in.readValue((Object.class.getClassLoader())));
        this.staff_collected = ((Object) in.readValue((Object.class.getClassLoader())));
        in.readList(this.img, (java.lang.String.class.getClassLoader()));
        this.card_payment_response = (String) in.readValue((Object.class.getClassLoader()));
    }

    public TicketListsResponse() {
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getTicketData() {
        return ticketData;
    }

    public void setTicketData(String ticketData) {
        this.ticketData = ticketData;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getValet_status_id() {
        return valet_status_id;
    }

    public void setValet_status_id(Integer valet_status_id) {
        this.valet_status_id = valet_status_id;
    }

    public String getValet_car_key_collected() {
        return valet_car_key_collected;
    }

    public void setValet_car_key_collected(String valet_car_key_collected) {
        this.valet_car_key_collected = valet_car_key_collected;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getUpfront_payment() {
        return upfront_payment;
    }

    public void setUpfront_payment(String upfront_payment) {
        this.upfront_payment = upfront_payment;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getEntryTimeReadable() {
        return entryTimeReadable;
    }

    public void setEntryTimeReadable(String entryTimeReadable) {
        this.entryTimeReadable = entryTimeReadable;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(Integer gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getDateGraceEnd() {
        return dateGraceEnd;
    }

    public void setDateGraceEnd(String dateGraceEnd) {
        this.dateGraceEnd = dateGraceEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Object getDatePark() {
        return datePark;
    }

    public void setDatePark(Object datePark) {
        this.datePark = datePark;
    }

    public Object getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Object datePickup) {
        this.datePickup = datePickup;
    }

    public Object getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Object dateCollected) {
        this.dateCollected = dateCollected;
    }

    public String getDatePay() {
        return datePay;
    }

    public void setDatePay(String datePay) {
        this.datePay = datePay;
    }

    public String getDatePayUnix() {
        return datePayUnix;
    }

    public void setDatePayUnix(String datePayUnix) {
        this.datePayUnix = datePayUnix;
    }

    public Object getRequestId() {
        return requestId;
    }

    public void setRequestId(Object requestId) {
        this.requestId = requestId;
    }

    public String getPaymentAuthCode() {
        return paymentAuthCode;
    }

    public void setPaymentAuthCode(String paymentAuthCode) {
        this.paymentAuthCode = paymentAuthCode;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getValet_status_desc() {
        return valet_status_desc;
    }

    public void setValet_status_desc(String valet_status_desc) {
        this.valet_status_desc = valet_status_desc;
    }

    public String getValet_park_loc() {
        return valet_park_loc;
    }

    public void setValet_park_loc(String valet_park_loc) {
        this.valet_park_loc = valet_park_loc;
    }

    public String getStaff_issuer() {
        return staff_issuer;
    }

    public void setStaff_issuer(String staff_issuer) {
        this.staff_issuer = staff_issuer;
    }

    public String getStaff_parker() {
        return staff_parker;
    }

    public void setStaff_parker(String staff_parker) {
        this.staff_parker = staff_parker;
    }

    public Object getStaff_pickup() {
        return staff_pickup;
    }

    public void setStaff_pickup(Object staff_pickup) {
        this.staff_pickup = staff_pickup;
    }

    public Object getStaff_collected() {
        return staff_collected;
    }

    public void setStaff_collected(Object staff_collected) {
        this.staff_collected = staff_collected;
    }

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }

    public String getCard_payment_response() {
        return card_payment_response;
    }

    public void setCard_payment_response(String card_payment_response) {
        this.card_payment_response = card_payment_response;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(siteId);
        dest.writeValue(ticketData);
        dest.writeValue(ticketId);
        dest.writeValue(service);
        dest.writeValue(valet_status_id);
        dest.writeValue(valet_car_key_collected);
        dest.writeValue(userId);
        dest.writeValue(mobile_no);
        dest.writeValue(amount);
        dest.writeValue(fee);
        dest.writeValue(upfront_payment);
        dest.writeValue(gst);
        dest.writeValue(entryTime);
        dest.writeValue(entryTimeReadable);
        dest.writeValue(reqTime);
        dest.writeValue(duration);
        dest.writeValue(gracePeriod);
        dest.writeValue(dateGraceEnd);
        dest.writeValue(status);
        dest.writeValue(dateCreate);
        dest.writeValue(dateUpdate);
        dest.writeValue(datePark);
        dest.writeValue(datePickup);
        dest.writeValue(dateCollected);
        dest.writeValue(datePay);
        dest.writeValue(datePayUnix);
        dest.writeValue(requestId);
        dest.writeValue(paymentAuthCode);
        dest.writeValue(receipt);
        dest.writeValue(flag);
        dest.writeValue(valet_status_desc);
        dest.writeValue(valet_park_loc);
        dest.writeValue(staff_issuer);
        dest.writeValue(staff_parker);
        dest.writeValue(staff_pickup);
        dest.writeValue(staff_collected);
        dest.writeList(img);
        dest.writeValue(card_payment_response);
    }

    public int describeContents() {
        return 0;
    }

}