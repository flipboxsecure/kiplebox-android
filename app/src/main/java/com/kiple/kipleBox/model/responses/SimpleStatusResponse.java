package com.kiple.kipleBox.model.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SimpleStatusResponse implements Parcelable {

    public final static Parcelable.Creator<SimpleStatusResponse> CREATOR = new Creator<SimpleStatusResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SimpleStatusResponse createFromParcel(Parcel in) {
            return new SimpleStatusResponse(in);
        }

        public SimpleStatusResponse[] newArray(int size) {
            return (new SimpleStatusResponse[size]);
        }

    };
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("kpCode")
    @Expose
    private String kpCode;
    @SerializedName("kipleCodeDesc")
    @Expose
    private String kipleCodeDesc;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    protected SimpleStatusResponse(Parcel in) {
        this.success = (Boolean) in.readValue((Boolean.class.getClassLoader()));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.kpCode = ((String) in.readValue((String.class.getClassLoader())));
        this.kipleCodeDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public SimpleStatusResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKpCode() {
        return kpCode;
    }

    public void setKpCode(String kpCode) {
        this.kpCode = kpCode;
    }

    public String getKipleCodeDesc() {
        return kipleCodeDesc;
    }

    public void setKipleCodeDesc(String kipleCodeDesc) {
        this.kipleCodeDesc = kipleCodeDesc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(status);
        dest.writeValue(kpCode);
        dest.writeValue(kipleCodeDesc);
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }


    public static class Data implements Parcelable {

        public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        };
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("vendorId")
        @Expose
        private String vendorId;
        @SerializedName("siteId")
        @Expose
        private String siteId;
        @SerializedName("ticketData")
        @Expose
        private String ticketData;
        @SerializedName("ticketId")
        @Expose
        private String ticketId;
        @SerializedName("plate_no")
        @Expose
        private String plateNo;
        @SerializedName("valet_status_id")
        @Expose
        private Integer valetStatusId;
        @SerializedName("valet_car_key_collected")
        @Expose
        private String valetCarKeyCollected;
        @SerializedName("valet_park_loc")
        @Expose
        private String valetParkLoc;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("pos_issuer")
        @Expose
        private String posIssuer;
        @SerializedName("staff_issuer")
        @Expose
        private String staffIssuer;
        @SerializedName("upfront_payment")
        @Expose
        private Integer upfrontPayment;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("entryTime")
        @Expose
        private String entryTime;
        @SerializedName("gracePeriod")
        @Expose
        private Integer gracePeriod;
        @SerializedName("dateGraceEnd")
        @Expose
        private String dateGraceEnd;
        @SerializedName("grace_exceed")
        @Expose
        private Integer graceExceed;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("dateCreate")
        @Expose
        private String dateCreate;
        @SerializedName("staff_issuer_id")
        @Expose
        private String staffIssuerId;
        @SerializedName("fee")
        @Expose
        private String fee;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("ticket_data_encrypted")
        @Expose
        private String ticketDataEncrypted;
        @SerializedName("date_park")
        @Expose
        private String datePark;
        @SerializedName("staff_parker")
        @Expose
        private String staffParker;
        @SerializedName("staff_parker_id")
        @Expose
        private String staffParkerId;
        @SerializedName("pos_parker")
        @Expose
        private String posParker;

        protected Data(Parcel in) {
            this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.vendorId = ((String) in.readValue((String.class.getClassLoader())));
            this.siteId = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketData = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketId = ((String) in.readValue((String.class.getClassLoader())));
            this.plateNo = ((String) in.readValue((String.class.getClassLoader())));
            this.valetStatusId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.valetCarKeyCollected = ((String) in.readValue((String.class.getClassLoader())));
            this.valetParkLoc = ((String) in.readValue((Object.class.getClassLoader())));
            this.userId = ((String) in.readValue((Object.class.getClassLoader())));
            this.service = ((String) in.readValue((String.class.getClassLoader())));
            this.posIssuer = ((String) in.readValue((String.class.getClassLoader())));
            this.staffIssuer = ((String) in.readValue((String.class.getClassLoader())));
            this.upfrontPayment = ((Integer) in.readValue((Object.class.getClassLoader())));
            this.amount = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.entryTime = ((String) in.readValue((String.class.getClassLoader())));
            this.gracePeriod = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.dateGraceEnd = ((String) in.readValue((String.class.getClassLoader())));
            this.graceExceed = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.status = ((String) in.readValue((String.class.getClassLoader())));
            this.dateCreate = ((String) in.readValue((String.class.getClassLoader())));
            this.staffIssuerId = ((String) in.readValue((String.class.getClassLoader())));
            this.fee = ((String) in.readValue((String.class.getClassLoader())));
            this.mobileNo = ((String) in.readValue((String.class.getClassLoader())));
            this.ticketDataEncrypted = ((String) in.readValue((String.class.getClassLoader())));
            this.datePark = ((String) in.readValue((String.class.getClassLoader())));
            this.staffParker = ((String) in.readValue((String.class.getClassLoader())));
            this.staffParkerId = ((String) in.readValue((String.class.getClassLoader())));
            this.posParker = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getVendorId() {
            return vendorId;
        }

        public void setVendorId(String vendorId) {
            this.vendorId = vendorId;
        }

        public String getSiteId() {
            return siteId;
        }

        public void setSiteId(String siteId) {
            this.siteId = siteId;
        }

        public String getTicketData() {
            return ticketData;
        }

        public void setTicketData(String ticketData) {
            this.ticketData = ticketData;
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public Integer getValetStatusId() {
            return valetStatusId;
        }

        public void setValetStatusId(Integer valetStatusId) {
            this.valetStatusId = valetStatusId;
        }

        public String getValetCarKeyCollected() {
            return valetCarKeyCollected;
        }

        public void setValetCarKeyCollected(String valetCarKeyCollected) {
            this.valetCarKeyCollected = valetCarKeyCollected;
        }

        public Object getValetParkLoc() {
            return valetParkLoc;
        }

        public void setValetParkLoc(String valetParkLoc) {
            this.valetParkLoc = valetParkLoc;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getPosIssuer() {
            return posIssuer;
        }

        public void setPosIssuer(String posIssuer) {
            this.posIssuer = posIssuer;
        }

        public String getStaffIssuer() {
            return staffIssuer;
        }

        public void setStaffIssuer(String staffIssuer) {
            this.staffIssuer = staffIssuer;
        }

        public Integer getUpfrontPayment() {
            return upfrontPayment;
        }

        public void setUpfrontPayment(Integer upfrontPayment) {
            this.upfrontPayment = upfrontPayment;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(String entryTime) {
            this.entryTime = entryTime;
        }

        public Integer getGracePeriod() {
            return gracePeriod;
        }

        public void setGracePeriod(Integer gracePeriod) {
            this.gracePeriod = gracePeriod;
        }

        public String getDateGraceEnd() {
            return dateGraceEnd;
        }

        public void setDateGraceEnd(String dateGraceEnd) {
            this.dateGraceEnd = dateGraceEnd;
        }

        public Integer getGraceExceed() {
            return graceExceed;
        }

        public void setGraceExceed(Integer graceExceed) {
            this.graceExceed = graceExceed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDateCreate() {
            return dateCreate;
        }

        public void setDateCreate(String dateCreate) {
            this.dateCreate = dateCreate;
        }

        public String getStaffIssuerId() {
            return staffIssuerId;
        }

        public void setStaffIssuerId(String staffIssuerId) {
            this.staffIssuerId = staffIssuerId;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getTicketDataEncrypted() {
            return ticketDataEncrypted;
        }

        public void setTicketDataEncrypted(String ticketDataEncrypted) {
            this.ticketDataEncrypted = ticketDataEncrypted;
        }

        public String getDatePark() {
            return datePark;
        }

        public void setDatePark(String datePark) {
            this.datePark = datePark;
        }

        public String getStaffParker() {
            return staffParker;
        }

        public void setStaffParker(String staffParker) {
            this.staffParker = staffParker;
        }

        public String getStaffParkerId() {
            return staffParkerId;
        }

        public void setStaffParkerId(String staffParkerId) {
            this.staffParkerId = staffParkerId;
        }

        public String getPosParker() {
            return posParker;
        }

        public void setPosParker(String posParker) {
            this.posParker = posParker;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(vendorId);
            dest.writeValue(siteId);
            dest.writeValue(ticketData);
            dest.writeValue(ticketId);
            dest.writeValue(plateNo);
            dest.writeValue(valetStatusId);
            dest.writeValue(valetCarKeyCollected);
            dest.writeValue(valetParkLoc);
            dest.writeValue(userId);
            dest.writeValue(service);
            dest.writeValue(posIssuer);
            dest.writeValue(staffIssuer);
            dest.writeValue(upfrontPayment);
            dest.writeValue(amount);
            dest.writeValue(entryTime);
            dest.writeValue(gracePeriod);
            dest.writeValue(dateGraceEnd);
            dest.writeValue(graceExceed);
            dest.writeValue(status);
            dest.writeValue(dateCreate);
            dest.writeValue(staffIssuerId);
            dest.writeValue(fee);
            dest.writeValue(mobileNo);
            dest.writeValue(ticketDataEncrypted);
            dest.writeValue(datePark);
            dest.writeValue(staffParker);
            dest.writeValue(staffParkerId);
            dest.writeValue(posParker);
        }

        public int describeContents() {
            return 0;
        }

    }
}