package com.kiple.kipleBox.model.printer_models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class PaymentReceiptModel implements Parcelable {

    private String site_name;
    private String site_address;
    private String site_phone_number;
    private String date;
    private String time;
    private String number_plate;
    private String ticket_id;
    private String staff_name;
    private String valet_amount;
    private String pos_info;
    private String qr_info;
    private String payment_method;
    private Bitmap bitmap;
    private String parking_operator;
    private String gst_id;
    private String receipt_id;
    private String entry_time;
    private String payment_time;
    private String duration;
    private String subtotal;
    private String discount;
    private String roundingAdj;
    private String change;
    private String paid;
    private boolean show_tax_info = false;
    private boolean duplicated_receipt_flag = false;

    protected PaymentReceiptModel(Parcel in) {
        this.site_name = ((String) in.readValue((String.class.getClassLoader())));
        this.site_address = ((String) in.readValue((String.class.getClassLoader())));
        this.site_phone_number = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.time = ((String) in.readValue((String.class.getClassLoader())));
        this.number_plate = ((String) in.readValue((String.class.getClassLoader())));
        this.staff_name = ((String) in.readValue((String.class.getClassLoader())));
        this.valet_amount = ((String) in.readValue((String.class.getClassLoader())));
        this.pos_info = ((String) in.readValue((String.class.getClassLoader())));
        this.qr_info = ((String) in.readValue((String.class.getClassLoader())));
        this.payment_method = ((String) in.readValue((String.class.getClassLoader())));
        this.bitmap = ((Bitmap) in.readValue((String.class.getClassLoader())));
        this.parking_operator = ((String) in.readValue((String.class.getClassLoader())));
        this.gst_id = ((String) in.readValue((String.class.getClassLoader())));
        this.receipt_id = ((String) in.readValue((String.class.getClassLoader())));
        this.entry_time = ((String) in.readValue((String.class.getClassLoader())));
        this.payment_time = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.subtotal = ((String) in.readValue((String.class.getClassLoader())));
        this.discount = ((String) in.readValue((String.class.getClassLoader())));
        this.roundingAdj = ((String) in.readValue((String.class.getClassLoader())));
        this.change = ((String) in.readValue((String.class.getClassLoader())));
        this.paid = ((String) in.readValue((String.class.getClassLoader())));
        this.show_tax_info = ((boolean) in.readValue((String.class.getClassLoader())));
        this.duplicated_receipt_flag = ((boolean) in.readValue((String.class.getClassLoader())));
    }

    public PaymentReceiptModel() {
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getSite_address() {
        return site_address;
    }

    public void setSite_address(String site_address) {
        this.site_address = site_address;
    }

    public String getSite_phone_number() {
        return site_phone_number;
    }

    public void setSite_phone_number(String site_phone_number) {
        this.site_phone_number = site_phone_number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumber_plate() {
        return number_plate;
    }

    public void setNumber_plate(String number_plate) {
        this.number_plate = number_plate;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getValet_amount() {
        return valet_amount;
    }

    public void setValet_amount(String valet_amount) {
        this.valet_amount = valet_amount;
    }

    public String getPos_info() {
        return pos_info;
    }

    public void setPos_info(String pos_info) {
        this.pos_info = pos_info;
    }

    public String getQr_info() {
        return qr_info;
    }

    public void setQr_info(String qr_info) {
        this.qr_info = qr_info;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getParking_operator() {
        return parking_operator;
    }

    public void setParking_operator(String parking_operator) {
        this.parking_operator = parking_operator;
    }

    public String getGst_id() {
        return gst_id;
    }

    public void setGst_id(String gst_id) {
        this.gst_id = gst_id;
    }

    public String getReceipt_id() {
        return receipt_id;
    }

    public void setReceipt_id(String receipt_id) {
        this.receipt_id = receipt_id;
    }

    public String getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(String entry_time) {
        this.entry_time = entry_time;
    }

    public String getPayment_time() {
        return payment_time;
    }

    public void setPayment_time(String payment_time) {
        this.payment_time = payment_time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getRoundingAdj() {
        return roundingAdj;
    }

    public void setRoundingAdj(String roundingAdj) {
        this.roundingAdj = roundingAdj;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public boolean isShow_tax_info() {
        return show_tax_info;
    }

    public void setShow_tax_info(boolean show_tax_info) {
        this.show_tax_info = show_tax_info;
    }

    public boolean isDuplicated_receipt_flag() {
        return duplicated_receipt_flag;
    }

    public void setDuplicated_receipt_flag(boolean duplicated_receipt_flag) {
        this.duplicated_receipt_flag = duplicated_receipt_flag;
    }

    public static final Creator<PaymentReceiptModel> CREATOR = new Creator<PaymentReceiptModel>() {
        @SuppressWarnings({
                "unchecked"
        })
        public PaymentReceiptModel createFromParcel(Parcel in) {
            return new PaymentReceiptModel(in);
        }

        @Override
        public PaymentReceiptModel[] newArray(int size) {
            return new PaymentReceiptModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(site_name);
        dest.writeValue(site_address);
        dest.writeValue(site_phone_number);
        dest.writeValue(date);
        dest.writeValue(time);
        dest.writeValue(number_plate);
        dest.writeValue(ticket_id);
        dest.writeValue(staff_name);
        dest.writeValue(valet_amount);
        dest.writeValue(pos_info);
        dest.writeValue(qr_info);
        dest.writeValue(payment_method);
        dest.writeValue(bitmap);
        dest.writeValue(parking_operator);
        dest.writeValue(gst_id);
        dest.writeValue(receipt_id);
        dest.writeValue(entry_time);
        dest.writeValue(payment_time);
        dest.writeValue(duration);
        dest.writeValue(subtotal);
        dest.writeValue(discount);
        dest.writeValue(roundingAdj);
        dest.writeValue(change);
        dest.writeValue(paid);
        dest.writeValue(show_tax_info);
        dest.writeValue(duplicated_receipt_flag);
    }
}
