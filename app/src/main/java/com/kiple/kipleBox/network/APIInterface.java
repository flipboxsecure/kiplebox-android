package com.kiple.kipleBox.network;

import com.kiple.kipleBox.common.APIErrorBody;
import com.kiple.kipleBox.model.histroy.SummaryResponse;
import com.kiple.kipleBox.model.histroy.TransactionResponse;
import com.kiple.kipleBox.model.histroy.VoidTransactionResponse;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.ApplyPackageResponse;
import com.kiple.kipleBox.model.responses.CashPaymentResponse;
import com.kiple.kipleBox.model.responses.EncryptionResponse;
import com.kiple.kipleBox.model.responses.FailedEntryResponse;
import com.kiple.kipleBox.model.responses.GetValetTicketResponse;
import com.kiple.kipleBox.model.responses.GetValidatedTicketResponse;
import com.kiple.kipleBox.model.responses.LoginResponse;
import com.kiple.kipleBox.model.responses.MiniDashboardResponse;
import com.kiple.kipleBox.model.responses.PackageListResponse;
import com.kiple.kipleBox.model.responses.RegisterResponse;
import com.kiple.kipleBox.model.responses.SimpleStatusResponse;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.model.responses.UpfrontAmountResponse;
import com.kiple.kipleBox.model.responses.UserDetailResponse;
import com.kiple.kipleBox.model.responses.UserListResponse;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by xenhao.yap on 22/01/2018.
 */

public interface APIInterface {

    /**
     *  device activation
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/authorize_code")
    Call<ActivationResponse> deviceActivation(@Body Map<String, Object> paramMap);

    /**
     *  POS config update
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/refresh_config")
    Call<ActivationResponse> updateConfig(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  Version & system Check API
     */
    @Headers("Content-Type: application/json")
    @POST("/application/status")
    Call<APIErrorBody> checkStatus(@Body Map<String, Object> params);

    /**
     *  manager_activation
     */
    @Headers("Content-Type: application/json")
    @POST("/api/user/login")
    Call<LoginResponse> login(@Body Map<String, Object> paramMap);

    /**
     *  update user (manager)
     */
    @Headers("Content-Type: application/json")
    @POST("/api/user/update")
    Call<UserListResponse> updateUser(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  delete user
     */
    @Headers("Content-Type: application/json")
    @POST("/api/user/delete")
    Call<SimpleStatusResponse> deleteUser(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  register user (staff)
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("/api/user/register")
    Call<RegisterResponse> registerUser(@Body Map<String, Object> paramMap);

    /**
     *  user list
     */
    @Headers("Content-Type: application/json")
    @POST("/api/user")
    Call<UserListResponse> userList(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  issue ticket
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/issues_ticket")
    Call<SimpleStatusResponse> issueTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  latest failed entry
     */
    @Headers("Content-Type: application/json")
    @GET("/api/lpr/valet/failed_entry")
    Call<FailedEntryResponse> failedEntry(@Header("Authorization") String accessToken, @Query("site_id") String site_id);

    /**
     *  search validated ticket
     */
    @Headers("Content-Type: application/json")
    @POST("/api/validation/ticket")
    Call<GetValidatedTicketResponse> getValidatedTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  print ticket from list
     */
    @Headers("Content-Type: application/json")
    @GET("/api/pos/valet/ticket")
    Call<SimpleStatusResponse> printTicketFromList(@Header("Authorization") String accessToken, @Query("ticket_data") String ticketData);

    /**
     *  full ticket lists
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/get_ticket_list")
    Call<ArrayList<TicketListsResponse>> fullTicketList(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  homepage mini dashboard
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/mini_dashboard")
    Call<ArrayList<MiniDashboardResponse>> miniDashboard(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

//    /**
//     *  update ticket status to parked
//     */
//    @Headers({"Content-Type: multipart/form-data", "Accept-Encoding: gzip, deflate"})
//    @Multipart
//    @POST("/api/pos/valet/ticket_update_2_park")
//    Call<ResponseBody> vehicleParked(@Header("Authorization") String accessToken, @Part List<MultipartBody.Part> images, @PartMap() Map<String, RequestBody> paramMap);

//    /**
//     *  update ticket status to parked
//     */
//    @Headers({"Content-Type: multipart/form-data", "Accept-Encoding: gzip, deflate"})
//    @Multipart
//    @POST("api/pos/valet/ticket_update_2_park")
//    Call<ResponseBody> vehicleParked(@Header("Authorization") String accessToken,
//                                     @Part MultipartBody.Part img1,
//                                     @Part MultipartBody.Part img2,
//                                     @Part MultipartBody.Part img3,
//                                     @Part MultipartBody.Part img4,
//                                     @Part MultipartBody.Part img5,
//                                     @Part MultipartBody.Part img6,
//                                     @Part("user_id") RequestBody user_id,
//                                     @Part("locationpark") RequestBody locationpark,
//                                     @Part("pos_id") RequestBody pos_id,
//                                     @Part("datetime") RequestBody datetime,
//                                     @Part("site_id") RequestBody site_id,
//                                     @Part("ticket_id") RequestBody ticket_id);

    /**
     *  update ticket status to parked (base64 encoded images)
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/ticket_update_2_park")
    Call<ResponseBody> vehicleParked(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  cash payment
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/payment/cash")
    Call<CashPaymentResponse> cashPayment(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  update status to ready for collection
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/ticket_update_2_collect")
    Call<SimpleStatusResponse> parkingStatusChange(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     * TRANSACTION HISTORY
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/history/transaction")
    Call<TransactionResponse> historyTransaction(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     * TRANSACTION SUMMARY
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/history/summary")
    Call<SummaryResponse> historySummary(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     * HISTORY VOID TRANSACTION
     */
    @Headers("Content-Type: application/json")
    @POST("/api/user/supervisor-confirmation")
    Call<VoidTransactionResponse> voidTransaction(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     * HISTORY VOID REMARK
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/payment/void")
    Call<VoidTransactionResponse> voidRemarkTransaction(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  logout
     */
    @POST("/api/user/logout")
    Call<SimpleStatusResponse> logout(@Header("Authorization") String accessToken);

    /**
     *  get user details
     */
    @POST("/api/user/details")
    Call<UserDetailResponse> getDetails(@Header("Authorization") String accessToken);

    /**
     *  resend activity_activation code
     */
    @POST("/api/pos/resend_activation_code")
    Call<SimpleStatusResponse> resendActivation(@Body Map<String, Object> paramMap);

    /**
     *  encrypt ticket data
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/encrypt_ticket")
    Call<EncryptionResponse> encryptTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  decrypt ticket data
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/decrypt_ticket")
    Call<EncryptionResponse> decryptTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  get valet ticket
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/valet/get_ticket")
    Call<GetValetTicketResponse> getValetTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  get hybrid ticket
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/hybrid/get_ticket")
    Call<GetValetTicketResponse> getHybridTicket(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  get package list
     */
    @Headers("Content-Type: application/json")
    @GET("/api/parking/package")
    Call<PackageListResponse> getPackageList(@Header("Authorization") String accessToken, @Query("ticket_id") String ticket_id, @Query("site_id") String site_id);

    /**
     *  select package
     */
    @Headers("Content-Type: application/json")
    @POST("/api/pos/ticket/update_rate_package")
    Call<ApplyPackageResponse> applyPackage(@Header("Authorization") String accessToken, @Body Map<String, Object> paramMap);

    /**
     *  upfront payment price list
     */
    @Headers("Content-Type: application/json")
    @GET("/api/pos/upfront_payment")
    Call<UpfrontAmountResponse> upfrontAmount(@Header("Authorization") String accessToken, @Query("site_id") String site_id);

}
