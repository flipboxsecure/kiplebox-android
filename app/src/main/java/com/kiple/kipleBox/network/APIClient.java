package com.kiple.kipleBox.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kiple.kipleBox.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by xenhao.yap on 22/01/2018.
 */

public class APIClient {

    private static Retrofit retrofit = null;
    private static Retrofit base_retrofit = null;

    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

//    public static Retrofit getAuth() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.AUTH_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

//    public static Retrofit getKipleUrl() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.KIPLE_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

//    public static Retrofit getKipleConnect() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.KIPLE_CONNECT)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

//    public static Retrofit getProfile() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.PROFILE_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

//    public static Retrofit getWallet() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.WALLET_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

    public static Retrofit getTempBase() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        if(BuildConfig.DEBUG) {
            client.addInterceptor(interceptor);
        }

        retrofit = new Retrofit.Builder()
//                    .baseUrl(Config.DOMAIN_DEV)
                .baseUrl("http://594fc8cc.ngrok.io/kiplepark-services/public/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client.build())
                .build();

        return retrofit;
    }

    public static Retrofit getBase() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        if(BuildConfig.DEBUG) {
            client.addInterceptor(interceptor);
        }

        retrofit = new Retrofit.Builder()
//                    .baseUrl(Config.DOMAIN_DEV)
                .baseUrl(BuildConfig.PARKING_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client.build())
                .build();

        return retrofit;
    }

    public static Retrofit getBaseParallel() {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(50);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.dispatcher(dispatcher);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if(BuildConfig.DEBUG) {
            client.addInterceptor(interceptor);
        }

        if(base_retrofit==null){
             retrofit=new Retrofit.Builder()
                    .baseUrl(BuildConfig.PARKING_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client.build())
                    .build();
            return retrofit;
        } else {
            return base_retrofit;
        }
    }

    public static Retrofit getLogin() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder().addInterceptor(interceptor);
        client.readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        if(BuildConfig.DEBUG) {
            client.addInterceptor(interceptor);
        }

        retrofit = new Retrofit.Builder()
//                    .baseUrl(Config.DOMAIN_DEV)
                .baseUrl(BuildConfig.LOGIN_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client.build())
                .build();

        return retrofit;
    }

//    public static Retrofit getClientNoConvert() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.AUTH_URL)
////                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

//    public static Retrofit checkVersion() {
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//
//        retrofit = new Retrofit.Builder()
////                    .baseUrl(Config.DOMAIN_DEV)
//                .baseUrl(BuildConfig.VERSION_CHECK)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        return retrofit;
//    }

    public static long getCallDuration(Response response){
        return response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis();
    }
}
