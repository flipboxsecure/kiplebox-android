package com.kiple.kipleBox.network;

import android.content.Context;

import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;

import java.util.HashMap;

import retrofit2.Call;

public class APICall {

    private static APIInterface baseInterface = APIClient.getBaseParallel().create(APIInterface.class);
    private static APIInterface loginInterface = APIClient.getLogin().create(APIInterface.class);

    public static void activation(CallBackInterface callBackInterface, Context context, HashMap<String, Object> activationParams){
        Call call = baseInterface.deviceActivation(activationParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.ACTIVATION);
    }

    public static void updateConfig(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> configParams){
        Call call = baseInterface.updateConfig(token, configParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.CONFIG_UPDATE);
    }

    public static void login(CallBackInterface callBackInterface, Context context, HashMap<String, Object> loginParams){
        Call call = loginInterface.login(loginParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.LOGIN);
    }

    public static void updateManager(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> updateParams){
        Call call = loginInterface.updateUser(token, updateParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.UPDATE);
    }

    public static void updateManager(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> updateParams, Constants.ApiFlags apiFlag){
        Call call = loginInterface.updateUser(token, updateParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, apiFlag);
    }

    public static void deleteUser(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> deleteParams){
        Call call = loginInterface.deleteUser(token, deleteParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.DELETE_USER);
    }

    public static void registerUser(CallBackInterface callBackInterface, Context context, HashMap<String, Object> updateParams){
        Call call = loginInterface.registerUser(updateParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.REGISTER);
    }

    public static void staffList(boolean isShowSpinner, CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> staffListParams){
        Call call = loginInterface.userList(token, staffListParams);
        APICallback.apiCallback(isShowSpinner, callBackInterface, context, call, true, Constants.ApiFlags.USER_LIST);
    }

    public static void issueTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> issueTicketParams){
        Call call = baseInterface.issueTicket(token, issueTicketParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.ISSUE_TICKET);
    }

    public static void fullTicketList(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> issueTicketParams){
        Call call = baseInterface.fullTicketList(token, issueTicketParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.GET_FULL_TICKET_LISTS);
    }
    public static void fullTicketList(boolean showSpinner, CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> issueTicketParams){
        Call call = baseInterface.fullTicketList(token, issueTicketParams);
        APICallback.apiCallback(showSpinner, callBackInterface, context, call, true, Constants.ApiFlags.GET_FULL_TICKET_LISTS_SERVICE);
    }

    public static void miniDashboard(boolean isShowSpinner, CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> miniDashboardParams){
        Call call = baseInterface.miniDashboard(token, miniDashboardParams);
        APICallback.apiCallback(isShowSpinner, callBackInterface, context, call, true, Constants.ApiFlags.MINI_DASHBOARD);
    }

    public static void latestEntry(CallBackInterface callBackInterface, Context context, String token, String site_id){
        Call call = baseInterface.failedEntry(token, site_id);
        APICallback.apiCallback(false, callBackInterface, context, call, true, Constants.ApiFlags.FAILED_ENTRY);
    }

    public static void getValidatedTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> getValidatedTicketParam){
        Call call = baseInterface.getValidatedTicket(token, getValidatedTicketParam);
        APICallback.apiCallback(false, callBackInterface, context, call, true, Constants.ApiFlags.GET_VALIDATED_TICKET);
    }

    public static void printTicketFromList(boolean isShowSpinner, CallBackInterface callBackInterface, Context context, String token, String ticketToPrint){
        Call call = baseInterface.printTicketFromList(token, ticketToPrint);
        APICallback.apiCallback(isShowSpinner, callBackInterface, context, call, true, Constants.ApiFlags.PRINT_FROM_LIST);
    }

//    public static void vehicleParked(CallBackInterface callBackInterface, Context context, String token, List<MultipartBody.Part> images, HashMap<String, RequestBody> vehicleParkedParams){
//        Call call = baseInterface.vehicleParked(token, images, vehicleParkedParams);
//        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.VEHICLE_PARKED);
//    }

    public static void vehicleParked(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> vehicleParkedParams){
        Call call = baseInterface.vehicleParked(token, vehicleParkedParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.VEHICLE_PARKED);
    }

    public static void cashPayment(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> cashPaymentParams){
        Call call = baseInterface.cashPayment(token, cashPaymentParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.CASH_PAYMENT);
    }

    public static void updatePayment(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> updatePaymentParams){
        Call call = baseInterface.cashPayment(token, updatePaymentParams);
        APICallback.apiCallback(false, callBackInterface, context, call, true, Constants.ApiFlags.UPDATE_PAYMENT_SERVICE);
    }

    public static void readyForCollection(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = baseInterface.parkingStatusChange(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.READY_FOR_COLLECTION);
    }

    public static void processCompleted(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = baseInterface.parkingStatusChange(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.PROCESS_COMPLETED);
    }

    public static void historyTransaction(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = baseInterface.historyTransaction(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.HISTORY_TRANSACTION);
    }
    public static void historySummary(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = baseInterface.historySummary(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.HISTORY_SUMMARY);
    }

    //history void txn api
    public static void  historyVoidTransaction(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = loginInterface.voidTransaction(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.HISTORY_VOID_TXN);
    }

    //history void remark api
    public static void  historyVoidRemarkTransaction(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> collectionParams){
        Call call = baseInterface.voidRemarkTransaction(token, collectionParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.HISTORY_VOID_REMARK);
    }

    public static void logout(CallBackInterface callBackInterface, Context context, String token){
        Call call = loginInterface.logout(token);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.LOGOUT);
    }

    public static void getDetails(CallBackInterface callBackInterface, Context context, String token){
        Call call = loginInterface.getDetails(token);
        APICallback.apiCallback(false, callBackInterface, context, call, true, Constants.ApiFlags.USER_DETAILS);
    }

    public static void resendActivation(CallBackInterface callBackInterface, Context context, HashMap<String, Object> param) {
        Call call = baseInterface.resendActivation(param);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.RESEND_ACTIVATION);
    }

    public static void encryptTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> encryptParams){
        Call call = baseInterface.encryptTicket(token, encryptParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.ENCRYPT);
    }

    public static void decryptTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> decryptParams){
        Call call = baseInterface.decryptTicket(token, decryptParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.DECRYPT);
    }

    public static void getValetTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> getTicketParams){
        Call call = baseInterface.getValetTicket(token, getTicketParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.GET_VALET_TICKET);
    }

    public static void getHybridTicket(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> getHybridParams){
        Call call = baseInterface.getHybridTicket(token, getHybridParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.GET_HYBRID_TICKET);
    }

    public static void getPackageList(CallBackInterface callBackInterface, Context context, String token, String ticket_id, String site_id){
        Call call = baseInterface.getPackageList(token, ticket_id, site_id);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.GET_PACKAGE_LIST);
    }

    public static void applyPackage(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> getTicketParams){
        Call call = baseInterface.applyPackage(token, getTicketParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.APPLY_PACKAGE);
    }

    public static void upfrontAmount(CallBackInterface callBackInterface, Context context, String token, String site_id){
        Call call = baseInterface.upfrontAmount(token, site_id);
        APICallback.apiCallback(false, callBackInterface, context, call, true, Constants.ApiFlags.UPFRONT_AMOUNT);
    }

    public static void maxChargePayment(CallBackInterface callBackInterface, Context context, String token, HashMap<String, Object> maxChargePaymentParams){
        Call call = baseInterface.cashPayment(token, maxChargePaymentParams);
        APICallback.apiCallback(true, callBackInterface, context, call, true, Constants.ApiFlags.MAX_CHARGE);
    }
}
