package com.kiple.kipleBox.common;

import androidx.fragment.app.Fragment;

public interface OnFragmentChangeListener {
	public void doFragmentChange(Fragment f, boolean history, String title);

	public void doFragmentChange(boolean doAnimation, Fragment f, boolean history, String title);

	public void doFragmentChange(String animationFlag, Fragment f, boolean history, String title);

	public void doLogout();

	public void triggerBack();

	public void hideBottomNavigation();

	public void showBottomNavigation();

	public void setBottomIcon(int index);

	public boolean getBottomStatus();

	public void changeTheme(int theme);

}
