package com.kiple.kipleBox.common;

import android.content.Context;
import android.content.Intent;
import android.os.Process;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.io.PrintWriter;
import java.io.StringWriter;

public class CustomUncaughtExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
    private final Context myContext;
    private final Class<?> myActivityClass;

    public CustomUncaughtExceptionHandler(Context context, Class<?> c) {

        myContext = context;
        myActivityClass = c;

    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);// You can use LogCat too
        Intent intent = new Intent(myContext, myActivityClass);
        String s = stackTrace.toString();
        //  log crash with firebase
        FirebaseCrashlytics.getInstance().recordException(new Exception(stackTrace.toString()));
        //you can use this String to know what caused the exception and in which Activity
        intent.putExtra(Constants.UNCAUGHT_EXCEPTION,
                "Exception is: " + stackTrace.toString());
        intent.putExtra("stacktrace", s);
        myContext.startActivity(intent);
        //for restarting the Activity
        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}
