package com.kiple.kipleBox.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.LogUtils;
import com.kiple.kipleBox.utils.NetworkConnectionCheck;
import com.kiple.kipleBox.utils.Utils;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by xenhao.yap on 02/02/2018.
 */

public class APICallback {

    private static Dialog progressDialog = null;
//    private static Handler progressHandler;
//    private static Runnable progressRunnable = new Runnable() {
//        @Override
//        public void run() {
//            //  dismiss progress dialog after 15 seconds if its still showing
//            if (progressDialog != null && progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//        }
//    };

    public static Dialog getDialog() {
        return progressDialog;
    }

    private static Dialog createDialog(Context context) {
        if (null == getDialog()) {
            progressDialog = new Dialog(context, android.R.style.Theme_Translucent);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.custom_progress);
            progressDialog.setCancelable(false);
            progressDialog.setOnCancelListener(progressDialog -> {
                // TODO Auto-generated method stub
            });
        }

        return progressDialog;
    }

    public static void showProgressBar(Context context) {
//        if (Looper.myLooper() == null)
//            Looper.prepare();
//
//        progressHandler = new Handler(context.getMainLooper());
//        progressHandler.postDelayed(progressRunnable, 15000);


        // only create the dialog if the dialog's context hasn't been finished or destroyed
        if (null != progressDialog) {
            Context mContext = ((ContextWrapper) progressDialog.getContext()).getBaseContext();
            if (mContext instanceof AppCompatActivity) {
                if (((Activity) mContext).isFinishing() || ((Activity) mContext).isDestroyed()) {
                    progressDialog = null;
                }
            }
        }

        createDialog(context).show();
    }

    public static void hideProgressBar() {
        if (null != getDialog() && getDialog().isShowing()) {
//            progressHandler.removeCallbacks(progressRunnable);

            //get the Context object that was used to create the dialog
            Context context = ((ContextWrapper) getDialog().getContext()).getBaseContext();

            // if the Context used here was an activity AND it hasn't been finished or destroyed
            // then dismiss it
            if (context instanceof AppCompatActivity) {

                // Api >=17
                if (!((Activity) context).isFinishing()) {
                    if (!((Activity) context).isDestroyed()) {
                        getDialog().dismiss();
                    }
                } else {
                    try {
                        progressDialog = null;
//                        getDialog().dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                // if the Context used wasn't an Activity, then dismiss it too
                try {
                    getDialog().dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void apiCallback(final CallBackInterface callBackInterface, Context context, Call call) {
        apiCallback(true, callBackInterface, context, call, false, null);
    }

    public static void apiCallback(boolean showSpinner, final CallBackInterface callBackInterface, Context context, Call call, @Nullable Constants.ApiFlags apiFlags) {
        apiCallback(showSpinner, callBackInterface, context, call, false, apiFlags);
    }

    public static void apiCallback(final CallBackInterface callBackInterface, Context context, Call call, @Nullable Constants.ApiFlags apiFlags) {
        apiCallback(true, callBackInterface, context, call, false, apiFlags);
    }

    public static void apiCallback(final CallBackInterface callBackInterface, Context context, Call call, boolean useCustomError) {
        apiCallback(true, callBackInterface, context, call, useCustomError, null);
    }

    public static void apiCallback(final CallBackInterface callBackInterface, Context context, Call call, boolean useCustomError, @Nullable Constants.ApiFlags apiFlags) {
        apiCallback(true, callBackInterface, context, call, useCustomError, apiFlags);
    }


    public static void apiCallback(boolean showSpinner, final CallBackInterface callBackInterface, Context context, Call call, boolean useCustomError, @Nullable Constants.ApiFlags apiFlags) {
        if (null != context) {
            new NetworkConnectionCheck(hasInternet -> {
                if (hasInternet) {
                    //  has internet connection
                    //  do API call
                    if (showSpinner)
                        showProgressBar(context);
                    call.enqueue(new retrofit2.Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            hideProgressBar();

                            //  special case for token verification
                            //  as long as not status 200, return as error
                            if (apiFlags != null && apiFlags.equals(Constants.ApiFlags.VERIFY_TOKEN) && response.code() != 200) {
                                callBackInterface.onError(response, apiFlags);
                                LogUtils.LOGI("APICallback", response.toString());
                                return;
                            }

                            switch (response.code()) {
                                case 200:
                                    LogUtils.LOGE(this.getClass().getSimpleName(), "apiFlags: " + apiFlags);
                                    callBackInterface.onSuccessCallBack(response, apiFlags);
                                    break;

                                case 401:
                                case 403:
                                case 400:
                                    if (apiFlags != null)
                                        LogUtils.LOGE(apiFlags.toString(), "api call failed");
                                    if (useCustomError)
                                        callBackInterface.onError(response, apiFlags);
                                    else
                                        onError(response);
                                    break;

                                case 500:
                                    //  condition for ticket check service, no dialog needed
                                    if(apiFlags == Constants.ApiFlags.GET_FULL_TICKET_LISTS_SERVICE) {
                                        FirebaseCrashlytics.getInstance().recordException(new Exception("500 error for: " + apiFlags + "\nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss") + "\n" + response.message()));
                                        return;
                                    }

                                    if ((apiFlags == Constants.ApiFlags.VERSION_CHECK && useCustomError) || (apiFlags == Constants.ApiFlags.CASH_PAYMENT && useCustomError) || (apiFlags == Constants.ApiFlags.UPDATE_PAYMENT_SERVICE && useCustomError)) {
                                        callBackInterface.onError(response, apiFlags);
                                    } else {
                                        FirebaseCrashlytics.getInstance().recordException(new Exception("500 error for: " + apiFlags + "\nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss") + "\n" + response.message()));
                                        CustomAlerts.showOKAlert(context, context.getString(R.string.system_error_title), context.getString(R.string.system_error_desc));
//                            final Intent systemError = new Intent(context, ErrorActivity.class);
//                            systemError.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.SYSTEM_ERROR);
//                            context.startActivity(systemError);
                                    }
//                        Toast.makeText(context, context.getString(R.string.internal_server_error), Toast.LENGTH_LONG).show();
//                        if(useCustomError)
//                            callBackInterface.onError(response, apiFlags);
//                        else
//                            onError(response);
                                    break;

                                case 502:
                                    if (useCustomError)
                                        callBackInterface.onError(response, apiFlags);
                                    else
                                        onError(response);
                                    break;

                                case 503:
                                    if((apiFlags == Constants.ApiFlags.CASH_PAYMENT && useCustomError) || (apiFlags == Constants.ApiFlags.UPDATE_PAYMENT_SERVICE && useCustomError))
                                        callBackInterface.onError(response, apiFlags);
                                    else
                                        CustomAlerts.showOKAlert(context, Constants.ERROR_MSG_BUNDLE, Constants.PAYMENT_FAILED);
//                        final Intent paymentFailed = new Intent(context, ErrorActivity.class);
//                        paymentFailed.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.PAYMENT_FAILED);
//                        context.startActivity(paymentFailed);
//                        if(useCustomError)
//                            callBackInterface.onError(response, apiFlags);
//                        else {
//                            CustomAlerts.showOKAlert(context,
//                                    context.getString(R.string.service_unavailable),
//                                    context.getString(R.string.server_down));
//                            onError(response);
//                        }
                                    break;

                                default:
                                    if (useCustomError)
                                        callBackInterface.onError(response, apiFlags);
                                    else
                                        onError(response);
                            }

                            LogUtils.LOGI("APICallback", response.toString());
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            hideProgressBar();

                            //  special case for token verification
                            //  as long as not status 200, return as error
                            if (apiFlags != null && apiFlags.equals(Constants.ApiFlags.VERIFY_TOKEN)) {
                                callBackInterface.onError(null, apiFlags);
                                return;
                            }

                            t.printStackTrace();

                            StringBuilder errDetails = new StringBuilder();
                            try {
                                LogUtils.LOGE(this.getClass().getName() + " onFailure API ", String.valueOf(apiFlags));
                                LogUtils.LOGE(this.getClass().getName() + " onFailure", t.getCause().getLocalizedMessage());
                                LogUtils.LOGE(this.getClass().getName() + " onFailure", t.getMessage());
                                errDetails.append(apiFlags).append("\n");
                                errDetails.append(t.getCause().getLocalizedMessage()).append("\n");
                                errDetails.append(t.getMessage()).append("\n");
                            } catch (NullPointerException e) {
                                LogUtils.LOGE(this.getClass().getName() + " onFailure API ", String.valueOf(apiFlags));
                                LogUtils.LOGE(this.getClass().getName() + " onFailure", "throwable message empty");
                                e.printStackTrace();
                                errDetails.append(apiFlags).append("\n");
                                errDetails.append("throwable message empty");
                            }

                            //  payment API error response
                            if (apiFlags != null && apiFlags.equals(Constants.ApiFlags.CASH_PAYMENT) || apiFlags != null && apiFlags.equals(Constants.ApiFlags.UPDATE_PAYMENT_SERVICE)) {
                                callBackInterface.onError(null, apiFlags);
                            }
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                            //  error tracing
                            if ("timeout".equalsIgnoreCase(t.getMessage())) {
//                    if(isContextValid(context)) {
                                FirebaseCrashlytics.getInstance().recordException(new Exception("timeout for: " + apiFlags + "\nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss") + "\n" + errDetails));
                                if (apiFlags != Constants.ApiFlags.GET_FULL_TICKET_LISTS_SERVICE) {
                                    CustomAlerts.showOKAlert(context,
                                            context.getString(R.string.something_went_wrong), context.getString(R.string.connection_timeout));
                                    if (apiFlags == Constants.ApiFlags.GET_VALIDATED_TICKET || apiFlags == Constants.ApiFlags.ISSUE_TICKET)
                                        callBackInterface.onError(null, apiFlags);
                                }
//                    }
                                if (apiFlags != null)
                                    LogUtils.LOGE(apiFlags.toString() + " onFailure", t.getMessage());
                            } else {
//                    if(isContextValid(context)) {
                                if (apiFlags != Constants.ApiFlags.GET_FULL_TICKET_LISTS_SERVICE) {
                                    FirebaseCrashlytics.getInstance().recordException(new Exception("API calling failed for: " + apiFlags + "\nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss") + "\n" + errDetails));
                                    CustomAlerts.showOKAlert(context,
                                            context.getString(R.string.error), context.getString(R.string.oops_something_went_wrong));
                                }
                            }
//                }
                        }
                    });
                } else {

                    //  payment API no internet
                    if (apiFlags != null && apiFlags.equals(Constants.ApiFlags.CASH_PAYMENT) || apiFlags != null && apiFlags.equals(Constants.ApiFlags.UPDATE_PAYMENT_SERVICE)) {
                        callBackInterface.onError(null, apiFlags);
                    }

                    //  no internet connection
                    if (apiFlags != Constants.ApiFlags.GET_FULL_TICKET_LISTS_SERVICE)
                        Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static void onError(Response response) {
        LogUtils.LOGI("APICallback", response.toString());
    }

    private static boolean isContextValid(Context context) {
        if (context instanceof AppCompatActivity) {
            AppCompatActivity appCompatActivity = (AppCompatActivity) context;
            return appCompatActivity.isFinishing();
        }

        if (context instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) context;
            return fragmentActivity.isFinishing();
        }

        return context != null;
    }

}
