package com.kiple.kipleBox.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.LoginResponse;
import com.kiple.kipleBox.model.responses.UserDetailResponse;
import com.kiple.kipleBox.utils.Utils;

/**
 * Created by xenhao.yap on 29/01/2018.
 */

public class SessionManager {

    private static SessionManager instance;
    private Context context;
    private final SharedPreferences session_prefs, setting_prefs;
    private byte[] imageDatas;//tmp save for update profile

    private SessionManager(Context context) {
        this.context = context;
        session_prefs = context.getSharedPreferences(Constants.SESSION_PREF_NAME, Context.MODE_PRIVATE);
//        setting_prefs = context.getSharedPreferences(getUserId() + Constants.SETTINGS_PREF_NAME, Context.MODE_PRIVATE);
        setting_prefs = context.getSharedPreferences(Constants.SETTINGS_PREF_NAME, Context.MODE_PRIVATE);
        instance = this;
    }

    public static SessionManager getInstance(Context context) {
        return instance != null ? instance : new SessionManager(context);
    }

    public void savePrinterStatus(boolean hasPrinter){
        SharedPreferences.Editor editor = setting_prefs.edit();
        editor.putBoolean(Constants.PRINTER_STATUS, hasPrinter);
        editor.apply();
    }

    public boolean getPrinterStatus(){
        return setting_prefs.getBoolean(Constants.PRINTER_STATUS, false);
    }

    public void saveActivationInfo(ActivationResponse info){
        SharedPreferences.Editor editor = setting_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(info);
        editor.putString(Constants.SESSION_INFO, json);
        editor.apply();
    }

    public ActivationResponse getActivationInfo(){
        Gson gson = new Gson();
        String json = setting_prefs.getString(Constants.SESSION_INFO, "");
        return gson.fromJson(json, ActivationResponse.class);
    }

    public void updateActivationInfo(ActivationResponse info){
        ActivationResponse activationResponse = getActivationInfo();

        if(null != info.getKpCode())
            activationResponse.setKpCode(info.getKpCode());

        if(null != info.getKipleCodeDesc())
            activationResponse.setKipleCodeDesc(info.getKipleCodeDesc());

        if(null != info.getManager_name())
            activationResponse.setManager_name(info.getManager_name());

        if(null != info.getManager_email())
            activationResponse.setManager_email(info.getManager_email());

        if(null != info.getManager_token())
            activationResponse.setManager_token(info.getManager_token());

        if(null != info.getSite_id())
            activationResponse.setSite_id(info.getSite_id());

        if(null != info.getSite_name())
            activationResponse.setSite_name(info.getSite_name());

        if(null != info.getPhone_number())
            activationResponse.setPhone_number(info.getPhone_number());

        if(null != info.getStreet1())
            activationResponse.setStreet1(info.getStreet1());

        if(null != info.getStreet2())
            activationResponse.setStreet2(info.getStreet2());

        if(null != info.getPostcode())
            activationResponse.setPostcode(info.getPostcode());

        if(null != info.getCity())
            activationResponse.setCity(info.getCity());

        if(null != info.getState())
            activationResponse.setState(info.getState());

        if(null != info.getPos_id())
            activationResponse.setPos_id(info.getPos_id());

        if(null != info.getPos_info1())
            activationResponse.setPos_info1(info.getPos_info1());

        if(null != info.getPos_info2())
            activationResponse.setPos_info2(info.getPos_info2());

        if(null != info.getIdentify())
            activationResponse.setIdentify(info.getIdentify());

        if(null != info.getRunning_no_start())
            activationResponse.setRunning_no_start(info.getRunning_no_start());

        if(null != info.getOld_deletion_after())
            activationResponse.setOld_deletion_after(info.getOld_deletion_after());

        if(null != info.getInactivity_allow())
            activationResponse.setInactivity_allow(info.getInactivity_allow());

        if(null != info.getValidity_for_use())
            activationResponse.setValidity_for_use(info.getValidity_for_use());

        if(null != info.getDateActivated())
            activationResponse.setDateActivated(info.getDateActivated());

        if(null != info.getDateExpired())
            activationResponse.setDateExpired(info.getDateExpired());

        if(null != info.getDelimeter())
            activationResponse.setDelimeter(info.getDelimeter());

        if(null != info.getGrace_period())
            activationResponse.setGrace_period(info.getGrace_period());

        if(null != info.getRate_formula())
            activationResponse.setRate_formula(info.getRate_formula());

        if(null != info.getPartner_name())
            activationResponse.setPartner_name(info.getPartner_name());

        if(null != info.getGst_id())
            activationResponse.setGst_id(info.getGst_id());

        if(null != info.getService_customize())
            activationResponse.setService_customize(info.getService_customize());

        if(null != info.getTimezone())
            activationResponse.setTimezone(info.getTimezone());

        if(null != info.getParkingValidationFlag())
            activationResponse.setParkingValidationFlag(info.getParkingValidationFlag());

        if(null != info.getRunningNoLenght())
            activationResponse.setRunningNoLenght(info.getRunningNoLenght());

        if(null != info.getManagerId())
            activationResponse.setManagerId(info.getManagerId());

        if(null != info.getPosNo())
            activationResponse.setPosNo(info.getPosNo());

        SharedPreferences.Editor editor = setting_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(activationResponse);
        editor.putString(Constants.SESSION_INFO, json);
        editor.apply();

    }

    public void setShowTicket(boolean showTicket){
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putBoolean(Constants.TICKET_DISPLAY_FEATURE, showTicket);
        editor.apply();
    }

    public boolean showTicket(){
        return session_prefs.getBoolean(Constants.TICKET_DISPLAY_FEATURE, true);
    }

    public void setHybridFlag(boolean flag){
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putBoolean(Constants.HYBRID_FEATURE, flag);
        editor.apply();
    }

    public boolean isHybrid(){
        return session_prefs.getBoolean(Constants.HYBRID_FEATURE, false);
    }

    public void saveActiveUserInfo(LoginResponse info){
        SharedPreferences.Editor editor = session_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(info);
        editor.putString(Constants.ACTIVE_USER_INFO, json);
        editor.apply();
    }

    public LoginResponse getActiveUserInfo(){
        Gson gson = new Gson();
        String json = session_prefs.getString(Constants.ACTIVE_USER_INFO, "");
        return gson.fromJson(json, LoginResponse.class);
    }

    public void saveActiveUserDetails(UserDetailResponse details){
        SharedPreferences.Editor editor = session_prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(details);
        editor.putString(Constants.ACTIVE_USER_DETAILS, json);
        editor.apply();
    }

    public UserDetailResponse getActiveUserDetails(){
        Gson gson = new Gson();
        String json = session_prefs.getString(Constants.ACTIVE_USER_DETAILS, "");
        return gson.fromJson(json, UserDetailResponse.class);
    }

    public void saveActiveUserEmail(String email){
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.ACTIVE_USER_EMAIL, email);
        editor.apply();
    }

    public void saveActiveUserName(String name){
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.ACTIVE_USER_NAME, name);
        editor.apply();
    }

    public String getActiveUser(){
        return session_prefs.getString(Constants.ACTIVE_USER, null);
    }

    public int getRunningNumber(){
        int num = setting_prefs.getInt(Constants.RUNNING_NUMBER, getActivationInfo().getRunning_no_start());
        if(num >= (getActivationInfo().getRunning_no_start() + Constants.RUNNING_NUMBER_INCREMENT)){
            return getActivationInfo().getRunning_no_start();
        } else{
            return num;
        }
    }

    public void saveRunningNumber(int num){
        SharedPreferences.Editor editor = setting_prefs.edit();
        editor.putInt(Constants.RUNNING_NUMBER, ++num);
        editor.apply();
    }

    public boolean checkActivationInfoExists(){
        return !setting_prefs.getString(Constants.SESSION_INFO, "").equalsIgnoreCase("");
    }

    public boolean checkUserSessionInfoExists(){
        return !session_prefs.getString(Constants.SESSION_INFO, "").equalsIgnoreCase("");
    }


    public void saveNavigationPreference(String naviPref){
        SharedPreferences.Editor editor = setting_prefs.edit();
        editor.putString(Constants.NAVIGATION_KEY, naviPref);
        editor.apply();
    }

    public String getNavigationPreference(){
        return setting_prefs.getString(Constants.NAVIGATION_KEY, Constants.GOOGLE_NAVI);
    }

    public String getToken() {
        return session_prefs.getString(Constants.ACCESS_TOKEN, null);
    }

    public void TokenSent(boolean isSent) {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putBoolean(Constants.TOKEN_SENT, isSent);
        editor.apply();
    }

    public boolean isTokenSent() {
        return session_prefs.getBoolean(Constants.TOKEN_SENT, false);
    }

    public String getAccountId() {
        return session_prefs.getString(Constants.ACCOUNT_ID, null);
    }

    public void saveToken(String accessToken, String accountId, String username) {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.ACCESS_TOKEN, accessToken);
        editor.putString(Constants.ACCOUNT_ID, accountId);
        editor.putString(Constants.USER_NAME, username);
        editor.apply();
    }

    public String checkUserInfoExists(){
        return session_prefs.getString(Constants.USER_INFO, null);
    }

//    public void saveUserInfo(UserInfoResponse model) {
//        SharedPreferences.Editor editor = session_prefs.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(model);
//        editor.putString(Constants.USER_INFO, json);
//        editor.apply();
//    }
//
//    public UserInfoResponse getUserInfo() {
//        Gson gson = new Gson();
//        String json = session_prefs.getString(Constants.USER_INFO, "");
//        return gson.fromJson(json, UserInfoResponse.class);
//    }
//
//    public void saveTicketList(List<GetTicketResponse> listTransaction){
//        SharedPreferences.Editor editor = session_prefs.edit();
//        Gson gson = new Gson();
//        String jsonList = gson.toJson(listTransaction);
//        editor.putString(Constants.TICKET_LIST, jsonList);
//        editor.apply();
//    }
//
//    public List<GetTicketResponse> getTicketList(){
//        Gson gson = new Gson();
//        Type type = new TypeToken<List<GetTicketResponse>>(){}.getType();
//        String ticketListJson = session_prefs.getString(Constants.TICKET_LIST, "");
//        return gson.fromJson(ticketListJson, type);
//    }
//
//    public String getUserId() {
//        UserInfoResponse user = getUserInfo();
//        if (user != null) {
//            return user.getId();
//        }
//        return "";
//    }
//
//    public String getUserEmail() {
//        UserInfoResponse user = getUserInfo();
//        if (user != null) {
//            return user.getEmail();
//        }
//        return "";
//    }

    public void saveBalanceAsString(String balance) {
        if(balance.equals("0.0"))
            balance = "0.00";
        balance = Utils.formatBalance(balance);
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.BALANCE, balance);
        editor.apply();
    }

//    public boolean hasWallet(){
//        if(getUserInfo().getHasWallet() == null)
//            return false;
//        else
//            return getUserInfo().getHasWallet();
//    }

    public void clearBalanceAsString() {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.remove(Constants.BALANCE);
        editor.apply();
    }

    public String getBalanceAsString() {
        String restoredText = session_prefs.getString(Constants.BALANCE, null);
        if(restoredText != null && restoredText.equals("0.0"))
            restoredText = "0.00";
        return restoredText != null ? session_prefs.getString(Constants.BALANCE, "0.00") : "0.00";
    }

    public void saveEmailAsUsername(String userName) {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.USER_NAME, userName);
        editor.putBoolean(Constants.REMEMBER, false);
        editor.apply();
    }

    public void clear() {
        SharedPreferences.Editor editor = session_prefs.edit();
//        editor.remove(Constants.TOKEN);
//        editor.remove(Constants.ACCOUNT_ID);
//        editor.remove(Constants.FULL_NAME);
//        editor.remove(Constants.AVATAR);
//        editor.remove(Constants.PROVIDER);
//        editor.remove(Constants.ACCESS_TOKEN);
//        editor.remove(Constants.USER_INFO);
//        editor.remove(Constants.BALANCE);
        editor.clear();
        editor.apply();
    }

    public String[] getFBInfo() {
        String[] fbInfos = new String[8];
        fbInfos[0] = session_prefs.getString(Constants.TOKEN, "");
        fbInfos[1] = session_prefs.getString(Constants.USER_NAME, "");
        fbInfos[2] = session_prefs.getString(Constants.FULL_NAME, "");
        fbInfos[4] = session_prefs.getString(Constants.AVATAR, "");
        fbInfos[5] = String.valueOf(session_prefs.getInt(Constants.PROVIDER, 0));
        fbInfos[6] = session_prefs.getString(Constants.ACCOUNT_ID, "");
        fbInfos[7] = session_prefs.getString(Constants.USER_ID, "");
        return fbInfos;
    }

    public void saveFBInfo(String tokenFB, String id, String email, String fullName, String avatar, int provider) {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.TOKEN, tokenFB);
        editor.putString(Constants.USER_ID, id);
        editor.putString(Constants.USER_NAME, email);
        editor.putBoolean(Constants.REMEMBER, false);
        editor.putString(Constants.FULL_NAME, fullName);
        editor.putString(Constants.AVATAR, avatar);
        editor.putInt(Constants.PROVIDER, provider);
        editor.apply();
    }

    public String getUserName() {
        return session_prefs.getString(Constants.USER_NAME, null);
    }

//    public void savePIN(String pin) {
//        KipleAccountManager.createAccount(context, pin);
//    }

    private String getPINKeyByCurrentUser() {
        String curUsername = session_prefs.getString(Constants.USER_NAME, "");
        return Constants.PIN + "_" + curUsername;
    }

//    public String getPIN() {
//        return KipleAccountManager.getAccountPin(context);
//    }

    private String getFingerStatusKeyByCurrentUser() {
        String curUsername = setting_prefs.getString(Constants.USER_NAME, "");
        return Constants.FINGER_STATUS + "_" + curUsername;
    }

//    public void deletePIN() {
//        KipleAccountManager.deleteAccount(context);
//    }

    public void deleteUser() {
        session_prefs.edit().remove(Constants.USER_NAME).apply();
    }

    public void setFingerStatus(boolean isOn) {
        SharedPreferences.Editor editor = setting_prefs.edit();
        editor.putBoolean(getFingerStatusKeyByCurrentUser(), isOn);
        editor.apply();
    }

    public boolean isFingerOn() {
        return setting_prefs.getBoolean(getFingerStatusKeyByCurrentUser(), false);
    }

    public void saveFCMToken(String token) {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.FCM_TOKEN, token);
        editor.apply();
    }

    public String getFCMTokenCheck() {
        return session_prefs.getString(Constants.FCM_TOKEN, "");
    }


//    public String getFCMToken() {
//        String token = prefs.getString(Constants.FCM_TOKEN, "");
//        if (TextUtils.isEmpty(token)) {
//            token = FirebaseInstanceId.getInstance().getToken();
//            saveFCMToken(token);
//        }
//        return token;
//    }

    public String getDeviceUDID() {
        return Utils.getDeviceUDID(this.context);
    }

    public void saveFeedback(String name, String mobile, String email,
                             String subject, String message){
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.putString(Constants.FEEDBACK_NAME, name);
        editor.putString(Constants.FEEDBACK_MOBILE, mobile);
        editor.putString(Constants.FEEDBACK_EMAIL, email);
        editor.putString(Constants.FEEDBACK_SUBJECT, subject);
        editor.putString(Constants.FEEDBACK_MESSAGE, message);
        editor.apply();
    }

    public String[] getFeedback(){
        String[] feedbackInfo = new String[5];
        feedbackInfo[0] = session_prefs.getString(Constants.FEEDBACK_NAME, "");
        feedbackInfo[1] = session_prefs.getString(Constants.FEEDBACK_MOBILE, "");
        feedbackInfo[2] = session_prefs.getString(Constants.FEEDBACK_EMAIL, "");
        feedbackInfo[3] = session_prefs.getString(Constants.FEEDBACK_SUBJECT, "");
        feedbackInfo[4] = session_prefs.getString(Constants.FEEDBACK_MESSAGE, "");
        return feedbackInfo;
    }

    public void clearFeedback() {
        SharedPreferences.Editor editor = session_prefs.edit();
        editor.remove(Constants.FEEDBACK_NAME);
        editor.remove(Constants.FEEDBACK_MOBILE);
        editor.remove(Constants.FEEDBACK_EMAIL);
        editor.remove(Constants.FEEDBACK_SUBJECT);
        editor.remove(Constants.FEEDBACK_MESSAGE);
        editor.apply();
    }

    public byte[] getImageDatas() {
        return imageDatas;
    }

    public void setImageDatas(byte[] data) {
        imageDatas = data;
    }
}
