package com.kiple.kipleBox.common;

/**
 * Created by xenhao.yap on 28/01/2018.
 */

public class Constants {

    //  credentials
    public final static String API_KEY = "r6w845CbAX3OYRp9VttAy8x1F6m9L8p59QEFfLUY";
    public final static String APPSEE_API_KEY = "abc0024fb22e4238818ba84c90d845c9";

    //  printer status
    public final static String SUCCESS = "success";
    public final static String INCOMPATIBLE_HARDWARE = "incompatible hardware";
    public final static String NOT_PAX_HARDWARE = "not pax hardware";
    public final static String NOT_PAX_PRINTER = "connect error";
    public final static String SUNMI_PRINTER = "sunmi_printer";
    public final static String PAX_PRINTER = "pax_printer";
    public final static String NO_PRINTER = "no_printer";
    public final static int PAX_END_MARGIN = 150;

    //  global variable for server date
    public static String SERVER_TIME = "";

    //  uncaught exception handling
    public final static String UNCAUGHT_EXCEPTION = "uncaught_exception";

    // # milliseconds, desired time passed between two back presses.
    public static final int TIME_INTERVAL = 2000;

    //  location constants
    public final static int HORIZONTAL_ACCURACY = 20;
    public final static int VERTICAL_ACCURACY = 20;

    //  parking site situational state
    public final static String COMING_SOON = "coming soon";
    public final static String ACTIVE = "active";

    //  google API client ID
    public final static int GOOGLE_SIGNIN_ID = 0;
    public final static int LOCATION_SERVICE_ID = 1;

    public static final String DATE_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss";
    public final static String AUTHORIZATION = "Bearer ";
    public final static String BALANCE_IS_NULL = "nil";
    public final static int LOCATION_PERMISSION = 9999;
    public final static String SEARCH = "search";
    public final static String HOME = "home";
    public final static String PAGE_LIMIT = "10";
    public final static String NORMAL_PARKING = "Normal Parking";
    public final static String WALLET = "Wallet";
    public final static String PRINT_LOGO = "logo";
    public final static String COMPANY_NAME = "company_name";
    public final static String PER_ENTRY = "per_entry";
    public final static String BIG_EXTERNAL_TAG = "KiplePark";
    public final static String SMALL_EXTERNAL_TAG = "kiplePark";

    //  parking query radius
    public static final String RADIUS = "1";
    public static final String RADIUS_SHORT = "0.5";

    //  timer constants (milliseconds)
    public static final int OTP_COOLDOWN = 30000;
    public static final int TIMER_INTERVAL = 1000;
    public static final long TEN_SECONDS = 10000;
    public static final long ZERO_SECONDS = 0;
    public static final int MINUTE_INTERVAL = 1000;

    //  animation status
    public static final String NO_ANIMATION = "no_animation";
    public static final String SLIDE_ANIMATION = "slide_animation";
    public static final String REVERSE_SLIDE_ANIMATION = "reverse_slide_animation";
    public static final String VERTICAL_ANIMATION = "vertical_animation";
    public static final String ACTIVITY_ANIMATION = "activity_animation";

    //  session info
    public static final String SESSION_PREF_NAME = "session";
    public static final String SETTINGS_PREF_NAME = "settings";
    public static final String PRINTER_STATUS = "printer_status";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String ACCOUNT_ID = "accountID";
    public static final String TOKEN = "token";
    public static final String FULL_NAME = "fullName";
    public static final String AVATAR = "avatar";
    public static final String PROVIDER = "provider";
    public static final String SESSION_INFO = "sessionInfo";
    public static final String USER_INFO = "userInfo";
    public static final String BALANCE = "balance";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String PIN = "pin";
    public static final String REMEMBER = "remember";
    public static final String TICKET_LIST = "ticket_list";
    public static final String GOOGLE_NAVI = "Google Maps";
    public static final String WAZE_NAVI = "Waze";
    public static final String NAVIGATION_KEY = "navigation";
    public static final String FINGER_STATUS = "finger_status";
    public static final String ACTIVE_USER = "active_user";
    public static final String ACTIVE_USER_EMAIL = "active_user_email";
    public static final String ACTIVE_USER_NAME = "active_user_name";
    public static final String ACTIVE_USER_INFO = "active_user_info";
    public static final String ACTIVE_USER_DETAILS = "active_user_details";

    //  running number interval
    public static final int RUNNING_NUMBER_INCREMENT = 1000000;
    public static final String RUNNING_NUMBER = "running_number";

    public static final String TOKEN_SENT = "token_sent";

    //  length limit
    public static final int NUMBER_PLATE_LENGTH = 15;
    public static final int PHONE_NUMBER_LENGTH = 15;

    //  feedback data
    public static final String FEEDBACK_NAME = "feedback_name";
    public static final String FEEDBACK_MOBILE = "feedback_mobile";
    public static final String FEEDBACK_EMAIL = "feedback_email";
    public static final String FEEDBACK_SUBJECT = "feedback_subject";
    public static final String FEEDBACK_MESSAGE = "feedback_message";

    //  payment method
    public static final String CASH_PAYMENT = "Cash";
    public static final String CHARGE_TO_ROOM = "Guest Room";
    public static final String CHARGE_TO_MASTER = "Master Account";
    public static final String CREDIT_DEBIT = "credit/debit";
    public static final String CREDIT_CARD = "Credit";
    public static final String DEBIT_CARD = "Debit";

    //  bundle data
    public static final String LOGIN_TYPE_BUNDLE = "login_type";
    public static final String LOGIN_EMAIL_BUNDLE = "login_email";
    public static final String ACTIVE_USERNAME_BUNDLE = "username";
    public static final String EDIT_PROFILE_BUNDLE = "edit_profile_bundle";
    public static final String ACCOUNTID_BUNDLE = "accountId";
    public static final String EMAIL_BUNDLE = "email";
    public static final String FULLNAME_BUNDLE = "fullname";
    public static final String PARKING_DETAIL_BUNDLE = "parking_details";
    public static final String WEBVIEW_BUNDLE = "webview_bundle";
    public static final String ERROR_MSG_BUNDLE = "error_bundle";
    public static final String ABOUT_BUNDLE = "about_webview";
    public static final String PRIVACY_POLICY_BUNDLE = "privacy_webview";
    public static final String TERMS_OF_USE_BUNDLE = "terms_webview";
    public static final String NUMBER_PLATE_BUNDLE = "number_plate";
    public static final String MOBILE_NUMBER_BUNDLE = "mobile_number";
    public static final String KEY_COLLECTION_BUNDLE = "key_collection";
    public static final String CHECK_IN_TIME_BUNDLE = "check_in_time";
    public static final String TICKET_ISSUE_DATE_BUNDLE = "ticket_issue_date";
    public static final String TICKET_ISSUE_TIME_BUNDLE = "ticket_issue_time";
    public static final String TICKET_DATA_BUNDLE = "ticket_data";
    public static final String TICKET_ID_BUNDLE = "ticket_id";
    public static final String RUNNING_ID_BUNDLE = "running_id";
    public static final String TICKET_LIST_TYPE = "ticket_list_type";
    public static final String CAR_TO_HIGHLIGHT = "car_to_highlight";
    public static final String TICKET_INFO_BUNDLE = "ticket_info";
    public static final String TICKET_UPFRONT_BUNDLE = "ticket_upfront";
    public static final String TICKET_UPFRONT_FLAG_BUNDLE = "ticket_upfront_flag";
    public static final String NEW_PRINT_RECEIPT = "new_print_receipt";
    public static final String PRINT_TICKET = "print_ticket";
    public static final String PRINT_DASHBOARD_COPY = "dashboard_copy";
    public static final String PRINT_STAFF_COPY = "staff_copy";
    public static final String QR_SIZE = "qr_size";
    public static final String STAFF_ISSUER = "staff_issuer";

    //  ticket list type
    public static final String TICKET_ISSUED_LIST = "Issued";
    public static final int TICKET_ISSUED_INT = 1;
    public static final String TICKET_PARKED_LIST = "Parked";
    public static final int TICKET_PARKED_INT = 2;
    public static final String TICKET_PAID_LIST = "Paid";
    public static final int TICKET_PAID_INT = 3;
    public static final String TICKET_PENDING_COLLECTION_LIST = "Pending collection";
    public static final int TICKET_PENDING_COLLECTION_INT = 4;
    public static final String LIST_REFRESH = "refresh_list";

    //  fixed ticketdata length
    public static final int TICKETDATA_LENGTH = 67;

    //  types
    public static final String MANAGER_LOGIN = "manager_login";
    public static final String MANAGER_ACTIVATION = "manager_activation";
    public static final String MANAGER_DETAILS = "manager_details";
    public static final String STAFF_LOGIN = "staff_login";
    public static final String ADD_STAFF = "add_staff";
    public static final String EDIT_STAFF = "edit_staff";

    //  error status
    public static final int INSUFFICIENT_BALANCE_CODE = 403;
    public static final String INSUFFICIENT_BALANCE = "insufficient_balance";
    public static final int TICKET_PAID_CODE = 10001;
    public static final String TICKET_PAID = "ticket_paid";
    public static final int TICKET_EXPIRED_CODE = 10002;
    public static final String TICKET_EXPIRED = "ticket_expired";
    public static final int SYSTEM_ERROR_CODE = 500;
    public static final String SYSTEM_ERROR = "system_error";
    public static final int FAILED_REFUND_CODE = 502;
    public static final String FAILED_REFUND = "failed_refund";
    public static final int PAYMENT_FAILED_CODE = 503;
    public static final String PAYMENT_FAILED = "payment_failed";
    public static final int INVALID_TICKET_CODE = 400;
    public static final String INVALID_TICKET = "invalid_ticket";
    public static final int CLOCK_OUT_CODE = 666;
    public static final String CLOCK_OUT = "clock_out";

    //  refund fail bundles
    public static final String REPORT_TICKET_BUNDLE = "report_ticket_bundle";
    public static final String FAILED_REFUND_TICKET_NUMBER = "payment_failed_ticket_number";
    public static final String FAILED_REFUND_TICKET_AMOUNT = "payment_failed_ticket_amount";

    //  fragment title
    public static final String FRAGMENT_SIGNUP = "fragment_sign_up";
    public static final String FRAGMENT_LOGIN = "manager_activation";
    public static final String FRAGMENT_OTP = "fragment_verify_otp";
    public static final String FRAGMENT_FORGOT_PW = "forgot_pw";

    //  ticket payment status
    public static final String PAID_VIA_WALLET = "Paid By Wallet";
    public static final String PAID_VIA_EXTERNAL = "Paid Not Using Wallet";
    public static final String NOT_PAID = "Not Paid";

    //  history flags
    public static final String TODAY = "TODAY";
    public static final String YESTERDAY = "YESTERDAY";
    public static final String LAST_WEEK = "LAST_WEEK";
    public static final String LAST_MONTH = "LAST_MONTH";
    public static final String ONLINEPAYMENT = "OnlinePayment";
    public static final String ONLINETOPUP = "OnlineTopup";

    //  service feature flags
    public static final String YES = "y";
    public static final String NO = "n";
    //  issue ticket
    public static final String KEY_COLLECTION_FEATURE = "car_key_collect";
    public static final String TICKET_DISPLAY_FEATURE = "qr_code_view";
    public static final String GRACE_PERIOD_FEATURE = "grace_period";
    public static final String MOBILE_NUMBER_FEATURE = "mobile_number";
    public static final String RUNNING_NUMBER_FEATURE = "running_number";
    public static final String CAR_PLATE_FEATURE = "plate_number";
    public static final String UPFRONT_PAYMENT_FEATURE = "upfront";
    public static final String PLACEHOLDER_MESSAGE = "PlaceHolderMessage";
    public static final String HYBRID_FEATURE = "Hybrid";
    public static final String LPR_VALET = "lpr_valet";
    //  parked
    public static final String CAR_LOCATION_FEATURE = "car_location";
    public static final String CAR_PICTURES_FEATURE = "pic";
    public static final String SURROUNDING_IMAGE_FEATURE = "surrounding";
    public static final String FRONT_IMAGE_FEATURE = "front";
    public static final String BACK_IMAGE_FEATURE = "back";
    public static final String RIGHT_IMAGE_FEATURE = "right";
    public static final String LEFT_IMAGE_FEATURE = "left";
    public static final String DASHBOARD_IMAGE_FEATURE = "dashboard";
    //  cash payment
    public static final String PRINT_RECEIPT_FEATURE = "print_receipt";
    public static final String PRINT_TICKET_FEATURE = "print_ticket";
    public static final String PAYMENT_BUTTON = "payment_btn";
    public static final String TAX_INFO = "tax_info";
    public static final String PAYMENT_MODE_FEATURE = "payment_mode";
    public static final String PAYMENT_MODE_WALLET = "wallet";
    public static final String PAYMENT_MODE_CASH = "cash";
    public static final String PAYMENT_MODE_GUEST_ROOM = "guest";
    public static final String PAYMENT_MODE_MASTER_ACCOUNT = "master_acct";
    public static final String PAYMENT_SKIP_FLAG = "payment_skip_flag";
    public static final String PAYMENT_USUAL_FLOW = "payment_usual_flow";
    public static final String PAYMENT_SKIP_COLLECT = "payment_skip_collect";
    public static final String PAYMENT_SKIP_COMPLETE = "payment_skip_complete";
    public static final String LOST_TICKET_BUTTON = "lost_ticket_btn";
    public static final String MAX_CHARGE_BUTTON = "max_charge_btn";
    public static final String TICKET_VERSION = "ticket_version";
    public static final String CUSTOM_QR_SIZE = "custom_qr_size";
    public static final String MAXIMUM_CHARGE = "Maximum Charge";
    public static final String LOST_TICKET = "Lost Ticket";
    public static final String DEBIT_CARD_PAYMENT = "MYDEBIT";
    public static final String VALIDATED_TICKET_PAYMENT = "ValidatedTicketPayment";

    //  car collection (paid)
    public static final String ENABLE_FEATURE = "enabled";

    //  api call flags
    public enum ApiFlags{
        VERSION_CHECK, ACTIVATION, VERIFY_TOKEN, LOGIN, USER_LIST, UPDATE, REGISTER, EDIT_PROFILE,
        DELETE_USER, ISSUE_TICKET, GET_FULL_TICKET_LISTS, UPDATE_PAYMENT_SERVICE, GET_FULL_TICKET_LISTS_SERVICE,
        MINI_DASHBOARD, VEHICLE_PARKED, CASH_PAYMENT, READY_FOR_COLLECTION, PROCESS_COMPLETED,
        HISTORY_TRANSACTION, HISTORY_SUMMARY, HISTORY_VOID_TXN, HISTORY_VOID_REMARK, LOGOUT,
        RESEND_ACTIVATION, USER_DETAILS, ENCRYPT, DECRYPT, GET_VALET_TICKET, GET_HYBRID_TICKET,
        GET_PACKAGE_LIST, APPLY_PACKAGE, CONFIG_UPDATE, UPFRONT_AMOUNT, MAX_CHARGE, PRINT_FROM_LIST,
        FAILED_ENTRY, GET_VALIDATED_TICKET
    }
}
