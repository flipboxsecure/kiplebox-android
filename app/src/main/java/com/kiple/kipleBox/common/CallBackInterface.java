package com.kiple.kipleBox.common;

import androidx.annotation.Nullable;

import retrofit2.Response;

/**
 * Created by xenhao.yap on 02/02/2018.
 */

public interface CallBackInterface {
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags);

    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags);
}
