package com.kiple.kipleBox.common;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kiple.kipleBox.BuildConfig;
import com.kiple.kipleBox.utils.ConnectivityListener;
import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;

public class CustomApplication extends Application {

    private static CustomApplication instance;

    private static FirebaseAnalytics mFirebaseAnalytics;

    //  PAX printer object
    private static IDAL dal;
    private static Context appContext;

    public static CustomApplication getInstance(){
        return instance;
    }

    public static CustomApplication get(Context context) {
        return (CustomApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;
        appContext = getApplicationContext();
        dal = getDal();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        registerReceiver(new ConnectivityListener(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

//        if(BuildConfig.FLAVOR.equalsIgnoreCase("live")) {
//            Fabric.with(this, new Crashlytics());
//            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        }
    }

    public static FirebaseAnalytics getFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public static void reportFirebaseCustomEvent(String eventName, Bundle bundle){
        if(BuildConfig.FLAVOR.equalsIgnoreCase("live"))
            mFirebaseAnalytics.logEvent(eventName, bundle);
    }

    public static IDAL getDal(){
        if(dal == null){
            try {
                long start = System.currentTimeMillis();
                dal = NeptuneLiteUser.getInstance().getDal(appContext);
                Log.i("Test","get dal cost: "+(System.currentTimeMillis() - start)+" ms");
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ERROR", "error occurred, DAL is null.");
                Toast.makeText(appContext, "error occurred, DAL is null.", Toast.LENGTH_LONG).show();
            }
        }
        return dal;
    }
}
