package com.kiple.kipleBox.common;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

public class APIErrorBody {

    public static final String INACTIVE = "ACCOUNT_INACTIVE";
    public static final String EMAIL_NOT_VERIFY = "EMAIL_NOT_VERIFY";
    public static final String PHONE_NOT_VERIFY = "PHONE_NUMBER_NOT_VERIFY";

    @SerializedName("Code")
    @Expose
    private String Code;
    @SerializedName("Codes")
    @Expose
    private Integer Codes;
    @SerializedName("Message")
    @Expose
    private String Message;
    @SerializedName("code")
    @Expose
    private String versionCheckCode;

    public APIErrorBody(String message) {
        this.Message = message;
    }

    public static APIErrorBody createInstance(Throwable error) {
        try {
            HttpException httpException = (HttpException) error;
            if (httpException.code() == 500) {
                return new APIErrorBody("Internal server error");
            } else if (httpException.code() == 204) {
                return new APIErrorBody("Content not found");
            }
            ResponseBody body = httpException.response().errorBody();
            if (body != null) {
                return new Gson().fromJson(body.string(), APIErrorBody.class);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static APIErrorBody createInstance(Response response) {
        try {
            if (response.code() == 500) {
                return new APIErrorBody("Internal server error");
            } else if (response.code() == 204) {
                return new APIErrorBody("Content not found");
            }
            ResponseBody body = response.errorBody();
            if (body != null) {
                return new Gson().fromJson(body.string(), APIErrorBody.class);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Integer getCodes() {
        return Codes;
    }

    public void setCodes(Integer codes) {
        Codes = codes;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getVersionCheckCode() {
        return versionCheckCode;
    }

    public void setVersionCheckCode(String versionCheckCode) {
        this.versionCheckCode = versionCheckCode;
    }
}
