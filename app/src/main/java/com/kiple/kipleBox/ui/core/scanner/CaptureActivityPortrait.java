package com.kiple.kipleBox.ui.core.scanner;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatDrawableManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.journeyapps.barcodescanner.CaptureActivity;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.kiple.kipleBox.R;

/**
 * Created by xenhao.yap on 12/02/2018.
 */

public class CaptureActivityPortrait extends CaptureActivity
        implements CompoundBarcodeView.TorchListener {

    private CaptureManager capture;
    private CompoundBarcodeView barcodeScannerView;
    private LinearLayout switchFlashlightButton;
    private TextView flash_label;
    private ImageView flash_icon;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        barcodeScannerView.setTorchListener(this);

        switchFlashlightButton  = findViewById(R.id.switch_flashlight);
        flash_icon              = findViewById(R.id.flash_icon);
        flash_label             = findViewById(R.id.flash_label);

//         if the device does not have flashlight in its camera,
//         then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }else{
            switchFlashlightButton.setVisibility(View.VISIBLE);
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
                flash_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on, this.getTheme()));
            else
                flash_icon.setImageDrawable(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_flash_on));
            flash_label.setText(getString(R.string.turn_on_flash));
        }

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
    }

    @Override
    protected CompoundBarcodeView initializeContent() {
        setContentView(R.layout.activity_scanner);
        return (CompoundBarcodeView)findViewById(R.id.zxing_barcode_scanner);
    }

    public void switchFlashlight(View view) {
        if (flash_label.getText().toString().equalsIgnoreCase(getString(R.string.turn_on_flash))) {
            barcodeScannerView.setTorchOn();
        } else {
            barcodeScannerView.setTorchOff();
        }
    }

    public void closeScanner(View view) {
        this.finish();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onTorchOn() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            flash_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off, this.getTheme()));
        else
            flash_icon.setImageDrawable(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_flash_off));
        flash_label.setText(getString(R.string.turn_off_flash));
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onTorchOff() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            flash_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on, this.getTheme()));
        else
            flash_icon.setImageDrawable(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_flash_on));
        flash_label.setText(getString(R.string.turn_on_flash));
    }

    /**
     * Check if the device's camera has a Flashlight.
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
}
