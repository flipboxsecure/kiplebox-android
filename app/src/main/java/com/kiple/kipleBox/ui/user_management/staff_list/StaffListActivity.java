package com.kiple.kipleBox.ui.user_management.staff_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.histroy.SummaryResponse;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.UserListResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.service.TicketUpdateService;
import com.kiple.kipleBox.ui.core.errors.ErrorActivity;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class StaffListActivity extends AppCompatActivity implements CallBackInterface, View.OnClickListener {

    @BindView(R.id.page_title)
    TextView title;
    @BindView(R.id.user_list)
    RecyclerView user_list;

    StaffListAdapter userListAdapter;
    boolean enableAddStaff;

    private Printer mSunmiPrinter;
    private String printerType;

    private long mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list);
        ButterKnife.bind(this);

        printerDetection();

        Bundle b = getIntent().getExtras();
        if(b != null) {
            if(Constants.MANAGER_LOGIN.equalsIgnoreCase(b.getString(Constants.LOGIN_TYPE_BUNDLE))){
                title.setText(getString(R.string.add_staff));
                enableAddStaff = true;
            }else{
                Toast.makeText(this, "select staff", Toast.LENGTH_SHORT).show();
                title.setText(getString(R.string.select_staff_title));
                enableAddStaff = false;
            }
        }

//        getListTransaction();
    }

    //  system back key handling
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (null == SessionManager.getInstance(this).getActiveUserInfo() && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (mBackPressed + Constants.TIME_INTERVAL > System.currentTimeMillis()) {
                //  exit app here
                this.finishAffinity();
                return super.onKeyDown(keyCode, event);
            }
            else {
                Toast.makeText(getBaseContext(), getString(R.string.exit_msg), Toast.LENGTH_SHORT).show();
            }

            mBackPressed = System.currentTimeMillis();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void printerDetection() {
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(this);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

                    printerType = Constants.SUNMI_PRINTER;
                } else {
                    printerType = Constants.NO_PRINTER;
                }

                if (mSunmiPrinter.isPrinterReady()) {
                    printerType = Constants.SUNMI_PRINTER;
                } else {
                    printerType = Constants.NO_PRINTER;
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                printerType = Constants.PAX_PRINTER;
            } else {
                printerType = Constants.NO_PRINTER;
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(this, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
        }
    }

    @Override
    protected void onDestroy(){
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER))
            mSunmiPrinter.disconnectPrinter();
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        getList(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == Constants.CLOCK_OUT_CODE){
            //  log out user only here
            APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());
        }
    }

    private void getList(boolean isShowSpinner){
        APICall.staffList(isShowSpinner, this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), getUserListParams());
    }

    private HashMap<String, Object> getUserListParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void setupList(UserListResponse listInfo){
        userListAdapter = new StaffListAdapter(listInfo.getData(), this, this, enableAddStaff);
        user_list.setLayoutManager(new GridLayoutManager(this, 2));
        user_list.setAdapter(userListAdapter);
    }

    @Override
    public void onClick(View v) {

    }

    void logoutManager(){
        //  get transaction summary first before logging out
        HashMap<String, Object> param = new HashMap<>();
        param.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
        param.put("email", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
        param.put("datetime", Utils.getCurrentDateOrTime("yyyy-MM-dd"));
        param.put("logout_print", true);
        APICall.historySummary(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), param);
//        APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());
    }

    private void printSummary(SummaryResponse summaryResponse){
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
            try {
                //  print summary before executing logout
                IssueTicketModel issueTicketModel = null;
                PaymentReceiptModel paymentReceiptModel = null;

                ActivationResponse activationInfo = SessionManager.getInstance(this).getActivationInfo();
                SummaryModel summaryModel = new SummaryModel();
                summaryModel.setSite_name(activationInfo.getSite_name());

                String address = summaryResponse.getData().getSite_information().getStreet1() + ", " +
                        summaryResponse.getData().getSite_information().getStreet2() + ",\n" +
//                        cashResponse.getData().getSite_information().getPostcode() + " " +
                        summaryResponse.getData().getSite_information().getCity() + ", " +
                        summaryResponse.getData().getSite_information().getState();
                summaryModel.setSite_address(address);

                String[] period = summaryResponse.getData().getAuth_history().split(" - ");

                summaryModel.setStartDateTime(period[0].trim());
                summaryModel.setEndDateTime(period[1].trim());
                summaryModel.setStaff_name(SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
                summaryModel.setCashArrayList(summaryResponse.getData().getTransactions().getCash());
                summaryModel.setGuestRoomArrayListArrayList(summaryResponse.getData().getTransactions().getGuest_room());
                summaryModel.setMasterAccountArrayListArrayList(summaryResponse.getData().getTransactions().getMaster_account());

                int cashAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getCash().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getCash().get(i).getType().toLowerCase().contains("void"))
                        cashAmount += Integer.valueOf(summaryResponse.getData().getTransactions().getCash().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getCash().get(i).getType().toLowerCase().contains("void"))
                        cashAmount -= Integer.valueOf(summaryResponse.getData().getTransactions().getCash().get(i).getAmount());
                }

                int guestRoomAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getGuest_room().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getGuest_room().get(i).getType().toLowerCase().contains("void"))
                        guestRoomAmount += Integer.valueOf(summaryResponse.getData().getTransactions().getGuest_room().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getGuest_room().get(i).getType().toLowerCase().contains("void"))
                        guestRoomAmount -= Integer.valueOf(summaryResponse.getData().getTransactions().getGuest_room().get(i).getAmount());
                }

                int masterAccAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getMaster_account().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getMaster_account().get(i).getType().toLowerCase().contains("void"))
                        masterAccAmount += Integer.valueOf(summaryResponse.getData().getTransactions().getMaster_account().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getMaster_account().get(i).getType().toLowerCase().contains("void"))
                        masterAccAmount -= Integer.valueOf(summaryResponse.getData().getTransactions().getMaster_account().get(i).getAmount());
                }

                double debitCreditAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getDebit_credit().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getDebit_credit().get(i).getType().toLowerCase().contains("void"))
                        debitCreditAmount += Double.parseDouble(summaryResponse.getData().getTransactions().getDebit_credit().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getDebit_credit().get(i).getType().toLowerCase().contains("void"))
                        debitCreditAmount -= Double.parseDouble(summaryResponse.getData().getTransactions().getDebit_credit().get(i).getAmount());
                }

                summaryModel.setCashTotal(String.valueOf(cashAmount));
                summaryModel.setGuestRoomTotal(String.valueOf(guestRoomAmount));
                summaryModel.setMasterAccTotal(String.valueOf(masterAccAmount));
                summaryModel.setDebitCreditTotal(String.valueOf(debitCreditAmount));
                summaryModel.setGrandTotal(String.valueOf(cashAmount + guestRoomAmount + masterAccAmount + debitCreditAmount));
//                summaryModel.transaction_type = data.type
//                summaryModel.pos = activationInfo.pos_info1
//                summaryModel.qr_info = activationInfo.pos_info2
//
//                summaryModel.bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ic_kiplepark_text_logo_black), 280, 60, true)

                new PrintReceipts(mSunmiPrinter, this, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {
            View view = LayoutInflater.from(this).inflate(
                    R.layout.summary_receipt, null);
            LinearLayout v = view.findViewById(R.id.receipt);

            ActivationResponse activationInfo = SessionManager.getInstance(this).getActivationInfo();

            TextView tvSiteName = v.findViewById(R.id.site_name1);
            tvSiteName.setText(activationInfo.getSite_name());

            String address = summaryResponse.getData().getSite_information().getStreet1() + ", " +
                    summaryResponse.getData().getSite_information().getStreet2() + ",\n" +
                    summaryResponse.getData().getSite_information().getCity() + ", " +
                    summaryResponse.getData().getSite_information().getState();
            TextView tvSiteAddress = v.findViewById(R.id.site_address1);
            tvSiteAddress.setText(address);

            TextView tvStaffName = v.findViewById(R.id.staff_name);
            tvStaffName.setText(SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());

            String[] period = summaryResponse.getData().getAuth_history().split(" - ");

            TextView tvStartDateTime = v.findViewById(R.id.start_date);
            tvStartDateTime.setText(period[0].trim());

            TextView tvEndDateTime = v.findViewById(R.id.end_date);
            tvEndDateTime.setText(period[1].trim());

            //  cash list
            LinearLayout llSummaryCashList = v.findViewById(R.id.summary_cash_list);
            int cashMultiplier = 0;
            List<SummaryResponse.Cash> cashList = summaryResponse.getData().getTransactions().getCash();
            for (int i = 0; i < cashList.size(); i++) {
                View cashItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = cashItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(cashList.get(i).getTicketId() + " - " + cashList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (cashList.get(i).getType().contains("["))
                    cashMultiplier++;

                if (cashList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(cashList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(cashList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llSummaryCashList.addView(cashItem);
            }

            llSummaryCashList.getLayoutParams().height = Utils.getViewHeight(llSummaryCashList) + (cashMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double cashAmount = 0;
            for (int i = 0; i < cashList.size(); i++) {
                if (!cashList.get(i).getType().toLowerCase().contains("void"))
                    cashAmount += Double.parseDouble(cashList.get(i).getAmount());
                else if (cashList.get(i).getType().toLowerCase().contains("void"))
                    cashAmount -= Double.parseDouble(cashList.get(i).getAmount());
            }
            TextView tvCashTotal = v.findViewById(R.id.cash_total);
            tvCashTotal.setText(getString(R.string.rm) + Utils.formatBalance(cashAmount));

            //  guest room list
            LinearLayout llGuestroomList = v.findViewById(R.id.guestroom_list);
            int guestroomMultiplier = 0;
            List<SummaryResponse.Guest_room> guestroomList = summaryResponse.getData().getTransactions().getGuest_room();
            for (int i = 0; i < guestroomList.size(); i++) {
                View guestroomItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = guestroomItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(guestroomList.get(i).getTicketId() + " - " + guestroomList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (guestroomList.get(i).getType().contains("["))
                    guestroomMultiplier++;

                if (guestroomList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(guestroomList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(guestroomList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llSummaryCashList.addView(guestroomItem);
            }

            llGuestroomList.getLayoutParams().height = Utils.getViewHeight(llGuestroomList) + (guestroomMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double guestroomAmount = 0;
            for (int i = 0; i < guestroomList.size(); i++) {
                if (!guestroomList.get(i).getType().toLowerCase().contains("void"))
                    guestroomAmount += Double.parseDouble(guestroomList.get(i).getAmount());
                else if (guestroomList.get(i).getType().toLowerCase().contains("void"))
                    guestroomAmount -= Double.parseDouble(guestroomList.get(i).getAmount());
            }
            TextView tvGuestroomTotal = v.findViewById(R.id.guestroom_total);
            tvGuestroomTotal.setText(getString(R.string.rm) + Utils.formatBalance(guestroomAmount));

            //  master account list
            LinearLayout llMasterAccountList = v.findViewById(R.id.masteraccount_list);
            int masterAccountMultiplier = 0;
            List<SummaryResponse.Master_account> masterAccountList = summaryResponse.getData().getTransactions().getMaster_account();
            for (int i = 0; i < masterAccountList.size(); i++) {
                View masterAccountItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = masterAccountItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(masterAccountList.get(i).getTicketId() + " - " + masterAccountList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (masterAccountList.get(i).getType().contains("["))
                    masterAccountMultiplier++;

                if (masterAccountList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(masterAccountList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(masterAccountList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llMasterAccountList.addView(masterAccountItem);
            }

            llMasterAccountList.getLayoutParams().height = Utils.getViewHeight(llMasterAccountList) + (masterAccountMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double masterAccountAmount = 0;
            for (int i = 0; i < guestroomList.size(); i++) {
                if (!guestroomList.get(i).getType().toLowerCase().contains("void"))
                    masterAccountAmount += Double.parseDouble(masterAccountList.get(i).getAmount());
                else if (guestroomList.get(i).getType().toLowerCase().contains("void"))
                    masterAccountAmount -= Double.parseDouble(masterAccountList.get(i).getAmount());
            }
            TextView tvMasterAccountTotal = v.findViewById(R.id.masteraccount_total);
            tvMasterAccountTotal.setText(getString(R.string.rm) + Utils.formatBalance(masterAccountAmount));

            //  debit credit list
            LinearLayout llDebitCreditList = v.findViewById(R.id.debit_credit_list);
            int debitCreditMultiplier = 0;
            List<SummaryResponse.Debit_credit> debitCreditList = summaryResponse.getData().getTransactions().getDebit_credit();
            for (int i = 0; i < debitCreditList.size(); i++) {
                View debitCreditItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = debitCreditItem.findViewById(R.id.tvTxnID);
                TextView tvTransactionAmount = debitCreditItem.findViewById(R.id.tvTxnAmount);
                tvTransactionId.setText(debitCreditList.get(i).getTicketId() + " - " + debitCreditList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (debitCreditList.get(i).getType().contains("["))
                    debitCreditMultiplier++;

                if (debitCreditList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionAmount.setText("- RM " + Utils.formatBalance(debitCreditList.get(i).getAmount()));
                else
                    tvTransactionAmount.setText(getString(R.string.rm) + Utils.formatBalance(debitCreditList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llDebitCreditList.addView(debitCreditItem);
            }

            llDebitCreditList.getLayoutParams().height = Utils.getViewHeight(llDebitCreditList) + (debitCreditMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double debitCreditAmount = 0;
            for (int i = 0; i < debitCreditList.size(); i++) {
                if (!debitCreditList.get(i).getType().toLowerCase().contains("void"))
                    debitCreditAmount += Double.parseDouble(debitCreditList.get(i).getAmount());
                else if (debitCreditList.get(i).getType().toLowerCase().contains("void"))
                    debitCreditAmount -= Double.parseDouble(debitCreditList.get(i).getAmount());
            }
            TextView tvDebitCreditTotal = v.findViewById(R.id.debit_credit_total);
            tvDebitCreditTotal.setText(getString(R.string.rm) + Utils.formatBalance(debitCreditAmount));

            double grandTotal = cashAmount + guestroomAmount + masterAccountAmount + debitCreditAmount;
            TextView tvGrandTotal = v.findViewById(R.id.grand_total);
            tvGrandTotal.setText(getString(R.string.rm) + Utils.formatBalance(grandTotal));

            new Thread(() -> {
                if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                    PAXPrinter.getInstance().printView(v);
                    PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                    PAXPrinter.getInstance().start();
                }
            }).start();
        } else {
            Toast.makeText(this, getString(R.string.no_printer), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case USER_LIST:
                UserListResponse userListResponse = (UserListResponse) response.body();
                setupList(userListResponse);
                break;

            case LOGOUT:
                //cancel service before logging out user and clearing session
                TicketUpdateService.stopRepeatingService();

                //  clear active user in SessionManager
                SessionManager.getInstance(this).clear();
                enableAddStaff = false;
                getList(true);
                Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show();
                break;

            case HISTORY_SUMMARY:
                SummaryResponse summaryResponse = (SummaryResponse) response.body();

                if (summaryResponse != null) {
                    printSummary(summaryResponse);
                    //  proceed with logout after receipt printing executed
                    final Intent insufficientBalance = new Intent(this, ErrorActivity.class);
                    insufficientBalance.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.CLOCK_OUT);
                    startActivityForResult(insufficientBalance, Constants.CLOCK_OUT_CODE);
//                    APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());
                }else{
                    Toast.makeText(this, "unable to get transaction summary info", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
    }
}
