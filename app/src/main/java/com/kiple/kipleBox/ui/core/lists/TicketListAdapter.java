package com.kiple.kipleBox.ui.core.lists;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.ui.core.cash_payment.ActivityCashPayment;
import com.kiple.kipleBox.ui.core.park.ActivityPark;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TicketListAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<TicketListsResponse> ticketListsResponses;
    private String listFlag;
    private TicketListsFragment mFragment;

    public TicketListAdapter(Context context, TicketListsFragment ticketListsFragment, String listFlag, ArrayList<TicketListsResponse> ticketList) {
        mContext = context;
        ticketListsResponses = ticketList;
        this.listFlag = listFlag;
        mFragment = ticketListsFragment;

//        Toast.makeText(mContext, "adapter initiated", Toast.LENGTH_SHORT).show();
//        Toast.makeText(mContext, "listTransaction size: " + ticketListsResponses.size(), Toast.LENGTH_SHORT).show();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (listFlag) {
            case Constants.TICKET_ISSUED_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket_issued, parent, false);
                return new TicketIssuedItem(v);

            case Constants.TICKET_PARKED_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parked, parent, false);
                return new TicketParkedItem(v);

            case Constants.TICKET_PAID_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_paid, parent, false);
                return new TicketPaidItem(v);

            case Constants.TICKET_PENDING_COLLECTION_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending_collection, parent, false);
                return new TicketPendingCollectionItem(v);

            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final TicketListsResponse data = ticketListsResponses.get(position);
        switch (listFlag){
            case Constants.TICKET_ISSUED_LIST:
                final TicketIssuedItem issuedItem = (TicketIssuedItem) holder;
                issuedItem.number_plate.setText(Utils.getNumberPlateFromTicketdata(data.getTicketData()));

                if(null != data.getMobile_no() && !"".equalsIgnoreCase(data.getMobile_no())) {
                    issuedItem.mobile_number.setText(mFragment.getString(R.string.mobile_number_short_colon) + data.getMobile_no());
                    issuedItem.mobile_number.setVisibility(View.VISIBLE);
                } else
                    issuedItem.mobile_number.setVisibility(View.GONE);

                issuedItem.item_parent.setOnClickListener(v -> {
                    final Intent intent = new Intent(mContext, ActivityPark.class);
                    intent.putExtra(Constants.TICKET_DATA_BUNDLE, data.getTicketData());
                    intent.putExtra(Constants.TICKET_ID_BUNDLE, data.getTicketId());
//                        mContext.startActivity(intent);
                    mFragment.startActivityForResult(intent, Constants.TICKET_PARKED_INT);
                });

                issuedItem.elapsed_time.setText(Utils.getElapsedTimeFromUnix(data.getEntryTime(), Utils.realCurrentUnix(mFragment.getCurrentServerUnix(), mFragment.getStartingElapsed(), (SystemClock.elapsedRealtime() / 1000L))));

                if(null != data.getEntryTime()) {
                    //  update time elapsed
                    issuedItem.ticketCounter = new CountDownTimer(Constants.MINUTE_INTERVAL, Constants.MINUTE_INTERVAL) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            issuedItem.elapsed_time.setText(Utils.getElapsedTimeFromUnix(data.getEntryTime(), Utils.realCurrentUnix(mFragment.getCurrentServerUnix(), mFragment.getStartingElapsed(), (SystemClock.elapsedRealtime() / 1000L))));
                        }

                        @Override
                        public void onFinish() {
                            // restart counter here
                            issuedItem.ticketCounter.start();
                        }
                    }.start();
                }
                break;

            case Constants.TICKET_PARKED_LIST:
                final TicketParkedItem parkedItem = (TicketParkedItem) holder;
                parkedItem.number_plate.setText(Utils.getNumberPlateFromTicketdata(data.getTicketData()));

                if(null != data.getMobile_no() && !"".equalsIgnoreCase(data.getMobile_no())) {
                    parkedItem.mobile_number.setText(mFragment.getString(R.string.mobile_number_short_colon) + data.getMobile_no());
                    parkedItem.mobile_number.setVisibility(View.VISIBLE);
                } else
                    parkedItem.mobile_number.setVisibility(View.GONE);

                //  hide bay number if unavailable
                if(null != data.getValet_park_loc()) {
                    parkedItem.parking_bay_number.setText(mFragment.getString(R.string.bay_title) + data.getValet_park_loc());
                    parkedItem.parking_bay_number.setVisibility(View.VISIBLE);
                }else
                    parkedItem.parking_bay_number.setVisibility(View.GONE);

                if(mFragment.isPrintFromList())
                    parkedItem.print_ticket.setOnClickListener(v -> mFragment.encryptAndPrintTicket(data.getTicketData()));
                else
                    parkedItem.print_ticket.setVisibility(View.GONE);

                parkedItem.item_parent.setOnClickListener(v -> {
                    final Intent intent = new Intent(mContext, ActivityCashPayment.class);
                    intent.putExtra(Constants.TICKET_INFO_BUNDLE, data);
                    intent.putExtra("activity", "TicketList");
                    mFragment.startActivityForResult(intent, Constants.TICKET_PAID_INT);
                });

                parkedItem.elapsed_time.setText(Utils.getElapsedTimeFromUnix(data.getEntryTime(), Utils.realCurrentUnix(mFragment.getCurrentServerUnix(), mFragment.getStartingElapsed(), (SystemClock.elapsedRealtime() / 1000L))));

                if(null != data.getEntryTime()) {
                    //  update time elapsed
                    parkedItem.ticketCounter = new CountDownTimer(Constants.MINUTE_INTERVAL, Constants.MINUTE_INTERVAL) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            parkedItem.elapsed_time.setText(Utils.getElapsedTimeFromUnix(data.getEntryTime(), Utils.realCurrentUnix(mFragment.getCurrentServerUnix(), mFragment.getStartingElapsed(), (SystemClock.elapsedRealtime() / 1000L))));
                        }

                        @Override
                        public void onFinish() {
                            // restart counter here
                            parkedItem.ticketCounter.start();
                        }
                    }.start();
                }
                break;

            case Constants.TICKET_PAID_LIST:
                final TicketPaidItem paidItem = (TicketPaidItem) holder;
                paidItem.number_plate.setText(Utils.getNumberPlateFromTicketdata(data.getTicketData()));

                if(null != data.getMobile_no() && !"".equalsIgnoreCase(data.getMobile_no())) {
                    paidItem.mobile_number.setText(mFragment.getString(R.string.mobile_number_short_colon) + data.getMobile_no());
                    paidItem.mobile_number.setVisibility(View.VISIBLE);
                } else
                    paidItem.mobile_number.setVisibility(View.GONE);

                //  hide bay number if unavailable
                if(null != data.getValet_park_loc()) {
                    paidItem.park_location.setText(mFragment.getString(R.string.bay_title) + data.getValet_park_loc());
                    paidItem.park_location.setVisibility(View.VISIBLE);
                }else
                    paidItem.park_location.setVisibility(View.GONE);

                paidItem.item_parent.setOnClickListener(v -> CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            mFragment.readyForCollection(data);
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }, "", mContext.getString(R.string.pickup_confirmation)));

                if(null != data.getDatePayUnix() && null != SessionManager.getInstance(mContext).getActivationInfo().getGrace_period()) {
                    //  grace period timer
                    paidItem.ticketCounter = new CountDownTimer(
                            Utils.getRemainingGrace(String.valueOf(data.getDatePayUnix()),
                                    String.valueOf(SessionManager.getInstance(mContext).getActivationInfo().getGrace_period()),
                                    mFragment.getCurrentServerUnix(), mFragment.getStartingElapsed(), (SystemClock.elapsedRealtime() / 1000L)), Constants.TIMER_INTERVAL) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String text = String.format(Locale.getDefault(), "%02d:%02d",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                            paidItem.pickup_timer.setText(text);
                        }

                        @Override
                        public void onFinish() {
                            //  remove ticket here

                            //  timer to display 00:00
                            String text = String.format(Locale.getDefault(), "%02d:%02d",
                                    TimeUnit.MILLISECONDS.toMinutes(0) % 60,
                                    TimeUnit.MILLISECONDS.toSeconds(0) % 60);
                            paidItem.pickup_timer.setText(text);
//                        Animation anim = AnimationUtils.loadAnimation(
//                                context, R.anim.slide_out_left
//                        );
//                        anim.setDuration(500);
//                        item.getV().startAnimation(anim);
//
//                        new Handler().postDelayed(() -> {
//
//                            list.remove(position);
//                            if(fragment !=null) {
//                                fragment.ticketAdapter.notifyDataSetChanged();
//                            }
//
//                        }, anim.getDuration());
//                    list.remove(position);
                        }
                    }.start();
                }
                break;

            case Constants.TICKET_PENDING_COLLECTION_LIST:
                final TicketPendingCollectionItem pendingCollectionItem = (TicketPendingCollectionItem) holder;
                pendingCollectionItem.number_plate.setText(Utils.getNumberPlateFromTicketdata(data.getTicketData()));

                if(null != data.getMobile_no() && !"".equalsIgnoreCase(data.getMobile_no())) {
                    pendingCollectionItem.mobile_number.setText(mFragment.getString(R.string.mobile_number_short_colon) + data.getMobile_no());
                    pendingCollectionItem.mobile_number.setVisibility(View.VISIBLE);
                } else
                    pendingCollectionItem.mobile_number.setVisibility(View.GONE);

                pendingCollectionItem.item_parent.setOnClickListener(v -> CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            mFragment.processCompleted(data);
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }, "", mContext.getString(R.string.complete_confirmation)));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (null != ticketListsResponses ? ticketListsResponses.size() : 0);
    }

    public class TicketIssuedItem extends RecyclerView.ViewHolder{

        @BindView(R.id.list_item)
        LinearLayout item_parent;
        @BindView(R.id.time)
        TextView elapsed_time;
        @BindView(R.id.number_plate)
        TextView number_plate;
        @BindView(R.id.mobile_number)
        TextView mobile_number;

        CountDownTimer ticketCounter;

        public TicketIssuedItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            number_plate = itemView.findViewById(R.id.number_plate);
        }
    }

    public class TicketParkedItem extends RecyclerView.ViewHolder{

        @BindView(R.id.list_item)
        LinearLayout item_parent;
        @BindView(R.id.time)
        TextView elapsed_time;
        @BindView(R.id.parking_bay_number)
        TextView parking_bay_number;
        @BindView(R.id.number_plate)
        TextView number_plate;
        @BindView(R.id.mobile_number)
        TextView mobile_number;
        @BindView(R.id.ivPrint)
        ImageView print_ticket;

        CountDownTimer ticketCounter;

        public TicketParkedItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class TicketPaidItem extends RecyclerView.ViewHolder{

        @BindView(R.id.list_item)
        LinearLayout item_parent;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.pickup_timer)
        TextView pickup_timer;
        @BindView(R.id.number_plate)
        TextView number_plate;
        @BindView(R.id.park_location)
        TextView park_location;
        @BindView(R.id.mobile_number)
        TextView mobile_number;

        CountDownTimer ticketCounter;

        public TicketPaidItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class TicketPendingCollectionItem extends RecyclerView.ViewHolder{

        @BindView(R.id.list_item)
        LinearLayout item_parent;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.number_plate)
        TextView number_plate;
        @BindView(R.id.mobile_number)
        TextView mobile_number;

        public TicketPendingCollectionItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
