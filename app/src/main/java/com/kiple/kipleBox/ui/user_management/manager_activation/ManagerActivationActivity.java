package com.kiple.kipleBox.ui.user_management.manager_activation;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.UserListResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.user_management.staff_list.StaffListActivity;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.LogUtils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class ManagerActivationActivity extends AppCompatActivity implements View.OnClickListener, CallBackInterface {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.manager_name)
    TextView manager_name;
    @BindView(R.id.manager_pin)
    TextView manager_pin;
    @BindView(R.id.submit_btn)
    Button submit_btn;
    @BindView(R.id.discard_btn)
    Button discard_btn;

    ActivationResponse activationResponse;
    private boolean isManagerLogin;
    private String actionFlag = "";
    private UserListResponse.Datum userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_activation);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();
        if(b != null && b.getParcelable(Constants.MANAGER_DETAILS) != null) {
            if(Constants.MANAGER_LOGIN.equalsIgnoreCase(b.getString(Constants.LOGIN_TYPE_BUNDLE))){
                title.setText(getString(R.string.site_manager_title));
                activationResponse = b.getParcelable(Constants.MANAGER_DETAILS);
                isManagerLogin = true;
                setupPage(isManagerLogin);
            }else{
                title.setText(getString(R.string.register_staff_title));
                submit_btn.setText(getString(R.string.login));
                isManagerLogin = false;
                setupPage(isManagerLogin);
            }
        }

        if(b != null && Constants.ADD_STAFF.equalsIgnoreCase(b.getString(Constants.LOGIN_TYPE_BUNDLE))){
            LogUtils.LOGI(this.getClass().getSimpleName(), "page & title set");
            title.setText(getString(R.string.add_staff));
//            submit_btn.setText(getString(R.string.login));
            isManagerLogin = false;
            actionFlag = b.getString(Constants.LOGIN_TYPE_BUNDLE);
            setupPage(isManagerLogin);
        }

        if(b != null && Constants.EDIT_STAFF.equalsIgnoreCase(b.getString(Constants.LOGIN_TYPE_BUNDLE))){
            LogUtils.LOGI(this.getClass().getSimpleName(), "edit staff page & title set");
            title.setText(getString(R.string.edit_staff));
            submit_btn.setText(getString(R.string.done));
            discard_btn.setText(getString(R.string.delete_profile));
            isManagerLogin = false;
            actionFlag = b.getString(Constants.LOGIN_TYPE_BUNDLE);
            userData = b.getParcelable(Constants.EDIT_PROFILE_BUNDLE);
            setupPage(userData);
        }
    }

    private void setupPage(UserListResponse.Datum datum){
        manager_name.setText(datum.getName());
        manager_pin.setText("123456");  //  pre-set default PIN change to 123456
    }

    private void setupPage(boolean isManager){
        if(isManager) {
            manager_name.setText(activationResponse.getManager_name());
            manager_pin.setText("123456");  //  this value supposed to get from backend response
        }else{
            discard_btn.setText(getString(R.string.dismiss));
        }
    }

    private HashMap<String, Object> getUpdateParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("name", manager_name.getText().toString());
            params.put("password", manager_pin.getText().toString());
            if(null != userData && null != userData.getEmail() && !userData.getEmail().isEmpty())
                params.put("email", userData.getEmail());
            else
                params.put("email", SessionManager.getInstance(this).getActivationInfo().getManager_email());
            params.put("activated", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object> getRegisterParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("name", manager_name.getText().toString());
            params.put("password", manager_pin.getText().toString());
            String email = manager_name.getText().toString() + "@" + SessionManager.getInstance(this).getActivationInfo().getSite_id() + ".com";
            params.put("email", email.replaceAll(" ", "_"));
            params.put("access_level", "staff");
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object>getDeleteParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", userData.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    protected void onDestroy(){
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_btn:
                if(isManagerLogin)
                    APICall.updateManager(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), getUpdateParams());
                else {
                    switch (actionFlag){
                        case Constants.ADD_STAFF:
                            APICall.registerUser(this, this, getRegisterParams());
                            break;

                        case Constants.EDIT_STAFF:
                            APICall.updateManager(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), getUpdateParams(), Constants.ApiFlags.EDIT_PROFILE);
                            break;
                    }
                }
                //  no API for now, proceed straight to staff list page
//                final Intent intent = new Intent(this, StaffListActivity.class);
//                Bundle b = new Bundle();
//                if(isManagerLogin)
//                    b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.MANAGER_LOGIN); //Your id
//                else
//                    b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.STAFF_LOGIN);
//                intent.putExtras(b); //Put your id to your next Intent
//                startActivity(intent);
//                finish();
                break;

            case R.id.discard_btn:
                switch (actionFlag){
                    case Constants.EDIT_STAFF:
                        //  delete profile
                        APICall.deleteUser(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), getDeleteParams());
                        break;

                        default:
                            onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case UPDATE:
                Toast.makeText(this, "Manager profile updated", Toast.LENGTH_SHORT).show();
                final Intent intent = new Intent(this, StaffListActivity.class);
                Bundle b = new Bundle();

                if(isManagerLogin)
                    b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.MANAGER_LOGIN); //Your id
                else
                    b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.STAFF_LOGIN);

                intent.putExtras(b); //Put your id to your next Intent
                startActivity(intent);
                finishAffinity();
                break;

            case REGISTER:
                //  staff registration success
                finish();
                break;

            case EDIT_PROFILE:
                Toast.makeText(this, "profile updated", Toast.LENGTH_SHORT).show();
                finish();
                break;

            case DELETE_USER:
                Toast.makeText(this, "profile deleted", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case DELETE_USER:
                if(response.code() == 423){
                    CustomAlerts.showOKAlert(this, getString(R.string.request_denied), getString(R.string.user_deletion_fail_msg));
                }
                break;
        }
    }
}
