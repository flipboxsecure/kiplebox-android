package com.kiple.kipleBox.ui.user_management.activation;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.user_management.manager_activation.ManagerActivationActivity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class ActivationActivity extends AppCompatActivity implements View.OnClickListener, CallBackInterface {

    @BindView(R.id.activation_desc)
    TextView activation_desc;
    @BindView(R.id.activation_parent)
    TextInputLayout activation_parent;
    @BindView(R.id.activation_code)
    TextInputEditText activation_code;
    @BindView(R.id.activate_btn)
    Button activate_btn;
    @BindView(R.id.resend_activation_btn)
    TextView resend_activation_btn;

    private boolean isResendCodeScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);
        ButterKnife.bind(this);

        setupActivation();

//        Bundle b = getIntent().getExtras();
//        if(b != null) {
//            b.getString(Constants.LOGIN_TYPE_BUNDLE).equalsIgnoreCase(Constants.MANAGER_LOGIN)
//        }
    }

    @Override
    protected void onDestroy(){
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    private void setupActivation(){
        isResendCodeScreen = false;
        activation_desc.setText(getString(R.string.activation_msg));
        activation_code.setInputType(InputType.TYPE_CLASS_NUMBER);
        activate_btn.setText(getString(R.string.activate));
        resend_activation_btn.setText(R.string.resend_activation);
    }

    private void setupResendActivation(){
        isResendCodeScreen = true;
        activation_desc.setText(getString(R.string.resend_activation_msg));
        activation_code.setInputType(InputType.TYPE_CLASS_NUMBER);
        activate_btn.setText(getString(R.string.resend_activation));
        resend_activation_btn.setText(R.string.cancel);
    }

    private HashMap<String, Object> getActivationParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("activation_code", activation_code.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object> getResendParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("device_id", activation_code.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activate_btn:
                //  device activation
                if(!isResendCodeScreen)
                    APICall.activation(this, this, getActivationParams());
                //  resend activation
                if(isResendCodeScreen)
                    APICall.resendActivation(this, this, getResendParams());
                    break;

            case R.id.resend_activation_btn:
                //  clear existing error message if available
                activation_parent.setError(null);
                //  clear existing edittext content
                activation_code.setText(null);
                //  setup resend activation activation
                if(!isResendCodeScreen)
                    setupResendActivation();
                //  setup device activation
                else if(isResendCodeScreen)
                    setupActivation();
                break;
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case ACTIVATION:
                ActivationResponse activationResponse = (ActivationResponse) response.body();
//                Toast.makeText(this, "activity_activation succeed", Toast.LENGTH_SHORT).show();

                //  save activity_activation response
                SessionManager.getInstance(this).saveActivationInfo(activationResponse);

                //  proceed to manager_activation page
                final Intent intent = new Intent(this, ManagerActivationActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.MANAGER_LOGIN); //Your id
                b.putParcelable(Constants.MANAGER_DETAILS, activationResponse);
                intent.putExtras(b); //Put your id to your next Intent
                startActivity(intent);
//                finish();
                break;

            case RESEND_ACTIVATION:
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case ACTIVATION:
                activation_parent.setError(getString(R.string.activation_mismatch));
                break;

            case RESEND_ACTIVATION:
                activation_parent.setError(getString(R.string.resend_activation_mismatch));
                break;
        }
    }
}
