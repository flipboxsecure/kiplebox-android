package com.kiple.kipleBox.ui.core.history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kiple.kipleBox.R
import com.kiple.kipleBox.common.Constants
import com.kiple.kipleBox.model.histroy.SummaryResponse
import com.kiple.kipleBox.utils.Utils
import kotlinx.android.synthetic.main.item_history_summary.view.*


class HistorySummaryAdapter(var summaryResponse: SummaryResponse.Transactions, var mContext: Context, var transactionType: String) : RecyclerView.Adapter<HistorySummaryAdapter.HistoryHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {

        return HistoryHolder(LayoutInflater.from(mContext).inflate(R.layout.item_history_summary, parent, false))
    }

    override fun getItemCount(): Int {
//        LogUtils.LOGE("Summary adapter check", "another transactionType: " + transactionType)
        return when (transactionType) {
            Constants.CASH_PAYMENT -> summaryResponse.cash.size
            Constants.CHARGE_TO_ROOM -> summaryResponse.guest_room.size
            Constants.CHARGE_TO_MASTER -> summaryResponse.master_account.size
            Constants.CREDIT_DEBIT -> summaryResponse.debit_credit.size
            else -> 0
        }
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {

        when {
            Constants.CASH_PAYMENT.equals(transactionType, true) -> holder.bindData(summaryResponse.cash[position])
            Constants.CHARGE_TO_ROOM.equals(transactionType, true) -> holder.bindData(summaryResponse.guest_room[position])
            Constants.CHARGE_TO_MASTER.equals(transactionType, true) -> holder.bindData(summaryResponse.master_account[position])
            Constants.CREDIT_DEBIT.equals(transactionType, true) -> holder.bindData(summaryResponse.debit_credit[position])
        }
    }


    class HistoryHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(data: SummaryResponse.Cash) {

            view.tvTxnID.text = data.ticketId + " - " + data.type

            if (data.type.contains("[Void]", ignoreCase = true))
                view.tvTxnAmount.text = "- RM " + Utils.formatBalance(data.amount)
            else
                view.tvTxnAmount.text = "RM " + Utils.formatBalance(data.amount)

        }

        fun bindData(data: SummaryResponse.Guest_room) {

            view.tvTxnID.text = data.ticketId + " - " + data.type

            if (data.type.contains("[Void]", ignoreCase = true))
                view.tvTxnAmount.text = "- RM " + Utils.formatBalance(data.amount)
            else
                view.tvTxnAmount.text = "RM " + Utils.formatBalance(data.amount)

        }

        fun bindData(data: SummaryResponse.Master_account) {

            if (data.type.contains("[Void]", ignoreCase = true)) {
                view.tvTxnID.text = data.ticketId + " - " + "Master Acc [void]"
                view.tvTxnAmount.text = "- RM " + Utils.formatBalance(data.amount)
            } else {
                view.tvTxnID.text = data.ticketId + " - " + "Master Acc"
                view.tvTxnAmount.text = "RM " + Utils.formatBalance(data.amount)
            }

        }

        fun bindData(data: SummaryResponse.Debit_credit) {

            if (data.type.contains("[Void]", ignoreCase = true)) {
                view.tvTxnID.text = data.ticketId + " - " + "Debit/Credit [void]"
                view.tvTxnAmount.text = "- RM " + Utils.formatBalance(data.amount)
            } else {
                view.tvTxnID.text = data.ticketId + " - " + "Debit/Credit"
                view.tvTxnAmount.text = "RM " + Utils.formatBalance(data.amount)
            }

        }
    }
}