package com.kiple.kipleBox.ui.core.history

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kiple.kipleBox.R
import com.kiple.kipleBox.base.BaseFragment
import com.kiple.kipleBox.base.Printer
import com.kiple.kipleBox.common.CallBackInterface
import com.kiple.kipleBox.common.Constants
import com.kiple.kipleBox.common.CustomApplication
import com.kiple.kipleBox.common.SessionManager
import com.kiple.kipleBox.model.histroy.SummaryResponse
import com.kiple.kipleBox.model.histroy.TransactionResponse
import com.kiple.kipleBox.model.histroy.VoidTransactionResponse
import com.kiple.kipleBox.model.printer_models.IssueTicketModel
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel
import com.kiple.kipleBox.model.printer_models.SummaryModel
import com.kiple.kipleBox.network.APICall
import com.kiple.kipleBox.utils.LogUtils
import com.kiple.kipleBox.utils.PrintReceipts
import com.kiple.kipleBox.utils.Utils
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter
import kotlinx.android.synthetic.main.dialog_remove_transaction_history.view.*
import kotlinx.android.synthetic.main.dialog_void_remark.view.*
import kotlinx.android.synthetic.main.fragment_history.view.*
import kotlinx.android.synthetic.main.item_history_summary.view.*
import kotlinx.android.synthetic.main.payment_receipt.view.*
import kotlinx.android.synthetic.main.summary_receipt.view.*
import kotlinx.android.synthetic.main.summary_receipt.view.logo
import kotlinx.android.synthetic.main.summary_receipt.view.site_address1
import kotlinx.android.synthetic.main.summary_receipt.view.site_name1
import retrofit2.Response
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates


/**
 * THIS CLASS IS USED TO SHOW THE HISTORY TRANSACTION AND SUMMARY OF HISTORY
 * USED FOUR API'S
 * 1. api/pos/history/transaction
 * 2. api/pos/history/summary
 * 3. k-box-> /api/pos/payment/void
 * 4. /api/user/supervisor-confirmation
 */


class HistoryTransactionFragment : BaseFragment(), CallBackInterface {


    private var globalView: View by Delegates.notNull()
    private var mContext: Context by Delegates.notNull()

    private var listTransaction = ArrayList<TransactionResponse.Data>()
    private var dataSummary = SummaryResponse()
    private var myCalendar = Calendar.getInstance()
    private var isTransaction = true
    private var isPrintLogo = false
    private var isPrintCompanyName = false
    private var isPerEntry = false

    private var strDateForLabel = ""
    private var strDateForAPI = ""
    //    var totalAmount = 0
    private var cashTotal = 0.00
    private var gRoomTotal = 0.00
    private var mAccTotal = 0.00
    private var dCreditTotal = 0.00
    private var grandTotal = 0.00
    var isPrinterAvailable = false
    private var isToday = true

    private var sunmiPrinter: Printer by Delegates.notNull()
    private var printerType = ""

    companion object {
        //        var selectedDate = ""
        var selectedTime = ""
    }

    private var txnData: TransactionResponse.Data by Delegates.notNull()
    private var historyFragment: HistoryTransactionFragment by Delegates.notNull()
    private var mBottomSheetDialogRemark: BottomSheetDialog by Delegates.notNull()
    private var mBottomSheetDialogTxn: BottomSheetDialog by Delegates.notNull()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        globalView = inflater.inflate(R.layout.fragment_history, null)

        //init all views
        initViews()

        //api calling
        callTransactionAPI()

        return globalView

    }

    override fun onResume() {
        super.onResume()
        listener.showBottomNavigation()
    }

    private fun getParams(): HashMap<String, Any> {
        val params = HashMap<String, Any>()
        try {
            params["site_id"] = SessionManager.getInstance(mContext).activationInfo.site_id //change this to signed in user
            params["email"] = SessionManager.getInstance(mContext).activeUserInfo.data.user.email
            params["datetime"] = strDateForAPI
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return params
    }

    private fun initViews() {
        mContext = this.activity!!

        historyFragment = this

        //  get logo config
        isPrintLogo = getLogoConfig()

        //  get company name config
        isPrintCompanyName = getCompanyNameConfig()

        //click function
        globalView.btnTransaction.setOnClickListener(this)
        globalView.btnSummary.setOnClickListener(this)
        globalView.ivCalendar.setOnClickListener(this)
        globalView.ivPrinter.setOnClickListener(this)

        globalView.rvCashHistory.layoutManager = LinearLayoutManager(mContext)
        globalView.rvGRHistory.layoutManager = LinearLayoutManager(mContext)
        globalView.rvMAHistory.layoutManager = LinearLayoutManager(mContext)
        globalView.rvDCHistory.layoutManager = LinearLayoutManager(mContext)
        // Access the RecyclerView Adapter and load the data into it

        //hide summary container
        globalView.ll_summaryContainer.visibility = View.GONE
        globalView.txtDateTransaction.visibility = View.VISIBLE

        //  hide grand total layout
        globalView.ll_grand_total.visibility = View.GONE

        //set current date on 1st time
        updateLabel()

        listTransaction = ArrayList()

        //  hide both guest room & master account lists
        globalView.gr_list.visibility = View.GONE
        globalView.ma_list.visibility = View.GONE
        globalView.dc_list.visibility = View.GONE

        //  hardware detection
        printerDetection()

        //  disable print button when no printer detected
//        this.sunmiPrinter = Printer(activity)
//        sunmiPrinter.printerInit()
//        val handler = Handler()
//        handler.postDelayed({
//            if (SessionManager.getInstance(mContext).printerStatus) {
//                isPrinterAvailable = true
//                globalView.ivPrinter.setOnClickListener(this)
//            } else {
//                isPrinterAvailable = false
//                globalView.ivPrinter.isClickable = false
//                globalView.ivPrinter.setColorFilter(resources.getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN)
//            }

//        }, 300)

    }

    private fun printerDetection() {
        //  check for PAX first due to API efficiency
        val printerStatus = PAXPrinter.getInstance().status
        when {
            printerStatus.equals(Constants.NOT_PAX_PRINTER, true) -> {
                //  not PAX hardware, start check for Sunmi hardware

                this.sunmiPrinter = Printer(activity)
                sunmiPrinter.printerInit()
                val handler = Handler()
                handler.postDelayed({
                    printerType = if (sunmiPrinter.printerModal != null) {
                        Log.e("Printer Info", sunmiPrinter.printerModal)
                        Log.e("Printer Info", sunmiPrinter.printerSerialNo)
                        Log.e("Printer Info", sunmiPrinter.printerVersion)

                        enablePrintButton()
                        Constants.SUNMI_PRINTER
                    } else {
                        disablePrintButton()
                        Constants.NO_PRINTER
                    }

                    printerType = if (sunmiPrinter.isPrinterReady) {
                        enablePrintButton()
                        Constants.SUNMI_PRINTER
                    } else {
                        disablePrintButton()
                        Constants.NO_PRINTER
                    }
                }, 300)
            }
            printerStatus.equals(Constants.SUCCESS, true) -> {
                //  PAX hardware, start initiate PAX printer
                printerType = if (PAXPrinter.getInstance().init().equals(Constants.SUCCESS, true)) {
                    enablePrintButton()
                    Constants.PAX_PRINTER
                } else {
                    disablePrintButton()
                    Constants.NO_PRINTER
                }
            }
            else -> {
                //  unexpected status received from PAX
                Toast.makeText(mContext, printerStatus, Toast.LENGTH_SHORT).show()
                printerType = Constants.NO_PRINTER
            }
        }
    }

    private fun enablePrintButton() {
        isPrinterAvailable = true
        globalView.ivPrinter.setOnClickListener(this)
        if (globalView.rvCashHistory.adapter is HistoryTransactionAdapter) {
            (globalView.rvCashHistory.adapter as HistoryTransactionAdapter).notifyDataSetChanged()
        }
    }

    private fun disablePrintButton() {
        isPrinterAvailable = false
        globalView.ivPrinter.isClickable = false
        globalView.ivPrinter.setColorFilter(resources.getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN)
    }

    private fun getShowTaxInfoConfig(): Boolean {
        val paymentService = SessionManager.getInstance(mContext).activationInfo.service_customize.pay.service_features
        //  tax info flag
        var showTaxInfo = false
        for (i in 0 until paymentService.size) {
            when (paymentService[i].name) {

                Constants.TAX_INFO -> {

                    showTaxInfo = null != paymentService[i].flag_value && paymentService[i].flag_value.equals(Constants.YES, ignoreCase = true)
                }
            }
        }
        return showTaxInfo
    }

    private fun getPerEntryConfig(): Boolean {
        val paymentService = SessionManager.getInstance(mContext).activationInfo.service_customize.pay.service_features
        //  print receipt flag
        for (i in 0 until paymentService.size) {
            when (paymentService[i].name) {

                Constants.PRINT_RECEIPT_FEATURE -> {

                    if (null != paymentService[i].attributes && paymentService[i].attributes.size > 0) {
                        for (j in 0 until paymentService[i].attributes.size) {
                            when (paymentService[i].attributes[j].name) {
                                Constants.PER_ENTRY -> isPerEntry = null != paymentService[i].attributes[j].default && paymentService[i].attributes[j].default.equals(Constants.YES, ignoreCase = true)
                            }
                        }
                    }
                }
            }
        }
        return isPerEntry
    }

    private fun getLogoConfig(): Boolean {
        val paymentService = SessionManager.getInstance(mContext).activationInfo.service_customize.pay.service_features
        //  print receipt flag
        for (i in 0 until paymentService.size) {
            when (paymentService[i].name) {

                Constants.PRINT_RECEIPT_FEATURE -> {

                    if (null != paymentService[i].attributes && paymentService[i].attributes.size > 0) {
                        for (j in 0 until paymentService[i].attributes.size) {
                            when (paymentService[i].attributes[j].name) {
                                Constants.PRINT_LOGO -> isPrintLogo = null != paymentService[i].attributes[j].default && paymentService[i].attributes[j].default.equals(Constants.YES, ignoreCase = true)
                            }
                        }
                    }
                }
            }
        }
        return isPrintLogo
    }

    private fun getCompanyNameConfig(): Boolean {
        val paymentService = SessionManager.getInstance(mContext).activationInfo.service_customize.pay.service_features
        //  print receipt flag
        for (i in 0 until paymentService.size) {
            when (paymentService[i].name) {

                Constants.PRINT_RECEIPT_FEATURE -> {

                    if (null != paymentService[i].attributes && paymentService[i].attributes.size > 0) {
                        for (j in 0 until paymentService[i].attributes.size) {
                            when (paymentService[i].attributes[j].name) {
                                Constants.COMPANY_NAME -> isPrintCompanyName = null != paymentService[i].attributes[j].default && paymentService[i].attributes[j].default.equals(Constants.YES, ignoreCase = true)
                            }
                        }
                    }
                }
            }
        }
        return isPrintCompanyName
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.btnTransaction -> {
                isTransaction = true
                //  hide both guest room & master account lists
                globalView.gr_list.visibility = View.GONE
                globalView.ma_list.visibility = View.GONE
                globalView.dc_list.visibility = View.GONE
                //  hide grand total
                globalView.ll_grand_total.visibility = View.GONE
                //hide summary container
                globalView.ivPrinter.visibility = View.GONE
                globalView.ll_summaryContainer.visibility = View.GONE
                globalView.ll_cash_totalPay.visibility = View.GONE
                globalView.txtDateTransaction.visibility = View.VISIBLE
                globalView.tvTotalDayAmount.visibility = View.VISIBLE
                globalView.rvCashHistory.visibility = View.INVISIBLE
                onTransactionClick()
            }

            R.id.btnSummary -> {
                isTransaction = false
                //show summary container
                globalView.ivPrinter.visibility = View.VISIBLE
                globalView.ll_grand_total.visibility = View.VISIBLE
                globalView.txtDateTransaction.visibility = View.GONE
                globalView.ll_summaryContainer.visibility = View.VISIBLE
//                globalView.ll_cash_totalPay.visibility = View.VISIBLE
                globalView.rvCashHistory.visibility = View.INVISIBLE
                globalView.tvTotalDayAmount.visibility = View.GONE

                var timeRange = SimpleDateFormat("HH:mm").format(SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(SessionManager.getInstance(mContext).activeUserDetails.data.auth_history.login_at)) + "-" + Utils.getCurrentDateOrTime("HH:mm")

                val date = "<b>Date </b> $strDateForLabel"
                val time = "<b>Time </b> $timeRange"
                globalView.tvSummaryDate.text = Html.fromHtml(date)
                globalView.tvSummaryTime.text = Html.fromHtml(time)
                onSummaryClick()
            }

            R.id.ivCalendar -> {
                openCalendar()
            }

            R.id.ivPrinter -> {
                makeToast("print summary")
                printSummary()
            }

        }
    }

    private fun openCalendar() {
        DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }

    var date: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val month = monthOfYear + 1

        val monthString = getMonth(month)

        strDateForLabel = "$dayOfMonth $monthString $year"
        strDateForAPI = "$year-$month-$dayOfMonth"


        //load data based on flag
        if (isTransaction) {
            globalView.txtDateTransaction.text = strDateForLabel
            callTransactionAPI()
        } else {
            val date = "<b>Date </b> $strDateForLabel"
            val time: String
            time = if (DateUtils.isToday(myCalendar.timeInMillis)) {
                LogUtils.LOGE("istoday?", "yes today")
                var timeRange = SimpleDateFormat("HH:mm").format(SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(SessionManager.getInstance(mContext).activeUserDetails.data.auth_history.login_at)) + "-" + Utils.getCurrentDateOrTime("HH:mm")
                LogUtils.LOGE("istoday?", timeRange)
                isToday = true
                "<b>Time </b> $timeRange"
            } else {
                isToday = false
                "<b>Time </b> -"
            }
            globalView.tvSummaryDate.text = Html.fromHtml(date)
            globalView.tvSummaryTime.text = Html.fromHtml(time)

            callSummaryAPI()
        }


    }

    fun printTransaction(data: TransactionResponse.Data) {
        //  print receipt
        when {
            printerType.equals(Constants.SUNMI_PRINTER, ignoreCase = true) -> {
                val issueTicketModel: IssueTicketModel? = null
                val summaryModel: SummaryModel? = null

                val activationInfo = SessionManager.getInstance(mContext).activationInfo
                val paymentReceiptModel = PaymentReceiptModel()
                paymentReceiptModel.site_name = activationInfo.site_name

                val address = if(activationInfo.street1.isNullOrEmpty() || activationInfo.street1.isNotBlank()) "\n" else activationInfo.street1 + ", " +
                        if(activationInfo.street2.isNullOrEmpty() || activationInfo.street2.isNotBlank()) "\n" else {activationInfo.street2 + ",\n"} +
                        if(activationInfo.postcode == null) "\n" else activationInfo.postcode.toString() + " " +
                                if(activationInfo.city.isNullOrEmpty() || activationInfo.city.isNotBlank()) "\n" else activationInfo.city + ", " +
                                        if(activationInfo.state.isNullOrEmpty() || activationInfo.state.isNotBlank()) "\n" else activationInfo.state

                paymentReceiptModel.site_address = address

                paymentReceiptModel.isDuplicated_receipt_flag = true

                if (activationInfo.phone_number != null)
                    paymentReceiptModel.site_phone_number = activationInfo.phone_number.toString()

                paymentReceiptModel.date = SimpleDateFormat("dd/MM/yyyy").format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.datePay))
                paymentReceiptModel.time = SimpleDateFormat("hh:mm aa").format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.datePay))
                paymentReceiptModel.number_plate = data.car_plate
                paymentReceiptModel.ticket_id = data.ticketId
                paymentReceiptModel.staff_name = SessionManager.getInstance(mContext).activeUserInfo.data.user.username
                paymentReceiptModel.valet_amount = getString(R.string.rm) + Utils.formatBalance(data.amount)
                paymentReceiptModel.pos_info = activationInfo.pos_info1
                paymentReceiptModel.qr_info = activationInfo.pos_info2
                paymentReceiptModel.payment_method = data.type
//        when (data.type) {
//            1 ->
//                //  cash
//                paymentReceiptModel.payment_method = Constants.CASH_PAYMENT
//
//            2 ->
//                //  charge to room
//                paymentReceiptModel.payment_method = Constants.CHARGE_TO_ROOM
//
//            3 ->
//                //  charge to master
//                paymentReceiptModel.payment_method = Constants.CHARGE_TO_MASTER
//        }
                if (isPrintLogo) {
//            paymentReceiptModel.bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ic_kiplepark_text_logo_black), 140, 30, true)
                    paymentReceiptModel.bitmap = Utils.getBitmapFromVectorDrawable(mContext, R.drawable.logo_four_seasons)
                } else {
                    paymentReceiptModel.bitmap = null
                }
                paymentReceiptModel.receipt_id = data.receipt
                paymentReceiptModel.entry_time = Utils.unixFormatter(data.entryTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).activationInfo.timezone)
                paymentReceiptModel.payment_time = Utils.unixFormatter(data.payTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).activationInfo.timezone)
                paymentReceiptModel.duration = if (isPerEntry) getString(R.string.per_entry) else Utils.getElapsedTimeFromUnix(data.entryTime, data.payTime)

                if (isPrintCompanyName)
                    paymentReceiptModel.parking_operator = SessionManager.getInstance(mContext).activationInfo.partner_name
                else
                    paymentReceiptModel.parking_operator = ""

                paymentReceiptModel.gst_id = SessionManager.getInstance(mContext).activationInfo.gst_id
                paymentReceiptModel.subtotal = getString(R.string.rm) + Utils.formatBalance(data.amount)
                paymentReceiptModel.roundingAdj = getString(R.string.rm) + "0.00"
                paymentReceiptModel.discount = null
                paymentReceiptModel.paid = getString(R.string.rm) + Utils.formatBalance(data.paidAmount)
                paymentReceiptModel.change = if (Utils.formatBalance(data.paymentBalance).contains(getString(R.string.rm))) Utils.formatBalance(data.paymentBalance) else getString(R.string.rm) + Utils.formatBalance(data.paymentBalance)

                PrintReceipts(sunmiPrinter, mContext, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("")
            }
            printerType.equals(Constants.PAX_PRINTER, ignoreCase = true) -> {
                val view = LayoutInflater.from(context).inflate(
                        R.layout.payment_receipt, null)
                val v = view.findViewById<LinearLayout>(R.id.receipt)

                if (isPrintLogo) {
                    v.logo.setImageBitmap(Utils.getBitmapFromVectorDrawable(mContext, R.drawable.logo_four_seasons))
                    v.logo.visibility = View.VISIBLE
                } else {
                    v.logo.setImageBitmap(null)
                    v.logo.visibility = View.GONE
                }

                val activationInfo = SessionManager.getInstance(mContext).activationInfo

                v.site_name1.text = activationInfo.site_name

                val address = if(activationInfo.street1.isNullOrEmpty() || activationInfo.street1.isNotBlank()) "\n" else activationInfo.street1 + ", " +
                        if(activationInfo.street2.isNullOrEmpty() || activationInfo.street2.isNotBlank()) "\n" else {activationInfo.street2 + ",\n"} +
                        if(activationInfo.postcode == null) "\n" else activationInfo.postcode.toString() + " " +
                                if(activationInfo.city.isNullOrEmpty() || activationInfo.city.isNotBlank()) "\n" else activationInfo.city + ", " +
                                        if(activationInfo.state.isNullOrEmpty() || activationInfo.state.isNotBlank()) "\n" else activationInfo.state
                v.site_address1.text = address

                v.receipt_title.text = "[DUPLICATED RECEIPT]"

                v.receipt_number.text = ": " + data.receipt
                v.ticket_id.text = ": " + data.ticketId
                v.car_plate_number.text = ": " + data.car_plate
                v.entry_time.text = ": " + Utils.unixFormatter(data.entryTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).activationInfo.timezone)
                v.payment_time.text = ": " + Utils.unixFormatter(data.payTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).activationInfo.timezone)
                v.duration.text = ": " + if (getPerEntryConfig()) getString(R.string.per_entry) else Utils.getElapsedTimeFromUnix(data.entryTime, data.payTime)
                v.payment_method.text = ": " + data.type
                v.parking_fee.text = ": " + getString(R.string.rm) + Utils.formatBalance(data.amount)
                v.total.text = ": " + getString(R.string.rm) + Utils.formatBalance(data.amount)

                if (isPrintCompanyName)
                    v.tax_info.text = SessionManager.getInstance(mContext).activationInfo.partner_name
                else if (getShowTaxInfoConfig())
                    v.tax_info.text = getString(R.string.tax_info)
                else {
                    v.tax_info.text = null
                    v.tax_info.visibility = View.GONE
                }

                Thread(Runnable {
                    if (PAXPrinter.getInstance().init().equals(Constants.SUCCESS, ignoreCase = true)) {
                        PAXPrinter.getInstance().printView(v)
                        PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN) //  spacing after printing
                        PAXPrinter.getInstance().start()
                    }
                }).start()

            }
            else -> {
                Toast.makeText(context, getString(R.string.no_printer), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun printSummary() {
        when {
            printerType.equals(Constants.SUNMI_PRINTER, ignoreCase = true) -> {
                //  print receipt
                val issueTicketModel: IssueTicketModel? = null
                val paymentReceiptModel: PaymentReceiptModel? = null

                val activationInfo = SessionManager.getInstance(mContext).activationInfo
                val summaryModel = SummaryModel()
                summaryModel.site_name = activationInfo.site_name

                val address = if(activationInfo.street1.isNullOrEmpty() || activationInfo.street1.isNotBlank()) "\n" else activationInfo.street1 + ", " +
                        if(activationInfo.street2.isNullOrEmpty() || activationInfo.street2.isNotBlank()) "\n" else {activationInfo.street2 + ",\n"} +
                        if(activationInfo.postcode == null) "\n" else activationInfo.postcode.toString() + " " +
                                if(activationInfo.city.isNullOrEmpty() || activationInfo.city.isNotBlank()) "\n" else activationInfo.city + ", " +
                                        if(activationInfo.state.isNullOrEmpty() || activationInfo.state.isNotBlank()) "\n" else activationInfo.state
                summaryModel.site_address = address

                val period = dataSummary.data.auth_history.split(" - ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

//        summaryModel.startDateTime = period[0].trim { it <= ' ' }
//        summaryModel.endDateTime = period[1].trim { it <= ' ' }

                summaryModel.startDateTime = strDateForLabel + " " + period[0].trim { it <= ' ' }
                if (isToday)
                    summaryModel.endDateTime = strDateForLabel + " " + period[1].trim { it <= ' ' }
                else
                    summaryModel.endDateTime = strDateForLabel + " " + period[1].trim { it <= ' ' }
                summaryModel.staff_name = SessionManager.getInstance(mContext).activeUserInfo.data.user.username
                summaryModel.cashArrayList = dataSummary.data.transactions.cash
                summaryModel.guestRoomArrayListArrayList = dataSummary.data.transactions.guest_room
                summaryModel.masterAccountArrayListArrayList = dataSummary.data.transactions.master_account
                summaryModel.debitCreditArrayListArrayList = dataSummary.data.transactions.debit_credit
//        summaryModel.ticket_id = data.ticketId
                summaryModel.cashTotal = cashTotal.toString()
                summaryModel.guestRoomTotal = gRoomTotal.toString()
                summaryModel.masterAccTotal = mAccTotal.toString()
                summaryModel.debitCreditTotal = dCreditTotal.toString()
//        summaryModel.transaction_type = data.type
                summaryModel.pos_info = activationInfo.pos_info1
                summaryModel.qr_info = activationInfo.pos_info2

                summaryModel.bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ic_kiplepark_text_logo_black), 280, 60, true)

                summaryModel.grandTotal = grandTotal.toString()

                PrintReceipts(sunmiPrinter, mContext, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("")
            }
            printerType.equals(Constants.PAX_PRINTER, ignoreCase = true) -> {

                Log.e("PAX PRINTER", "Printing conditions all met!!!")
                val view = LayoutInflater.from(this.context).inflate(
                        R.layout.summary_receipt, null)
                val v = view.findViewById<LinearLayout>(R.id.receipt)

                val activationInfo = SessionManager.getInstance(mContext).activationInfo

                v.site_name1.text = activationInfo.site_name

                val address = if(activationInfo.street1.isNullOrEmpty() || activationInfo.street1.isNotBlank()) "\n" else activationInfo.street1 + ", " +
                        if(activationInfo.street2.isNullOrEmpty() || activationInfo.street2.isNotBlank()) "\n" else {activationInfo.street2 + ",\n"} +
                        if(activationInfo.postcode == null) "\n" else activationInfo.postcode.toString() + " " +
                                if(activationInfo.city.isNullOrEmpty() || activationInfo.city.isNotBlank()) "\n" else activationInfo.city + ", " +
                                        if(activationInfo.state.isNullOrEmpty() || activationInfo.state.isNotBlank()) "\n" else activationInfo.state
                v.site_address1.text = address

                v.staff_name.text = SessionManager.getInstance(mContext).activeUserInfo.data.user.username

                val period = dataSummary.data.auth_history.split(" - ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                v.start_date.text = strDateForLabel + " " + period[0].trim { it <= ' ' }
                if (isToday)
                    v.end_date.text = strDateForLabel + " " + period[1].trim { it <= ' ' }
                else
                    v.end_date.text = strDateForLabel + " " + period[1].trim { it <= ' ' }

                //  cash list
                var cashMultiplier = 0
                for (item in dataSummary.data.transactions.cash) {
                    val cashItem = LayoutInflater.from(this.context).inflate(R.layout.item_history_summary_receipt, null)
                    cashItem.tvTxnID.text = item.ticketId + " - " + item.type
                    cashItem.tvTxnID.textSize = 12F

                    if (item.type.contains("["))
                        cashMultiplier++

                    if (item.type.contains("[Void]", ignoreCase = true))
                        cashItem.tvTxnAmount.text = "- RM " + Utils.formatBalance(item.amount)
                    else
                        cashItem.tvTxnAmount.text = getString(R.string.rm) + Utils.formatBalance(item.amount)

                    cashItem.tvTxnAmount.textSize = 12F

                    v.summary_cash_list.addView(cashItem)
                    Log.i("cash item ID", item.ticketId)
                }

                v.summary_cash_list.layoutParams.height = Utils.getViewHeight(v.summary_cash_list) + (cashMultiplier * 28)  //  28 = height of textview with default text size + parent layout top margin

                v.cash_total.text = getString(R.string.rm) + Utils.formatBalance(cashTotal)

                //  guestroom list
                var guestroomMultiplier = 0
                for (item in dataSummary.data.transactions.guest_room) {
                    val guestroomItem = LayoutInflater.from(this.context).inflate(R.layout.item_history_summary_receipt, null)
                    guestroomItem.tvTxnID.text = item.ticketId + " - " + item.type
                    guestroomItem.tvTxnID.textSize = 12F

//                    if(item.type.contains("["))
                    guestroomMultiplier++

                    if (item.type.contains("[Void]", ignoreCase = true))
                        guestroomItem.tvTxnAmount.text = "- RM " + Utils.formatBalance(item.amount)
                    else
                        guestroomItem.tvTxnAmount.text = getString(R.string.rm) + Utils.formatBalance(item.amount)

                    guestroomItem.tvTxnAmount.textSize = 12F

                    v.guestroom_list.addView(guestroomItem)
                    Log.i("guestroom item ID", item.ticketId)
                }

                v.guestroom_list.layoutParams.height = Utils.getViewHeight(v.guestroom_list) + (guestroomMultiplier * 28)  //  28 = height of textview with default text size + parent layout top margin

                v.guestroom_total.text = getString(R.string.rm) + Utils.formatBalance(gRoomTotal)

                //  master account list
                var masteraccountMultiplier = 0
                for (item in dataSummary.data.transactions.master_account) {
                    val masterAccountItem = LayoutInflater.from(this.context).inflate(R.layout.item_history_summary_receipt, null)
                    masterAccountItem.tvTxnID.text = item.ticketId + " - " + item.type
                    masterAccountItem.tvTxnID.textSize = 12F

//                    if(item.type.contains("["))
                    masteraccountMultiplier++

                    if (item.type.contains("[Void]", ignoreCase = true))
                        masterAccountItem.tvTxnAmount.text = "- RM " + Utils.formatBalance(item.amount)
                    else
                        masterAccountItem.tvTxnAmount.text = getString(R.string.rm) + Utils.formatBalance(item.amount)

                    masterAccountItem.tvTxnAmount.textSize = 12F

                    v.masteraccount_list.addView(masterAccountItem)
                    Log.i("masteraccount item ID", item.ticketId)
                }

                v.masteraccount_list.layoutParams.height = Utils.getViewHeight(v.masteraccount_list) + (masteraccountMultiplier * 28)  //  28 = height of textview with default text size + parent layout top margin

                v.masteraccount_total.text = getString(R.string.rm) + Utils.formatBalance(mAccTotal)

                //  debit credit list
                var debitcreditMultiplier = 0
                for (item in dataSummary.data.transactions.debit_credit) {
                    val debitCreditItem = LayoutInflater.from(this.context).inflate(R.layout.item_history_summary_receipt, null)
                    debitCreditItem.tvTxnID.text = item.ticketId + " - " + item.type
                    debitCreditItem.tvTxnID.textSize = 12F

//                    if(item.type.contains("["))
                    debitcreditMultiplier++

                    if (item.type.contains("[Void]", ignoreCase = true))
                        debitCreditItem.tvTxnAmount.text = "- RM " + Utils.formatBalance(item.amount)
                    else
                        debitCreditItem.tvTxnAmount.text = getString(R.string.rm) + Utils.formatBalance(item.amount)

                    debitCreditItem.tvTxnAmount.textSize = 12F

                    v.debit_credit_list.addView(debitCreditItem)
                    Log.i("debit credit item ID", item.ticketId)
                }

                v.debit_credit_list.layoutParams.height = Utils.getViewHeight(v.debit_credit_list) + (debitcreditMultiplier)

                v.debit_credit_total.text = getString(R.string.rm) + Utils.formatBalance(dCreditTotal)

                v.grand_total.text = getString(R.string.rm) + Utils.formatBalance(grandTotal)

                Thread(Runnable {
                    if (PAXPrinter.getInstance().init().equals(Constants.SUCCESS, ignoreCase = true)) {
                        PAXPrinter.getInstance().printView(v)
                        PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN) //  spacing after printing
                        PAXPrinter.getInstance().start()
                    }
                }).start()
            }
            else -> {
                Toast.makeText(context, getString(R.string.no_printer), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateLabel() {
        val sdfDate = SimpleDateFormat("yyyy-MM-dd")
        val sdfDateLabel = SimpleDateFormat("dd MMM yyyy")
        val sdfTime = SimpleDateFormat("hh:mm aa")

        strDateForLabel = Utils.unixFormatter(Constants.SERVER_TIME, "dd MMM yyyy", SessionManager.getInstance(mContext).activationInfo.timezone)
        strDateForAPI = Utils.unixFormatter(Constants.SERVER_TIME, "yyyy-MM-dd", SessionManager.getInstance(mContext).activationInfo.timezone)

//        Log.i("mmmm ", "strDateForLabel = " + strDateForLabel + "strDateForAPI = " + strDateForAPI)

        selectedTime = Utils.unixFormatter(Constants.SERVER_TIME, "hh:mm aa", SessionManager.getInstance(mContext).activationInfo.timezone)

        globalView.txtDateTransaction.text = strDateForLabel

        // selectedDate = sdfDateReq.format(Date())

    }

    //06-27 09:41:37.647 30914-30914/com.kiple.kiplepbox.sandbox I/mmmm: strDateForLabel = 27 Jun 2018strDateForAPI = 2018-06-27
    private fun getMonth(month: Int): String {
        return DateFormatSymbols().shortMonths[month - 1]
    }

    override fun onSuccessCallBack(response: Response<*>?, apiFlags: Constants.ApiFlags?) {

        if (null != apiFlags && null != context) {
            when (apiFlags) {
                Constants.ApiFlags.HISTORY_SUMMARY -> {
                    if (response?.body() != null) {
                        var summaryResponse: SummaryResponse = response.body() as SummaryResponse
                        dataSummary = summaryResponse

                        //set header data
                        globalView.tvSummaryPlaceName.text = dataSummary.data.site_information.name
                        globalView.tvSummaryAddress.text = dataSummary.data.site_information.street1 + ", " + if(dataSummary.data.site_information.street2.isNullOrEmpty() || dataSummary.data.site_information.street2.isNullOrBlank()) "" else {dataSummary.data.site_information.street2 + ", "} + dataSummary.data.site_information.city + ", " + dataSummary.data.site_information.state
                        globalView.tvSummaryPersonName.text = SessionManager.getInstance(mContext).activeUserInfo.data.user.username

                        //  set time
                        if (!dataSummary.data.auth_history.isNullOrEmpty()) {
                            val time = "<b>Time </b> ${dataSummary.data.auth_history}"
                            globalView.tvSummaryTime.text = Html.fromHtml(time)
                        }

                        if (dataSummary.data.transactions.cash.size > 0 || dataSummary.data.transactions.guest_room.size > 0 || dataSummary.data.transactions.master_account.size > 0 || dataSummary.data.transactions.debit_credit.size > 0) {
                            //  cash payment summary list
                            if (dataSummary.data.transactions.cash.size > 0) {
                                globalView.cash_list.visibility = View.VISIBLE
                                globalView.rvCashHistory.visibility = View.VISIBLE
                                globalView.tvNoData.visibility = View.GONE

//                            var totalAmount = 0
                                cashTotal = 0.00
                                summaryResponse.data.transactions.cash.forEachIndexed { index, transactions ->
                                    if (!summaryResponse.data.transactions.cash[index].type.isNullOrEmpty() && !summaryResponse.data.transactions.cash[index].type.contains("[Void]", ignoreCase = true))
                                        cashTotal += summaryResponse.data.transactions.cash[index].amount.toDouble()
                                    else if (!summaryResponse.data.transactions.cash[index].type.isNullOrEmpty() && summaryResponse.data.transactions.cash[index].type.contains("[Void]", ignoreCase = true))
                                        cashTotal -= summaryResponse.data.transactions.cash[index].amount.toDouble()
                                }
                                globalView.ll_cash_totalPay.visibility = View.VISIBLE
                                globalView.tvCashTotalAmount.text = "RM ${Utils.formatBalance(cashTotal)}"

                                globalView.rvCashHistory.adapter = HistorySummaryAdapter(dataSummary.data.transactions, mContext, Constants.CASH_PAYMENT)
                                globalView.rvCashHistory.visibility = View.VISIBLE
                            } else {
                                //  clear & hide list if no relevant data
                                globalView.rvCashHistory.adapter = null
                                globalView.rvCashHistory.visibility = View.GONE
                            }

                            //  guest room payment summary list
                            if (dataSummary.data.transactions.guest_room.size > 0) {
                                globalView.gr_list.visibility = View.VISIBLE
                                globalView.rvGRHistory.visibility = View.VISIBLE
                                globalView.tvNoData.visibility = View.GONE

//                            var totalAmount = 0
                                gRoomTotal = 0.00
                                summaryResponse.data.transactions.guest_room.forEachIndexed { index, transactions ->
                                    if (!summaryResponse.data.transactions.guest_room[index].type.isNullOrEmpty() && !summaryResponse.data.transactions.guest_room[index].type.contains("[Void]", ignoreCase = true))
                                        gRoomTotal += summaryResponse.data.transactions.guest_room[index].amount.toDouble()
                                    else if (!summaryResponse.data.transactions.guest_room[index].type.isNullOrEmpty() && summaryResponse.data.transactions.guest_room[index].type.contains("[Void]", ignoreCase = true))
                                        gRoomTotal -= summaryResponse.data.transactions.guest_room[index].amount.toDouble()
                                }
                                globalView.tvGRTotalAmount.text = "RM ${Utils.formatBalance(gRoomTotal)}"

                                globalView.rvGRHistory.adapter = HistorySummaryAdapter(dataSummary.data.transactions, mContext, Constants.CHARGE_TO_ROOM)
                                globalView.rvGRHistory.visibility = View.VISIBLE
                            } else {
                                //  clear & hide list if no relevant data
                                globalView.rvGRHistory.adapter = null
                                globalView.rvGRHistory.visibility = View.GONE
                            }

                            //  master account payment summary list
                            if (dataSummary.data.transactions.master_account.size > 0) {
                                globalView.ma_list.visibility = View.VISIBLE
                                globalView.rvMAHistory.visibility = View.VISIBLE
                                globalView.tvNoData.visibility = View.GONE

//                            var totalAmount = 0
                                mAccTotal = 0.00
                                summaryResponse.data.transactions.master_account.forEachIndexed { index, transactions ->
                                    if (!summaryResponse.data.transactions.master_account[index].type.isNullOrEmpty() && !summaryResponse.data.transactions.master_account[index].type.contains("[Void]", ignoreCase = true))
                                        mAccTotal += summaryResponse.data.transactions.master_account[index].amount.toDouble()
                                    else if (!summaryResponse.data.transactions.master_account[index].type.isNullOrEmpty() && summaryResponse.data.transactions.master_account[index].type.contains("[Void]", ignoreCase = true))
                                        mAccTotal -= summaryResponse.data.transactions.master_account[index].amount.toDouble()
                                }
                                globalView.tvMATotalAmount.text = "RM ${Utils.formatBalance(mAccTotal)}"

                                globalView.rvMAHistory.adapter = HistorySummaryAdapter(dataSummary.data.transactions, mContext, Constants.CHARGE_TO_MASTER)
                                globalView.rvMAHistory.visibility = View.VISIBLE
                            } else {
                                //  clear & hide list if no relevant data
                                globalView.rvMAHistory.adapter = null
                                globalView.rvMAHistory.visibility = View.GONE
                            }

                            //  debit card payment summary list
                            if (dataSummary.data.transactions.debit_credit.size > 0) {
                                globalView.dc_list.visibility = View.VISIBLE
                                globalView.rvDCHistory.visibility = View.VISIBLE
                                globalView.tvNoData.visibility = View.GONE

//                            var totalAmount = 0
                                dCreditTotal = 0.00
                                summaryResponse.data.transactions.debit_credit.forEachIndexed { index, transactions ->
                                    if (!summaryResponse.data.transactions.debit_credit[index].type.isNullOrEmpty() && !summaryResponse.data.transactions.debit_credit[index].type.contains("[Void]", ignoreCase = true))
                                        dCreditTotal += summaryResponse.data.transactions.debit_credit[index].amount.toDouble()
                                    else if (!summaryResponse.data.transactions.debit_credit[index].type.isNullOrEmpty() && summaryResponse.data.transactions.debit_credit[index].type.contains("[Void]", ignoreCase = true))
                                        dCreditTotal -= summaryResponse.data.transactions.debit_credit[index].amount.toDouble()
                                }
                                globalView.tvDCTotalAmount.text = "RM ${Utils.formatBalance(dCreditTotal)}"

                                globalView.rvDCHistory.adapter = HistorySummaryAdapter(dataSummary.data.transactions, mContext, Constants.CREDIT_DEBIT)
                                globalView.rvDCHistory.visibility = View.VISIBLE
                            } else {
                                //  clear & hide list if no relevant data
                                globalView.rvDCHistory.adapter = null
                                globalView.rvDCHistory.visibility = View.GONE
                            }

                            //  set grand total
                            grandTotal = cashTotal + gRoomTotal + mAccTotal + dCreditTotal
                            globalView.tvGrandTotalAmount.text = "RM ${Utils.formatBalance(grandTotal)}"

//                            setSummaryAdapter()
                        } else {
                            globalView.tvNoData.visibility = View.VISIBLE
                            globalView.cash_list.visibility = View.GONE
                            globalView.gr_list.visibility = View.GONE
                            globalView.ma_list.visibility = View.GONE
                            globalView.dc_list.visibility = View.GONE
                            //  set grand total
                            grandTotal = 0.00
                            globalView.tvGrandTotalAmount.text = "RM ${Utils.formatBalance(grandTotal)}"
                        }
                    } else {

                    }
                }

                Constants.ApiFlags.HISTORY_TRANSACTION -> {
                    if (null != response?.body()) {
                        var transactionResponse: TransactionResponse = response.body() as TransactionResponse
                        listTransaction = arrayListOf()
                        listTransaction.addAll(transactionResponse.data)

                        var totalAmount = 0.00
                        if (transactionResponse.data.size > 0) {
                            globalView.tvNoData.visibility = View.GONE
                            globalView.rvCashHistory.visibility = View.VISIBLE

                            transactionResponse.data.forEachIndexed { index, transactions ->
                                if (!transactionResponse.data[index].type.contains("[void]", ignoreCase = true))
                                    totalAmount += transactionResponse.data[index].amount.toDouble()
                            }
                            globalView.tvTotalDayAmount.visibility = View.VISIBLE
                            globalView.tvTotalDayAmount.text = getString(R.string.rm) + Utils.formatBalance(totalAmount)

//                            globalView.rvCashHistory.adapter = HistorySummaryAdapter(listTransaction, mContext, Constants.CASH_PAYMENT)
//                            globalView.rvCashHistory.visibility = View.VISIBLE
                            globalView.cash_list.visibility = View.VISIBLE
                            setDataAdapter()
                        } else {
                            globalView.tvTotalDayAmount.visibility = View.GONE
                            globalView.tvNoData.visibility = View.VISIBLE
                            globalView.rvCashHistory.visibility = View.GONE
                        }
                    } else {

                    }

                }

                Constants.ApiFlags.HISTORY_VOID_TXN -> {
                    if (response?.body() != null) {
                        var voidTransactionResponse: VoidTransactionResponse = response.body() as VoidTransactionResponse
                        if (voidTransactionResponse.status.equals("success", ignoreCase = true)) {
                            openVoidRemarkDialog()
                            mBottomSheetDialogTxn.dismiss()
                        } else {
                            Toast.makeText(activity, voidTransactionResponse.message, Toast.LENGTH_SHORT).show()

                        }

                    }

                    //  firebase event tracking
                    val bundle = Bundle()
                    bundle.putString("Status", "void_payment")
                    bundle.putString("PaymentType", txnData.type)
                    bundle.putDouble("Value", java.lang.Double.parseDouble(txnData.amount))
                    CustomApplication.reportFirebaseCustomEvent("Payment", bundle)

                }

                Constants.ApiFlags.HISTORY_VOID_REMARK -> {
                    if (response?.body() != null) {
                        var voidTransactionResponse: VoidTransactionResponse = response.body() as VoidTransactionResponse
                        if (voidTransactionResponse.status.equals("success", ignoreCase = true)) {
                            if (mBottomSheetDialogRemark != null) {
                                mBottomSheetDialogRemark.dismiss()

                            }

                            if (mBottomSheetDialogTxn != null) {
                                mBottomSheetDialogTxn.dismiss()
                            }
                            //refresh the list
                            callTransactionAPI()
                        } else {
                            Toast.makeText(activity, voidTransactionResponse.message, Toast.LENGTH_SHORT).show()

                        }

                    }

                }

            }
        }

    }

    override fun onError(response: Response<*>?, apiFlags: Constants.ApiFlags?) {

        when (apiFlags) {

            Constants.ApiFlags.HISTORY_VOID_TXN -> {
                if (response?.errorBody() != null) {

                    if (response.code() == 401) {
                        makeToast("Incorrect PIN")
                    }
                }
            }
            Constants.ApiFlags.HISTORY_VOID_REMARK -> {

            }
            else -> {

            }
        }

    }


    private fun setDataAdapter() {
        globalView.rvCashHistory.adapter = HistoryTransactionAdapter(historyFragment, listTransaction, mContext)
        globalView.rvCashHistory.visibility = View.VISIBLE
    }

//    private fun setSummaryAdapter() {
//        globalView.rvCashHistory.adapter = HistorySummaryAdapter(dataSummary, mContext)
//        globalView.rvCashHistory.visibility = View.VISIBLE
//    }

    private fun onTransactionClick() {
        globalView.btnTransaction.setBackgroundResource(R.drawable.button_rounded_blue)
        globalView.btnTransaction.setTextColor(ContextCompat.getColor(mContext, R.color.white))

        globalView.btnSummary.setBackgroundResource(R.drawable.button_rounded_grey)
        globalView.btnSummary.setTextColor(ContextCompat.getColor(mContext, R.color.kiple_blue))

        //api calling
        callTransactionAPI()
    }

    private fun onSummaryClick() {
        globalView.btnSummary.setBackgroundResource(R.drawable.button_rounded_blue)
        globalView.btnSummary.setTextColor(ContextCompat.getColor(mContext, R.color.white))

        globalView.btnTransaction.setBackgroundResource(R.drawable.button_rounded_grey)
        globalView.btnTransaction.setTextColor(ContextCompat.getColor(mContext, R.color.kiple_blue))
        //api calling
        callSummaryAPI()
    }


    private fun getVoidParams(strPIN: String): HashMap<String, Any> {
        val params = HashMap<String, Any>()
        try {
            params["site_id"] = SessionManager.getInstance(mContext).activationInfo.site_id //change this to signed in user
            params["password"] = strPIN
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return params
    }

    private fun getVoidReviewParams(strRemark: String): HashMap<String, Any> {
        val params = HashMap<String, Any>()
        try {
            params["site_id"] = SessionManager.getInstance(mContext).activationInfo.site_id //change this to signed in user
            params["ticket_id"] = txnData.ticketId
            params["remarks"] = strRemark
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return params
    }

    /**
     * calling from adapter item click
     */

    fun openVoidTxnDialog(data: TransactionResponse.Data) {
        val activity = mContext as Activity
        //save data locally
        txnData = data
        mBottomSheetDialogTxn = BottomSheetDialog(activity)
        val sheetView = activity.layoutInflater.inflate(R.layout.dialog_remove_transaction_history, null)
        mBottomSheetDialogTxn.setContentView(sheetView)
        sheetView.btnSubmitVoidTxn.setOnClickListener {
            if (!TextUtils.isEmpty(sheetView.etVoidTxnPin.text.toString())) {
                callVoidTransactionAPI(sheetView.etVoidTxnPin.text.toString())
            } else {
                Toast.makeText(mContext, "Enter your PIN", Toast.LENGTH_SHORT).show()

            }
        }


        sheetView.btnDiscardVoidTxn.setOnClickListener {
            mBottomSheetDialogTxn.dismiss()
        }

        mBottomSheetDialogTxn.show()
    }


    /**
     * void remark dialog
     */
    private fun openVoidRemarkDialog() {
        val activity = mContext as Activity

        mBottomSheetDialogRemark = BottomSheetDialog(activity)
        val sheetView = activity.layoutInflater.inflate(R.layout.dialog_void_remark, null)
        mBottomSheetDialogRemark.setContentView(sheetView)
        sheetView.btnSubmitRemark.setOnClickListener {
            if (!TextUtils.isEmpty(sheetView.etReview.text.toString())) {
                callVoidRemarkAPI(sheetView.etReview.text.toString())
            } else {
                Toast.makeText(mContext, "Enter your remark", Toast.LENGTH_SHORT).show()

            }
        }
        mBottomSheetDialogRemark.show()
    }


    /**
     * VOID API
     */

    private fun callVoidTransactionAPI(strPIN: String) {


        APICall.historyVoidTransaction(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).activeUserInfo.data.token, getVoidParams(strPIN))

    }

    /**
     * VOID REMARK API
     */

    private fun callVoidRemarkAPI(strReview: String) {
        APICall.historyVoidRemarkTransaction(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).activeUserInfo.data.token, getVoidReviewParams(strReview))

    }

    /**
     * TRANSACTION API
     */

    private fun callTransactionAPI() {
        APICall.historyTransaction(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).activeUserInfo.data.token, getParams())

    }

    /**
     * SUMMARY API
     */
    private fun callSummaryAPI() {
        APICall.historySummary(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).activeUserInfo.data.token, getParams())

    }


}