package com.kiple.kipleBox.ui.core.cash_payment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.utils.Utils;

public class PackageSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final String[] items;
    private final int mResource;

    public PackageSpinnerAdapter(@NonNull Context context, int resource, @NonNull Object[] objects) {
        super(context, resource, 0, (String[]) objects);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.mResource = resource;
        this.items = (String[]) objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView text1 = view.findViewById(R.id.text1);
        TextView text2 = view.findViewById(R.id.text2);

        String[] label = items[position].split(",");

        text1.setText(label[0]);
        if(position > 0 && position < items.length - 1)    //  skip first & last item
            text2.setText(mContext.getString(R.string.rm) + Utils.formatBalance(label[1]));

//        LogUtils.LOGI("spinner adapter", label[0]);
//        LogUtils.LOGI("spinner adapter", label[1]);

        return view;
    }
}
