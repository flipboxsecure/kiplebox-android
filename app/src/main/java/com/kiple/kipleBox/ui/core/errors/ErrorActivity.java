package com.kiple.kipleBox.ui.core.errors;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;

/**
 * Created by xenhao.yap on 19/03/2018.
 */

public class ErrorActivity extends AppCompatActivity implements View.OnClickListener {

    TextView error_title, error_desc;
    ImageView error_image;
    Button error_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_errors);

        this.error_btn      = findViewById(R.id.error_btn);
        this.error_title    = findViewById(R.id.error_title);
        this.error_desc     = findViewById(R.id.error_description);
        this.error_image    = findViewById(R.id.image);
        error_btn.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
            processError(bundle.getString(Constants.ERROR_MSG_BUNDLE));
    }

    @Override
    public void onBackPressed() {
        //  do nothing here
        //  so that users cannot override or skip this page
    }

    @Override
    public void onStop() {
        //  set default behaviour if user kill app at this stage
        setResult(Activity.RESULT_OK);
        finish();
        super.onStop();
    }

    private void processError(String type){
        switch (type){
            case Constants.INSUFFICIENT_BALANCE:
                initInsufficientBalance();
                break;

            case Constants.SYSTEM_ERROR:
                initSystemError();
                break;

            case Constants.FAILED_REFUND:
                initFailedRefund();
                break;

            case Constants.PAYMENT_FAILED:
                initPaymentFailed();
                break;

            case Constants.TICKET_PAID:
                initTicketPaid();
                break;

            case Constants.TICKET_EXPIRED:
                initTicketExpired();
                break;

            case Constants.INVALID_TICKET:
                initInvalidTicket();
                break;

            case Constants.CLOCK_OUT:
                initClockOut();
                break;
        }
    }

    private void initClockOut() {
        error_title.setText(getString(R.string.clock_out));
        error_desc.setText(getString(R.string.clock_out_msg));
        error_image.setImageResource(R.drawable.clock_out);
        error_btn.setText(getString(R.string.goodbye));
        error_btn.setOnClickListener(v -> {
            setResult(Activity.RESULT_OK);
            finish();
        });
    }

    private void initSystemError() {
//        error_title.setText(getString(R.string.system_error_title));
//        error_desc.setText(getString(R.string.system_error_desc));
//        error_image.setImageResource(R.drawable.system_error);
//        error_btn.setText(getString(R.string.retry));
//        error_btn.setOnClickListener(v -> {
//            setResult(Activity.RESULT_OK);
//            finish();
//        });
    }

    private void initFailedRefund() {
//        error_title.setText(getString(R.string.failed_refund_title));
//        error_desc.setText(getString(R.string.failed_refund_desc));
//        error_image.setImageResource(R.drawable.fail_refund);
//        error_btn.setText(getString(R.string.contact_us));
//        error_btn.setOnClickListener(v -> {
//            setResult(Activity.RESULT_OK);
//            finish();
//        });
    }

    private void initPaymentFailed() {
//        error_title.setText(getString(R.string.payment_failed_title));
//        error_desc.setText(getString(R.string.payment_failed_desc));
//        error_image.setImageResource(R.drawable.payment_fail);
//        error_btn.setText(getString(R.string.retry));
//        error_btn.setOnClickListener(v -> {
//            setResult(Activity.RESULT_OK);
//            finish();
//        });
    }

    private void initTicketPaid() {
        error_title.setText(getString(R.string.ticket_paid_title));
        error_desc.setText(getString(R.string.ticket_paid_desc));
        error_image.setImageResource(R.drawable.ticket_paid);
        error_btn.setText(getString(R.string.ok));
        error_btn.setOnClickListener(v -> {
            setResult(Activity.RESULT_OK);
            finish();
        });
    }

    private void initTicketExpired() {
        error_title.setText(getString(R.string.ticket_expired_title));
        error_desc.setText(getString(R.string.ticket_expired_desc));
        error_image.setImageResource(R.drawable.ticket_expired);
        error_btn.setText(getString(R.string.ok));
        error_btn.setOnClickListener(v -> {
            setResult(Activity.RESULT_OK);
            finish();
        });
    }

    private void initInsufficientBalance(){
//        error_title.setText(getString(R.string.insufficient_balance_title));
//        error_desc.setText(getString(R.string.insufficient_balance_desc));
//        error_image.setImageResource(R.drawable.wallet_balance);
//        error_btn.setText(getString(R.string.topup));
//        error_btn.setOnClickListener(v -> {
//            setResult(Activity.RESULT_OK);
//            finish();
//        });
    }

    private void initInvalidTicket(){
        error_title.setText(getString(R.string.invalid_ticket_title));
        error_desc.setText(getString(R.string.invalid_ticket_desc));
        error_image.setImageResource(R.drawable.invalid_ticket);
        error_btn.setText(getString(R.string.zxing_button_ok));
        error_btn.setOnClickListener(v -> {
            setResult(Activity.RESULT_OK);
            finish();
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.error_btn:
                break;
        }
    }
}
