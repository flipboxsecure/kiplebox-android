package com.kiple.kipleBox.ui.core.homepage;

import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.kiple.kipleBox.R;
import com.sunmi.impl.V1Printer;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomepageActivity extends AppCompatActivity implements View.OnClickListener{

    @BindView(R.id.number_plate_parent)
    TextInputLayout number_plate_parent;
    @BindView(R.id.number_plate)
    TextInputEditText number_plate;
    @BindView(R.id.mobile_number_parent)
    TextInputLayout mobile_number_parent;
    @BindView(R.id.mobile_number)
    TextInputEditText mobile_number;
    @BindView(R.id.car_key_switch)
    Switch car_key_collected;
    @BindView(R.id.issue_ticket_btn)
    Button issue_ticket_btn;

    private V1Printer printer;
    private String content = "eyAgDQogICAic2l0ZV9pZCI6IlNJRzAwMDAwMDciLA0KICAgInBvc19pZCI6IjAwMDAwMDAwMSIsDQogICAibnVtYmVyX3BsYXRlIjoiUFVUUkFKQVlBOTk5OV5eIiwNCiAgICJkYXRldGltZSI6IjIwMTgwNDIwMjMxMiIsDQogICAicGhvbmVfbm8iOiI2MDE3NTY0ODQyNF5eXl4iLA0KICAgImlkZW50aWZ5IjoiVENWIiwNCiAgICJ0aWNrZXRfbm8iOiIwMDAwMDAxIg0KfQ==";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_homepage);
        ButterKnife.bind(this);

        this.printer = new V1Printer(this);
        printer.printerInit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.issue_ticket_btn:
//                this.runOnUiThread(new Runnable() {
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "issue ticket button clicked", Toast.LENGTH_SHORT).show();
//                        new PrintReceipts(printer, getApplicationContext(), content).execute("");
//                    }
//                });

//                new Handler().postDelayed(
////                        new Runnable() {
////                            @Override
////                            public void run() {
////                                Toast.makeText(getApplicationContext(), "issue ticket button clicked", Toast.LENGTH_SHORT).show();
////                                topUpPrint(printer, content);
////                            }
////                        },
////                        0
////                );
//                new PrintReceipts(printer, this, content).execute("");
                break;
        }
    }

    private void topUpPrint(V1Printer printer, String QR) {
        try {

            Toast.makeText(this, "print receipt method called", Toast.LENGTH_SHORT).show();

            DateFormat df = new DateFormat();
            Date date = new Date();
            String str_date = String.valueOf(df.format("dd MMMM yyyy", date));
            String str_time = String.valueOf(df.format("h:mm a", date));

//            printer.printOriginalText("Kiple Sale\n");
//            printer.printText("========================\n");
//            printer.printText("\n");
//            printer.setFontSize(26);
//            printer.printText(pos.getBrandName() + "\n");
//            printer.setFontSize(23);
//            printer.printText(pos.getMerchantAddress() + "\n");
//            printer.printText("Tel: " + pos.getMerchantPH() + "\n\n");
//            printer.setAlignment(0);
            printer.printText("Date: " + str_date + " | " + str_time);
            printer.setAlignment(1);
            printer.printBarCode(QR, BarcodeFormat.QR_CODE, 400, 400);
//            printer.printBarCode(topUp.getQRcode(), BarcodeFormat.QR_CODE, 400, 400);
//            printer.printText("--------------------------------\n");
//            printer.printText("Top Up Amount:" + "\n" + topUp.getAmount() + "\n");
//            printer.printText("--------------------------------\n");
//            printer.setAlignment(1);
//            printer.setFontSize(32);
//            printer.printText("\n");
//            printer.printOriginalText("Approved" + "\n");
//            printer.printText("\n");
//            printer.setFontSize(22);
//            printer.printText("HAVE A NICE DAY\n");
//            printer.printText("Thank you for topup with Kiple!\n");
//            printer.printText("\n");
//            printer.printOriginalText("www.kiplepay.com\n");
//            printer.lineWrap(3);

            printer.commitTransaction();
        } catch (Exception e) {
            Log.e("PrintTopup", e.getMessage());

        }
    }
}
