package com.kiple.kipleBox.ui.core.lists;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.BaseFragment;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.RoundedImageView;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.database.DBInterface;
import com.kiple.kipleBox.database.LocalCacheManager;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.SimpleStatusResponse;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.core.homepage.HomepageFragment;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class TicketListsFragment extends BaseFragment implements CallBackInterface{

    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.ticket_list)
    RecyclerView ticket_list;
    @BindView(R.id.refresh_list)
    SwipeRefreshLayout refresh_layout;
    @BindView(R.id.ticket_issued_tab)
    LinearLayout ticket_issued_tab;
    @BindView(R.id.parked_tab)
    LinearLayout parked_tab;
    @BindView(R.id.paid_tab)
    LinearLayout paid_tab;
    @BindView(R.id.pending_collection_tab)
    LinearLayout pending_collection_tab;
    @BindView(R.id.ticket_issued_txt)
    TextView ticket_issued_txt;
    @BindView(R.id.parked_txt)
    TextView parked_txt;
    @BindView(R.id.paid_txt)
    TextView paid_txt;
    @BindView(R.id.pending_collection_txt)
    TextView pending_collection_txt;
    @BindView(R.id.edtSearch)
    EditText edtSearch;

    @BindView(R.id.avatar)
    RoundedImageView logout_btn;

    private Context mContext;
    private String listFlag;
    private TicketListAdapter ticketListAdapter;
    private ArrayList<TicketListsResponse> fullTicketList, ticketList;
    private int mCurrentTab = 0;

    private long currentServerUnix;
    private long startingElapsed;

    private String idToSkip = null;

    private String carToHighlight;

    public static final String BROADCAST_ACTION = "com.kipleBox.broadcastreceiverupdate";

    private Printer mSunmiPrinter;
    private String printerType;
    private int qrSize = 200;
    private boolean isCustomQrSize = false;
    private boolean upfrontFlag = false;
    private boolean isPrintFromList = false;

    MyBroadCastReceiver myBroadCastReceiver;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        listener.hideBottomNavigation();

        View v = inflater.inflate(R.layout.ticket_lists, null);

        ButterKnife.bind(this, v);


        //  get lists
        if(getArguments() != null){
            listFlag = getArguments().getString(Constants.TICKET_LIST_TYPE);
            if(listFlag != null) {
                switch (listFlag) {
                    case Constants.TICKET_ISSUED_LIST:
                        resetOriginalTabs();
                        ticket_issued_tab.setBackground(mContext.getResources().getDrawable(R.drawable.ticket_issued_button_transparent));
                        ticket_issued_txt.setTextColor(mContext.getResources().getColor(R.color.ticket_issued_button));
                        mCurrentTab = 0;
                        doGetList(Constants.TICKET_ISSUED_INT);
                        break;

                    case Constants.TICKET_PARKED_LIST:
                        resetOriginalTabs();
                        parked_tab.setBackground(mContext.getResources().getDrawable(R.drawable.parked_button_transparent));
                        parked_txt.setTextColor(mContext.getResources().getColor(R.color.parked_button));
                        mCurrentTab = 1;
                        doGetList(Constants.TICKET_PARKED_INT);
                        break;

                    case Constants.TICKET_PAID_LIST:
                        resetOriginalTabs();
                        paid_tab.setBackground(mContext.getResources().getDrawable(R.drawable.paid_button_transparent));
                        paid_txt.setTextColor(mContext.getResources().getColor(R.color.paid_button));
                        mCurrentTab = 2;
                        doGetList(Constants.TICKET_PAID_INT);
                        break;

                    case Constants.TICKET_PENDING_COLLECTION_LIST:
                        resetOriginalTabs();
                        pending_collection_tab.setBackground(mContext.getResources().getDrawable(R.drawable.pending_collection_button_transparent));
                        pending_collection_txt.setTextColor(mContext.getResources().getColor(R.color.pending_collection_button));
                        mCurrentTab = 3;
                        doGetList(Constants.TICKET_PENDING_COLLECTION_INT);
                        break;
                }
            }

            carToHighlight = getArguments().getString(Constants.CAR_TO_HIGHLIGHT, "");
        }
//        doGetList();

        back_btn.setOnClickListener(this);
        ticket_issued_tab.setOnClickListener(this);
        parked_tab.setOnClickListener(this);
        paid_tab.setOnClickListener(this);
        pending_collection_tab.setOnClickListener(this);
        logout_btn.setOnClickListener(this);

        //  pull down refresh
        refresh_layout.setOnRefreshListener(this::refreshList);

        //  search bar text change listener
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //  do car plate search here
                if(edtSearch.getText() != null && !edtSearch.getText().toString().equalsIgnoreCase("")) {
                    refresh_layout.setEnabled(false);
                    doTicketSearch(edtSearch.getText().toString());
                }else{
                    refresh_layout.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //  search bar action button
        edtSearch.setOnEditorActionListener((v1, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard(edtSearch);
                doTicketSearch(edtSearch.getText().toString());
                return true;
            }
            return true;
        });

        init();
        myBroadCastReceiver = new MyBroadCastReceiver();
        registerUpdateReceiver();

        return v;
    }

    private void init(){
        ActivationResponse.Service_customize services = SessionManager.getInstance(mContext).getActivationInfo().getService_customize();

        List<ActivationResponse.Service_feature> issueService = services.getIssue().getService_features();

        //  issue ticket status
        if (null != services.getIssue().getActive() && services.getIssue().getActive()) {
            //  issue ticket features
            for (int i = 0; i < issueService.size(); i++) {
                switch (issueService.get(i).getName()) {

                    case Constants.UPFRONT_PAYMENT_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            upfrontFlag = true;
                        }
                            break;

                    case Constants.CUSTOM_QR_SIZE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            isCustomQrSize = true;
                            qrSize = Integer.parseInt(issueService.get(i).getDefault_values());
                        }
                        break;

                }
            }

        }

        List<ActivationResponse.Service_feature> paymentService = services.getPay().getService_features();

        for (int i = 0; i < paymentService.size(); i++) {
            if (Constants.PRINT_TICKET_FEATURE.equals(paymentService.get(i).getName())) {
                if (null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                    isPrintFromList = true;
                    printerDetection();
                } else {
                    isPrintFromList = false;
                }
            }
        }

        if(!services.getPark().getActive()){
            //  disable dashboard ticket issued tab
            ticket_issued_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            ticket_issued_tab.setOnClickListener(null);
        }

        if(!services.getPay().getActive()){
            //  disable dashboard ticket parked tab
            parked_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            parked_tab.setOnClickListener(null);
        }

        if(!services.getCollect().getActive()){
            //  disable dashboard ticket paid tab
            paid_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            paid_tab.setOnClickListener(null);
        } else {
            List<ActivationResponse.Service_feature> collectService = services.getCollect().getService_features();
            for (int i = 0; i < collectService.size(); i++) {
                if (Constants.ENABLE_FEATURE.equals(collectService.get(i).getName())) {
                    if (null != collectService.get(i).getFlag_value() && collectService.get(i).getFlag_value().equalsIgnoreCase(Constants.NO)) {
                        paid_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
                        paid_tab.setOnClickListener(null);
                    }
                }
            }
        }

        if(!services.getComplete().getActive()){
            //  disable dashboard ticket pending collection tab
            pending_collection_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            pending_collection_tab.setOnClickListener(null);
        }
    }

    boolean isPrintFromList() {
        return isPrintFromList;
    }

    private void resetOriginalTabs(){
        if(SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPark().getActive()) {
            ticket_issued_tab.setBackground(mContext.getResources().getDrawable(R.drawable.ticket_issued_button_solid));
            ticket_issued_txt.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            ticket_issued_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            ticket_issued_tab.setOnClickListener(null);
        }

        if(SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPay().getActive()) {
            parked_tab.setBackground(mContext.getResources().getDrawable(R.drawable.parked_button_solid));
            parked_txt.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            parked_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            parked_tab.setOnClickListener(null);
        }

        if(SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getCollect().getActive()) {
            paid_tab.setBackground(mContext.getResources().getDrawable(R.drawable.paid_button_solid));
            paid_txt.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            paid_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            paid_tab.setOnClickListener(null);
        }

        if(SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getComplete().getActive()) {
            pending_collection_tab.setBackground(mContext.getResources().getDrawable(R.drawable.pending_collection_button_solid));
            pending_collection_txt.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            pending_collection_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            pending_collection_tab.setOnClickListener(null);
        }

        //  clear search bar contents
        edtSearch.setText(null);
    }

    private void createList(){
        ticketListAdapter = new TicketListAdapter(mContext, this, listFlag, fullTicketList);
        ticket_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if(!refresh_layout.isRefreshing()) {
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ticket_list.getContext(),
                    LinearLayoutManager.VERTICAL);
            ticket_list.addItemDecoration(dividerItemDecoration);
        }
        ticket_list.setAdapter(ticketListAdapter);

        //  set highlight car if available
        edtSearch.setText(carToHighlight);
    }

    private void doTicketSearch(String s){
        //don't search if the list is empty or null
        if(fullTicketList!=null&&fullTicketList.size()>0) {
            ArrayList<TicketListsResponse> searchList = new ArrayList<>();
            for (int i = 0; i < fullTicketList.size(); i++) {
                if (Utils.getNumberPlateFromTicketdata(fullTicketList.get(i).getTicketData()).contains(s)) {
                    //  compile a list of tickets that matched criteria & update list
                    searchList.add(fullTicketList.get(i));
                }
            }
            //  create search list
            createSearchList(searchList);
        }
    }

    private void createSearchList(ArrayList<TicketListsResponse> list){
        ticketListAdapter = new TicketListAdapter(mContext, this, listFlag, list);
//        ticket_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
//        if(!refresh_layout.isRefreshing()) {
//            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ticket_list.getContext(),
//                    LinearLayoutManager.VERTICAL);
//            ticket_list.addItemDecoration(dividerItemDecoration);
//        }
        ticket_list.setAdapter(ticketListAdapter);
    }

    public void hideKeyboard(View view) {
        if(isAdded()) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private ArrayList<TicketListsResponse> filterList(ArrayList<TicketListsResponse> fullList){
        ArrayList<TicketListsResponse> newList = new ArrayList<TicketListsResponse>();
        for(int i = 0; i < fullList.size(); i++){
            if(fullList.get(i).getValet_status_desc().equalsIgnoreCase(listFlag))
                newList.add(fullList.get(i));
        }
        return newList;
    }

    private void refreshList(){
        switch (mCurrentTab){
            case 0:
                //  issue ticket
                doGetList(Constants.TICKET_ISSUED_INT);
                break;

            case 1:
                //  parked ticket
                doGetList(Constants.TICKET_PARKED_INT);
                break;

            case 2:
                //  paid ticket
                doGetList(Constants.TICKET_PAID_INT);
                break;

            case 3:
                //  pending collection
                doGetList(Constants.TICKET_PENDING_COLLECTION_INT);
                break;
        }
    }

    void processCompleted(TicketListsResponse data){
        APICall.processCompleted(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getStatusChangeParams(data, "5"));
    }

    void processCompleted(String id){
        APICall.processCompleted(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getStatusChangeParams(id));
    }

    void readyForCollection(TicketListsResponse data){
        idToSkip = data.getTicketId();
        APICall.readyForCollection(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getStatusChangeParams(data, "4"));
    }

    private HashMap<String, Object> getStatusChangeParams(TicketListsResponse data, String statusToChange){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
            params.put("user_email", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
            params.put("pos_id", SessionManager.getInstance(mContext).getActivationInfo().getPos_id());
            params.put("datetime", Utils.getCurrentTimeUnixString());
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
            params.put("ticket_id", data.getTicketId());
            //  which status to proceed to
            ActivationResponse.Service_customize services = SessionManager.getInstance(mContext).getActivationInfo().getService_customize();
            if (!services.getComplete().getActive()) {
                params.put("status", "5");
                params.put("next_status", "5");
            }else{
                params.put("status", statusToChange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object> getStatusChangeParams(String id){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
            params.put("user_email", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
            params.put("pos_id", SessionManager.getInstance(mContext).getActivationInfo().getPos_id());
            params.put("status", "5");
            params.put("datetime", Utils.getCurrentTimeUnixString());
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
            params.put("ticket_id", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object> getParams(int listType){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail()); //change this to signed in user
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
            params.put("status", String.valueOf(listType));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    void encryptAndPrintTicket(String ticketData){
        APICall.printTicketFromList(true, this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), ticketData);
    }

    private void doGetList(int listType){
        APICall.fullTicketList(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getParams(listType));
    }

    private void selectIssued(){
        listFlag = Constants.TICKET_ISSUED_LIST;
        resetOriginalTabs();
        ticket_issued_tab.setBackground(mContext.getResources().getDrawable(R.drawable.ticket_issued_button_transparent));
        ticket_issued_txt.setTextColor(mContext.getResources().getColor(R.color.ticket_issued_button));
        doGetList(Constants.TICKET_ISSUED_INT);
        mCurrentTab = 0;
    }

    private void selectPark(){
        listFlag = Constants.TICKET_PARKED_LIST;
        resetOriginalTabs();
        parked_tab.setBackground(mContext.getResources().getDrawable(R.drawable.parked_button_transparent));
        parked_txt.setTextColor(mContext.getResources().getColor(R.color.parked_button));
        doGetList(Constants.TICKET_PARKED_INT);
        mCurrentTab = 1;
    }

    private void selectPaid(){
        listFlag = Constants.TICKET_PAID_LIST;
        resetOriginalTabs();
        paid_tab.setBackground(mContext.getResources().getDrawable(R.drawable.paid_button_transparent));
        paid_txt.setTextColor(mContext.getResources().getColor(R.color.paid_button));
        doGetList(Constants.TICKET_PAID_INT);
        mCurrentTab = 2;
    }

    private void selectPendingCollection(){
        listFlag = Constants.TICKET_PENDING_COLLECTION_LIST;
        resetOriginalTabs();
        pending_collection_tab.setBackground(mContext.getResources().getDrawable(R.drawable.pending_collection_button_transparent));
        pending_collection_txt.setTextColor(mContext.getResources().getColor(R.color.pending_collection_button));
        doGetList(Constants.TICKET_PENDING_COLLECTION_INT);
        mCurrentTab = 3;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
//                listener.triggerBack();
                listener.doFragmentChange(Constants.REVERSE_SLIDE_ANIMATION, new HomepageFragment(), false, HomepageFragment.class.getSimpleName());
                break;

            case R.id.ticket_issued_tab:
                if(Constants.TICKET_ISSUED_LIST.equalsIgnoreCase(listFlag))
                    return;
                mCurrentTab = 0;
                selectIssued();
                break;

            case R.id.parked_tab:
                if(Constants.TICKET_PARKED_LIST.equalsIgnoreCase(listFlag))
                    return;
                mCurrentTab = 1;
                selectPark();
                break;

            case R.id.paid_tab:
                if(Constants.TICKET_PAID_LIST.equalsIgnoreCase(listFlag))
                    return;
                mCurrentTab = 2;
                selectPaid();
                break;

            case R.id.pending_collection_tab:
                if(Constants.TICKET_PENDING_COLLECTION_LIST.equalsIgnoreCase(listFlag))
                    return;
                mCurrentTab = 3;
                selectPendingCollection();
                break;

            case R.id.avatar:
                listener.doLogout();
                break;
        }
    }

    public long getCurrentServerUnix() {
        return currentServerUnix;
    }

    public long getStartingElapsed() {
        return startingElapsed;
    }

    private void printTicket(SimpleStatusResponse data){
        if (!isAdded()) {
            FirebaseCrashlytics.getInstance().recordException(new Exception("Print Ticket Disrupted for: \n " + data.getData().getTicketDataEncrypted() + " \nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss")));
            return;
        }

        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
//        Date date = new Date();
            IssueTicketModel issueTicketModel = new IssueTicketModel();
            issueTicketModel.setSite_name(SessionManager.getInstance(mContext).getActivationInfo().getSite_name());
            issueTicketModel.setSite_address(SessionManager.getInstance(mContext).getActivationInfo().getCity());
            issueTicketModel.setTicket_data(data.getData().getTicketDataEncrypted());
            issueTicketModel.setNumber_plate(data.getData().getPlateNo());
            issueTicketModel.setTicket_info(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
            issueTicketModel.setRunning_id(Utils.addCharAtIndex(Utils.getRunningIdFromTicketdata(data.getData().getTicketData()), 1, "-"));
            issueTicketModel.setDate_issued(Utils.convertStringDateToNewFormat(data.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));
            issueTicketModel.setTime_issued(Utils.convertStringDateToNewFormat(data.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));
            issueTicketModel.setStaff_issuer(getString(R.string.issue_ticket) + " - " + data.getData().getStaffIssuer());
            issueTicketModel.setCustom_qr_size(isCustomQrSize);
            issueTicketModel.setQr_size(qrSize);
            if (upfrontFlag) {
                issueTicketModel.setUpfront_flag(upfrontFlag);
                issueTicketModel.setUpfront_amount(getString(R.string.rm) + Utils.formatBalance(data.getData().getUpfrontPayment().toString(), "0"));
            } else {
                issueTicketModel.setUpfront_flag(upfrontFlag);
            }

            new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "ticket").execute("");

        } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {
//                if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
            View view = LayoutInflater.from(mContext).inflate(
                    R.layout.valet_ticket, null);
            LinearLayout v = view.findViewById(R.id.ticket);

            ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

            TextView tvSiteName = v.findViewById(R.id.site_name1);
            tvSiteName.setText(activationInfo.getSite_name());

            TextView tvDateIssued = v.findViewById(R.id.date);
            tvDateIssued.setText(Utils.convertStringDateToNewFormat(data.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));

            TextView tvTimeIssued = v.findViewById(R.id.time);
            tvTimeIssued.setText(Utils.convertStringDateToNewFormat(data.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

            TextView tvRunningId = v.findViewById(R.id.id);
            tvRunningId.setText(Utils.addCharAtIndex(Utils.addCharAtIndex(Utils.getRunningIdFromTicketdata(data.getData().getTicketData()), 1, "-"), 1, "-"));

            String text = data.getData().getTicketDataEncrypted();
            ImageView imQrCode = v.findViewById(R.id.qrcode);
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, qrSize, qrSize);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                imQrCode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
                Toast.makeText(mContext, "QR generation error", Toast.LENGTH_SHORT).show();
            }

            TextView tvNumberPlate = v.findViewById(R.id.number_plate);
            tvNumberPlate.setText(data.getData().getPlateNo());

            if (upfrontFlag) {
                TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
                tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1() + getString(R.string.rm) + Utils.formatBalance(data.getData().getUpfrontPayment().toString(), "0"));
            } else {
                TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
                tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
            }

            new Thread(() -> {
                if(PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                    PAXPrinter.getInstance().printView(v);
                    PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                    PAXPrinter.getInstance().start();
                }
            }).start();

        } else {
            Toast.makeText(mContext, "No printer detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case GET_FULL_TICKET_LISTS:
                if(fullTicketList != null)
                    fullTicketList.clear();
                fullTicketList = (ArrayList<TicketListsResponse>) response.body();
                currentServerUnix = response.headers().getDate("Date").getTime() / 1000L;
                startingElapsed = SystemClock.elapsedRealtime() / 1000L;
                createList();
                //  remove refresh animation if its showing
                if(refresh_layout.isRefreshing())
                    refresh_layout.setRefreshing(false);
                if(null != listFlag && listFlag.equals(Constants.TICKET_PAID_LIST)){
                    compareWithDbForRecentPaid();
                }
                break;

            case READY_FOR_COLLECTION:
                if (SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getComplete().getActive()){
//                    processCompleted(idToSkip);
//                }else{
                    //  go to pending collection list
                    selectPendingCollection();
                }else{
                    listener.doFragmentChange(Constants.REVERSE_SLIDE_ANIMATION, new HomepageFragment(), false, HomepageFragment.class.getSimpleName());
                }
                break;

            case PROCESS_COMPLETED:
                //  go back to home page
                listener.doFragmentChange(Constants.REVERSE_SLIDE_ANIMATION, new HomepageFragment(), false, HomepageFragment.class.getSimpleName());
                break;

            case PRINT_FROM_LIST:
                SimpleStatusResponse printTicketResponse = (SimpleStatusResponse) response.body();
                printTicket(printTicketResponse);
                break;
        }
    }

    private void addTicket(TicketListsResponse ticket) {

           LocalCacheManager.getInstance(mContext).addTicket(new DBInterface() {
                @Override
                public void onDBTicketLoaded(List<TicketListsResponse >tickets) {

                }

                @Override
                public void onDBTicketUpdated() {
                    Log.d(TicketListsFragment.class.getSimpleName(), "onDBTicketInserted: " );
                }

                @Override
                public void onDBTaskFailed() {
                    Log.d(TicketListsFragment.class.getSimpleName(), "onDBTaskFailed: db insert task failed");
                }
            }, ticket);

    }

    private void compareWithDbForRecentPaid() {
        LocalCacheManager localCacheManager=LocalCacheManager.getInstance(mContext);
        for (TicketListsResponse ticket:fullTicketList){
            localCacheManager.getTicket(new DBInterface() {
                @Override
                public void onDBTicketLoaded(List<TicketListsResponse > list2) {
                    if(list2.size()==0){
                        Utils.sendNotification(mContext,ticket);
                        addTicket(ticket);
                    }
                }

                @Override
                public void onDBTicketUpdated() {

                }

                @Override
                public void onDBTaskFailed() {

                }
            }, ticket.getTicketId(),ticket.getStatus());
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case GET_FULL_TICKET_LISTS:
                break;

            case READY_FOR_COLLECTION:
                SimpleStatusResponse collectionResponse;
                Gson collectGson = new Gson();
                TypeAdapter<SimpleStatusResponse> collectAdapter = collectGson.getAdapter(SimpleStatusResponse.class);
                if (response.errorBody() != null) {
                    try {
                        collectionResponse = collectAdapter.fromJson(response.errorBody().string());
                        if(null != collectionResponse.getKpCode() && "kp_314".equalsIgnoreCase(collectionResponse.getKpCode())){
                            //  ticket payment failure
                            CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
                                if(which == DialogInterface.BUTTON_POSITIVE) {
                                    //  go back to refreshed parked ticket list
                                    selectPark();
                                }
                            }, getString(R.string.payment_fail), collectionResponse.getMessage());
                        }else {
                            CustomAlerts.showOKAlert(mContext, getString(R.string.payment_fail), collectionResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case PROCESS_COMPLETED:
                SimpleStatusResponse completedResponse;
                Gson completedGson = new Gson();
                TypeAdapter<SimpleStatusResponse> completedAdapter = completedGson.getAdapter(SimpleStatusResponse.class);
                if (response.errorBody() != null) {
                    try {
                        completedResponse = completedAdapter.fromJson(response.errorBody().string());
                        if(null != completedResponse.getKpCode() && "kp_314".equalsIgnoreCase(completedResponse.getKpCode())){
                            //  ticket payment failure
                            CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
                                if(which == DialogInterface.BUTTON_POSITIVE) {
                                    //  go back to refreshed paid ticket list
                                    selectPaid();
                                }
                            }, getString(R.string.payment_fail), completedResponse.getMessage());
                        }else {
                            CustomAlerts.showOKAlert(mContext, getString(R.string.payment_fail), completedResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.TICKET_ISSUED_INT:
                    selectIssued();
                    break;

                case Constants.TICKET_PARKED_INT:
                    selectPark();
                    break;

                case Constants.TICKET_PAID_INT:
                    //  normal flow
                    if(Constants.PAYMENT_USUAL_FLOW.equalsIgnoreCase(data.getStringExtra(Constants.PAYMENT_SKIP_FLAG)))
                        selectPaid();

                    //  skip paid list, got straight to pending collection
                    if(Constants.PAYMENT_SKIP_COLLECT.equalsIgnoreCase(data.getStringExtra(Constants.PAYMENT_SKIP_FLAG)))
                        selectPendingCollection();

                    //  skip everything, go back to home page
                    if(Constants.PAYMENT_SKIP_COMPLETE.equalsIgnoreCase(data.getStringExtra(Constants.PAYMENT_SKIP_FLAG)))
//                        listener.doFragmentChange(Constants.REVERSE_SLIDE_ANIMATION, new HomepageFragment(), false, HomepageFragment.class.getSimpleName());
                        listener.triggerBack();
                    break;

                case Constants.TICKET_PENDING_COLLECTION_INT:
                    selectPendingCollection();
                    break;
            }
        }

        if(resultCode == Activity.RESULT_CANCELED && requestCode == Constants.TICKET_PAID_INT){
            if(null != data && data.hasExtra(Constants.LIST_REFRESH) && data.getBooleanExtra(Constants.LIST_REFRESH, false)){
                refreshList();
            }
        }
    }


    /**
     * MyBroadCastReceiver is responsible to receive broadCast from service when ticket paid 
     * */
    class MyBroadCastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            try
            {
                Log.d(TicketListsFragment.class.getName(), "onReceive() called");
                refreshList();


            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
     * */
    private void registerUpdateReceiver() {

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            mContext.registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // make sure to unregister your receiver after finishing of this activity
        unregisterReceiver(myBroadCastReceiver);
    }

    private void unregisterReceiver(MyBroadCastReceiver myBroadCastReceiver) {
        mContext.unregisterReceiver(myBroadCastReceiver);
    }

    private void printerDetection() {
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(mContext);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

//                enablePrintButton()
                    printerType = Constants.SUNMI_PRINTER;
                } else {
//                disablePrintButton()
                    printerType = Constants.NO_PRINTER;
                }

                if (mSunmiPrinter.isPrinterReady()) {
//                enablePrintButton()
                    printerType = Constants.SUNMI_PRINTER;
                } else {
//                disablePrintButton()
                    printerType = Constants.NO_PRINTER;
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
//                enablePrintButton()
                printerType = Constants.PAX_PRINTER;
            } else {
//                disablePrintButton()
                printerType = Constants.NO_PRINTER;
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(mContext, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
        }
    }
}
