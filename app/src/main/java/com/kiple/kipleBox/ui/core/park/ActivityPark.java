package com.kiple.kipleBox.ui.core.park;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.FileUtils;
import com.kiple.kipleBox.utils.LogUtils;
import com.kiple.kipleBox.utils.Utils;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ActivityPark extends AppCompatActivity implements View.OnClickListener, CallBackInterface {

    @BindView(R.id.surrounding_view_parent)
    ConstraintLayout surrounding_view_parent;
    @BindView(R.id.surrounding_view)
    ImageView surrounding_view;
    @BindView(R.id.surrounding_view_close)
    ImageView surrounding_view_close;
    @BindView(R.id.front_view_parent)
    ConstraintLayout front_view_parent;
    @BindView(R.id.front_view)
    ImageView front_view;
    @BindView(R.id.front_view_close)
    ImageView front_view_close;
    @BindView(R.id.back_view_parent)
    ConstraintLayout back_view_parent;
    @BindView(R.id.back_view)
    ImageView back_view;
    @BindView(R.id.back_view_close)
    ImageView back_view_close;
    @BindView(R.id.right_view_parent)
    ConstraintLayout right_view_parent;
    @BindView(R.id.right_view)
    ImageView right_view;
    @BindView(R.id.right_view_close)
    ImageView right_view_close;
    @BindView(R.id.left_view_parent)
    ConstraintLayout left_view_parent;
    @BindView(R.id.left_view)
    ImageView left_view;
    @BindView(R.id.left_view_close)
    ImageView left_view_close;
    @BindView(R.id.dashboard_view_parent)
    ConstraintLayout dashboard_view_parent;
    @BindView(R.id.dashboard_view)
    ImageView dashboard_view;
    @BindView(R.id.dashboard_view_close)
    ImageView dashboard_view_close;

    @BindView(R.id.parked_btn)
    Button parked_btn;
    @BindView(R.id.parking_bay_label)
    TextView parking_bay_label;
    @BindView(R.id.parking_bay_number)
    EditText parking_bay_number;
    @BindView(R.id.number_plate)
    TextView number_plate;
    @BindView(R.id.back_btn)
    ImageView back_btn;

    public static final int CAPTURE_PHOTO = 1;
    public static final int PICK_PHOTO = 2;
    public static final int PERMISSIONS_REQUEST_STORAGE = 2;
    public static final int PERMISSIONS_REQUEST_CAMERA = 3;

    private int currentImageIndex = 0;
    private ImageView mCurrentView;
    private String mTicketData, mTicketId;
    private Uri imageUriFromCamera;
    // create list of file parts (photo, video, ...)
    private List<MultipartBody.Part> parts = new ArrayList<>();
    private Uri[] imageUriList = new Uri[6];

    //  parking features
    private ActivationResponse.Service_feature parkingLocation, parkingPictures;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park);
        ButterKnife.bind(this);

        mTicketData = getIntent().getStringExtra(Constants.TICKET_DATA_BUNDLE);

        if (mTicketData == null || "".equalsIgnoreCase(getIntent().getStringExtra(Constants.TICKET_DATA_BUNDLE))) {
            CustomAlerts.showOKAlert(this, (dialog, which) -> {
                if (which == RESULT_OK) {
                    dialog.dismiss();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }, "Error", "TicketData unavailable.");
        }

        mTicketId = getIntent().getStringExtra(Constants.TICKET_ID_BUNDLE);

        if (mTicketId == null || "".equalsIgnoreCase(getIntent().getStringExtra(Constants.TICKET_ID_BUNDLE))) {
            CustomAlerts.showOKAlert(this, (dialog, which) -> {
                if (which == RESULT_OK) {
                    dialog.dismiss();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }, "Error", "TicketId unavailable.");
        }

        init();

        number_plate.setText(Utils.getNumberPlateFromTicketdata(mTicketData));

//        this.registerForContextMenu(surrounding_view);
//        this.registerForContextMenu(front_view);
//        this.registerForContextMenu(back_view);
//        this.registerForContextMenu(right_view);
//        this.registerForContextMenu(left_view);
//        this.registerForContextMenu(dashboard_view);
    }

    private void init(){
        List<ActivationResponse.Service_feature> featureList = SessionManager.getInstance(this).getActivationInfo().getService_customize().getPark().getService_features();
        for(int i = 0; i < featureList.size(); i++){
            if(Constants.CAR_LOCATION_FEATURE.equalsIgnoreCase(featureList.get(i).getName())){
                parkingLocation = featureList.get(i);
                if(Constants.YES.equalsIgnoreCase(parkingLocation.getFlag_value())){
                    parking_bay_number.setText(parkingLocation.getDefault_values());
                    parking_bay_label.setVisibility(View.VISIBLE);
                    parking_bay_number.setVisibility(View.VISIBLE);
                }else{
                    parking_bay_label.setVisibility(View.INVISIBLE);
                    parking_bay_number.setVisibility(View.INVISIBLE);
                }
            }

            if(Constants.CAR_PICTURES_FEATURE.equalsIgnoreCase(featureList.get(i).getName())){
                parkingPictures = featureList.get(i);
                List<ActivationResponse.Attribute> parkingAttributes = parkingPictures.getAttributes();
                for(int j = 0; j < parkingAttributes.size(); j++){
                    switch (parkingAttributes.get(j).getName()){

                        case Constants.SURROUNDING_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(surrounding_view);
                            }else{
                                surrounding_view_parent.setVisibility(View.GONE);
                            }
                            break;

                        case Constants.FRONT_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(front_view);
                            }else{
                                front_view_parent.setVisibility(View.GONE);
                            }
                            break;

                        case Constants.BACK_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(back_view);
                            }else{
                                back_view_parent.setVisibility(View.GONE);
                            }
                            break;

                        case Constants.RIGHT_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(right_view);
                            }else{
                                right_view_parent.setVisibility(View.GONE);
                            }
                            break;

                        case Constants.LEFT_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(left_view);
                            }else{
                                left_view_parent.setVisibility(View.GONE);
                            }
                            break;

                        case Constants.DASHBOARD_IMAGE_FEATURE:
                            if(Constants.YES.equalsIgnoreCase(parkingAttributes.get(j).getValues())){
                                this.registerForContextMenu(dashboard_view);
                            }else{
                                dashboard_view_parent.setVisibility(View.GONE);
                            }
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // take photo for avatar
//        if (v.getId() == avatar.getId()) {
        if (v instanceof ImageView) {
            menu.add(1, 10001, Menu.NONE, getString(R.string.take_picture));
            menu.add(1, 10002, Menu.NONE, getString(R.string.select_from_albums));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // take photo
        if (item.getItemId() == 10001) {
            if (showPermission(PERMISSIONS_REQUEST_CAMERA, Manifest.permission.CAMERA)) {
                if (showPermission(PERMISSIONS_REQUEST_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (showPermission(PERMISSIONS_REQUEST_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        startCameraActivity();
                    }
                }
            }
        } else if (item.getItemId() == 10002) {
            selectPhoto();
        }
        return super.onContextItemSelected(item);
    }

    private void startCameraActivity() {
        Toast.makeText(this, "start camera", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            imageUriFromCamera = Uri.fromFile(Utils.getOutputMediaFile(this));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriFromCamera);
        } else {
            File file = new File(Uri.fromFile(Utils.getOutputMediaFile(this)).getPath());
            imageUriFromCamera = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriFromCamera);
        }

//        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
//            intent.setClipData(ClipData.newRawUri("", imageUriFromCamera));
//            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, CAPTURE_PHOTO);
//            selectPhotoAction();
        }
    }

    private void selectPhoto() {
        Toast.makeText(this, "Select Photo", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        intent = Intent.createChooser(intent, getString(R.string.select_picture));

        startActivityForResult(intent, PICK_PHOTO);
//        selectPhotoAction();
    }

    public boolean showPermission(int requestCode, String requireString) {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, requireString);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    requireString)) {
                showMessageRequirePermission(this);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{requireString}, requestCode);
            }
            return false;
        }
        return true;
    }

    public static void showMessageRequirePermission(Activity activity) {
        AlertDialog.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, R.style.AppCompat_ProgressDialog);
        } else {
            builder = new AlertDialog.Builder(activity);
        }
        builder.setCancelable(false).setMessage(R.string.not_allow_permission)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    dialog.cancel();
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                    intent.setData(uri);
                    activity.startActivity(intent);
                }).setNegativeButton(android.R.string.yes, (dialog, which) -> dialog.cancel()).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_PHOTO:
                    if (imageUriFromCamera != null) {
                        //  set image to UI with image URI
                        imageUriList[currentImageIndex] = imageUriFromCamera;
                        LogUtils.LOGE("index tracking", "imageUrlList index: " + imageUriList[currentImageIndex]);
                        //  store all image URI in a list
                        //  compile list of image multipart when triggered to send
                        mCurrentView.setImageURI(imageUriFromCamera);
                    }
                    break;
                case PICK_PHOTO:
                    if (data != null && data.getData() != null) {
                        //  set image to UI with image URI
                        imageUriList[currentImageIndex] = data.getData();
                        LogUtils.LOGE("index tracking", "imageUrlList index: " + imageUriList[currentImageIndex]);
                        //  store all image URI in a list
                        //  compile list of image multipart when triggered to send
                        mCurrentView.setImageURI(data.getData());
                    }
                    break;

//                case CROP_PHOTO:
//                    if (data != null && data.hasExtra("data")) {
//                        SessionManager.getInstance(this).setImageDatas(data.getByteArrayExtra("data"));
//                        byte[] data_image = SessionManager.getInstance(this).getImageDatas();
//                        bmpAvatar = BitmapFactory.decodeByteArray(data_image, 0, data_image.length);
//                        uploadAvatar();
//                    } else {
//                        SessionManager.getInstance(this).setImageDatas(null);
//                    }
//                    break;
            }
        }
    }

    //  methods to create multipartbody for image uploading
    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        //  method to create image description
//        return RequestBody.create(
//                okhttp3.MultipartBody.FORM, descriptionString);
        return RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);

        // create RequestBody instance from file
//        RequestBody requestFile =
//                RequestBody.create(
//                        MediaType.parse(getContentResolver().getType(fileUri)),
//                        file
//                );
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                //  hide keyboard
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(
                        (null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                onBackPressed();
                break;

            case R.id.surrounding_view:
                mCurrentView = surrounding_view;
                currentImageIndex = 0;
                this.openContextMenu(surrounding_view);
                break;

            case R.id.front_view:
                mCurrentView = front_view;
                currentImageIndex = 1;
                this.openContextMenu(front_view);
                break;

            case R.id.back_view:
                mCurrentView = back_view;
                currentImageIndex = 2;
                this.openContextMenu(back_view);
                break;

            case R.id.right_view:
                mCurrentView = right_view;
                currentImageIndex = 3;
                this.openContextMenu(right_view);
                break;

            case R.id.left_view:
                mCurrentView = left_view;
                currentImageIndex = 4;
                this.openContextMenu(left_view);
                break;

            case R.id.dashboard_view:
                mCurrentView = dashboard_view;
                currentImageIndex = 5;
                this.openContextMenu(dashboard_view);
                break;

            case R.id.surrounding_view_close:
                currentImageIndex = 0;
                imageUriList[currentImageIndex] = null;
                //  remove pic from UI
                surrounding_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_surrounding_view));
                //  remove pic URI from list
                break;

            case R.id.front_view_close:
                currentImageIndex = 1;
                imageUriList[currentImageIndex] = null;
                //  remove pic from UI
                front_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_front_view));
                //  remove pic URI from list
                break;

            case R.id.back_view_close:
                currentImageIndex = 2;
                imageUriList[currentImageIndex] = null;
                //  remove pic from UI
                back_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_back_view));
                //  remove pic URI from list
                break;

            case R.id.right_view_close:
                currentImageIndex = 3;
                imageUriList[currentImageIndex] = null;
                //  remove pic from UI
                right_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_right_view));
                //  remove pic URI from list
                break;

            case R.id.left_view_close:
                currentImageIndex = 4;
                imageUriList[currentImageIndex] = null;
                //  remove pic from UI
                left_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_left_view));
                //  remove pic URI from list
                break;

            case R.id.dashboard_view_close:
                currentImageIndex = 5;
                imageUriList[currentImageIndex] = null;
                LogUtils.LOGE("index tracking", "imageUrlList index: " + imageUriList[currentImageIndex] + " : " + imageUriList[currentImageIndex-1]);
                //  remove pic from UI
                dashboard_view.setImageDrawable(getResources().getDrawable(R.drawable.parked_dashboard));
                //  remove pic URI from list
                break;

            case R.id.parked_btn:
//                Toast.makeText(this, "parked button clicked", Toast.LENGTH_SHORT).show();
                //  compile list of image uri to send off
                triggerParked();
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void triggerParked() {
        //  compile list of image multipart
//        LogUtils.LOGE("Image Troubleshooting", "imageUriList.size: " + imageUriList.size());
//        for(int i = 0; i < imageUriList.size(); i++){
//            LogUtils.LOGE("Image Troubleshooting", "imageUriList.get(i).toString: " + imageUriList.get(i).toString());
//            parts.add(prepareFilePart("img" + String.valueOf(i + 1), imageUriList.get(i)));
//            LogUtils.LOGE("Image Troubleshooting", "parts.get(i).body().toString(): " + parts.get(i).body().toString());
//        }
//        LogUtils.LOGE("Image Troubleshooting", "parts.size(): " + parts.size());
        //  send off to API
//        APICall.vehicleParked(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), parts, getParkedParams());
        APICall.vehicleParked(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParkedParams());

//        APIInterface baseInterface = APIClient.getTempBase().create(APIInterface.class);
//        Call call = baseInterface.vehicleParked(Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(),
//                prepareFilePart("img1", Uri.parse("content://media/external/images/media/371")),
//                prepareFilePart("img2", Uri.parse("content://media/external/images/media/370")),
//                prepareFilePart("img3", Uri.parse("content://media/external/images/media/369")),
//                prepareFilePart("img4", Uri.parse("content://media/external/images/media/366")),
//                prepareFilePart("img5", Uri.parse("content://media/external/images/media/367")),
//                prepareFilePart("img6", Uri.parse("content://media/external/images/media/368")),
//                createPartFromString(SessionManager.getInstance(this).getActiveUser()),
//                createPartFromString(parking_bay_number.getText().toString()),
//                createPartFromString(SessionManager.getInstance(this).getActivationInfo().getPos_id()),
//                createPartFromString(String.valueOf(System.currentTimeMillis() / 1000)),
//                createPartFromString(SessionManager.getInstance(this).getActivationInfo().getSite_id()),
//                createPartFromString(mTicketId));
//        APICallback.apiCallback(true, this, this, call, true, Constants.ApiFlags.VEHICLE_PARKED);
    }

    private HashMap<String, Object> getParkedParams() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
            params.put("user_email", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("locationpark", parking_bay_number.getText().toString());
            params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("datetime", String.valueOf(System.currentTimeMillis() / 1000));
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("ticket_id", mTicketId);
            for (int i = 0; i < imageUriList.length; i++) {
                if(imageUriList[i] != null) {
                    InputStream iStream = getContentResolver().openInputStream(imageUriList[i]);
                    if (iStream != null) {
                        params.put("img" + String.valueOf(i + 1), Base64.encodeToString(Utils.getImageInputStreamToByteArray(iStream), Base64.DEFAULT));
                        LogUtils.LOGE("Image Troubleshooting", "imageUriList.get(i).toString: " + imageUriList[i].toString());
                    }
                }
            }
//            params.put("user_id", createPartFromString(SessionManager.getInstance(this).getActiveUser())); //change this to signed in user
//            params.put("locationpark", createPartFromString(parking_bay_number.getText().toString()));
//            params.put("pos_id", createPartFromString(SessionManager.getInstance(this).getActivationInfo().getPos_id()));
//            params.put("datetime", createPartFromString(String.valueOf(System.currentTimeMillis() / 1000)));
//            params.put("site_id", createPartFromString(SessionManager.getInstance(this).getActivationInfo().getSite_id()));
//            params.put("ticket_id", createPartFromString(mTicketId));
            LogUtils.LOGE("Image Troubleshooting", "params.size:  " + params.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

//    private HashMap<String, RequestBody> getParkedParams() {
//        HashMap<String, RequestBody> params = new HashMap<>();
//        try{
//            params.put("user_id", createPartFromString(SessionManager.getInstance(this).getActiveUser())); //change this to signed in user
//            params.put("locationpark", createPartFromString(parking_bay_number.getText().toString()));
//            params.put("pos_id", createPartFromString(SessionManager.getInstance(this).getActivationInfo().getPos_id()));
//            params.put("datetime", createPartFromString(String.valueOf(System.currentTimeMillis() / 1000)));
//            params.put("site_id", createPartFromString(SessionManager.getInstance(this).getActivationInfo().getSite_id()));
//            params.put("ticket_id", createPartFromString(mTicketId));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return params;
//    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case VEHICLE_PARKED:
                Toast.makeText(this, getString(R.string.vehicle_parked), Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case VEHICLE_PARKED:
                CustomAlerts.showOKAlert(this, (dialog, which) -> {
                    if (which == RESULT_OK) {
                        dialog.dismiss();
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                }, getString(R.string.error), getString(R.string.parking_unsuccessful));
                break;
        }
    }
}
