package com.kiple.kipleBox.ui.user_management.splash;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.kiple.kipleBox.BuildConfig;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.APIErrorBody;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.network.APIClient;
import com.kiple.kipleBox.network.APIInterface;
import com.kiple.kipleBox.ui.core.landing.AppActivity;
import com.kiple.kipleBox.ui.user_management.activation.ActivationActivity;
import com.kiple.kipleBox.ui.user_management.staff_list.StaffListActivity;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.LogUtils;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements CallBackInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.splash_screen);

        //  crashlytics initialization
        if(BuildConfig.FLAVOR.equalsIgnoreCase("live"))
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);

//        checkVersion();
        checkSession();
//        showActivation();
    }

    public void checkSession(){
        if(SessionManager.getInstance(this).checkActivationInfoExists()){
            //  auto activate POS
            ActivationResponse activationResponse = SessionManager.getInstance(this).getActivationInfo();
            LogUtils.LOGI(this.getClass().getSimpleName(), "Session exist");
            LogUtils.LOGI(this.getClass().getSimpleName(), new Gson().toJson(activationResponse));

            //  get updated POS config info
            //  **  TESTING **
            HashMap<String, Object> params = new HashMap<>();
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("software_version", BuildConfig.VERSION_NAME);
            APICall.updateConfig(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActivationInfo().getManager_token(), params);

        }else{
            //  show activation page
            LogUtils.LOGI(this.getClass().getSimpleName(), "Session does not exist");
            final Intent intent = new Intent(this, ActivationActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.MANAGER_LOGIN); //Your id
            intent.putExtras(b); //Put your id to your next Intent
            startActivity(intent);
            finish();
//            showActivation();
        }
    }

    private void activeUserCheck(){
        if(null != SessionManager.getInstance(this).getActiveUserInfo()){
            //   perform auto login
            Toast.makeText(this, "auto login", Toast.LENGTH_SHORT).show();
            final Intent intent = new Intent(this, AppActivity.class);
            startActivity(intent);
            finishAffinity();
        }else{
            //  show staff list
            final Intent intent = new Intent(this, StaffListActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.STAFF_LOGIN); //Your id
            intent.putExtras(b); //Put your id to your next Intent
            startActivity(intent);
            finish();
        }
    }

    private void showActivation(){
        //  show activation page

        //  test activation API
        Call call = APIClient.getBase().create(APIInterface.class).deviceActivation(getActivationParams());
        APICallback.apiCallback(false, this, this, call, true, Constants.ApiFlags.ACTIVATION);

    }

    private HashMap<String, Object> getActivationParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("activation_code", "58539212");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void doActivate(){
        //  auto activate POS

    }

//    private void checkVersion(){
//        Call call = APIClient.checkVersion().create(APIInterface.class).checkStatus(getVersionParams());
//        APICallback.apiCallback(false, this, this, call, true, Constants.ApiFlags.VERSION_CHECK);
//    }

    private HashMap<String, Object> getVersionParams(){
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("origin", "kipleBox");
            params.put("type", "android");
            params.put("version", getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    protected void onDestroy(){
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case VERSION_CHECK:
                //  everything ok
                //  proceed
                if(SessionManager.getInstance(this).getToken() != null
                        && SessionManager.getInstance(this).checkUserInfoExists() != null){
                    doActivate();
                }//else{
//                    showActivation();
//                }
                break;

            case ACTIVATION:
                ActivationResponse activationResponse = (ActivationResponse) response.body();
                SessionManager.getInstance(this).saveActivationInfo(activationResponse);
                //  proceed to site manager registration page
                break;

            case CONFIG_UPDATE:
                //  update config info
                ActivationResponse updatedActivationResponse = (ActivationResponse) response.body();

                SessionManager.getInstance(this).updateActivationInfo(updatedActivationResponse);

                //  if active user exists
                activeUserCheck();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags){
            case VERSION_CHECK:
                //  NS002   Maintenance
                //  NS003   Update
                try {
                    AppCompatActivity activity = this;
                    ResponseBody body = response.errorBody();
                    if (body != null) {
                        APIErrorBody apiErrorBody = new Gson().fromJson(body.string(), APIErrorBody.class);
                        if (apiErrorBody != null) {
                            String errorCode = apiErrorBody.getVersionCheckCode();
                            switch (errorCode){
                                case "NS002":
                                    CustomAlerts.showOKAlert(this, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    switch (which) {
                                                        case RESULT_OK:
                                                            dialog.dismiss();
                                                            activity.finish();
                                                            break;
                                                    }
                                                }
                                            }, getString(R.string.maintenance_title),
                                            getString(R.string.maintenance_content));
                                    break;

                                case "NS003":
                                    CustomAlerts.showOKAlert(this, (dialog, which) -> {
                                                switch (which) {
                                                    case RESULT_OK:
                                                        dialog.dismiss();
                                                        final String appPackageName = "com.kiple.kiplepark"; // getPackageName() from Context or Activity object
                                                        try {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                        } catch (android.content.ActivityNotFoundException exception) {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                        }
                                                        activity.finish();
                                                        break;
                                                }
                                            }, getString(R.string.update_title),
                                            getString(R.string.update_content));
                                    break;
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case ACTIVATION:
                Toast.makeText(this, "activation failed", Toast.LENGTH_SHORT).show();
                break;

            case CONFIG_UPDATE:
                CustomAlerts.showOKAlert(this, (dialog, which) -> {
                    dialog.dismiss();
                    activeUserCheck();
                }, "", getString(R.string.update_config_fail));
                break;
        }
    }
}
