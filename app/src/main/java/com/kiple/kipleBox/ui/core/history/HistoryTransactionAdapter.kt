package com.kiple.kipleBox.ui.core.history

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.kiple.kipleBox.R
import com.kiple.kipleBox.common.Constants
import com.kiple.kipleBox.model.histroy.TransactionResponse
import com.kiple.kipleBox.utils.Utils
import kotlinx.android.synthetic.main.item_history_transaction.view.*
import java.text.SimpleDateFormat
import kotlin.properties.Delegates


class HistoryTransactionAdapter(var historyTransactionFragment: HistoryTransactionFragment, var list: ArrayList<TransactionResponse.Data>, var mContext: Context) : RecyclerView.Adapter<HistoryTransactionAdapter.HistoryHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {

        return HistoryHolder(LayoutInflater.from(mContext).inflate(R.layout.item_history_transaction, parent, false))
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {

        holder.bindData(list[position], mContext)

        if (historyTransactionFragment.isPrinterAvailable) {
            //print pressed
            holder.ivPrint.setOnClickListener {
                Toast.makeText(mContext, "Print", Toast.LENGTH_SHORT).show()
                historyTransactionFragment.printTransaction(list[position])
            }
            holder.ivPrint.setColorFilter(mContext.resources.getColor(R.color.kiple_blue), android.graphics.PorterDuff.Mode.SRC_IN)
        } else{
            holder.ivPrint.isClickable = false
            holder.ivPrint.setColorFilter(mContext.resources.getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN)
        }

        //remove pressed
        holder.ivRemove.setOnClickListener {
            openVoidTxnDialog(list[position])
        }
    }


    class HistoryHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var ivPrint: ImageView by Delegates.notNull()
        var ivRemove: ImageView by Delegates.notNull()

        fun bindData(data: TransactionResponse.Data, context: Context) {


            val ticketID = "<b>Tx ID </b> " + data.ticketId
            val type = "<b>Type </b> " + data.type
            val carPlate = "<b>Car Plate No.</b> " + data.car_plate
            val amount = "<b>Amount </b> " + context.getString(R.string.rm) + Utils.formatBalance(data.amount)

            view.tvTxID.text = Html.fromHtml(ticketID)
            view.tvTime.text = SimpleDateFormat("hh:mm aa").format(SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(data.datePay))
            view.tvPaymentType.text = Html.fromHtml(type)
            view.tvCarPlateNumber.text = Html.fromHtml(carPlate)
            view.tvAmount.text = Html.fromHtml(amount)


            ivPrint = view.ivPrint
            ivRemove = view.ivRemove

            if ((!data.type.isNullOrEmpty() && data.type.contains("[void]", ignoreCase = true))
                    || (!data.type.isNullOrEmpty() && data.type.contains("kiple", ignoreCase = true))
                    || (!data.type.isNullOrEmpty() && data.type.contains("debit", ignoreCase = true))
                    || (!data.type.isNullOrEmpty() && data.type.contains("credit", ignoreCase = true)))
                ivRemove.visibility = View.INVISIBLE
            else
                ivRemove.visibility = View.VISIBLE

//            if (!data.type.isNullOrEmpty() && data.type.equals(Constants.CASH_PAYMENT, true))
//                ivRemove.visibility = View.VISIBLE
//            else
//                ivRemove.visibility = View.INVISIBLE

//            if (data.type.isNullOrEmpty() || data.type.contains("Cash [Void]",ignoreCase = true)){
//                ivRemove.visibility = View.INVISIBLE
//            }else{
//                ivRemove.visibility = View.VISIBLE
//            }
        }
    }


    private fun openVoidTxnDialog(data: TransactionResponse.Data) {

        historyTransactionFragment.openVoidTxnDialog(data)
//        val activity = mContext as Activity
//
//
//        val mBottomSheetDialog = BottomSheetDialog(activity)
//        val sheetView = activity.layoutInflater.inflate(R.layout.dialog_remove_transaction_history, null)
//        mBottomSheetDialog.setContentView(sheetView)
//        sheetView.btnSubmitVoidTxn.setOnClickListener {
//            if (!TextUtils.isEmpty(sheetView.etVoidTxnPin.text.toString())) {
//
////                Toast.makeText(mContext, "Sending...", Toast.LENGTH_SHORT).show()
//            } else {
//                Toast.makeText(mContext, "Enter your PIN", Toast.LENGTH_SHORT).show()
//
//            }
//        }
//
//
//        sheetView.btnDiscardVoidTxn.setOnClickListener {
//            mBottomSheetDialog.dismiss()
//        }

//        mBottomSheetDialog.show()
    }
}