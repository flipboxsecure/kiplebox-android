package com.kiple.kipleBox.ui.user_management.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.kiple.kipleBox.BuildConfig;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.LoginResponse;
import com.kiple.kipleBox.model.responses.UserDetailResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.core.landing.AppActivity;
import com.kiple.kipleBox.utils.CustomAlerts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CallBackInterface {

    @BindView(R.id.pin_parent)
    TextInputLayout pin_parent;
    @BindView(R.id.pin)
    TextInputEditText pin;
    @BindView(R.id.forget_pin_btn)
    Button forget_pin_btn;
    @BindView(R.id.submit_btn)
    Button submit_btn;
    @BindView(R.id.back_btn)
    ImageView back_btn;

    private String loginEmail = "";
    private String username = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();
        if (b != null && b.getString(Constants.LOGIN_EMAIL_BUNDLE) != null) {
            this.loginEmail = b.getString(Constants.LOGIN_EMAIL_BUNDLE);
            this.username = b.getString(Constants.ACTIVE_USERNAME_BUNDLE);
        } else {
            CustomAlerts.showOKAlert(this, (dialog, which) -> {
                if (which == RESULT_OK) {
                    dialog.dismiss();
                    onBackPressed();
                }
            }, getString(R.string.oops_something_went_wrong), getString(R.string.login_username_unavailable));
        }

//        setupPage();
    }

    @Override
    public void onPause() {
        super.onPause();
        //  hide keyboard
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(pin.getWindowToken(), 0);
        }

    }

    private HashMap<String, Object> getParams() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("email", loginEmail);
            params.put("password", Objects.requireNonNull(pin.getText()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void startHomepage() {
        //  go to home page
        Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(this, AppActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                Bundle b = new Bundle();
//                b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.EDIT_STAFF); //Your id
//                b.putParcelable(Constants.EDIT_PROFILE_BUNDLE, data);
//                intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_btn:
                pin_parent.setError(null);
                APICall.login(this, this, getParams());
                break;

            case R.id.back_btn:
                finish();
                break;
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case LOGIN:
                LoginResponse loginResponse = (LoginResponse) response.body();

                //  save login user
//                SessionManager.getInstance(this).saveActiveUserEmail(loginEmail);
//                SessionManager.getInstance(this).saveActiveUserName(username);
                SessionManager.getInstance(this).saveActiveUserInfo(loginResponse);

                //  get user details
                APICall.getDetails(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());

                break;

            case USER_DETAILS:
                UserDetailResponse userDetailResponse = (UserDetailResponse) response.body();
                //  save user details
                SessionManager.getInstance(this).saveActiveUserDetails(userDetailResponse);

                //  get updated POS config info
                //  **  NOT READY **
                HashMap<String, Object> params = new HashMap<>();
                params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
                params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());
                params.put("software_version", BuildConfig.VERSION_NAME);
                APICall.updateConfig(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);

                //  proceed to fragment_homepage
                startHomepage();
                break;

            case CONFIG_UPDATE:
                //  update config info
                ActivationResponse activationResponse = (ActivationResponse) response.body();

                if (activationResponse != null) {
                    SessionManager.getInstance(this).updateActivationInfo(activationResponse);
                }

                //  proceed to fragment_homepage
                startHomepage();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case LOGIN:
                LoginResponse loginResponse;
                Gson gson = new Gson();
                TypeAdapter<LoginResponse> adapter = gson.getAdapter(LoginResponse.class);
                if (response.errorBody() != null) {
                    try {
                        loginResponse = adapter.fromJson(response.errorBody().string());
                        if (null != loginResponse.getCode() && "auth_failed".equalsIgnoreCase(loginResponse.getCode())) {
                            pin_parent.setError("PIN incorrect");
                        } else if (null != loginResponse.getCode() && "multiple_login".equalsIgnoreCase(loginResponse.getCode())) {
                            pin_parent.setError("Account logged in on another device");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case USER_DETAILS:
                CustomAlerts.showOKAlert(this, (dialog, which) -> dialog.dismiss(), getString(R.string.error), getString(R.string.user_detail_fail));
                break;

            case CONFIG_UPDATE:
                CustomAlerts.showOKAlert(this, (dialog, which) -> {
                    dialog.dismiss();
                    startHomepage();
                }, "", getString(R.string.update_config_fail));
                break;
        }
    }
}
