package com.kiple.kipleBox.ui.core.display_ticket;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.BaseFragment;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.EncryptionResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.core.lists.TicketListsFragment;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.GenerateQR;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class DisplayTicketFragment extends BaseFragment implements CallBackInterface {

    @BindView(R.id.qr_code)
    ImageView qrCode;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.number_plate)
    TextView number_plate;
    @BindView(R.id.mobile_number)
    TextView mobile_number;
    @BindView(R.id.key_collected)
    TextView key_collected;
    @BindView(R.id.check_in_time)
    TextView check_in_time;
    @BindView(R.id.ticket_id)
    TextView ticket_id;
    @BindView(R.id.print_ticket_btn)
    LinearLayout print_ticket_btn;
    @BindView(R.id.park_btn)
    LinearLayout park_btn;
    @BindView(R.id.print_ticket_icon)
    ImageView print_ticket_icon;
    @BindView(R.id.print_ticket_text)
    TextView print_ticket_text;
    @BindView(R.id.upfront_amount)
    TextView upfront_amount;

    private String ticketData, dateIssued, timeIssued;
    private Printer mSunmiPrinter;
    private String printerType;
    private String encodedTicketdata = "";
    private int printCount = 0;

    private Context mContext;

    private boolean upfrontFlag = false;
    private boolean newPrintReceipt = false;
    private boolean isPrintTicket = true;
    private boolean isPrintDashboard = true;
    private boolean isPrintStaff = true;
    private IssueTicketModel issueTicketModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        listener.hideBottomNavigation();

        View v = inflater.inflate(R.layout.display_ticket, null);

        ButterKnife.bind(this, v);

//        final Handler handler = new Handler();
//        handler.postDelayed(() -> {
//        if (SessionManager.getInstance(mContext).getPrinterStatus())
//            print_ticket_btn.setOnClickListener(DisplayTicketFragment.this);
//        else {
//            print_ticket_btn.setClickable(false);
//            print_ticket_icon.setColorFilter(getResources().getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN);
//            print_ticket_text.setTextColor(getResources().getColor(R.color.text_hint));
//        }

        printerDetection();

//        }, 300);

//        issue_ticket_btn.setOnClickListener(this);

        back_btn.setOnClickListener(this);
        park_btn.setOnClickListener(this);

        return v;
    }

    private void printerDetection(){
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(mContext);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

                enablePrintButton();
                    printerType = Constants.SUNMI_PRINTER;
                } else {
                disablePrintButton();
                    printerType = Constants.NO_PRINTER;
                }

                if (mSunmiPrinter.isPrinterReady()) {
                enablePrintButton();
                    printerType = Constants.SUNMI_PRINTER;
                } else {
                disablePrintButton();
                    printerType = Constants.NO_PRINTER;
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                enablePrintButton();
                printerType = Constants.PAX_PRINTER;
            } else {
                disablePrintButton();
                printerType = Constants.NO_PRINTER;
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(mContext, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
        }
    }

    private void enablePrintButton(){
        print_ticket_btn.setOnClickListener(DisplayTicketFragment.this);
        print_ticket_icon.setColorFilter(getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        print_ticket_text.setTextColor(getResources().getColor(R.color.white));
    }

    private void disablePrintButton(){
        print_ticket_btn.setClickable(false);
        print_ticket_icon.setColorFilter(getResources().getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN);
        print_ticket_text.setTextColor(getResources().getColor(R.color.text_hint));
    }

    @Override
    public void onDestroyView() {
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER))
            mSunmiPrinter.disconnectPrinter();
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        this.printer = new Printer(mContext);
//        printer.printerInit();

        if (getArguments() != null) {
            //  encrypt ticket data
            APICall.encryptTicket(this, mContext,
                    Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getDecryptParams());

            //  get time & date ticket issued
            dateIssued = getArguments().getString(Constants.TICKET_ISSUE_DATE_BUNDLE, "");
            timeIssued = getArguments().getString(Constants.TICKET_ISSUE_TIME_BUNDLE, "");
            newPrintReceipt = getArguments().getBoolean(Constants.NEW_PRINT_RECEIPT);
//        setupPage();
        } else {
            CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
                dialog.dismiss();
                listener.triggerBack();
            }, getString(R.string.error), getString(R.string.ticketdata_error_msg));
        }

    }

    private HashMap<String, Object> getDecryptParams() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
            params.put("ticket_data", getArguments().getString(Constants.TICKET_DATA_BUNDLE, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void setupPage() {
        if (getArguments() != null && !encodedTicketdata.equalsIgnoreCase("")) {

            number_plate.setText(String.format("%s%s", getString(R.string.number_plate_colon),
                    getArguments().getString(Constants.NUMBER_PLATE_BUNDLE)));
            mobile_number.setText("".equalsIgnoreCase(getArguments().getString(Constants.MOBILE_NUMBER_BUNDLE)) ?
                    getString(R.string.mobile_number_colon) + getString(R.string.not_provided) :
                    getString(R.string.mobile_number_colon) + getArguments().getString(Constants.MOBILE_NUMBER_BUNDLE));
            key_collected.setText(getArguments().getBoolean(Constants.KEY_COLLECTION_BUNDLE, false) ?
                    getString(R.string.car_key_collection_colon) + getString(R.string.yes) :
                    getString(R.string.car_key_collection_colon) + getString(R.string.no));
            check_in_time.setText(String.format("%s%s", getString(R.string.check_in_time_colon),
                    getArguments().getString(Constants.CHECK_IN_TIME_BUNDLE)));
            ticket_id.setText(String.format("%s%s", getString(R.string.ticket_id_colon), getArguments().getString(Constants.TICKET_ID_BUNDLE, "")));
            ticketData = getArguments().getString(Constants.TICKET_DATA_BUNDLE, "");

            isPrintTicket = getArguments().getBoolean(Constants.PRINT_TICKET);
            isPrintDashboard = getArguments().getBoolean(Constants.PRINT_DASHBOARD_COPY);
            isPrintStaff = getArguments().getBoolean(Constants.PRINT_STAFF_COPY);

            if(newPrintReceipt && !isPrintTicket && !isPrintDashboard && !isPrintStaff) {
                //  all printing options disabled, disable printing button
                print_ticket_btn.setClickable(false);
                print_ticket_icon.setColorFilter(getResources().getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN);
                print_ticket_text.setTextColor(getResources().getColor(R.color.text_hint));
            }

            //  upfront amount
            String amount = getArguments().getString(Constants.TICKET_UPFRONT_BUNDLE, "0");
            upfrontFlag = getArguments().getBoolean(Constants.TICKET_UPFRONT_FLAG_BUNDLE, false);
            if (upfrontFlag) {
                upfront_amount.setText(String.format("%s%s", getString(R.string.upfront_colon), getString(R.string.rm) + Utils.formatBalance(amount)));
                upfront_amount.setVisibility(View.VISIBLE);
//                upfrontFlag = true;
            } else {
//                upfrontFlag = false;
            }

            qrCode.setImageBitmap(new GenerateQR().createQR(mContext, encodedTicketdata, 400));

//            doIssueTicket();
        }
    }

    private void doIssueTicket() {

    }

    private void printButtonStatus(int count) {
        if (count > 2) {
            print_ticket_btn.setClickable(false);
            print_ticket_icon.setColorFilter(getResources().getColor(R.color.text_hint), android.graphics.PorterDuff.Mode.SRC_IN);
            print_ticket_text.setTextColor(getResources().getColor(R.color.text_hint));
        }
    }

    private void showPrintDialogPAX() {
        if (printCount < 3) {
            CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
                switch (which) {
                    case BUTTON_POSITIVE:
                        new Thread(() -> {
                            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                                PAXPrinter.getInstance().printView(getTicketView());
                                PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                                PAXPrinter.getInstance().start();
                            }
                        }).start();
                        printButtonStatus(++printCount);
                        if (printCount < 3)
                            showPrintDialogPAX();
                        dialog.dismiss();
                        break;

                    case BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }, "", getString(R.string.another_copy_ticket));
        }
    }

    private void showPrintDialog(IssueTicketModel issueTicketModel, PaymentReceiptModel paymentReceiptModel, SummaryModel summaryModel) {
        if (printCount < 3) {
            CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
                switch (which) {
                    case BUTTON_POSITIVE:
                        new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("");
                        printButtonStatus(++printCount);
                        if (printCount < 3)
                            showPrintDialog(issueTicketModel, paymentReceiptModel, summaryModel);
                        dialog.dismiss();
                        break;

                    case BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }, "", getString(R.string.another_copy_ticket));
        }
    }

    private void printDialogDashBoardPAX() {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // dashboard param to print ticket with large Car plate number and no qr code
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(getDashboardCopyView());
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();
                    dialog.dismiss();
                    if(isPrintStaff)
                        printDialogValetCopyPAX();
                    break;
            }
        }, "", getString(R.string.dashboard_ticket));
    }

    private void printDialogValetCopyPAX() {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // valet param to print ticket for valet person
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(getValetCopyView());
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.valet_ticket));
    }

    private void printDialogDashBoard(IssueTicketModel issueTicketModel, PaymentReceiptModel paymentReceiptModel, SummaryModel summaryModel) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // dashboard param to print ticket with large Car plate number and no qr code
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "dashboard").execute("");
                    dialog.dismiss();
                    if(isPrintStaff)
                        printDialogValetCopy(issueTicketModel, paymentReceiptModel, summaryModel);
                    break;
            }
        }, "", getString(R.string.dashboard_ticket));
    }

    private void printDialogValetCopy(IssueTicketModel issueTicketModel, PaymentReceiptModel paymentReceiptModel, SummaryModel summaryModel) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // valet param to print ticket for valet person
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "valet").execute("");
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.valet_ticket));
    }

    private void compileTicketData(){
        issueTicketModel = new IssueTicketModel();
        issueTicketModel.setSite_name(SessionManager.getInstance(mContext).getActivationInfo().getSite_name());
        issueTicketModel.setSite_address(SessionManager.getInstance(mContext).getActivationInfo().getCity());
        issueTicketModel.setTicket_data(encodedTicketdata);
        issueTicketModel.setNumber_plate(getArguments() != null ? getArguments().getString(Constants.NUMBER_PLATE_BUNDLE) : null);
        issueTicketModel.setTicket_info(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
        issueTicketModel.setRunning_id(Utils.addCharAtIndex(Objects.requireNonNull(getArguments()).getString(Constants.RUNNING_ID_BUNDLE, ""), 1, "-"));
        issueTicketModel.setDate_issued(dateIssued);
        issueTicketModel.setTime_issued(Utils.convertStringDateToNewFormat(timeIssued, "hh:mm aa", "HH:mm"));
        if (upfrontFlag) {
            issueTicketModel.setUpfront_flag(upfrontFlag);
            issueTicketModel.setUpfront_amount(getString(R.string.rm) + Utils.formatBalance(getArguments().getString(Constants.TICKET_UPFRONT_BUNDLE), "0"));
        } else {
            issueTicketModel.setUpfront_flag(upfrontFlag);
        }
        issueTicketModel.setStaff_issuer(getArguments().getString(Constants.STAFF_ISSUER));
    }

    private LinearLayout getTicketView(){
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.valet_ticket, null);
        LinearLayout v = view.findViewById(R.id.ticket);

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

        TextView tvSiteName = v.findViewById(R.id.site_name1);
        tvSiteName.setText(activationInfo.getSite_name());

        TextView tvDateIssued = v.findViewById(R.id.date);
        tvDateIssued.setText(dateIssued);

        TextView tvTimeIssued = v.findViewById(R.id.time);
        tvTimeIssued.setText(Utils.convertStringDateToNewFormat(timeIssued, "hh:mm aa", "HH:mm"));

        TextView tvRunningId = v.findViewById(R.id.id);
        tvRunningId.setText(Utils.addCharAtIndex(Objects.requireNonNull(getArguments()).getString(Constants.RUNNING_ID_BUNDLE, ""), 1, "-"));

        String text = encodedTicketdata;
        ImageView imQrCode = v.findViewById(R.id.qrcode);
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        int qrSize = Objects.requireNonNull(getArguments()).getInt(Constants.RUNNING_ID_BUNDLE, 200);
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, qrSize, qrSize);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imQrCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "QR generation error", Toast.LENGTH_SHORT).show();
        }

        TextView tvNumberPlate = v.findViewById(R.id.number_plate);
        tvNumberPlate.setText(number_plate.getText().toString());

        if (upfrontFlag) {
            TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
            tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1() + getString(R.string.rm) + Utils.formatBalance(getArguments().getString(Constants.TICKET_UPFRONT_BUNDLE), "0"));
        } else {
            TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
            tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
        }

        return v;
    }

    private LinearLayout getValetCopyView() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.staff_copy, null);
        LinearLayout v = view.findViewById(R.id.ticket);

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

        TextView tvSiteName = v.findViewById(R.id.site_name1);
        tvSiteName.setText(activationInfo.getSite_name());

        TextView tvDateIssued = v.findViewById(R.id.date);
        tvDateIssued.setText(dateIssued);

        TextView tvTimeIssued = v.findViewById(R.id.time);
        tvTimeIssued.setText(Utils.convertStringDateToNewFormat(timeIssued, "hh:mm aa", "HH:mm"));

        TextView tvRunningId = v.findViewById(R.id.id);
        tvRunningId.setText(Utils.addCharAtIndex(Objects.requireNonNull(getArguments()).getString(Constants.RUNNING_ID_BUNDLE, ""), 1, "-"));

        TextView tvNumberPlate = v.findViewById(R.id.number_plate);
        tvNumberPlate.setText(number_plate.getText().toString());

        TextView tvUpfrontInfo = v.findViewById(R.id.upfront_info);
        if (upfrontFlag) {
            tvUpfrontInfo.setText(getString(R.string.upfront_payment) + ": " + getString(R.string.rm) + Utils.formatBalance(getArguments().getString(Constants.TICKET_UPFRONT_BUNDLE), "0"));
        }

        TextView tvIssuedStaff = v.findViewById(R.id.issued_staff);
        tvIssuedStaff.setText(getString(R.string.issue_ticket) + " - " + getArguments().getString(Constants.STAFF_ISSUER));

        return v;
    }

    private LinearLayout getDashboardCopyView() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.dashboard_copy, null);
        LinearLayout v = view.findViewById(R.id.ticket);

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

        TextView tvSiteName = v.findViewById(R.id.site_name1);
        tvSiteName.setText(activationInfo.getSite_name());

        TextView tvDateIssued = v.findViewById(R.id.date);
        tvDateIssued.setText(dateIssued);

        TextView tvTimeIssued = v.findViewById(R.id.time);
        tvTimeIssued.setText(Utils.convertStringDateToNewFormat(timeIssued, "hh:mm aa", "HH:mm"));

        TextView tvRunningId = v.findViewById(R.id.id);
        tvRunningId.setText(Utils.addCharAtIndex(Objects.requireNonNull(getArguments()).getString(Constants.RUNNING_ID_BUNDLE, ""), 1, "-"));

        String numberPlate = number_plate.getText().toString().replaceAll("[^0-9]", "");
        TextView tvCarDigits = v.findViewById(R.id.digits);
        tvCarDigits.setText(numberPlate);

        TextView tvUpfrontInfo = v.findViewById(R.id.upfront_info);
        if (upfrontFlag) {
            tvUpfrontInfo.setText(getString(R.string.upfront_payment) + ": " + getString(R.string.rm) + Utils.formatBalance(getArguments().getString(Constants.TICKET_UPFRONT_BUNDLE), "0"));
        } else {
            tvUpfrontInfo.setVisibility(View.GONE);
            View vDivider3 = v.findViewById(R.id.divider3);
            vDivider3.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                listener.triggerBack();
                break;

            case R.id.print_ticket_btn:
                compileTicketData();
                if(printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
                    if (isPrintTicket) {
                        if (newPrintReceipt) {
                            if (!encodedTicketdata.equalsIgnoreCase("")) {

                                new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "ticket").execute("");

                                if (isPrintDashboard)
                                    printDialogDashBoard(issueTicketModel, null, null);
                                else if (isPrintStaff)
                                    printDialogValetCopy(issueTicketModel, null, null);

                                disablePrintButton();
                            }
                        } else {
                            if (!encodedTicketdata.equalsIgnoreCase("") && printCount < 3) {

                                new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "").execute("");

                                showPrintDialog(issueTicketModel, null, null);

                                printButtonStatus(++printCount);
                            }
                        }
                    } else if (isPrintDashboard) {
                        printDialogDashBoard(issueTicketModel, null, null);
                        disablePrintButton();
                    } else if (isPrintStaff) {
                        printDialogValetCopy(issueTicketModel, null, null);
                        disablePrintButton();
                    }
                } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {
                    if (isPrintTicket) {
                        if (newPrintReceipt) {
                            if (!encodedTicketdata.equalsIgnoreCase("")) {

                                new Thread(() -> {
                                    if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                                        PAXPrinter.getInstance().printView(getTicketView());
                                        PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                                        PAXPrinter.getInstance().start();
                                    }
                                }).start();

                                if (isPrintDashboard)
                                    printDialogDashBoardPAX();
                                else if (isPrintStaff)
                                    printDialogValetCopyPAX();

                                disablePrintButton();
                            }
                        } else {
                            if (!encodedTicketdata.equalsIgnoreCase("") && printCount < 3) {

                                new Thread(() -> {
                                    if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                                        PAXPrinter.getInstance().printView(getTicketView());
                                        PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                                        PAXPrinter.getInstance().start();
                                    }
                                }).start();

                                showPrintDialogPAX();

                                printButtonStatus(++printCount);
                            }
                        }
                    } else if (isPrintDashboard) {
                        printDialogDashBoardPAX();
                        disablePrintButton();
                    } else if (isPrintStaff) {
                        printDialogValetCopyPAX();
                        disablePrintButton();
                    }
                } else {
                    Toast.makeText(mContext, "No printer detected", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.park_btn:
                if (SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPark().getActive()) {
                    //  go to ticket issued list
                    Bundle tickets_bundle = new Bundle();
                    TicketListsFragment f = new TicketListsFragment();
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_ISSUED_LIST);
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, false, TicketListsFragment.class.getSimpleName());
                } else {
                    //  auto park ticket, then proceed to ticket parked list
                    //  go got ticket parked list
                    Bundle tickets_bundle = new Bundle();
                    TicketListsFragment f = new TicketListsFragment();
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_PARKED_LIST);
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, false, TicketListsFragment.class.getSimpleName());
                }
                break;
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case ENCRYPT:
                EncryptionResponse encryptionResponse = (EncryptionResponse) response.body();
                encodedTicketdata = encryptionResponse.getEncoded_ticket();
                setupPage();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case ENCRYPT:
                break;
        }
    }

}
