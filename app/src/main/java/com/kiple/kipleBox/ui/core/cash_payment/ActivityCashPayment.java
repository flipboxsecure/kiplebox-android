package com.kiple.kipleBox.ui.core.cash_payment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.CustomApplication;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.database.DBInterface;
import com.kiple.kipleBox.database.LocalCacheManager;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.ApplyPackageResponse;
import com.kiple.kipleBox.model.responses.CashPaymentResponse;
import com.kiple.kipleBox.model.responses.GetValidatedTicketResponse;
import com.kiple.kipleBox.model.responses.PackageListResponse;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.NetworkConnectionCheck;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class ActivityCashPayment extends AppCompatActivity implements View.OnClickListener, CallBackInterface {

    public static final int CAPTURE_PHOTO = 1;
    public static final int PICK_PHOTO = 2;
    public static final int PERMISSIONS_REQUEST_STORAGE = 2;
    public static final int PERMISSIONS_REQUEST_CAMERA = 3;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.ticket_id)
    TextView ticket_id;
    @BindView(R.id.entry_time)
    TextView entry_time;
    @BindView(R.id.elapsed_time)
    TextView elapsed_time;
    @BindView(R.id.number_plate)
    TextView number_plate;
    @BindView(R.id.upfront_icon)
    ImageView upfront_icon;
    @BindView(R.id.upfront_amount)
    TextView upfront_amount;
    @BindView(R.id.total_amount_icon)
    ImageView total_amount_icon;
    @BindView(R.id.total_amount)
    TextView total_amount;
    @BindView(R.id.discount_parent)
    ConstraintLayout discount_parent;
    @BindView(R.id.discount_amount)
    TextView discount_amount;
    @BindView(R.id.parking_amount)
    TextView parking_amount;
    @BindView(R.id.pay_amount_parent)
    ConstraintLayout pay_amount_parent;
    @BindView(R.id.paid_amount)
    EditText paid_amount;
    @BindView(R.id.balance_amount)
    TextView balance_amount;
    @BindView(R.id.cash_btn)
    TextView cash_btn;
    @BindView(R.id.charge_to_room_btn)
    TextView charge_to_room;
    @BindView(R.id.charge_to_master_btn)
    TextView charge_to_master;
    @BindView(R.id.print_receipt_btn)
    Button print_receipt_btn;
    @BindView(R.id.credit_card_btn)
    Button credit_card_btn;
    @BindView(R.id.charging_details)
    EditText charging_details;
    @BindView(R.id.menu_btn)
    ImageView menu_btn;
    @BindView(R.id.package_spinner)
    Spinner package_spinner;
    @BindView(R.id.payment_info)
    ConstraintLayout payment_info;
    @BindView(R.id.package_selection)
    ConstraintLayout package_selection;
    @BindView(R.id.payment_action_container)
    ConstraintLayout payment_action_container;
    @BindView(R.id.loss_ticket_container)
    ConstraintLayout loss_ticket_container;
    @BindView(R.id.package_confirm_btn)
    Button package_confirm_btn;
    @BindView(R.id.package_img)
    ImageView package_img;
    @BindView(R.id.package_note_parent)
    TextInputLayout package_note_parent;
    @BindView(R.id.package_note)
    TextInputEditText package_note;
    @BindView(R.id.lost_ticket_btn)
    Button lost_ticket_btn;
    @BindView(R.id.max_charge_btn)
    Button max_charge_btn;

    private Printer mSunmiPrinter;
    private String printerType;
    private TicketListsResponse info;
    private Integer chargeType = null;
    private boolean hasPackage = false;
    private int selectedPackageIndex = -1;
    private PackageListResponse packageListResponse;
    private String[] packageDetails;
    private PackageSpinnerAdapter adapter;
    private ApplyPackageResponse applyPackageResponse;
    private String newAmount;
    private boolean refreshList = false;
    private Uri imageUriFromCamera;
    private Uri finalImageUri;

    private boolean isPrintReceipt = true;
    private boolean isPrintLogo = false;
    private boolean isPrintCompanyName = false;
    private boolean isPaymentInProgress = false;
    private boolean isPackageApplying = false;
    private String activityName = "";
    private int lostTicketAmount = 0;
    private int lostTicketRate = 0;
    private int maxChargeAmount = 0;
    private int maxChargeRate = 0;
    private int buttonClicked = 0;  // 1 means Lost ticket and 2 means Maximum charge is clicked
    private boolean showTaxInfo = false;
    private boolean isPerEntry = false;
    private boolean isLostTicketEnabled = false;
    private boolean isMaxChargeEnabled = false;
    private boolean isCardPayment = true;
    private JsonObject cardPaymentResponse;
    private String cardPaymentScheme;
    private boolean isCollectEnabled = false;

    private boolean isValidatedPayment = false;
    private String validatedCarPlate = "";
    private String validatedDiscountAmount = "";
    private boolean lostTicketMaxChargeNeeded = false;

    public static void showMessageRequirePermission(Activity activity) {
        AlertDialog.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, R.style.AppCompat_ProgressDialog);
        } else {
            builder = new AlertDialog.Builder(activity);
        }
        builder.setCancelable(false).setMessage(R.string.not_allow_permission)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    dialog.cancel();
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                    intent.setData(uri);
                    activity.startActivity(intent);
                }).setNegativeButton(android.R.string.yes, (dialog, which) -> dialog.cancel()).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_payment);
        ButterKnife.bind(this);

        if (null == getIntent().getParcelableExtra(Constants.TICKET_INFO_BUNDLE)) {
            CustomAlerts.showOKAlert(this, (dialog, which) -> {
                if (which == RESULT_OK) {
                    dialog.dismiss();
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }, "Error", "Ticket info unavailable.");
        } else {
            //  flag to determine payment triggered from where, if lost ticket & max charge button needed
            activityName = getIntent().getStringExtra("activity");

            if (!activityName.equalsIgnoreCase(Constants.VALIDATED_TICKET_PAYMENT)) {
                info = getIntent().getParcelableExtra(Constants.TICKET_INFO_BUNDLE);
                lostTicketMaxChargeNeeded = false;
            } else {
                mapInfo(getIntent().getParcelableExtra(Constants.TICKET_INFO_BUNDLE));
                lostTicketMaxChargeNeeded = true;
            }
        }

//        this.mSunmiPrinter = new Printer(this);
//        mSunmiPrinter.printerInit();
        printerDetection();

        init();
    }

    private <T extends Parcelable> void mapInfo(T parcelableExtra) {
        isValidatedPayment = true;
        GetValidatedTicketResponse.Data data = (GetValidatedTicketResponse.Data) parcelableExtra;

        info = new TicketListsResponse();
        info.setTicketId(data.getTicketId());
        info.setSiteId(data.getSiteId());
        info.setTicketData(data.getTicketData());
        info.setService(data.getService());
        info.setValet_status_id(data.getValetStatusId());
        info.setValet_car_key_collected(data.getValetCarKeyCollected());
        info.setUserId(data.getUserId());
        info.setMobile_no(data.getMobileNo());
        info.setAmount(data.getAmount());
        if (null != data.getFee())
            info.setFee(data.getFee());
        if (null != data.getUpfrontPayment())
            info.setUpfront_payment(data.getUpfrontPayment());
        info.setGst(data.getGst());
        info.setEntryTime(data.getEntryTime());
        info.setEntryTimeReadable(data.getEntryTimeReadable());
        info.setReqTime(data.getReqTime());
        info.setDuration(data.getDuration());
        info.setGracePeriod(data.getGracePeriod());
        info.setDateGraceEnd(data.getDateGraceEnd());
        info.setStatus(data.getStatus());
        info.setDateCreate(data.getDateCreate());
        info.setDateUpdate(data.getDateUpdate());
        info.setDatePark(data.getDatePark());
        info.setDatePickup(data.getDatePickup());
        info.setDateCollected(data.getDateCollected());
        info.setDatePay(data.getDatePay());
        info.setDatePayUnix(data.getDatePayUnix());
        info.setRequestId(data.getRequestId());
        info.setPaymentAuthCode(data.getPaymentAuthCode());
        info.setReceipt(data.getReceipt());
        info.setFlag(data.getFlag());
        info.setValet_status_desc(data.getValetStatusDesc());
        info.setValet_park_loc(data.getValetParkLoc());
        info.setStaff_parker(data.getStaffParker());
        info.setStaff_pickup(data.getStaffPickup());
        info.setStaff_collected(data.getStaffCollected());
        info.setStaff_issuer(data.getStaffIssuer());
        validatedCarPlate = data.getPlateNo();
        validatedDiscountAmount = data.getDiscountAmount();
    }

    private void printerDetection() {
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(this);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

                    printerType = Constants.SUNMI_PRINTER;
                    enablePrintButton();
                } else {
                    printerType = Constants.NO_PRINTER;
                    disablePrintButton();
                }

                if (mSunmiPrinter.isPrinterReady()) {
                    printerType = Constants.SUNMI_PRINTER;
                    enablePrintButton();
                } else {
                    printerType = Constants.NO_PRINTER;
                    disablePrintButton();
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                printerType = Constants.PAX_PRINTER;
                enablePrintButton();
                // enable credit card button
                credit_card_btn.setVisibility(View.VISIBLE);
            } else {
                printerType = Constants.NO_PRINTER;
                disablePrintButton();
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(this, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
            disablePrintButton();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (refreshList) {
                Intent intent = new Intent();
                intent.putExtra(Constants.LIST_REFRESH, refreshList);
                setResult(RESULT_CANCELED, intent);
                this.finish();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER))
            mSunmiPrinter.disconnectPrinter();
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // take photo for avatar
//        if (v.getId() == avatar.getId()) {
        if (v instanceof ImageView) {
            menu.add(1, 10001, Menu.NONE, getString(R.string.take_picture));
            menu.add(1, 10002, Menu.NONE, getString(R.string.select_from_albums));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // take photo
        if (item.getItemId() == 10001) {
            if (showPermission(PERMISSIONS_REQUEST_CAMERA, Manifest.permission.CAMERA)) {
                if (showPermission(PERMISSIONS_REQUEST_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (showPermission(PERMISSIONS_REQUEST_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        startCameraActivity();
                    }
                }
            }
        } else if (item.getItemId() == 10002) {
            selectPhoto();
        }
        return super.onContextItemSelected(item);
    }

    private void startCameraActivity() {
        Toast.makeText(this, "start camera", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            imageUriFromCamera = Uri.fromFile(Utils.getOutputMediaFile(this));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriFromCamera);
        } else {
            File file = new File(Uri.fromFile(Utils.getOutputMediaFile(this)).getPath());
            imageUriFromCamera = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriFromCamera);
        }

//        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
//            intent.setClipData(ClipData.newRawUri("", imageUriFromCamera));
//            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, CAPTURE_PHOTO);
//            selectPhotoAction();
        }
    }

    public boolean showPermission(int requestCode, String requireString) {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, requireString);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    requireString)) {
                showMessageRequirePermission(this);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{requireString}, requestCode);
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_PHOTO:
                    if (imageUriFromCamera != null) {
                        //  set image to UI with image URI
                        finalImageUri = imageUriFromCamera;
//                        LogUtils.LOGE("index tracking", "imageUrlList index: " + imageUriList[currentImageIndex]);
                        //  store all image URI in a list
                        //  compile list of image multipart when triggered to send
                        package_img.setImageURI(finalImageUri);
                    }
                    break;
                case PICK_PHOTO:
                    if (data != null && data.getData() != null) {
                        //  set image to UI with image URI
                        finalImageUri = data.getData();
//                        LogUtils.LOGE("index tracking", "imageUrlList index: " + imageUriList[currentImageIndex]);
                        //  store all image URI in a list
                        //  compile list of image multipart when triggered to send
                        package_img.setImageURI(finalImageUri);
                    }
                    break;

//                case CROP_PHOTO:
//                    if (data != null && data.hasExtra("data")) {
//                        SessionManager.getInstance(this).setImageDatas(data.getByteArrayExtra("data"));
//                        byte[] data_image = SessionManager.getInstance(this).getImageDatas();
//                        bmpAvatar = BitmapFactory.decodeByteArray(data_image, 0, data_image.length);
//                        uploadAvatar();
//                    } else {
//                        SessionManager.getInstance(this).setImageDatas(null);
//                    }
//                    break;
            }
        }
    }

    private void selectPhoto() {
        Toast.makeText(this, "Select Photo", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        intent = Intent.createChooser(intent, getString(R.string.select_picture));

        startActivityForResult(intent, PICK_PHOTO);
//        selectPhotoAction();
    }

    private void checknFetchPackages() {
        APICall.getPackageList(this, this,
                Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(),
                info.getTicketId(), info.getSiteId());
    }

    private void init() {
        //  check for & fetch packages
        checknFetchPackages();

        ActivationResponse activationResponse = SessionManager.getInstance(this).getActivationInfo();
        List<ActivationResponse.Datum> rateFormula = activationResponse.getRate_formula().getData();

        for (int j = 0; j < rateFormula.size(); j++) {
            if (rateFormula.get(j).getPackage_id() == null && rateFormula.get(j).getType().equalsIgnoreCase("general")) {
                maxChargeRate = Integer.parseInt(rateFormula.get(j).getMax_rate());
                lostTicketRate = Integer.parseInt(rateFormula.get(j).getLost_ticket());
                if (null != info.getUpfront_payment()) {
                    maxChargeAmount = Integer.parseInt(rateFormula.get(j).getMax_rate()) - Integer.parseInt(info.getUpfront_payment());
                } else {
                    maxChargeAmount = Integer.parseInt(rateFormula.get(j).getMax_rate());
                }

                if (null != info.getUpfront_payment()) {
                    lostTicketAmount = Integer.parseInt(rateFormula.get(j).getLost_ticket()) - Integer.parseInt(info.getUpfront_payment());
                } else {
                    lostTicketAmount = Integer.parseInt(rateFormula.get(j).getLost_ticket());
                }
            }
        }


        List<ActivationResponse.Service_feature> paymentService = activationResponse.getService_customize().getPay().getService_features();
        //  print receipt flag
        for (int i = 0; i < paymentService.size(); i++) {
            switch (paymentService.get(i).getName()) {

                case Constants.PRINT_RECEIPT_FEATURE:
                    isPrintReceipt = null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES);

                    if (null != paymentService.get(i).getAttributes() && paymentService.get(i).getAttributes().size() > 0) {
                        for (int j = 0; j < paymentService.get(i).getAttributes().size(); j++)
                            switch (paymentService.get(i).getAttributes().get(j).getName()) {
                                case Constants.PRINT_LOGO:
                                    isPrintLogo = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;

                                case Constants.COMPANY_NAME:
                                    isPrintCompanyName = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;

                                case Constants.PER_ENTRY:
                                    isPerEntry = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;
                            }
                    }
                    break;

                case Constants.PAYMENT_BUTTON:
                    if (activityName.equalsIgnoreCase("TicketList")) {
                        if (null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            print_receipt_btn.setVisibility(View.VISIBLE);
                        } else {
                            print_receipt_btn.setVisibility(View.GONE);
                        }
                    } else {
                        print_receipt_btn.setVisibility(View.VISIBLE);
                    }
                    break;

                case Constants.LOST_TICKET_BUTTON:
                    if (activityName.equalsIgnoreCase("TicketList")) {
                        //  if ticket validation enabled then turn off feature
                        if (lostTicketMaxChargeNeeded && null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            lost_ticket_btn.setVisibility(View.VISIBLE);
//                            loss_ticket_container.setVisibility(View.VISIBLE);
                            isLostTicketEnabled = true;
                        } else {
                            lost_ticket_btn.setVisibility(View.GONE);
//                            loss_ticket_container.setVisibility(View.GONE);
                        }

                        lostTicketContainerShowHide();
                    }

                    break;

                case Constants.MAX_CHARGE_BUTTON:
                    if (activityName.equalsIgnoreCase("TicketList")) {
                        //  if ticket validation enabled then turn off feature
                        if (lostTicketMaxChargeNeeded && null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            max_charge_btn.setVisibility(View.VISIBLE);
//                            loss_ticket_container.setVisibility(View.VISIBLE);
                        } else {
                            max_charge_btn.setVisibility(View.GONE);
//                            loss_ticket_container.setVisibility(View.GONE);
                        }

                        lostTicketContainerShowHide();
                    }

                    break;


                case Constants.PAYMENT_MODE_FEATURE:
                    if (null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                        List<ActivationResponse.Attribute> paymentMode = paymentService.get(i).getAttributes();
                        for (int j = 0; j < paymentMode.size(); j++)
                            switch (paymentMode.get(j).getName()) {
                                case Constants.WALLET:
                                    //  not available by default
                                    break;

                                case Constants.PAYMENT_MODE_CASH:
                                    if (null != paymentMode.get(j).getValues() && Constants.YES.equalsIgnoreCase(paymentMode.get(j).getValues())) {
                                        cash_btn.setVisibility(View.VISIBLE);
                                        if (Constants.YES.equalsIgnoreCase(paymentMode.get(j).getDefault())) {
                                            chargeType = 0;
                                            resetChargeOptionsButtons();
                                            cash_btn.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                                            cash_btn.setTextColor(getResources().getColor(R.color.white));
                                        }
                                    } else
                                        cash_btn.setVisibility(View.GONE);
                                    break;

                                case Constants.PAYMENT_MODE_GUEST_ROOM:
                                    if (null != paymentMode.get(j).getValues() && Constants.YES.equalsIgnoreCase(paymentMode.get(j).getValues())) {
                                        charge_to_room.setVisibility(View.VISIBLE);
                                        if (Constants.YES.equalsIgnoreCase(paymentMode.get(j).getDefault())) {
                                            chargeType = 1;
                                            charging_details.setVisibility(View.VISIBLE);
                                            charging_details.setText(null);
                                            charging_details.setHint(getString(R.string.room_number));
                                            resetChargeOptionsButtons();
                                            charge_to_room.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                                            charge_to_room.setTextColor(getResources().getColor(R.color.white));
                                        }
                                    } else
                                        charge_to_room.setVisibility(View.GONE);
                                    break;

                                case Constants.PAYMENT_MODE_MASTER_ACCOUNT:
                                    if (null != paymentMode.get(j).getValues() && Constants.YES.equalsIgnoreCase(paymentMode.get(j).getValues())) {
                                        charge_to_master.setVisibility(View.VISIBLE);
                                        if (Constants.YES.equalsIgnoreCase(paymentMode.get(j).getDefault())) {
                                            chargeType = 2;
                                            charging_details.setVisibility(View.VISIBLE);
                                            charging_details.setText(null);
                                            charging_details.setHint(getString(R.string.event_name));
                                            resetChargeOptionsButtons();
                                            charge_to_master.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                                            charge_to_master.setTextColor(getResources().getColor(R.color.white));
                                        }
                                    } else
                                        charge_to_master.setVisibility(View.GONE);
                                    break;

                            }
                    }
                    break;

                case Constants.TAX_INFO:
                    showTaxInfo = null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES);
                    break;
            }
        }

        List<ActivationResponse.Service_feature> collectService = activationResponse.getService_customize().getCollect().getService_features();

        for (int i = 0; i < collectService.size(); i++) {
            if (Constants.ENABLE_FEATURE.equals(collectService.get(i).getName())) {
                isCollectEnabled = null == collectService.get(i).getFlag_value() || !collectService.get(i).getFlag_value().equalsIgnoreCase(Constants.NO);
            }
        }

        title.setText(activationResponse.getSite_name());
        address.setText(activationResponse.getCity() + ", " + activationResponse.getState());
        ticket_id.setText(info.getTicketId());
        entry_time.setText(Utils.getEntryTimeFromUnix(info.getEntryTime(), activationResponse.getTimezone()));
        elapsed_time.setText(Utils.getElapsedTimeFromUnix(info.getEntryTime(), info.getReqTime()));
        if (!isValidatedPayment) {
            number_plate.setText("Ticketdata Corrupted".equalsIgnoreCase(Utils.getNumberPlateFromTicketdata(info.getTicketData())) ? getString(R.string.empty_placeholder) : Utils.getNumberPlateFromTicketdata(info.getTicketData()));
        } else {
            number_plate.setText(validatedCarPlate);
        }

        //  upfront payment
        if (null != info.getUpfront_payment() && null != info.getFee()) {
            upfront_amount.setText(getString(R.string.upfront_colon) + getString(R.string.rm) + Utils.formatBalance(info.getUpfront_payment()));
            upfront_amount.setVisibility(View.VISIBLE);
            upfront_icon.setVisibility(View.VISIBLE);

            total_amount.setText(getString(R.string.total_fees) + getString(R.string.rm) + Utils.formatBalance(info.getFee()));
            total_amount.setVisibility(View.VISIBLE);
            total_amount_icon.setVisibility(View.VISIBLE);
        }

        //  ticket validation discount
        if (isValidatedPayment) {
            discount_amount.setText(getString(R.string.rm) + Utils.formatBalance(validatedDiscountAmount));
            discount_parent.setVisibility(View.VISIBLE);
        } else {
            discount_parent.setVisibility(View.GONE);
        }

        parking_amount.setText(getString(R.string.rm) + Utils.formatBalance(info.getAmount()));

        paid_amount.setText(Utils.formatBalance(info.getAmount()));

        //  removed payment calculation
//        paid_amount.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (!"".equalsIgnoreCase(paid_amount.getText().toString()))
//                    tabulateAndShowBalance();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        //  select cash payment by default
//        resetChargeOptionsButtons();
//        cash_btn.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
//        cash_btn.setTextColor(getResources().getColor(R.color.white));

    }

    private void enablePrintButton() {
        print_receipt_btn.setOnClickListener(ActivityCashPayment.this);
        print_receipt_btn.setClickable(true);
        print_receipt_btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        print_receipt_btn.setTextColor(getResources().getColor(R.color.white));
    }

    private void disablePrintButton() {
        print_receipt_btn.setClickable(false);
        print_receipt_btn.setBackgroundColor(getResources().getColor(R.color.text_hint));
        print_receipt_btn.setTextColor(getResources().getColor(R.color.text_hint));
    }

    private void lostTicketContainerShowHide() {
        if (!isLostTicketEnabled && !isMaxChargeEnabled) {
            loss_ticket_container.setVisibility(View.GONE);
        } else {
            loss_ticket_container.setVisibility(View.VISIBLE);
        }
    }

    private void refreshPage(boolean isPackageRefresh) {
        if (isPackageRefresh) {
            if (null != info.getUpfront_payment()) {
                //  upfront payment transaction
                parking_amount.setText(getString(R.string.rm) + Utils.formatBalance(applyPackageResponse.getData().getAmount()));

                if (Utils.isNumerical(applyPackageResponse.getData().getAmount()) && Utils.isNumerical(info.getUpfront_payment())) {
                    //  both amounts provided are valid figures, find the sum which is total ticket amount
                    String tAmount = Utils.formatBalance(Double.parseDouble(applyPackageResponse.getData().getAmount()) + Double.parseDouble(info.getUpfront_payment()));
                    total_amount.setText(getString(R.string.total_fees) + getString(R.string.rm) + Utils.formatBalance(Double.parseDouble(applyPackageResponse.getData().getAmount()) + Double.parseDouble(info.getUpfront_payment())));
                    info.setFee(tAmount);
                    info.setAmount(applyPackageResponse.getData().getAmount());
                } else {
                    //  one or both of the amounts provided is not a valid figure
                    total_amount.setText(getString(R.string.total_fees) + getString(R.string.data_corrupted));
                }
                refreshList = true;
                goBackToPayment();
            } else {
                //  normal transaction
                parking_amount.setText(getString(R.string.rm) + Utils.formatBalance(applyPackageResponse.getData().getAmount()));
                paid_amount.setText(Utils.formatBalance(applyPackageResponse.getData().getAmount()));
                //  update payment amount locally to use for payment later
                info.setAmount(applyPackageResponse.getData().getAmount());
                info.setFee(applyPackageResponse.getData().getAmount());
                goBackToPayment();
            }
        } else {
            if (null != newAmount && !newAmount.isEmpty()) {
                refreshList = true;
                parking_amount.setText(getString(R.string.rm) + Utils.formatBalance(newAmount));
                //  update payment amount locally to use for payment later
                info.setAmount(newAmount);
                info.setFee(newAmount);
            }
        }
    }

    private void goBackToPayment() {
        payment_info.setVisibility(View.VISIBLE);
        payment_action_container.setVisibility(View.VISIBLE);
        package_selection.setVisibility(View.GONE);
        package_confirm_btn.setVisibility(View.GONE);
    }

    private void tabulateAndShowBalance() {
//        String balance = Utils.getBalance(info.getAmount(), paid_amount.getText().toString());
        balance_amount.setText(getString(R.string.rm) + Utils.getBalance(parking_amount.getText().toString(), paid_amount.getText().toString()));
    }

    private void resetChargeOptionsButtons() {
        cash_btn.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue_transparent));
        cash_btn.setTextColor(getResources().getColor(R.color.payment_type));
        charge_to_room.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue_transparent));
        charge_to_room.setTextColor(getResources().getColor(R.color.payment_type));
        charge_to_master.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue_transparent));
        charge_to_master.setTextColor(getResources().getColor(R.color.payment_type));
    }

    private HashMap<String, Object> getParams(boolean isCC) {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("ticketId", info.getTicketId());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("pos_payer", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("service", SessionManager.getInstance(this).getActivationInfo().getIdentify());
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("staff_payer", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
            params.put("paymentAmount", info.getAmount());
            params.put("paymentReceived", paid_amount.getText().toString());
            params.put("paymentBalance", Utils.getBalance(info.getAmount(), paid_amount.getText().toString()));
            params.put("entryTime", info.getEntryTime());
            long currentUnix = Utils.getCurrentTimeUnix();
            params.put("reqTime", String.valueOf(currentUnix));
            params.put("duration", String.valueOf(currentUnix - Long.parseLong(info.getEntryTime())));
            params.put("gracePeriod", SessionManager.getInstance(this).getActivationInfo().getGrace_period().toString());
            params.put("datePayUnix", String.valueOf(currentUnix));
            if (isCC) {
                //  // TODO: 2019-11-19 confirm credit card charge type with backend
                //  credit card -> 3
                //  debit card  -> 4
                if (cardPaymentScheme.equalsIgnoreCase(Constants.DEBIT_CARD_PAYMENT)) {
                    chargeType = 3;
                } else {
                    chargeType = 4;
                }
                params.put("payment_pos_method", chargeType + 1);
                //  credit card payment response
                params.put("card_payment_response", cardPaymentResponse);
            } else {
                params.put("payment_pos_method", chargeType + 1);
                if (chargeType == 1 || chargeType == 2)
                    params.put("payment_pos_method_detail", charging_details.getText().toString());

                //  empty credit card response
                params.put("card_payment_response", "");
            }
            params.put("receipt", SessionManager.getInstance(this).getActivationInfo().getSite_id() + info.getTicketId());
            //  which status to proceed to
            ActivationResponse.Service_customize services = SessionManager.getInstance(this).getActivationInfo().getService_customize();
            if (!services.getCollect().getActive() && services.getComplete().getActive()) {
                params.put("next_status", "4");
            } else if (!services.getCollect().getActive() && !services.getComplete().getActive()) {
                params.put("next_status", "5");
            }

            //  LPSM ticket payment check
            if (activityName.equalsIgnoreCase(Constants.VALIDATED_TICKET_PAYMENT)) {
                params.put("lpsm_flag", true);
            } else {
                params.put("lpsm_flag", false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extra = intent.getExtras();
        compileIntent(intent);
        String pay_resp_code = extra.getString("pay_resp_code");
        String pay_resp_scheme = extra.getString("pay_resp_scheme");
        String pay_type = extra.getString("pay_type");
        Log.e("PAYMENT RESPONSE DATA", pay_resp_code);
        Log.e("PAYMENT RESPONSE SCHEME", pay_resp_scheme);

        cardPaymentScheme = pay_resp_scheme;

        cardPaymentResult(pay_resp_code, pay_type);
    }

    private void cardPaymentResult(String responseCode, String payType) {
        isPaymentInProgress = false;
        if (null != responseCode) {
            switch (responseCode) {
                case "00":
                    //  transaction approved
                    //  do payment & print receipt
                    APICall.cashPayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParams(true));
                    break;

                case "99":
                    //  For QR, transaction pending, please do inquiry
                    //  For card scheme, transaction declined
                    //  todo
                    if (payType.equalsIgnoreCase("01")) {
                        //  card payment
                        cardDeclined();
                    } else if (payType.equalsIgnoreCase("02")) {
                        //  QR payment

                    }
                    break;

                case "-2":
                case "-3":
                    //  For QR, transaction pending, please do inquiry
                    //  todo
                    break;

                case "ND":
                    //  User press cancel / no host response etc
                    //  For QR sale only, transaction pending, please do inquiry
                    //  For card scheme, when no host response, reversal will be auto triggered at next transaction or before settlement
                    //  todo
                    if (payType.equalsIgnoreCase("01")) {
                        //  card payment
                        cardDeclined();
                    } else if (payType.equalsIgnoreCase("02")) {

                    }
                    break;

                case "CE":
                    //  For card scheme, transaction declined
                    //  todo
                    if (payType.equalsIgnoreCase("01")) {
                        //  card payment
                        cardDeclined();
                    } else if (payType.equalsIgnoreCase("02")) {

                    }
                    break;

                default:
                    //  Transaction declined
                    //  todo
                    cardDeclined();
            }
        } else {
            Toast.makeText(this, "Invalid Payment Response", Toast.LENGTH_LONG).show();
        }
    }

    private void cardDeclined() {
        CustomAlerts.showOKAlert(this, "Transaction Error", "Card Declined");
    }

    public void compileIntent(Intent i) {
        String LOG_TAG = "Card Payment Extras";
        cardPaymentResponse = new JsonObject();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e(LOG_TAG, "Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                cardPaymentResponse.add(key, new Gson().toJsonTree(bundle.get(key)));

                Log.e(LOG_TAG, "[" + key + "=" + bundle.get(key) + "]");

            }
            Log.e(LOG_TAG, "Dumping Intent end");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exit_btn:
                this.finish();
                return;

            case R.id.cash_btn:
                if (null == chargeType)
                    chargeType = 0;
                else if (chargeType == 0)
                    return;

                charging_details.setVisibility(View.GONE);
                charging_details.setText(null);
                chargeType = 0;
                resetChargeOptionsButtons();
                cash_btn.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                cash_btn.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.charge_to_room_btn:
                if (null == chargeType)
                    chargeType = 1;
                else if (chargeType == 1)
                    return;

                charging_details.setVisibility(View.VISIBLE);
                charging_details.setText(null);
                charging_details.setHint(getString(R.string.room_number));
                chargeType = 1;
                resetChargeOptionsButtons();
                charge_to_room.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                charge_to_room.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.charge_to_master_btn:
                if (null == chargeType)
                    chargeType = 2;
                else if (chargeType == 2)
                    return;

                charging_details.setVisibility(View.VISIBLE);
                charging_details.setText(null);
                charging_details.setHint(getString(R.string.event_name));
                chargeType = 2;
                resetChargeOptionsButtons();
                charge_to_master.setBackground(getResources().getDrawable(R.drawable.payment_type_btn));
                charge_to_master.setTextColor(getResources().getColor(R.color.white));
                break;

            //  credit card -> 3
            //  debit card  -> 4

            case R.id.credit_card_btn:
                if (isPaymentInProgress)
                    return;

                isCardPayment = true;
                isPaymentInProgress = true;

                //  credit card payment
                Bundle extra = new Bundle();
                String pay_function = "01";
                String pay_type = "01";
                String pay_camera_mode = "01";
                String pay_print_receipt_id = "Y";
                extra.putString("pay_amount", String.valueOf((int) (Double.parseDouble(info.getAmount()) * 100)));
                extra.putString("pay_function", pay_function);
                extra.putString("pay_type", pay_type);
                extra.putString("pay_camera_mode", pay_camera_mode);
                extra.putString("pay_print_receipt_id", pay_print_receipt_id);

                Intent intent = new Intent("com.revenue.edc.hlb.pro.app2app");
                intent.putExtras(extra);

                startActivity(intent);
                break;

            case R.id.print_receipt_btn:
                if (isPaymentInProgress)
                    return;

                isPaymentInProgress = true;
                //  payment calculation
                if ("".equalsIgnoreCase(paid_amount.getText().toString()) || Float.parseFloat(info.getAmount()) > Float.parseFloat(paid_amount.getText().toString())) {
                    isPaymentInProgress = false;
                    paid_amount.setError("Amount must be more than parking cost");
                    paid_amount.requestFocus();
                    return;
                }

                if (null == chargeType) {
                    isPaymentInProgress = false;
                    CustomAlerts.showOKAlert(this, "", getString(R.string.select_payment_type));
                    return;
                }

                //  do payment & print receipt
                APICall.cashPayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParams(false));
                break;

            case R.id.pay_amount_parent:
                paid_amount.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.showSoftInput(paid_amount, InputMethodManager.SHOW_IMPLICIT);
                }
                break;

            case R.id.menu_btn:
                if (hasPackage) {
                    //  set cancel or select package
//                    if(selectedPackageIndex > 0 && packageListResponse.getData().get(0).getId() == -1){
//                        //  switch to cancel package
//                        packageListResponse.getData().get(0).setId(0);
//                        packageListResponse.getData().get(0).setName(getString(R.string.cancel_package));
//                        packageDetails[0] = packageListResponse.getData().get(0).getName() + "," + packageListResponse.getData().get(0).getAmount();
//
//                        adapter.notifyDataSetChanged();
//                    } else if(selectedPackageIndex == 0 && packageListResponse.getData().get(0).getId() == 0){
//                        //  switch to select package
//                        packageListResponse.getData().get(0).setId(-1);
//                        packageListResponse.getData().get(0).setName(getString(R.string.select_package));
//                        packageDetails[0] = packageListResponse.getData().get(0).getName() + "," + packageListResponse.getData().get(0).getAmount();
//
//                        adapter.notifyDataSetChanged();
//                    }

                    //  hide payment info, show package selection, only if there's package available
                    payment_info.setVisibility(View.GONE);
                    payment_action_container.setVisibility(View.GONE);
                    package_selection.setVisibility(View.VISIBLE);
                    package_confirm_btn.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.package_img:
//            case R.id.package_img_parent:
                this.openContextMenu(package_img);
                break;

            case R.id.package_confirm_btn:
                if (isPackageApplying)
                    return;

                isPackageApplying = true;
                HashMap<String, Object> params = new HashMap<>();
                params.clear();
                switch (packageListResponse.getData().get(selectedPackageIndex).getId()) {
                    case -1:    //  select valid package
                        //  toast msg reminder for valid package selection
                        Toast.makeText(this, getString(R.string.please_select_package), Toast.LENGTH_SHORT).show();
                        isPackageApplying = false;
                        break;

                    case 0:     //  cancel package
                        //  cancel package
                        params.put("ticket_id", info.getTicketId());
                        params.put("site_id", packageListResponse.getData().get(selectedPackageIndex - 1).getSite_id());
                        String packageId = null;
                        params.put("rate_package_id", packageId);

                        APICall.applyPackage(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
                        break;

                    default:    //  apply packages
                        //  confirm package selection, update payment details page
                        params.put("ticket_id", info.getTicketId());
                        params.put("site_id", packageListResponse.getData().get(selectedPackageIndex).getSite_id());
                        params.put("rate_package_id", packageListResponse.getData().get(selectedPackageIndex).getId());
                        params.put("remarks", package_note.getText().toString());
                        try {
                            InputStream iStream = getContentResolver().openInputStream(finalImageUri);
                            params.put("image", Base64.encodeToString(Utils.getImageInputStreamToByteArray(iStream), Base64.DEFAULT));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        APICall.applyPackage(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
                }
                break;

            case R.id.back_btn:
                //  hide package selection, show payment info
                goBackToPayment();
                break;

            case R.id.lost_ticket_btn:
                if (isPaymentInProgress)
                    return;

                isPaymentInProgress = true;

                lostTicketAPICall();

                break;

            case R.id.max_charge_btn:
                if (isPaymentInProgress)
                    return;

                isPaymentInProgress = true;

                maxChargeAPICall();
                break;
        }
    }

    private void printReceipt(String receipt, String entryTime, String payTime) {
        //  print receipt
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
            IssueTicketModel issueTicketModel = null;
            SummaryModel summaryModel = null;

            ActivationResponse activationInfo = SessionManager.getInstance(this).getActivationInfo();
            PaymentReceiptModel paymentReceiptModel = new PaymentReceiptModel();
            paymentReceiptModel.setSite_name(activationInfo.getSite_name());
            StringBuilder address = new StringBuilder();

            if(activationInfo.getStreet1() != null || !TextUtils.isEmpty(activationInfo.getStreet1())) {
                address.append(activationInfo.getStreet1());
            }

            if (activationInfo.getStreet2() != null && !TextUtils.isEmpty(activationInfo.getStreet2())) {
                address.append(activationInfo.getStreet2());
            }

            if (activationInfo.getPostcode() != null) {
                address.append(activationInfo.getPostcode()).append(" ");
            }

            if (activationInfo.getCity() != null && !TextUtils.isEmpty(activationInfo.getCity())) {
                address.append(activationInfo.getCity()).append(", ");
            }

            if (activationInfo.getState() != null && !TextUtils.isEmpty(activationInfo.getState())) {
                address.append(activationInfo.getState());
            }

            paymentReceiptModel.setSite_address(address.toString());

            if (activationInfo.getPhone_number() != null)
                paymentReceiptModel.setSite_phone_number(activationInfo.getPhone_number().toString());

            paymentReceiptModel.setDate(Utils.getCurrentDateOrTime("dd/MM/yyyy"));
            paymentReceiptModel.setTime(Utils.getCurrentDateOrTime("hh:mm aa"));
            if (!isValidatedPayment) {
                paymentReceiptModel.setNumber_plate(Utils.getNumberPlateFromTicketdata(info.getTicketData()));
            } else {
                paymentReceiptModel.setNumber_plate(validatedCarPlate);
            }
            paymentReceiptModel.setTicket_id(info.getTicketId());
            paymentReceiptModel.setStaff_name(SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());

            if (buttonClicked == 1) {
                paymentReceiptModel.setValet_amount(getString(R.string.rm) + Utils.formatBalance(lostTicketAmount + ""));
            } else if (buttonClicked == 2) {
                paymentReceiptModel.setValet_amount(getString(R.string.rm) + Utils.formatBalance(maxChargeAmount + ""));
            } else {
                paymentReceiptModel.setValet_amount(getString(R.string.rm) + (null == info.getFee() ? Utils.formatBalance(info.getAmount()) : Utils.formatBalance(info.getFee())));
            }
            paymentReceiptModel.setPos_info(activationInfo.getPos_info1());
            paymentReceiptModel.setQr_info(activationInfo.getPos_info2());
            switch (chargeType) {
                case 0:
                    //  cash
                    if (buttonClicked == 1) {
                        // lost ticket
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " [" + Constants.LOST_TICKET + "] (" + getString(R.string.rm) + lostTicketRate + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                        } else {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " " + Constants.LOST_TICKET);
                        }
                    } else if (buttonClicked == 2) {
                        //  max charge
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " [" + Constants.MAXIMUM_CHARGE + "] (" + getString(R.string.rm) + maxChargeRate + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                        } else {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " " + Constants.MAXIMUM_CHARGE);
                        }
                    } else {
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                        } else {
                            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getAmount() + ")");
                        }
                    }
                    break;

                case 1:
                    //  charge to room
//                paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_ROOM);
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_ROOM + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_ROOM + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;

                case 2:
                    //  charge to master
//                paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_MASTER);
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_MASTER + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentReceiptModel.setPayment_method(Constants.CHARGE_TO_MASTER + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;

                case 3:
                    //  credit card payment
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentReceiptModel.setPayment_method(Constants.DEBIT_CARD + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentReceiptModel.setPayment_method(Constants.DEBIT_CARD + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;

                case 4:
                    //  debit card payment
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentReceiptModel.setPayment_method(Constants.CREDIT_CARD + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentReceiptModel.setPayment_method(Constants.CREDIT_CARD + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;
            }
            if (isPrintLogo) {
//        paymentReceiptModel.setBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_kiplepark_text_logo_black), 140, 30, true));
                paymentReceiptModel.setBitmap(Utils.getBitmapFromVectorDrawable(this, R.drawable.logo_four_seasons));
            } else {
                paymentReceiptModel.setBitmap(null);
            }
            paymentReceiptModel.setReceipt_id(receipt);
            paymentReceiptModel.setEntry_time(Utils.unixFormatter(entryTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(this).getActivationInfo().getTimezone()));
            paymentReceiptModel.setPayment_time(Utils.unixFormatter(payTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(this).getActivationInfo().getTimezone()));

            if (isPerEntry)
                paymentReceiptModel.setDuration(getString(R.string.per_entry));
            else
                paymentReceiptModel.setDuration(Utils.getElapsedTimeFromUnix(entryTime, payTime));

            if (isPrintCompanyName)
                paymentReceiptModel.setParking_operator(SessionManager.getInstance(this).getActivationInfo().getPartner_name());
            else
                paymentReceiptModel.setParking_operator("");

            if (showTaxInfo) {
                paymentReceiptModel.setGst_id(SessionManager.getInstance(this).getActivationInfo().getGst_id());
                paymentReceiptModel.setShow_tax_info(showTaxInfo);
            }

            if (buttonClicked == 1) {
                //  lost ticket
                paymentReceiptModel.setSubtotal(getString(R.string.rm) + Utils.formatBalance(lostTicketAmount + ""));
            } else if (buttonClicked == 2) {
                //  maximum charge
                paymentReceiptModel.setSubtotal(getString(R.string.rm) + Utils.formatBalance(maxChargeAmount + ""));
            } else {
//            paymentReceiptModel.setSubtotal(getString(R.string.rm) + (null == info.getFee() ? Utils.formatBalance(info.getAmount()) : Utils.formatBalance(info.getFee())));
                paymentReceiptModel.setSubtotal(getString(R.string.rm) + Utils.formatBalance(info.getFee()));
            }

            //paymentReceiptModel.setSubtotal(getString(R.string.rm) + (null == info.getFee() ? Utils.formatBalance(info.getAmount()) : Utils.formatBalance(info.getFee())));
            paymentReceiptModel.setRoundingAdj(getString(R.string.rm) + "0.00");
            paymentReceiptModel.setDiscount(null);
            if (buttonClicked == 1) {
                paymentReceiptModel.setPaid(getString(R.string.rm) + Utils.formatBalance(lostTicketAmount + ""));
            } else if (buttonClicked == 2) {
                paymentReceiptModel.setPaid(getString(R.string.rm) + Utils.formatBalance(maxChargeAmount + ""));
            } else {
                paymentReceiptModel.setPaid(getString(R.string.rm) + Utils.formatBalance(paid_amount.getText().toString()));
            }
            paymentReceiptModel.setChange(balance_amount.getText().toString().contains(getString(R.string.rm)) ? balance_amount.getText().toString() : (getString(R.string.rm) + balance_amount.getText().toString()));

            buttonClicked = 0;
            new PrintReceipts(mSunmiPrinter, this, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("");

        } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {

            View view = LayoutInflater.from(this).inflate(
                    R.layout.payment_receipt, null);
            LinearLayout v = view.findViewById(R.id.receipt);

            ImageView ivLogo = v.findViewById(R.id.logo);
            if (isPrintLogo) {
                ivLogo.setImageBitmap(Utils.getBitmapFromVectorDrawable(this, R.drawable.logo_four_seasons));
                ivLogo.setVisibility(View.VISIBLE);
            } else {
                ivLogo.setImageBitmap(null);
                ivLogo.setVisibility(View.GONE);
            }

            ActivationResponse activationInfo = SessionManager.getInstance(this).getActivationInfo();

            TextView tvSiteName = v.findViewById(R.id.site_name1);
            tvSiteName.setText(activationInfo.getSite_name());

            StringBuilder address = new StringBuilder();
            if(activationInfo.getStreet1() != null || !TextUtils.isEmpty(activationInfo.getStreet1())) {
                address.append(activationInfo.getStreet1());
            }

            if (activationInfo.getStreet2() != null && !TextUtils.isEmpty(activationInfo.getStreet2())) {
                address.append(activationInfo.getStreet2());
            }

            if (activationInfo.getPostcode() != null) {
                address.append(activationInfo.getPostcode()).append(" ");
            }

            if (activationInfo.getCity() != null && !TextUtils.isEmpty(activationInfo.getCity())) {
                address.append(activationInfo.getCity()).append(", ");
            }

            if (activationInfo.getState() != null && !TextUtils.isEmpty(activationInfo.getState())) {
                address.append(activationInfo.getState());
            }

            TextView tvSiteAddress = v.findViewById(R.id.site_address1);
            tvSiteAddress.setText(address);

            TextView tvReceiptNumber = v.findViewById(R.id.receipt_number);
            tvReceiptNumber.setText(": " + receipt);

            TextView tvTicketId = v.findViewById(R.id.ticket_id);
            tvTicketId.setText(": " + info.getTicketId());

            TextView tvNumberPlate = v.findViewById(R.id.car_plate_number);
            if (!isValidatedPayment) {
                tvNumberPlate.setText(": " + Utils.getNumberPlateFromTicketdata(info.getTicketData()));
            } else {
                tvNumberPlate.setText(": " + validatedCarPlate);
            }

            TextView tvEntryTime = v.findViewById(R.id.entry_time);
            tvEntryTime.setText(": " + Utils.unixFormatter(entryTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(this).getActivationInfo().getTimezone()));

            TextView tvPaymentTime = v.findViewById(R.id.payment_time);
            tvPaymentTime.setText(": " + Utils.unixFormatter(payTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(this).getActivationInfo().getTimezone()));

            TextView tvDuration = v.findViewById(R.id.duration);
            if (isPerEntry)
                tvDuration.setText(": " + getString(R.string.per_entry));
            else
                tvDuration.setText(": " + Utils.getElapsedTimeFromUnix(entryTime, payTime));

            TextView tvPaymentMethod = v.findViewById(R.id.payment_method);
            String paymentMethod = "";
            switch (chargeType) {
                case 0:
                    //  cash
                    if (buttonClicked == 1) {
                        // lost ticket
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " [" + Constants.LOST_TICKET + "] (" + getString(R.string.rm) + lostTicketRate + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")";
                        } else {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " " + Constants.LOST_TICKET;
                        }
                    } else if (buttonClicked == 2) {
                        //  max charge
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " [" + Constants.MAXIMUM_CHARGE + "] (" + getString(R.string.rm) + maxChargeRate + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")";
                        } else {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " " + Constants.MAXIMUM_CHARGE;
                        }
                    } else {
                        Log.e("upfront payment amount", info.getUpfront_payment() == null ? "upfront null" : info.getUpfront_payment());
                        if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")";
                        } else {
                            paymentMethod = ": " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getAmount() + ")";
                        }
                    }
                    break;

                case 1:
                    //  charge to room
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentMethod = ": " + Constants.CHARGE_TO_ROOM + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")";
                    } else {
                        paymentMethod = ": " + Constants.CHARGE_TO_ROOM + " (" + getString(R.string.rm) + info.getAmount() + ")";
                    }
                    break;

                case 2:
                    //  charge to master
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentMethod = ": " + Constants.CHARGE_TO_MASTER + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")";
                    } else {
                        paymentMethod = ": " + Constants.CHARGE_TO_MASTER + " (" + getString(R.string.rm) + info.getAmount() + ")";
                    }
                    break;

                case 3:
                    //  credit card payment
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentMethod = ": " + (Constants.DEBIT_CARD + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentMethod = ": " + (Constants.DEBIT_CARD + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;

                case 4:
                    //  debit card payment
                    if (null != info.getUpfront_payment() && !info.getUpfront_payment().isEmpty() && !info.getUpfront_payment().equalsIgnoreCase("0")) {
                        paymentMethod = ": " + (Constants.CREDIT_CARD + " (" + getString(R.string.rm) + info.getFee() + ") + " + Constants.CASH_PAYMENT + " (" + getString(R.string.rm) + info.getUpfront_payment() + ")");
                    } else {
                        paymentMethod = ": " + (Constants.CREDIT_CARD + " (" + getString(R.string.rm) + info.getAmount() + ")");
                    }
                    break;
            }
            tvPaymentMethod.setText(paymentMethod);

            TextView tvParkingFee = v.findViewById(R.id.parking_fee);
            String parkingFee;
            if (buttonClicked == 1) {
                parkingFee = ": " + getString(R.string.rm) + Utils.formatBalance(lostTicketAmount + "");
            } else if (buttonClicked == 2) {
                parkingFee = ": " + getString(R.string.rm) + Utils.formatBalance(maxChargeAmount + "");
            } else {
                parkingFee = ": " + getString(R.string.rm) + (null == info.getFee() ? Utils.formatBalance(info.getAmount()) : Utils.formatBalance(info.getFee()));
            }
            tvParkingFee.setText(parkingFee);

            TextView tvTotal = v.findViewById(R.id.total);
            String total;
            if (buttonClicked == 1) {
                //  lost ticket
                total = ": " + getString(R.string.rm) + Utils.formatBalance(lostTicketAmount + "");
            } else if (buttonClicked == 2) {
                //  maximum charge
                total = ": " + getString(R.string.rm) + Utils.formatBalance(maxChargeAmount + "");
            } else {
                total = ": " + getString(R.string.rm) + Utils.formatBalance(info.getFee());
            }
            tvTotal.setText(total);

            TextView tvTaxInfo = v.findViewById(R.id.tax_info);
            String additionalInfo;
            if (isPrintCompanyName)
                additionalInfo = SessionManager.getInstance(this).getActivationInfo().getPartner_name();
            else {
                additionalInfo = "";
                tvTaxInfo.setVisibility(View.GONE);
            }

            if (showTaxInfo) {
                additionalInfo = getString(R.string.tax_info);
            }
            tvTaxInfo.setText(additionalInfo);

            new Thread(() -> {
                if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                    PAXPrinter.getInstance().printView(v);
                    PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                    PAXPrinter.getInstance().start();
                }
            }).start();

        } else {
            Toast.makeText(this, getString(R.string.no_printer), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case CASH_PAYMENT:
                isPaymentInProgress = false;
                CashPaymentResponse data = (CashPaymentResponse) response.body();
//                //  print receipt
                if (isPrintReceipt && null != data)
                    printReceipt(data.getReceipt(), data.getEntrytime(), data.getPaytime());

                //  firebase event tracking
                Bundle bundle = new Bundle();
                bundle.putString("Status", "payment_success");
                switch (chargeType) {
                    case 0:
                        //  cash
                        bundle.putString("PaymentType", Constants.CASH_PAYMENT);
                        break;

                    case 1:
                        //  charge to room
                        bundle.putString("PaymentType", Constants.CHARGE_TO_ROOM);
                        break;

                    case 2:
                        //  charge to master
                        bundle.putString("PaymentType", Constants.CHARGE_TO_MASTER);
                        break;
                }
                bundle.putDouble("Value", Double.parseDouble(info.getAmount()));
                CustomApplication.reportFirebaseCustomEvent("Payment", bundle);

                ActivationResponse.Service_customize services = SessionManager.getInstance(this).getActivationInfo().getService_customize();
                if (services.getCollect().getActive() && isCollectEnabled) {
                    //  do usual flow
                    Intent intent = new Intent();
                    intent.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_USUAL_FLOW);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else if (services.getComplete().getActive()) {
                    //  set flag to skip collection
                    Intent intent = new Intent();
                    intent.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_SKIP_COLLECT);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
//                    //  change status of ticket to pending collection
//                    HashMap<String, Object> params = new HashMap<>();
//                    params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
//                    params.put("user_email", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
//                    params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());
//                    params.put("status", "4");
//                    params.put("datetime", Utils.getCurrentTimeUnixString());
//                    params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
//                    params.put("ticket_id", info.getTicketId());
//                    APICall.readyForCollection(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
                } else {
                    //  set flag to skip both collection & complete flow
                    Intent skipCompleteIntent = new Intent();
                    skipCompleteIntent.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_SKIP_COMPLETE);
                    setResult(Activity.RESULT_OK, skipCompleteIntent);
                    finish();
                }
                break;

            case GET_PACKAGE_LIST:
                packageListResponse = (PackageListResponse) response.body();

                if (null != packageListResponse && packageListResponse.getData().size() > 0) {
                    //  create package spinner list
                    package_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedPackageIndex = position;
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    //  create string array
                    packageDetails = new String[packageListResponse.getData().size() + 2];
                    //  inject dummy info
                    //  select package
                    PackageListResponse.Datum selectDummy = new PackageListResponse.Datum();
                    selectDummy.setName(getString(R.string.select_package));
                    selectDummy.setAmount("");
                    selectDummy.setDefault(0);
                    selectDummy.setId(-1);
                    selectDummy.setSite_id("");
                    packageListResponse.getData().add(0, selectDummy);

                    //  cancel package
                    PackageListResponse.Datum cancelDummy = new PackageListResponse.Datum();
                    cancelDummy.setName(getString(R.string.cancel_package));
                    cancelDummy.setAmount("");
                    cancelDummy.setDefault(0);
                    cancelDummy.setId(0);
                    cancelDummy.setSite_id("");
                    packageListResponse.getData().add(packageListResponse.getData().size(), cancelDummy);

                    for (int i = 0; i < packageListResponse.getData().size(); i++) {
                        packageDetails[i] = packageListResponse.getData().get(i).getName() + "," + packageListResponse.getData().get(i).getAmount();
                        Log.e("TRACK", packageDetails[i]);
                    }

                    //  create adapter
                    adapter = new PackageSpinnerAdapter(this, R.layout.item_package, packageDetails);
                    //  set adapter
                    package_spinner.setAdapter(adapter);
                    //  confirm button listener
                    package_confirm_btn.setOnClickListener(this);

                    //  set boolean to enable package list
                    hasPackage = true;

                    //  context menu for package image
                    this.registerForContextMenu(package_img);
                } else {
                    //  no packages available
                    //  set boolean to disable packages
                    hasPackage = false;
                }
                break;

            case APPLY_PACKAGE:
                isPackageApplying = false;
                applyPackageResponse = (ApplyPackageResponse) response.body();

                refreshPage(true);
                break;

            case READY_FOR_COLLECTION:
                if (!SessionManager.getInstance(this).getActivationInfo().getService_customize().getComplete().getActive()) {
                    //  change status of ticket to complete
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
                    params.put("user_email", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
                    params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());
                    params.put("status", "5");
                    params.put("datetime", Utils.getCurrentTimeUnixString());
                    params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
                    params.put("ticket_id", info.getTicketId());
                    APICall.processCompleted(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
                } else {
                    //  set flag to skip collection
                    Intent skipCollectionIntent = new Intent();
                    skipCollectionIntent.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_SKIP_COLLECT);
                    setResult(Activity.RESULT_OK, skipCollectionIntent);
                    finish();
                }
                break;

            case PROCESS_COMPLETED:
                //  set flag to skip both collection & complete flow
                Intent skipCompleteIntent = new Intent();
                skipCompleteIntent.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_SKIP_COMPLETE);
                setResult(Activity.RESULT_OK, skipCompleteIntent);
                finish();
                break;

            case MAX_CHARGE:
                isPaymentInProgress = false;
                CashPaymentResponse dataMax = (CashPaymentResponse) response.body();
//                //  print receipt
                if (isPrintReceipt && null != dataMax)
                    printReceipt(dataMax.getReceipt(), dataMax.getEntrytime(), dataMax.getPaytime());

                //  firebase event tracking
                Bundle bundleMax = new Bundle();
                bundleMax.putString("Status", "payment_success");
                switch (chargeType) {
                    case 0:
                        //  cash
                        bundleMax.putString("PaymentType", Constants.CASH_PAYMENT);
                        break;

                    case 1:
                        //  charge to room
                        bundleMax.putString("PaymentType", Constants.CHARGE_TO_ROOM);
                        break;

                    case 2:
                        //  charge to master
                        bundleMax.putString("PaymentType", Constants.CHARGE_TO_MASTER);
                        break;
                }
                bundleMax.putDouble("Value", Double.parseDouble(info.getAmount()));
                CustomApplication.reportFirebaseCustomEvent("Payment", bundleMax);

                // skip and directly goto home page, set flag to skip both collection & complete flow
                Intent skipCompleteIntentMax = new Intent();
                skipCompleteIntentMax.putExtra(Constants.PAYMENT_SKIP_FLAG, Constants.PAYMENT_SKIP_COMPLETE);
                setResult(Activity.RESULT_OK, skipCompleteIntentMax);
                finish();
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case CASH_PAYMENT:
                if (!isCardPayment) {
                    isPaymentInProgress = false;
                    CashPaymentResponse cashPaymentResponse;
                    Gson gson = new Gson();
                    TypeAdapter<CashPaymentResponse> adapter = gson.getAdapter(CashPaymentResponse.class);
                    if (response.errorBody() != null) {
                        try {
                            cashPaymentResponse = adapter.fromJson(response.errorBody().string());
                            if (null != cashPaymentResponse.getKpCode() && "kp005".equalsIgnoreCase(cashPaymentResponse.getKpCode())) {
                                CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                                CustomAlerts.showOKAlert(this, (dialog, which) -> {
                                    if (which == RESULT_OK) {
                                        newAmount = finalCashPaymentResponse.getNew_amount();
                                        refreshPage(false);
                                        dialog.dismiss();
                                    }
                                }, getString(R.string.alert), getString(R.string.amount_updated));
                            } else if (null != cashPaymentResponse.getKpCode() && "kp_314".equalsIgnoreCase(cashPaymentResponse.getKpCode())) {
                                //  ticket payment failure
                                CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                                CustomAlerts.showOKAlert(this, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                            } else {
                                CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                                CustomAlerts.showOKAlert(this, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    if (cardPaymentScheme != null && !cardPaymentScheme.equalsIgnoreCase("Transaction Cancelled")) {
                        //  get credit card payment success response
                        //  convert credit card payment success response into array
                        //  store update failed transactions into local Room database

                        //  insert card payment response into ticket object
                        info.setCard_payment_response(cardPaymentResponse.toString());

                        Log.e("toString", info.getCard_payment_response());
                        Log.e("failed update ticket", "receipt number: " + info.getReceipt());

                        LocalCacheManager.getInstance(this).addTicket(new DBInterface() {
                            @Override
                            public void onDBTicketLoaded(List<TicketListsResponse> tickets) {

                            }

                            @Override
                            public void onDBTicketUpdated() {
                                Log.e("Insert transaction", "fail update ticket Inserted: " +  (isValidatedPayment ? validatedCarPlate : Utils.getNumberPlateFromTicketdata(info.getTicketData())));
                            }

                            @Override
                            public void onDBTaskFailed() {
                                Log.e("Insert transaction", "onDBTaskFailed: db insert task failed");
                            }
                        }, info);

                        isCardPayment = false;
                    }

                }

                //  firebase event tracking
                Bundle bundle = new Bundle();
                bundle.putString("Status", "payment_failed");
                switch (chargeType) {
                    case 0:
                        //  cash
                        bundle.putString("PaymentType", Constants.CASH_PAYMENT);
                        break;

                    case 1:
                        //  charge to room
                        bundle.putString("PaymentType", Constants.CHARGE_TO_ROOM);
                        break;

                    case 2:
                        //  charge to master
                        bundle.putString("PaymentType", Constants.CHARGE_TO_MASTER);
                        break;
                }
                CustomApplication.reportFirebaseCustomEvent("Payment", bundle);
                break;

            case GET_PACKAGE_LIST:
                break;

            case APPLY_PACKAGE:
                isPackageApplying = false;
                break;

            case READY_FOR_COLLECTION:
                break;

            case PROCESS_COMPLETED:
                break;

            case MAX_CHARGE:
                isPaymentInProgress = false;
                CashPaymentResponse maxChargePaymentResponse;
                Gson gsonMax = new Gson();
                TypeAdapter<CashPaymentResponse> adapterMax = gsonMax.getAdapter(CashPaymentResponse.class);
                if (response.errorBody() != null) {
                    try {
                        maxChargePaymentResponse = adapterMax.fromJson(response.errorBody().string());
                        if (null != maxChargePaymentResponse.getKpCode() && "kp005".equalsIgnoreCase(maxChargePaymentResponse.getKpCode())) {
                            CashPaymentResponse finalCashPaymentResponse = maxChargePaymentResponse;
                            CustomAlerts.showOKAlert(this, (dialog, which) -> {
                                if (which == RESULT_OK) {
                                    newAmount = finalCashPaymentResponse.getNew_amount();
                                    refreshPage(false);
                                    dialog.dismiss();
                                }
                            }, getString(R.string.alert), getString(R.string.amount_updated));
                        } else if (null != maxChargePaymentResponse.getKpCode() && "kp_314".equalsIgnoreCase(maxChargePaymentResponse.getKpCode())) {
                            //  ticket payment failure
                            CashPaymentResponse finalCashPaymentResponse = maxChargePaymentResponse;
                            CustomAlerts.showOKAlert(this, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                        } else {
                            CashPaymentResponse finalCashPaymentResponse = maxChargePaymentResponse;
                            CustomAlerts.showOKAlert(this, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //  firebase event tracking
                Bundle bundleMax = new Bundle();
                bundleMax.putString("Status", "payment_failed");
                switch (chargeType) {
                    case 0:
                        //  cash
                        bundleMax.putString("PaymentType", Constants.CASH_PAYMENT);
                        break;

                    case 1:
                        //  charge to room
                        bundleMax.putString("PaymentType", Constants.CHARGE_TO_ROOM);
                        break;

                    case 2:
                        //  charge to master
                        bundleMax.putString("PaymentType", Constants.CHARGE_TO_MASTER);
                        break;
                }
                CustomApplication.reportFirebaseCustomEvent("Payment", bundleMax);
                break;
        }
    }

    private HashMap<String, Object> getParamsLostTicket() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("ticketId", info.getTicketId());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("pos_payer", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("service", SessionManager.getInstance(this).getActivationInfo().getIdentify());
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("staff_payer", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
            params.put("paymentAmount", lostTicketAmount);
            Log.e("max charge price set at", String.valueOf(lostTicketAmount));
            long currentUnix = Utils.getCurrentTimeUnix();
            params.put("reqTime", String.valueOf(currentUnix));
            params.put("datePayUnix", String.valueOf(currentUnix));
            params.put("payment_pos_method", 1);
            params.put("receipt", SessionManager.getInstance(this).getActivationInfo().getSite_id() + lostTicketAmount);
            params.put("special_scenario", Constants.LOST_TICKET);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private HashMap<String, Object> getParamsMaxCharge() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("ticketId", info.getTicketId());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("pos_payer", SessionManager.getInstance(this).getActivationInfo().getPos_id());
            params.put("service", SessionManager.getInstance(this).getActivationInfo().getIdentify());
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("staff_payer", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
            params.put("paymentAmount", maxChargeAmount);
            Log.e("max charge price", String.valueOf(maxChargeAmount));
            long currentUnix = Utils.getCurrentTimeUnix();
            params.put("reqTime", String.valueOf(currentUnix));
            params.put("datePayUnix", String.valueOf(currentUnix));
            params.put("payment_pos_method", 1);
            params.put("receipt", SessionManager.getInstance(this).getActivationInfo().getSite_id() + maxChargeAmount);
            params.put("special_scenario", Constants.MAXIMUM_CHARGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void cardOptionDialog(String paymentTarget) {
        CustomAlerts.showResponseAlert(this, (dialog, which) -> {
            if (which == BUTTON_POSITIVE) {
                //  card payment
                switch (paymentTarget) {
                    case "lostTicket":
                        dialog.dismiss();
                        //  credit card payment

                        Bundle lost_ticket_bundle = new Bundle();
//                        String pay_amount = "1000";
                        String lost_ticket_pay_function = "01";
                        String lost_ticket_pay_type = "01";
                        String lost_ticket_pay_camera_mode = "01";
                        String lost_ticket_pay_print_receipt_id = "Y";
                        lost_ticket_bundle.putString("pay_amount", String.valueOf((int) getParamsLostTicket().get("paymentAmount") * 100));
                        lost_ticket_bundle.putString("pay_function", lost_ticket_pay_function);
                        lost_ticket_bundle.putString("pay_type", lost_ticket_pay_type);
                        lost_ticket_bundle.putString("pay_camera_mode", lost_ticket_pay_camera_mode);
                        lost_ticket_bundle.putString("pay_print_receipt_id", lost_ticket_pay_print_receipt_id);

                        Intent lost_ticket_intent = new Intent("com.revenue.edc.hlb.pro.app2app");
                        lost_ticket_intent.putExtras(lost_ticket_bundle);

                        startActivity(lost_ticket_intent);
                        break;

                    case "maxCharge":
                        dialog.dismiss();
                        //  credit card payment

                        Log.e("max charge price", String.valueOf(getParamsMaxCharge().get("paymentAmount")));
                        Bundle max_charge_bundle = new Bundle();
//                        String pay_amount = "1000";
                        String max_charge_pay_function = "01";
                        String max_charge_pay_type = "01";
                        String max_charge_pay_camera_mode = "01";
                        String max_charge_pay_print_receipt_id = "Y";
                        max_charge_bundle.putString("pay_amount", String.valueOf((int) getParamsMaxCharge().get("paymentAmount") * 100));
                        max_charge_bundle.putString("pay_function", max_charge_pay_function);
                        max_charge_bundle.putString("pay_type", max_charge_pay_type);
                        max_charge_bundle.putString("pay_camera_mode", max_charge_pay_camera_mode);
                        max_charge_bundle.putString("pay_print_receipt_id", max_charge_pay_print_receipt_id);

                        Intent max_charge_intent = new Intent("com.revenue.edc.hlb.pro.app2app");
                        max_charge_intent.putExtras(max_charge_bundle);

                        startActivity(max_charge_intent);
                        break;

                    default:
                }
            }

            if (which == BUTTON_NEGATIVE) {
                //  non-card payment
                switch (paymentTarget) {
                    case "lostTicket":
                        dialog.dismiss();
                        new NetworkConnectionCheck(hasInternet -> {
                            if (hasInternet) {
                                chargeType = 0;
                                buttonClicked = 1;
                                //  do payment
                                APICall.cashPayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParamsLostTicket());
                                dialog.dismiss();
                            }
                        });
                        break;

                    case "maxCharge":
                        dialog.dismiss();
                        new NetworkConnectionCheck(hasInternet -> {
                            if (hasInternet) {
                                chargeType = 0;
                                buttonClicked = 2;
                                //  do payment
                                APICall.maxChargePayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParamsMaxCharge());
                                dialog.dismiss();
                            }
                        });
                        break;

                    default:
                }
            }
        }, "", "Select Payment Method", "Card", "Cash/e-wallet");
    }

    private void lostTicketAPICall() {
        CustomAlerts.showResponseAlert(this, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    new NetworkConnectionCheck(hasInternet -> {
                        if (hasInternet) {
                            chargeType = 0;
                            buttonClicked = 1;
                            //  do payment
                            cardOptionDialog("lostTicket");
//                            APICall.cashPayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParamsLostTicket());
                            dialog.dismiss();
                        }
                    });
                    break;

                case BUTTON_NEGATIVE:
                    isPaymentInProgress = false;
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.lost_ticket_str, lostTicketAmount));

    }

    private void maxChargeAPICall() {
        CustomAlerts.showResponseAlert(this, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    new NetworkConnectionCheck(hasInternet -> {
                        if (hasInternet) {
                            chargeType = 0;
                            buttonClicked = 2;
                            //  do payment
                            cardOptionDialog("maxCharge");
//                            APICall.maxChargePayment(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), getParamsMaxCharge());
                            dialog.dismiss();
                        }
                    });
                    break;

                case BUTTON_NEGATIVE:
                    isPaymentInProgress = false;
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.max_charge_str, maxChargeAmount));

    }
}
