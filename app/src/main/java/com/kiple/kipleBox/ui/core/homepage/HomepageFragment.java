package com.kiple.kipleBox.ui.core.homepage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.BaseFragment;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.RoundedImageView;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.CashPaymentResponse;
import com.kiple.kipleBox.model.responses.EncryptionResponse;
import com.kiple.kipleBox.model.responses.FailedEntryResponse;
import com.kiple.kipleBox.model.responses.GetValidatedTicketResponse;
import com.kiple.kipleBox.model.responses.LoginResponse;
import com.kiple.kipleBox.model.responses.MiniDashboardResponse;
import com.kiple.kipleBox.model.responses.SimpleStatusResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.ui.core.cash_payment.ActivityCashPayment;
import com.kiple.kipleBox.ui.core.display_ticket.DisplayTicketFragment;
import com.kiple.kipleBox.ui.core.lists.TicketListsFragment;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.LogUtils;
import com.kiple.kipleBox.utils.NetworkConnectionCheck;
import com.kiple.kipleBox.utils.PanViewAdjuster;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class HomepageFragment extends BaseFragment implements CallBackInterface {

    @BindView(R.id.issue_ticket_btn)
    Button issue_ticket_btn;
    @BindView(R.id.validate_ticket_btn)
    Button validate_ticket_btn;
    @BindView(R.id.number_plate_parent)
    TextInputLayout number_plate_parent;
    @BindView(R.id.number_plate)
    TextInputEditText number_plate;
    @BindView(R.id.mobile_number_parent)
    TextInputLayout mobile_number_parent;
    @BindView(R.id.mobile_number)
    TextInputEditText mobile_number;
    @BindView(R.id.car_key_parent)
    LinearLayout car_key_parent;
    @BindView(R.id.car_key_switch)
    Switch car_key_switch;
    @BindView(R.id.ticket_issued_tab)
    LinearLayout ticket_issued_tab;
    @BindView(R.id.parked_tab)
    LinearLayout parked_tab;
    @BindView(R.id.paid_tab)
    LinearLayout paid_tab;
    @BindView(R.id.pending_collection_tab)
    LinearLayout pending_collection_tab;
    @BindView(R.id.issue_ticket_amount)
    TextView issue_ticket_amount;
    @BindView(R.id.parked_amount)
    TextView parked_amount;
    @BindView(R.id.paid_amount)
    TextView paid_amount;
    @BindView(R.id.pending_collection_amount)
    TextView pending_collection_amount;
    @BindView(R.id.upfront_amount_parent)
    LinearLayout upfront_amount_parent;
    @BindView(R.id.upfront_button1)
    Button upfront_button1;
    @BindView(R.id.upfront_button2)
    Button upfront_button2;
    @BindView(R.id.upfront_button3)
    Button upfront_button3;
    @BindView(R.id.placeholder_message)
    TextView placeholder_message;
    @BindView(R.id.home_parent)
    LinearLayout home_parent;
    @BindView(R.id.home_content)
    LinearLayout home_content;
    @BindView(R.id.dashboard_parent)
    LinearLayout dashboard_parent;
    @BindView(R.id.avatar)
    RoundedImageView logout_btn;
    @BindView(R.id.refresh_home)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.latest_entry_image)
    PhotoView latest_entry_image;

    int amount = 0;          // Amount for Lost Ticket
    private Context mContext;
    private StringBuffer ticketData = new StringBuffer();
    private String ticketId = "";
    private int running_id;
    private Bundle tickets_bundle = new Bundle();
    private TicketListsFragment f = new TicketListsFragment();
    private String upfront_amount = null;
    private List<ActivationResponse.Attribute> upfrontAmountList;
    private boolean upfrontFlag = false;
    private int printCount = 0;
//    private Printer printer;
    private String encodedTicketdata;
    private boolean isIssuingTicket = false;
    private boolean isSearchingTicket = false;
    private boolean newPrintTicket = false;
    private boolean isPrintReceipt = true;
    private boolean isPrintLogo = false;
    private boolean isPrintCompanyName = false;
    private Printer mSunmiPrinter;
    private String printerType;
    private SimpleStatusResponse simpleStatusResponse;
    private boolean maxButtonClicked = false;
    private boolean isKeyCollected = false;
    private int qrSize = 200;
    private boolean isCustomQrSize = false;
    private boolean isPrintTicket = true;
    private boolean isPrintDashboard = true;
    private boolean isPrintStaff = true;
    private boolean isPerEntry = false;
    private boolean isLPRValet = false;
    private Integer entry_log_id;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_homepage, null);

        ButterKnife.bind(this, v);

        LoginResponse activeUserInfo = SessionManager.getInstance(mContext).getActiveUserInfo();
        APICall.miniDashboard(true, this, mContext, Constants.AUTHORIZATION + activeUserInfo.getData().getToken(), getDashboardParams());

        //  upfront payment value
//        APICall.upfrontAmount(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), SessionManager.getInstance(mContext).getActivationInfo().getSite_id());

        listener.showBottomNavigation();

        issue_ticket_btn.setOnClickListener(this);
        validate_ticket_btn.setOnClickListener(this);
        ticket_issued_tab.setOnClickListener(this);
        paid_tab.setOnClickListener(this);
        parked_tab.setOnClickListener(this);
        pending_collection_tab.setOnClickListener(this);
        logout_btn.setOnClickListener(this);
        upfront_button1.setOnClickListener(this);
        upfront_button2.setOnClickListener(this);
        upfront_button3.setOnClickListener(this);

        refreshLayout.setOnRefreshListener(() -> {
            if(isLPRValet){
                //  refresh latest entry API
                APICall.latestEntry( this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
                //  refresh dashboard
                APICall.miniDashboard(true, this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getDashboardParams());
            } else {
                APICall.miniDashboard(true, this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getDashboardParams());
            }
        });

//        this.mPrinter = new Printer(mContext);
//        mPrinter.printerInit();

        //  printer hardware detection
        printerDetection();

        init();

        //  extra pan for phone number edit text to be visible
        PanViewAdjuster.AdjustView(home_parent, home_content, mobile_number, 1);

        return v;
    }

    private void printerDetection() {
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(mContext);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

//                enablePrintButton()
                    printerType = Constants.SUNMI_PRINTER;
                } else {
//                disablePrintButton()
                    printerType = Constants.NO_PRINTER;
                }

                if (mSunmiPrinter.isPrinterReady()) {
//                enablePrintButton()
                    printerType = Constants.SUNMI_PRINTER;
                } else {
//                disablePrintButton()
                    printerType = Constants.NO_PRINTER;
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
//                enablePrintButton()
                printerType = Constants.PAX_PRINTER;
            } else {
//                disablePrintButton()
                printerType = Constants.NO_PRINTER;
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(mContext, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
        }
    }

    private void init() {
        //  ticket validation setting
        ActivationResponse activationResponse = SessionManager.getInstance(mContext).getActivationInfo();
        Log.e("activation data", activationResponse.toString());
        if (activationResponse.getParkingValidationFlag() == 1) {
            //  show parking validation feature
            validate_ticket_btn.setVisibility(View.VISIBLE);
        } else {
            //  no parking validation feature
            validate_ticket_btn.setVisibility(View.GONE);
        }
        // Getting the print receipt value
        List<ActivationResponse.Service_feature> paymentService = activationResponse.getService_customize().getPay().getService_features();
        //  print receipt flag
        for (int i = 0; i < paymentService.size(); i++) {
            switch (paymentService.get(i).getName()) {

                case Constants.PRINT_RECEIPT_FEATURE:
                    isPrintReceipt = null != paymentService.get(i).getFlag_value() && paymentService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES);
                    if (null != paymentService.get(i).getAttributes() && paymentService.get(i).getAttributes().size() > 0) {
                        for (int j = 0; j < paymentService.get(i).getAttributes().size(); j++)
                            switch (paymentService.get(i).getAttributes().get(j).getName()) {
                                case Constants.PRINT_LOGO:
                                    isPrintLogo = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;

                                case Constants.COMPANY_NAME:
                                    isPrintCompanyName = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;

                                case Constants.PER_ENTRY:
                                    isPerEntry = null != paymentService.get(i).getAttributes().get(j).getDefault() && paymentService.get(i).getAttributes().get(j).getDefault().equalsIgnoreCase(Constants.YES);
                                    break;
                            }
                    }
                    break;
            }
        }

        List<ActivationResponse.Service_feature> issueService = SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getIssue().getService_features();

        //  dashboard features
        ActivationResponse.Service_customize services = SessionManager.getInstance(mContext).getActivationInfo().getService_customize();

        if (null != services.getPark().getActive() && !services.getPark().getActive()) {
            //  disable dashboard ticket issued tab
            ticket_issued_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            ticket_issued_tab.setOnClickListener(null);
        }

        if (null != services.getPay().getActive() && !services.getPay().getActive()) {
            //  disable dashboard ticket parked tab
            parked_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            parked_tab.setOnClickListener(null);
        }

        if (null != services.getCollect().getActive() && !services.getCollect().getActive()) {
            //  disable dashboard ticket paid tab
            paid_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            paid_tab.setOnClickListener(null);
        } else {
            List<ActivationResponse.Service_feature> collectService = services.getCollect().getService_features();
            for (int i = 0; i < collectService.size(); i++) {
                if (Constants.ENABLE_FEATURE.equals(collectService.get(i).getName())) {
                    if (null != collectService.get(i).getFlag_value() && collectService.get(i).getFlag_value().equalsIgnoreCase(Constants.NO)) {
                        paid_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
                        paid_tab.setOnClickListener(null);
                    }
                }
            }
        }

        if (null != services.getComplete().getActive() && !services.getComplete().getActive()) {
            //  disable dashboard ticket pending collection tab
            pending_collection_tab.setBackground(getResources().getDrawable(R.drawable.dashboard_disabled));
            pending_collection_tab.setOnClickListener(null);
        }

        //  issue ticket status
        if (null != services.getIssue().getActive() && services.getIssue().getActive()) {
            //  issue ticket enabled for this site
            //  issue ticket features
            for (int i = 0; i < issueService.size(); i++) {
                switch (issueService.get(i).getName()) {

                    case Constants.KEY_COLLECTION_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            isKeyCollected = true;
                            car_key_parent.setVisibility(View.VISIBLE);
                            //  set default value
                            if (Constants.YES.equalsIgnoreCase(issueService.get(i).getDefault_values()))
                                car_key_switch.setChecked(true);
                            else
                                car_key_switch.setChecked(false);
                        } else {
                            isKeyCollected = false;
                            car_key_parent.setVisibility(View.GONE);
                        }
                        break;

                    case Constants.TICKET_DISPLAY_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            SessionManager.getInstance(mContext).setShowTicket(true);
                        } else {
                            SessionManager.getInstance(mContext).setShowTicket(false);
//                            this.printer = new Printer(mContext);
//                            printer.printerInit();
                        }
                        break;

                    case Constants.GRACE_PERIOD_FEATURE:
                        //  update grace period if value provided by backend
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            if (issueService.get(i).getAttributes().size() > 0) {
                                activationResponse.setGrace_period(Integer.valueOf(issueService.get(i).getAttributes().get(0).getValues()));
                                SessionManager.getInstance(mContext).saveActivationInfo(activationResponse);
                            }
                        }
                        break;

                    case Constants.MOBILE_NUMBER_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            mobile_number_parent.setVisibility(View.VISIBLE);
                            mobile_number.setText(issueService.get(i).getDefault_values());
//                        mobile_number.clearFocus();
                        } else {
                            mobile_number_parent.setVisibility(View.GONE);
                        }
                        break;

                    case Constants.RUNNING_NUMBER_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            //  update running number if value given is number
                            if (issueService.get(i).getDefault_values().matches("\\d+(?:\\.\\d+)?"))
                                SessionManager.getInstance(mContext).getActivationInfo().setRunning_no_start(Integer.valueOf(issueService.get(i).getDefault_values()));
                        }
                        break;

                    case Constants.CAR_PLATE_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            number_plate_parent.setVisibility(View.VISIBLE);
                            number_plate.setText(issueService.get(i).getDefault_values());
//                        number_plate_parent.requestFocus();
                        } else {
                            number_plate_parent.setVisibility(View.GONE);
                        }
                        break;

                    case Constants.UPFRONT_PAYMENT_FEATURE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            upfront_amount_parent.setVisibility(View.VISIBLE);
                            upfrontFlag = true;

                            upfrontAmountList = issueService.get(i).getAttributes();
                            switch (upfrontAmountList.size()) {
                                case 1:
                                    upfront_button1.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(0).getValues());
                                    upfront_button2.setVisibility(View.GONE);
                                    upfront_button3.setVisibility(View.GONE);
                                    upfront_amount_parent.setVisibility(View.VISIBLE);
                                    break;

                                case 2:
                                    upfront_button1.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(0).getValues());
                                    upfront_button2.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(1).getValues());
                                    upfront_button3.setVisibility(View.GONE);
                                    upfront_amount_parent.setVisibility(View.VISIBLE);
                                    break;

                                case 3:
                                    upfront_button1.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(0).getValues());
                                    upfront_button2.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(1).getValues());
                                    upfront_button3.setText(getString(R.string.rm) + issueService.get(i).getAttributes().get(2).getValues());
                                    upfront_amount_parent.setVisibility(View.VISIBLE);
                                    break;

                                default:
                                    upfront_amount_parent.setVisibility(View.GONE);
                            }
                        } else {
                            upfront_amount_parent.setVisibility(View.GONE);
                        }
                        break;

                    case Constants.TICKET_VERSION:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES) && issueService.get(i).getDefault_values().equalsIgnoreCase("1")) {
                            newPrintTicket = true;
                            List<ActivationResponse.Attribute> ticketVersionAttr = issueService.get(i).getAttributes();
                            for (int j = 0; j < ticketVersionAttr.size(); j++) {
                                if (Constants.PRINT_TICKET.equalsIgnoreCase(ticketVersionAttr.get(j).getName())) {
                                    isPrintTicket = Constants.YES.equalsIgnoreCase(ticketVersionAttr.get(j).getDefault());
                                }

                                if (Constants.PRINT_DASHBOARD_COPY.equalsIgnoreCase(ticketVersionAttr.get(j).getName())) {
                                    isPrintDashboard = Constants.YES.equalsIgnoreCase(ticketVersionAttr.get(j).getDefault());
                                }

                                if (Constants.PRINT_STAFF_COPY.equalsIgnoreCase(ticketVersionAttr.get(j).getName())) {
                                    isPrintStaff = Constants.YES.equalsIgnoreCase(ticketVersionAttr.get(j).getDefault());
                                }
                            }
                        }
                        break;

                    case Constants.CUSTOM_QR_SIZE:
                        if (null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES)) {
                            isCustomQrSize = true;
                            qrSize = Integer.parseInt(issueService.get(i).getDefault_values());
                        }
                        break;

                    case Constants.LPR_VALET:
                        isLPRValet = null != issueService.get(i).getFlag_value() && issueService.get(i).getFlag_value().equalsIgnoreCase(Constants.YES);
                        if (isLPRValet) {
                            //  get latest failed entry
                            APICall.latestEntry(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
                            latest_entry_image.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        } else {
            //  issue ticket disabled for this site
            //  issue ticket features
            for (int i = 0; i < issueService.size(); i++) {
                switch (issueService.get(i).getName()) {

                    case Constants.HYBRID_FEATURE:
                        if (Constants.NO.equalsIgnoreCase(issueService.get(i).getFlag_value())) {
                            SessionManager.getInstance(mContext).setHybridFlag(false);
                            dashboard_parent.setVisibility(View.VISIBLE);
//                            placeholder_message.setText(null);
//                            placeholder_message.setVisibility(View.GONE);
//                            return;
                        } else {
                            SessionManager.getInstance(mContext).setHybridFlag(true);
                            dashboard_parent.setVisibility(View.INVISIBLE);
                        }
                        break;

                    case Constants.PLACEHOLDER_MESSAGE:
                        number_plate_parent.setVisibility(View.GONE);
                        upfront_amount_parent.setVisibility(View.GONE);
                        mobile_number_parent.setVisibility(View.GONE);
                        car_key_parent.setVisibility(View.GONE);
                        issue_ticket_btn.setVisibility(View.GONE);
                        placeholder_message.setText(issueService.get(i).getDefault_values());
                        placeholder_message.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private HashMap<String, Object> getDashboardParams() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail()); //change this to signed in user
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    public void onDestroyView() {
        if (null != mSunmiPrinter)
            mSunmiPrinter.disconnectPrinter();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        //  get mini dashboard info upon resume
        APICall.miniDashboard(false, this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getDashboardParams());
    }

    private HashMap<String, Object> getValidatedTicketParams() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("plate_no", number_plate.getText().toString());
        params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());

        return params;
    }

    private HashMap<String, Object> getParams() {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername()); //change this to signed in user
            params.put("user_email", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
            params.put("ticket_data", ticketData.toString());
            params.put("car_key_collected", car_key_switch.isChecked());
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
//            params.put("entry_time", String.valueOf(DateFormat.format("MMM dd, yyyy HH:mm aa", new Date())));
            params.put("entry_time", Utils.getCurrentTimeUnixString());
            params.put("grace_period", SessionManager.getInstance(mContext).getActivationInfo().getGrace_period());
            if (null != upfront_amount) {
                params.put("upfront_payment", upfront_amount);
            }
            //  which status to proceed to
            if (!SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPark().getActive()) {
                params.put("next_status", 2);
            }
            params.put("enable_max_charge", newPrintTicket);

            //  LPR Valet
            if (isLPRValet) {
                params.put("lpsm_log_id", entry_log_id);
                params.put("lpr_valet_flag", isLPRValet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void compileTicketData() {
        //  reset ticketData info every time before recompiling
        ticketData.setLength(0);
        LogUtils.LOGI(this.getClass().getSimpleName(), "ticketData after reset: " + ticketData);

        ActivationResponse activationResponse = SessionManager.getInstance(getActivity()).getActivationInfo();
        running_id = SessionManager.getInstance(mContext).getRunningNumber();
        ticketId = activationResponse.getIdentify() +
                String.format(Locale.getDefault(), "%07d", running_id);
        ticketData.append(activationResponse.getSite_id());
        ticketData.append(activationResponse.getPos_id());
        ticketData.append(Utils.appendDelimeter(number_plate.getText().toString(), activationResponse.getDelimeter(), Constants.NUMBER_PLATE_LENGTH));
        ticketData.append(DateFormat.format("yyyyMMddHHmm", new Date()));
        ticketData.append(Utils.appendDelimeter(mobile_number.getText().toString(), activationResponse.getDelimeter(), Constants.NUMBER_PLATE_LENGTH));
        ticketData.append(ticketId);

        LogUtils.LOGI(this.getClass().getSimpleName(), "ticketData: " + ticketData);
    }

    @Override
    public void onClick(View v) {
        if (isAdded())
            switch (v.getId()) {
                case R.id.issue_ticket_btn:
                case R.id.validate_ticket_btn:
                    if (!isAdded())
                        return;

                    if (number_plate.getText().toString().equalsIgnoreCase("")) {
                        number_plate_parent.setError(getString(R.string.car_plate_compulsory));
                        number_plate.requestFocus();
                        return;
                    }

                    new NetworkConnectionCheck(hasInternet -> {
                        if (hasInternet) {

                            //  do not proceed if there's an issue ticket process running
                            if (isIssuingTicket || isSearchingTicket)
                                return;

                            hideKeyboard(v);
                            if (number_plate.getText().toString().equalsIgnoreCase("")) {
                                number_plate_parent.setError(getString(R.string.car_plate_compulsory));
                                number_plate.requestFocus();
                                isIssuingTicket = false;
                                return;
                            }
                            if(v.getId() == R.id.issue_ticket_btn) {
                                //  issue ticket flag
                                isIssuingTicket = true;
//                new PrintReceipts(printer, getActivity(), content).execute("");
//                topUpPrint(printer, content);

                                compileTicketData();

                                APICall.issueTicket(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getParams());
//                Bundle bundle = new Bundle();
//                bundle.putString(Constants.NUMBER_PLATE_BUNDLE, number_plate.getText().toString());
//                bundle.putString(Constants.MOBILE_NUMBER_BUNDLE, mobile_number.getText().toString());
//                bundle.putBoolean(Constants.KEY_COLLECTION_BUNDLE, car_key_switch.isChecked());
//                bundle.putString(Constants.CHECK_IN_TIME_BUNDLE, String.valueOf(DateFormat.format("MMM dd, yyyy HH:mm aa", new Date())));
//                bundle.putString(Constants.TICKET_DATA_BUNDLE, ticketData.toString());
//                bundle.putString(Constants.TICKET_ID_BUNDLE, ticketId);
//                DisplayTicketFragment fragment = new DisplayTicketFragment();
//                fragment.setArguments(bundle);
//                listener.doFragmentChange(fragment, true, DisplayTicketFragment.class.getSimpleName());
                            } else if (v.getId() == R.id.validate_ticket_btn) {
                                //  search for validated ticket flag
                                isSearchingTicket = true;
                                APICall.getValidatedTicket(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getValidatedTicketParams());
                            }
                        } else {
                            //  no internet connection
                            if (mContext != null)
                                Toast.makeText(mContext, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;

                case R.id.ticket_issued_tab:
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_ISSUED_LIST);
//                TicketListsFragment f = new TicketListsFragment();
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
                    break;

                case R.id.parked_tab:
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_PARKED_LIST);
//                TicketListsFragment f = new TicketListsFragment();
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
                    break;

                case R.id.paid_tab:
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_PAID_LIST);
//                TicketListsFragment f = new TicketListsFragment();
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
                    break;

                case R.id.pending_collection_tab:
                    tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_PENDING_COLLECTION_LIST);
//                TicketListsFragment f = new TicketListsFragment();
                    f.setArguments(tickets_bundle);
                    listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
                    break;

                case R.id.avatar:
                    listener.doLogout();
                    break;

                case R.id.upfront_button1:
                    changeButton(1);
                    break;

                case R.id.upfront_button2:
                    changeButton(2);
                    break;

                case R.id.upfront_button3:
                    changeButton(3);
                    break;
            }
    }

    private void changeButton(int buttonId) {
        switch (buttonId) {
            case 1:
                upfront_button1.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button1.setTextColor(getResources().getColor(R.color.white));
                upfront_button2.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button2.setTextColor(getResources().getColor(R.color.black));
                upfront_button3.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button3.setTextColor(getResources().getColor(R.color.black));
                //  set upfront amount
                upfront_amount = upfrontAmountList.get(0).getValues();
                break;

            case 2:
                upfront_button1.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button1.setTextColor(getResources().getColor(R.color.black));
                upfront_button2.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button2.setTextColor(getResources().getColor(R.color.white));
                upfront_button3.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button3.setTextColor(getResources().getColor(R.color.black));
                //  set upfront amount
                upfront_amount = upfrontAmountList.get(1).getValues();
                break;

            case 3:
                upfront_button1.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button1.setTextColor(getResources().getColor(R.color.black));
                upfront_button2.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button2.setTextColor(getResources().getColor(R.color.black));
                upfront_button3.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue, Objects.requireNonNull(getActivity()).getTheme()));
                upfront_button3.setTextColor(getResources().getColor(R.color.white));
                //  set upfront amount
                upfront_amount = upfrontAmountList.get(2).getValues();
                break;
        }
    }

    private void gotoTicketDisplay() {
        //  go to display & print ticket page
        Bundle bundle = new Bundle();
        bundle.putString(Constants.NUMBER_PLATE_BUNDLE, number_plate.getText().toString());
        bundle.putString(Constants.MOBILE_NUMBER_BUNDLE, mobile_number.getText().toString());
        bundle.putBoolean(Constants.KEY_COLLECTION_BUNDLE, car_key_switch.isChecked());
        //  clear number plate field after use
        number_plate.setText(null);
        //  clear phone number field after use
        mobile_number.setText(null);
        //  reset key collection switch after use
        car_key_switch.setChecked(false);
//        Date date = new Date();
        bundle.putString(Constants.CHECK_IN_TIME_BUNDLE, Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy, hh:mm aa"));
        bundle.putString(Constants.TICKET_ISSUE_DATE_BUNDLE, Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));
        bundle.putString(Constants.TICKET_ISSUE_TIME_BUNDLE, Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "hh:mm aa"));
        bundle.putString(Constants.TICKET_DATA_BUNDLE, simpleStatusResponse.getData().getTicketData());
        bundle.putString(Constants.TICKET_ID_BUNDLE, simpleStatusResponse.getData().getTicketId());
        bundle.putString(Constants.RUNNING_ID_BUNDLE, String.valueOf(running_id));
        bundle.putBoolean(Constants.NEW_PRINT_RECEIPT, newPrintTicket);
        bundle.putBoolean(Constants.PRINT_TICKET, isPrintTicket);
        bundle.putBoolean(Constants.PRINT_DASHBOARD_COPY, isPrintDashboard);
        bundle.putBoolean(Constants.PRINT_STAFF_COPY, isPrintStaff);
        bundle.putInt(Constants.QR_SIZE, qrSize);
        bundle.putString(Constants.STAFF_ISSUER, simpleStatusResponse.getData().getStaffIssuer());
        //  upfront amount if available
        if (upfrontFlag) {
            bundle.putString(Constants.TICKET_UPFRONT_BUNDLE, upfront_amount);
            bundle.putBoolean(Constants.TICKET_UPFRONT_FLAG_BUNDLE, true);
        }

        DisplayTicketFragment fragment = new DisplayTicketFragment();
        fragment.setArguments(bundle);
        listener.doFragmentChange(fragment, true, DisplayTicketFragment.class.getSimpleName());
    }

    private void printTickets() {
        if (!isAdded()) {
            FirebaseCrashlytics.getInstance().recordException(new Exception("Print Ticket Disrupted for: \n " + encodedTicketdata + " \nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss")));
            return;
        }

        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
//        Date date = new Date();
            IssueTicketModel issueTicketModel = new IssueTicketModel();
            issueTicketModel.setSite_name(SessionManager.getInstance(mContext).getActivationInfo().getSite_name());
            issueTicketModel.setSite_address(SessionManager.getInstance(mContext).getActivationInfo().getCity());
            issueTicketModel.setTicket_data(simpleStatusResponse.getData().getTicketDataEncrypted());
            issueTicketModel.setNumber_plate(number_plate.getText().toString());
            issueTicketModel.setTicket_info(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
            issueTicketModel.setRunning_id(Utils.addCharAtIndex(String.valueOf(running_id), 1, "-"));
            issueTicketModel.setDate_issued(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));
            issueTicketModel.setTime_issued(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));
            issueTicketModel.setStaff_issuer(getString(R.string.issue_ticket) + " - " + simpleStatusResponse.getData().getStaffIssuer());
            issueTicketModel.setCustom_qr_size(isCustomQrSize);
            issueTicketModel.setQr_size(qrSize);
            if (upfrontFlag) {
                issueTicketModel.setUpfront_flag(upfrontFlag);
                issueTicketModel.setUpfront_amount(getString(R.string.rm) + Utils.formatBalance(upfront_amount, "0"));
            } else {
                issueTicketModel.setUpfront_flag(upfrontFlag);
            }


            if (isPrintTicket) {
                if (newPrintTicket) {
                    // Ticket Param to Print normal Ticket
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "ticket").execute("");
                    //  ticket type customization
                    if (isPrintDashboard) {
                        printDialogDashBoard(issueTicketModel);
                    } else if (isPrintStaff) {
                        printDialogValetCopy(issueTicketModel);
                    } else {
//                    number_plate.setText(null);
                        //  check if per-entry site
                        if (isPerEntry) {
                            //  compile data & go to parked list & search car
                            gotoParkedList();
                        }
                    }
                } else {
                    if (printCount < 3) {
                        new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "ticket").execute("");
                        printDialog(issueTicketModel);

                        ++printCount;
                    }
                }
            } else if (isPrintDashboard) {
                printDialogDashBoard(issueTicketModel);
            } else if (isPrintStaff) {
                printDialogValetCopy(issueTicketModel);
            }
        } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {
//                if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
            View view = LayoutInflater.from(mContext).inflate(
                    R.layout.valet_ticket, null);
            LinearLayout v = view.findViewById(R.id.ticket);

            ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

            TextView tvSiteName = v.findViewById(R.id.site_name1);
            tvSiteName.setText(activationInfo.getSite_name());

            TextView tvDateIssued = v.findViewById(R.id.date);
            tvDateIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));

            TextView tvTimeIssued = v.findViewById(R.id.time);
            tvTimeIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

            TextView tvRunningId = v.findViewById(R.id.id);
            tvRunningId.setText(Utils.addCharAtIndex(String.valueOf(running_id), 1, "-"));

            String text = simpleStatusResponse.getData().getTicketDataEncrypted();
            ImageView imQrCode = v.findViewById(R.id.qrcode);
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, qrSize, qrSize);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                imQrCode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
                Toast.makeText(mContext, "QR generation error", Toast.LENGTH_SHORT).show();
            }

            TextView tvNumberPlate = v.findViewById(R.id.number_plate);
            tvNumberPlate.setText(number_plate.getText().toString());

            if (upfrontFlag) {
                TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
                tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1() + getString(R.string.rm) + Utils.formatBalance(upfront_amount, "0"));
            } else {
                TextView tvTicketInfo = v.findViewById(R.id.ticket_info);
                tvTicketInfo.setText(SessionManager.getInstance(mContext).getActivationInfo().getPos_info1());
            }

//                PAXPrinter.getInstance().printView(v);
//                PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
//                PAXPrinter.getInstance().start();

            if (isPrintTicket) {
                if (newPrintTicket) {
                    // Ticket Param to Print normal Ticket
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(v);
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();
                    //  ticket type customization
                    if (isPrintDashboard) {
                        printDialogDashBoard(dashboardCopyPrinting());
                    } else if (isPrintStaff) {
                        printDialogValetCopy(valetCopyPrinting());
                    } else {
//                    number_plate.setText(null);
                        //  check if per-entry site
                        if (isPerEntry) {
                            //  compile data & go to parked list & search car
                            gotoParkedList();
                        }
                    }
                } else {
                    if (printCount < 3) {
                        new Thread(() -> {
                            if(PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                                PAXPrinter.getInstance().printView(v);
                                PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                                PAXPrinter.getInstance().start();
                            }
                        }).start();

                        printDialog(v);

                        ++printCount;
                    }
                }
            } else if (isPrintDashboard) {
                printDialogDashBoard(dashboardCopyPrinting());
            } else if (isPrintStaff) {
                printDialogValetCopy(valetCopyPrinting());
            }
//                }
        } else {
            Toast.makeText(mContext, "No printer detected", Toast.LENGTH_SHORT).show();
        }
    }

    private void printDialog(IssueTicketModel issueTicketModel) {
        if (!isAdded())
            return;

        CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "").execute("");
                    if (++printCount > 2) {
                        dialog.dismiss();
                        gotoLists();
                    } else {
                        printDialog(issueTicketModel);
                    }
                    break;

                case BUTTON_NEGATIVE:
                    gotoLists();
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.another_copy_ticket));
    }

    private void printDialog(LinearLayout v) {
        if (!isAdded())
            return;

        CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(v);
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();

                    if (++printCount > 2) {
                        dialog.dismiss();
                        gotoLists();
                    } else {
                        printDialog(v);
                    }
                    break;

                case BUTTON_NEGATIVE:
                    gotoLists();
                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.another_copy_ticket));
    }


    private void printDialogDashBoard(IssueTicketModel issueTicketModel) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // dashboard param to print ticket with large Car plate number and no qr code
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "dashboard").execute("");
                    dialog.dismiss();
                    if (isPrintStaff)
                        printDialogValetCopy(issueTicketModel);
                    else
                        gotoLists();
//                    if(isKeyCollected) {
//                        printDialogValetCopy(issueTicketModel);
//                    } else {
//                        gotoLists();
//                    }

                    break;
            }
        }, "", getString(R.string.dashboard_ticket));
    }

    private LinearLayout valetCopyPrinting() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.staff_copy, null);
        LinearLayout v = view.findViewById(R.id.ticket);

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

        TextView tvSiteName = v.findViewById(R.id.site_name1);
        tvSiteName.setText(activationInfo.getSite_name());

        TextView tvDateIssued = v.findViewById(R.id.date);
        tvDateIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));

        TextView tvTimeIssued = v.findViewById(R.id.time);
        tvTimeIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

        TextView tvRunningId = v.findViewById(R.id.id);
        tvRunningId.setText(Utils.addCharAtIndex(String.valueOf(running_id), 1, "-"));

        TextView tvNumberPlate = v.findViewById(R.id.number_plate);
        tvNumberPlate.setText(number_plate.getText().toString());

        TextView tvUpfrontInfo = v.findViewById(R.id.upfront_info);
        if (upfrontFlag) {
            tvUpfrontInfo.setText(getString(R.string.upfront_payment) + ": " + getString(R.string.rm) + Utils.formatBalance(upfront_amount, "0"));
        }

        TextView tvIssuedStaff = v.findViewById(R.id.issued_staff);
        tvIssuedStaff.setText(getString(R.string.issue_ticket) + " - " + simpleStatusResponse.getData().getStaffIssuer());

        return v;
    }

    private LinearLayout dashboardCopyPrinting() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.dashboard_copy, null);
        LinearLayout v = view.findViewById(R.id.ticket);

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

        TextView tvSiteName = v.findViewById(R.id.site_name1);
        tvSiteName.setText(activationInfo.getSite_name());

        TextView tvDateIssued = v.findViewById(R.id.date);
        tvDateIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "yyMMdd"));

        TextView tvTimeIssued = v.findViewById(R.id.time);
        tvTimeIssued.setText(Utils.convertStringDateToNewFormat(simpleStatusResponse.getData().getDateCreate(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

        TextView tvRunningId = v.findViewById(R.id.id);
        tvRunningId.setText(Utils.addCharAtIndex(String.valueOf(running_id), 1, "-"));

        String numberPlate = number_plate.getText().toString().replaceAll("[^0-9]", "");
        TextView tvCarDigits = v.findViewById(R.id.digits);
        tvCarDigits.setText(numberPlate);

        TextView tvUpfrontInfo = v.findViewById(R.id.upfront_info);
        if (upfrontFlag) {
            tvUpfrontInfo.setText(getString(R.string.upfront_payment) + ": " + getString(R.string.rm) + Utils.formatBalance(upfront_amount, "0"));
        } else {
            tvUpfrontInfo.setVisibility(View.GONE);
            View vDivider3 = v.findViewById(R.id.divider3);
            vDivider3.setVisibility(View.GONE);
        }

        return v;
    }

    private void printDialogDashBoard(LinearLayout v) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // dashboard param to print ticket with large Car plate number and no qr code
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(v);
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();
                    dialog.dismiss();
                    if (isPrintStaff)
                        printDialogValetCopy(valetCopyPrinting());
                    else
                        gotoLists();
//                    if(isKeyCollected) {
//                        printDialogValetCopy(issueTicketModel);
//                    } else {
//                        gotoLists();
//                    }

                    break;
            }
        }, "", getString(R.string.dashboard_ticket));
    }

    private void printDialogValetCopy(IssueTicketModel issueTicketModel) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // valet param to print ticket for valet person
                    new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, null, null, "valet").execute("");
                    dialog.dismiss();
                    gotoLists();
                    break;
            }
        }, "", getString(R.string.valet_ticket));
    }

    private void printDialogValetCopy(LinearLayout v) {
        if (!isAdded())
            return;

        CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    // valet param to print ticket for valet person
                    new Thread(() -> {
                        if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                            PAXPrinter.getInstance().printView(v);
                            PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                            PAXPrinter.getInstance().start();
                        }
                    }).start();
                    dialog.dismiss();
                    gotoLists();
                    break;
            }
        }, "", getString(R.string.valet_ticket));
    }

    private void gotoLists() {
        if (SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPark().getActive()) {
            gotoTicketIssueList();
        } else {
            //  go to parked list
            gotoParkedList();
        }
    }

    private void autoParkStatus() {
        //  skip parking of cars
        HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername());
        params.put("user_email", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
        params.put("locationpark", "");
        params.put("pos_id", SessionManager.getInstance(mContext).getActivationInfo().getPos_id());
        params.put("datetime", String.valueOf(System.currentTimeMillis() / 1000));
        params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
        params.put("ticket_id", Utils.getCurrentDateWithFormat("yyMMdd") + running_id);
        APICall.vehicleParked(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), params);
    }

    private void encryptData() {
        //  here to start ticket printing process
        HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
        params.put("ticket_data", ticketData.toString());
        APICall.encryptTicket(this, mContext,
                Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), params);
    }

    private void gotoTicketIssueList() {
        //  go to ticket issued list
        Bundle tickets_bundle = new Bundle();
        TicketListsFragment f = new TicketListsFragment();
        tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_ISSUED_LIST);
        f.setArguments(tickets_bundle);
        listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
    }

    private void gotoParkedList() {
        //  go to ticket parked list
        Bundle tickets_bundle = new Bundle();
        TicketListsFragment f = new TicketListsFragment();
        if (isPerEntry) {
            tickets_bundle.putString(Constants.CAR_TO_HIGHLIGHT, number_plate.getText().toString());
        }
        tickets_bundle.putString(Constants.TICKET_LIST_TYPE, Constants.TICKET_PARKED_LIST);
        f.setArguments(tickets_bundle);

        //  clear number plate field before leaving page
        number_plate.setText(null);

        listener.doFragmentChange(f, true, TicketListsFragment.class.getSimpleName());
    }

    private void issueTicketSuccessProcess(SimpleStatusResponse simpleStatusResponse) {
        switch (simpleStatusResponse.getKpCode()) {
            case "KP_000":
                //  increment & save running number
                SessionManager.getInstance(mContext).saveRunningNumber(SessionManager.getInstance(mContext).getRunningNumber());

                //  car needs to be parked by staff
                if (SessionManager.getInstance(mContext).showTicket()) {
                    //  ticket needs to be displayed
                    gotoTicketDisplay();
                } else {
                    //  ticket does not need to be displayed
                    //  encrypt ticket data and prompt printing here
//                    encryptData();
                    //  ticket encryption done in issue ticket API, proceed to print ticket straight
                    printTickets();
                }
                break;

            case "KP_311":
                //  invalid request
                //  ticketdata error (eg: mismatched data length)
                //  general shit
                CustomAlerts.showOKAlert(mContext, getString(R.string.error), getString(R.string.issue_ticket_error));
                break;

            case "KP_312":
                if (newPrintTicket) {
                    //  duplicated unpaid new ticket version
                    duplicateTicketDialog();
                } else {
                    //  duplicated ticket old version
                    CustomAlerts.showOKAlert(mContext, getString(R.string.error), getString(R.string.duplicate_ticket));
                }
                break;

            case "KP_313":
                //  unhandled duplicated ticket error code (19/06/2019)
                if (newPrintTicket) {
                    CustomAlerts.showOKAlert(mContext, getString(R.string.error), simpleStatusResponse.getKipleCodeDesc());
                } else {
                    CustomAlerts.showOKAlert(mContext, getString(R.string.error), simpleStatusResponse.getKipleCodeDesc());
                }
                break;

            case "KP_314":
                //  increment & save running number
                SessionManager.getInstance(mContext).saveRunningNumber(SessionManager.getInstance(mContext).getRunningNumber());

                //  reissue ticket due to duplicated running number
                compileTicketData();
                APICall.issueTicket(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getParams());
                break;

            default:
                //  unexpected response
                CustomAlerts.showOKAlert(mContext, getString(R.string.error), simpleStatusResponse.getKipleCodeDesc());
                FirebaseCrashlytics.getInstance().recordException(new Exception("Unexpected Response\n" + simpleStatusResponse.getKpCode() + "\n" + simpleStatusResponse.getKipleCodeDesc() + "\n" + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss")));
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case ISSUE_TICKET:
                isIssuingTicket = false;
                simpleStatusResponse = (SimpleStatusResponse) response.body();

                if (null != simpleStatusResponse && null != simpleStatusResponse.getKpCode()) {

                    issueTicketSuccessProcess(simpleStatusResponse);

//                    //  issue ticket errors
//                    if ("KP_311".equalsIgnoreCase(simpleStatusResponse.getKpCode())) {
//                        //  invalid request
//                        //  ticketdata error (eg: mismatched data length)
//                        //  general shit
//                        CustomAlerts.showOKAlert(mContext, getString(R.string.error), getString(R.string.issue_ticket_error));
//                        return;
//                    } else if ("KP_312".equalsIgnoreCase(simpleStatusResponse.getKpCode()) && newPrintTicket) {
//                        //  duplicated unpaid ticket
//                        duplicateTicketDialog();
//                        return;
//                    } else if ("KP_313".equalsIgnoreCase(simpleStatusResponse.getKpCode()) && newPrintTicket) {
//                        //  unhandled duplicated ticket error code (19/06/2019)
//                        CustomAlerts.showOKAlert(mContext, getString(R.string.error), simpleStatusResponse.getKipleCodeDesc());
//                        return;
//                    } else if ("KP_312".equalsIgnoreCase(simpleStatusResponse.getKpCode())) {
//                        CustomAlerts.showOKAlert(mContext, getString(R.string.error), getString(R.string.duplicate_ticket));
//                        return;
//                    } else if ("KP_313".equalsIgnoreCase(simpleStatusResponse.getKpCode())) {
//                        CustomAlerts.showOKAlert(mContext, getString(R.string.error), simpleStatusResponse.getKipleCodeDesc());
//                        return;
//                    } else if("KP_314".equalsIgnoreCase(simpleStatusResponse.getKpCode())) {
//                        //  increment & save running number
//                        SessionManager.getInstance(mContext).saveRunningNumber(SessionManager.getInstance(mContext).getRunningNumber());
//
//                        //  reissue ticket due to duplicated running number
//                        compileTicketData();
//                        APICall.issueTicket(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getParams());
//                        return;
//                    }
//
//                    //  issue ticket success
//                    if("KP_000".equalsIgnoreCase(simpleStatusResponse.getKpCode())) {
//                        //  increment & save running number
//                        SessionManager.getInstance(mContext).saveRunningNumber(SessionManager.getInstance(mContext).getRunningNumber());
//
////                if(SessionManager.getInstance(mContext).getActivationInfo().getService_customize().getPark().getActive()) {
//                        //  car needs to be parked by staff
//                        if (SessionManager.getInstance(mContext).showTicket()) {
//                            //  ticket needs to be displayed
//                            gotoTicketDisplay();
//                        } else {
//                            //  ticket does not need to be displayed
//                            //  encrypt ticket data and prompt printing here
//                            encryptData();
//                        }
//                    }
//                }else{
//                    //  car does not need to be parked by staff
//                    autoParkStatus();
//                }
                } else {
                    //  unexpected response
                    CustomAlerts.showOKAlert(mContext, getString(R.string.error), getString(R.string.unexpected_issue_ticket_error));
                    FirebaseCrashlytics.getInstance().recordException(new Exception("Issue ticket response null" + "\n" + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss")));
                }
                break;

            case FAILED_ENTRY:
                refreshLayout.setRefreshing(false);
                FailedEntryResponse failedEntryResponse = (FailedEntryResponse) response.body();
                Glide.with(mContext).load(failedEntryResponse.getData().getImage()).fitCenter().into(latest_entry_image);
                entry_log_id = failedEntryResponse.getData().getEntryLogId();
//                if(isLPRValet && entry_log_id == null) {
//                    issue_ticket_btn.setBackground(getResources().getDrawable(R.drawable.button_rounded_grey));
//                    issue_ticket_btn.setEnabled(false);
//                } else {
//                    issue_ticket_btn.setBackground(getResources().getDrawable(R.drawable.button_rounded_blue));
//                    issue_ticket_btn.setEnabled(true);
//                }
                break;

            case GET_VALIDATED_TICKET:
                GetValidatedTicketResponse getValidatedTicketResponse = (GetValidatedTicketResponse) response.body();

                isSearchingTicket = false;

                if (getValidatedTicketResponse.getSuccess()) {
                    //  clear car plate field
                    number_plate.setText(null);
                    //  proceed to payment page
                    final Intent intent = new Intent(mContext, ActivityCashPayment.class);
                    intent.putExtra(Constants.TICKET_INFO_BUNDLE, getValidatedTicketResponse.getData());
                    intent.putExtra("activity", Constants.VALIDATED_TICKET_PAYMENT);
                    this.startActivityForResult(intent, Constants.TICKET_PAID_INT);
                } else {
                    CustomAlerts.showOKAlert(mContext, "", getValidatedTicketResponse.getMessage());
                }
                break;

            case ENCRYPT:
                EncryptionResponse encryptionResponse = (EncryptionResponse) response.body();
                encodedTicketdata = encryptionResponse.getEncoded_ticket();
                //  print ticket & proceed to lists
//                printTickets();
                break;

            case MINI_DASHBOARD:
                refreshLayout.setRefreshing(false);
                //  update global server time
                Constants.SERVER_TIME = String.valueOf(response.headers().getDate("Date").getTime() / 1000L);

                ArrayList<MiniDashboardResponse> miniDashboardResponses = (ArrayList<MiniDashboardResponse>) response.body();
                if (miniDashboardResponses != null) {
                    issue_ticket_amount.setText(String.valueOf(miniDashboardResponses.get(0).getTotal()));
                    parked_amount.setText(String.valueOf(miniDashboardResponses.get(1).getTotal()));
                    paid_amount.setText(String.valueOf(miniDashboardResponses.get(2).getTotal()));
                    pending_collection_amount.setText(String.valueOf(miniDashboardResponses.get(3).getTotal()));
                }
                break;

            case VEHICLE_PARKED:
                Toast.makeText(mContext, getString(R.string.ticket_for) + number_plate.getText().toString() + getString(R.string.issued), Toast.LENGTH_SHORT).show();
                //  check if ticket needs to be displayed
                if (SessionManager.getInstance(mContext).showTicket()) {
                    //  ticket needs to be displayed
                    gotoTicketDisplay();
                } else {
                    //  ticket does not need to be displayed
                    //  encrypt ticket data and prompt printing here
                    encryptData();
                }
                break;

            case CASH_PAYMENT:
                CashPaymentResponse data = (CashPaymentResponse) response.body();
                if (isPrintReceipt && null != data)
                    printReceipt(data.getReceipt(), data.getEntrytime(), data.getPaytime());

                CustomAlerts.showOKAlert(mContext, (dialog, which) -> dialog.dismiss(), mContext.getString(R.string.payment_notification_heading), data != null ? data.getKipleCodeDesc() : null);

                break;

//            case UPFRONT_AMOUNT:
//                upfrontAmountResponse = (UpfrontAmountResponse) response.body();
//
//                switch (upfrontAmountResponse.getData().size()){
//                    case 1:
//                        upfront_button1.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(0).getAmount()));
//                        upfront_button2.setVisibility(View.GONE);
//                        upfront_button3.setVisibility(View.GONE);
//                        upfront_amount_parent.setVisibility(View.VISIBLE);
//                        break;
//
//                    case 2:
//                        upfront_button1.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(0).getAmount()));
//                        upfront_button2.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(1).getAmount()));
//                        upfront_button3.setVisibility(View.GONE);
//                        upfront_amount_parent.setVisibility(View.VISIBLE);
//                        break;
//
//                    case 3:
//                        upfront_button1.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(0).getAmount()));
//                        upfront_button2.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(1).getAmount()));
//                        upfront_button3.setText(getString(R.string.rm) + Utils.formatBalance(upfrontAmountResponse.getData().get(2).getAmount()));
//                        upfront_amount_parent.setVisibility(View.VISIBLE);
//                        break;
//
//                        default:
//                            upfront_amount_parent.setVisibility(View.GONE);
//                }
//
//                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case ISSUE_TICKET:
                isIssuingTicket = false;
                break;

            case ENCRYPT:
                CustomAlerts.showOKAlert(mContext, "Error", "Data Encryption Error");
                FirebaseCrashlytics.getInstance().recordException(new Exception("Ticket Data Encryption Error for: \n " + encodedTicketdata + " \nat: " + Utils.getCurrentDateOrTime("yyyy-MM-dd HH:mm:ss")));
                break;

            case MINI_DASHBOARD:
            case FAILED_ENTRY:
                refreshLayout.setRefreshing(false);
                break;

            case GET_VALIDATED_TICKET:
                GetValidatedTicketResponse getValidatedTicketResponse;
                isSearchingTicket = false;

                TypeAdapter<GetValidatedTicketResponse> getValidatedTicketTypeAdapter = new Gson().getAdapter(GetValidatedTicketResponse.class);
                if (response.errorBody() != null) {
                    try {
                        getValidatedTicketResponse = getValidatedTicketTypeAdapter.fromJson(response.errorBody().string());
                        if (null != getValidatedTicketResponse && null != getValidatedTicketResponse.getStatus()) {
                            //  invalid token
                            CustomAlerts.showOKAlert(mContext, getValidatedTicketResponse.getStatus(), "");
                        } else if (null != getValidatedTicketResponse) {
                            CustomAlerts.showOKAlert(mContext, getValidatedTicketResponse.getCode(), getValidatedTicketResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();;
                    }
                }
                break;

            case VEHICLE_PARKED:
                CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
                    if (which == RESULT_OK) {
                        dialog.dismiss();
                    }
                }, getString(R.string.error), getString(R.string.ticket_status_update_failure) + number_plate.getText().toString());
                break;

            case CASH_PAYMENT:
                CashPaymentResponse cashPaymentResponse;
                Gson gson = new Gson();
                TypeAdapter<CashPaymentResponse> adapter = gson.getAdapter(CashPaymentResponse.class);
                if (response.errorBody() != null) {
                    try {
                        cashPaymentResponse = adapter.fromJson(response.errorBody().string());
                        if (null != cashPaymentResponse.getKpCode() && "kp005".equalsIgnoreCase(cashPaymentResponse.getKpCode())) {
                            CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                            CustomAlerts.showOKAlert(mContext, (dialog, which) -> {
                                if (which == RESULT_OK) {
                                    dialog.dismiss();
                                }
                            }, getString(R.string.alert), getString(R.string.amount_updated));
                        } else if (null != cashPaymentResponse.getKpCode() && "kp_314".equalsIgnoreCase(cashPaymentResponse.getKpCode())) {
                            //  ticket payment failure
                            CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                            CustomAlerts.showOKAlert(mContext, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                        } else {
                            CashPaymentResponse finalCashPaymentResponse = cashPaymentResponse;
                            CustomAlerts.showOKAlert(mContext, getString(R.string.payment_fail), finalCashPaymentResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

//            case UPFRONT_AMOUNT:
//                upfront_amount_parent.setVisibility(View.GONE);
//                break;
        }
    }

    private void duplicateTicketDialog() {

        amount = simpleStatusResponse.getData().getAmount();

        CustomAlerts.showResponseAlert(mContext, (dialog, which) -> {
            switch (which) {
                case BUTTON_POSITIVE:
                    new NetworkConnectionCheck(hasInternet -> {
                        if (hasInternet) {
                            //  do payment
                            maxButtonClicked = true;
                            APICall.cashPayment(this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getParamsCash(simpleStatusResponse.getData().getTicketId(), simpleStatusResponse.getData().getEntryTime(), amount));
                            dialog.dismiss();
                        }
                    });
                    break;

                case BUTTON_NEGATIVE:

                    dialog.dismiss();
                    break;
            }
        }, "", getString(R.string.ticket_not_paid, amount));
    }

    private HashMap<String, Object> getParamsCash(String ticketId, String entryTime, int amount) {
        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put("ticketId", ticketId);
            params.put("site_id", SessionManager.getInstance(mContext).getActivationInfo().getSite_id());
            params.put("pos_payer", SessionManager.getInstance(mContext).getActivationInfo().getPos_id());
            params.put("service", SessionManager.getInstance(mContext).getActivationInfo().getIdentify());
            params.put("user_id", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getEmail());
            params.put("staff_payer", SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername());
            params.put("paymentAmount", amount);
            long currentUnix = Utils.getCurrentTimeUnix();
            params.put("reqTime", String.valueOf(currentUnix));
            params.put("datePayUnix", String.valueOf(currentUnix));
            params.put("entryTime", entryTime);
            params.put("payment_pos_method", 1);
            params.put("receipt", SessionManager.getInstance(mContext).getActivationInfo().getSite_id() + ticketId);
            //params.put("special_scenario", "Lost Ticket");
            params.put("special_scenario", "Maximum Charge");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    private void printReceipt(String receipt, String entryTime, String payTime) {
        //  print receipt
        IssueTicketModel issueTicketModel = null;
        SummaryModel summaryModel = null;

        ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();
        PaymentReceiptModel paymentReceiptModel = new PaymentReceiptModel();
        paymentReceiptModel.setSite_name(activationInfo.getSite_name());
        String address = activationInfo.getStreet1() + ", " +
                activationInfo.getStreet2() + ",\n" +
                activationInfo.getPostcode() + " " +
                activationInfo.getCity() + ", " +
                activationInfo.getState();
        paymentReceiptModel.setSite_address(address);

        if (activationInfo.getPhone_number() != null)
            paymentReceiptModel.setSite_phone_number(activationInfo.getPhone_number().toString());

        paymentReceiptModel.setDate(Utils.getCurrentDateOrTime("dd/MM/yyyy"));
        paymentReceiptModel.setTime(Utils.getCurrentDateOrTime("hh:mm aa"));
        paymentReceiptModel.setNumber_plate(Utils.getNumberPlateFromTicketdata(simpleStatusResponse.getData().getTicketData()));
        paymentReceiptModel.setTicket_id(simpleStatusResponse.getData().getTicketId());
        paymentReceiptModel.setStaff_name(SessionManager.getInstance(mContext).getActiveUserInfo().getData().getUser().getUsername());
        if (maxButtonClicked) {
            paymentReceiptModel.setValet_amount(getString(R.string.rm) + Utils.formatBalance(amount + ""));
        } else {
            paymentReceiptModel.setValet_amount(getString(R.string.rm) + (null == simpleStatusResponse.getData().getFee() ? Utils.formatBalance(simpleStatusResponse.getData().getAmount().toString()) : Utils.formatBalance(simpleStatusResponse.getData().getFee())));
        }
        paymentReceiptModel.setPos_info(activationInfo.getPos_info1());
        paymentReceiptModel.setQr_info(activationInfo.getPos_info2());
        if (maxButtonClicked) {
            APICall.miniDashboard(true, this, mContext, Constants.AUTHORIZATION + SessionManager.getInstance(mContext).getActiveUserInfo().getData().getToken(), getDashboardParams());
            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT + " " + Constants.MAXIMUM_CHARGE);
        } else {
            paymentReceiptModel.setPayment_method(Constants.CASH_PAYMENT);
        }
        if (isPrintLogo) {
//        paymentReceiptModel.setBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_kiplepark_text_logo_black), 140, 30, true));
            paymentReceiptModel.setBitmap(Utils.getBitmapFromVectorDrawable(mContext, R.drawable.logo_four_seasons));
        } else {
            paymentReceiptModel.setBitmap(null);
        }
        paymentReceiptModel.setReceipt_id(receipt);
        paymentReceiptModel.setEntry_time(Utils.unixFormatter(entryTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).getActivationInfo().getTimezone()));
        paymentReceiptModel.setPayment_time(Utils.unixFormatter(payTime, "dd/MM/yyyy hh:mm aa", SessionManager.getInstance(mContext).getActivationInfo().getTimezone()));
        paymentReceiptModel.setDuration(Utils.getElapsedTimeFromUnix(entryTime, payTime));

        if (isPrintCompanyName)
            paymentReceiptModel.setParking_operator(SessionManager.getInstance(mContext).getActivationInfo().getPartner_name());
        else
            paymentReceiptModel.setParking_operator("");

        paymentReceiptModel.setGst_id(SessionManager.getInstance(mContext).getActivationInfo().getGst_id());
        if (maxButtonClicked) {
            paymentReceiptModel.setSubtotal(getString(R.string.rm) + Utils.formatBalance(amount + ""));
        } else {
            paymentReceiptModel.setSubtotal(getString(R.string.rm) + (null == simpleStatusResponse.getData().getFee() ? Utils.formatBalance(simpleStatusResponse.getData().getAmount().toString()) : Utils.formatBalance(simpleStatusResponse.getData().getFee())));
        }
        paymentReceiptModel.setRoundingAdj(getString(R.string.rm) + "0.00");
        paymentReceiptModel.setDiscount(null);
        if (maxButtonClicked) {
            paymentReceiptModel.setPaid(getString(R.string.rm) + Utils.formatBalance(amount + ""));
        } else {
            paymentReceiptModel.setPaid(getString(R.string.rm) + Utils.formatBalance(paid_amount.getText().toString()));
        }
        //paymentReceiptModel.setChange(balance_amount.getText().toString().contains(getString(R.string.rm)) ? balance_amount.getText().toString() : (getString(R.string.rm) + balance_amount.getText().toString()));
        maxButtonClicked = false;
        new PrintReceipts(mSunmiPrinter, mContext, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("");
    }
}
