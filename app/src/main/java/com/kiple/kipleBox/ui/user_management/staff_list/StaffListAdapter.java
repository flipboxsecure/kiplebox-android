package com.kiple.kipleBox.ui.user_management.staff_list;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.RoundedImageView;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.responses.UserListResponse;
import com.kiple.kipleBox.ui.user_management.login.LoginActivity;
import com.kiple.kipleBox.ui.user_management.manager_activation.ManagerActivationActivity;
import com.kiple.kipleBox.utils.CustomAlerts;

import java.util.List;

public class StaffListAdapter extends RecyclerView.Adapter {

    List<UserListResponse.Datum> userList;
    Context context;
    StaffListActivity activity;
    boolean isAddStaff;

    public StaffListAdapter(List<UserListResponse.Datum> data, Context context, StaffListActivity appCompatActivity, boolean enableAddStaff) {
        userList = data;
        this.context = context;
        this.activity = appCompatActivity;
        this.isAddStaff = enableAddStaff;

        //  manually add another entry for add staff button
        if(isAddStaff) {
            UserListResponse.Datum datum = new UserListResponse.Datum();
            datum.setActivated(false);
            datum.setCreated_at("-");
            datum.setEmail("-");
            datum.setId(0);
            datum.setName("aDd sTaFF");
            data.add(datum);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        return new UserItem(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final UserItem item = (UserItem) holder;
        final UserListResponse.Datum data = userList.get(position);

        if (isAddStaff && "Add Staff".equalsIgnoreCase(data.getName())) {
            //  create add staff item
//            item.avatar.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
            Glide.with(context).load(R.drawable.add).into(item.avatar);
            item.user_name.setText(context.getString(R.string.add_staff));
            item.profile_action.setVisibility(View.GONE);

            item.profile.setOnClickListener(v -> {
                //  go to add staff page
                final Intent intent = new Intent(activity, ManagerActivationActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.ADD_STAFF); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                activity.startActivity(intent);
            });
        } else {
            //  normal staff item
            item.user_name.setText(data.getName());

            item.profile.setOnClickListener(v -> {
                //  check if manager logged out
                //  proceed to login page if no active user info
                if(null == SessionManager.getInstance(activity).getActiveUserInfo()) {
                    //  staff profile selected
                    //  proceed with sign in process
                    final Intent intent = new Intent(activity, LoginActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.LOGIN_EMAIL_BUNDLE, data.getEmail()); //Your id
                    b.putString(Constants.ACTIVE_USERNAME_BUNDLE, data.getName());
                    intent.putExtras(b); //Put your id to your next Intent
                    activity.startActivity(intent);
//                activity.finish();
                }else{
                    if(SessionManager.getInstance(activity).getActiveUserInfo().getData().getUser().getEmail().equalsIgnoreCase(data.getEmail())) {
                        //  go back to fragment_homepage if active user info matches profile selected
//                        Toast.makeText(context, "went back straight to fragment_homepage", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }else{
                        CustomAlerts.showOKAlert(context, (dialog, which) -> dialog.dismiss(),
                                context.getString(R.string.reminder_title), context.getString(R.string.manager_logout_reminder));
                    }
                }
            });
        }

        //  set profile action button
        //  if AddStaff is permitted
        if (isAddStaff){
            if (position == 0) {  //  site manager
                Glide.with(context).load(context.getResources().getDrawable(R.drawable.logout_btn)).into(item.profile_action);
                //  manager logout action
                if(null != SessionManager.getInstance(activity).getActiveUserInfo()) {
                    item.profile_action.setOnClickListener(v -> {
                        //  logout confirmation
                        CustomAlerts.showResponseAlert(activity, (dialog, which) -> {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //  do manager logout
//                                        Toast.makeText(activity, "Log Out Manager", Toast.LENGTH_SHORT).show();
                                    activity.logoutManager();
                                    dialog.dismiss();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        }, "", activity.getString(R.string.confirm_logout));
                    });
                }else{
                    item.profile_action.setVisibility(View.GONE);
                }
            } else {
                Glide.with(context).load(context.getResources().getDrawable(R.drawable.edit_btn)).into(item.profile_action);
                //  edit profile action
                item.profile_action.setOnClickListener(v -> {
                    //  do edit profile
                    final Intent intent = new Intent(activity, ManagerActivationActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.LOGIN_TYPE_BUNDLE, Constants.EDIT_STAFF); //Your id
                    b.putParcelable(Constants.EDIT_PROFILE_BUNDLE, data);
                    intent.putExtras(b); //Put your id to your next Intent
                    activity.startActivity(intent);
                });
            }
        }else{
            //  hide profile action button
            item.profile_action.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return (null != userList ? userList.size() : 0);
    }

    public class UserItem extends RecyclerView.ViewHolder{

        RoundedImageView avatar, profile_action;
        TextView user_name;
        ConstraintLayout profile;

        public UserItem(View itemView) {
            super(itemView);

            this.avatar     = itemView.findViewById(R.id.profile_img);
            this.user_name  = itemView.findViewById(R.id.user_name);
            this.profile    = itemView.findViewById(R.id.profile);
            this.profile_action    = itemView.findViewById(R.id.profile_action);
        }
    }
}
