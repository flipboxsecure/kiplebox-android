package com.kiple.kipleBox.ui.core.landing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kiple.kipleBox.R;
import com.kiple.kipleBox.base.BaseActivity;
import com.kiple.kipleBox.base.Printer;
import com.kiple.kipleBox.common.APICallback;
import com.kiple.kipleBox.common.CallBackInterface;
import com.kiple.kipleBox.common.Constants;
import com.kiple.kipleBox.common.CustomUncaughtExceptionHandler;
import com.kiple.kipleBox.common.OnFragmentChangeListener;
import com.kiple.kipleBox.common.SessionManager;
import com.kiple.kipleBox.model.histroy.SummaryResponse;
import com.kiple.kipleBox.model.printer_models.IssueTicketModel;
import com.kiple.kipleBox.model.printer_models.PaymentReceiptModel;
import com.kiple.kipleBox.model.printer_models.SummaryModel;
import com.kiple.kipleBox.model.responses.ActivationResponse;
import com.kiple.kipleBox.model.responses.GetValetTicketResponse;
import com.kiple.kipleBox.model.responses.TicketListsResponse;
import com.kiple.kipleBox.network.APICall;
import com.kiple.kipleBox.service.TicketUpdateService;
import com.kiple.kipleBox.ui.core.cash_payment.ActivityCashPayment;
import com.kiple.kipleBox.ui.core.errors.ErrorActivity;
import com.kiple.kipleBox.ui.core.history.HistoryTransactionFragment;
import com.kiple.kipleBox.ui.core.homepage.HomepageFragment;
import com.kiple.kipleBox.ui.core.scanner.CaptureActivityPortrait;
import com.kiple.kipleBox.ui.user_management.staff_list.StaffListActivity;
import com.kiple.kipleBox.utils.CustomAlerts;
import com.kiple.kipleBox.utils.LogUtils;
import com.kiple.kipleBox.utils.PrintReceipts;
import com.kiple.kipleBox.utils.Utils;
import com.kiple.kipleBox.utils.pax_printer.PAXPrinter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class AppActivity extends BaseActivity implements OnFragmentChangeListener, View.OnClickListener, CallBackInterface {

    @BindView(R.id.content_frame)
    FrameLayout content_frame;
    @BindView(R.id.ll_bottombar_buttons)
    LinearLayout bottom_navigation;
    @BindView(R.id.ll_scan)
    LinearLayout scan_btn;

    @BindView(R.id.ll_history)
    LinearLayout ll_history;

    @BindView(R.id.tv_history)
    TextView tv_history;

    @BindView(R.id.ll_ticket)
    LinearLayout ll_ticket;

    @BindView(R.id.tv_ticket)
    TextView tv_ticket;

    private Printer mSunmiPrinter;
    private String printerType;

    private boolean isScrolling = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_landing);
        ButterKnife.bind(this);

        //  uncaught exception handling
        Thread.setDefaultUncaughtExceptionHandler(new CustomUncaughtExceptionHandler(this,
                AppActivity.class));

        //  check if app crashed previously
        Intent intent = getIntent();
        if(intent.hasExtra(Constants.UNCAUGHT_EXCEPTION)){
            CustomAlerts.showOKAlert(this, getString(R.string.app_crash_title), getString(R.string.app_crash_content));
        }

//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        //  detect and set printer status
        printerDetection();

        TicketUpdateService.startRepeatingService(this);
        replaceFragment(false, R.id.content_frame, new HomepageFragment(), true, HomepageFragment.class.getSimpleName());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void printerDetection() {
        //  hardware detection
        //  check for PAX first due to API efficiency
        String printerStatus = PAXPrinter.getInstance().getStatus();
        if (printerStatus.equalsIgnoreCase(Constants.NOT_PAX_PRINTER)) {
            //  not PAX hardware, start check for Sunmi hardware

            this.mSunmiPrinter = new Printer(this);
            mSunmiPrinter.printerInit();
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (mSunmiPrinter.getPrinterModal() != null) {
                    Log.e("Printer Info", mSunmiPrinter.getPrinterModal());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterSerialNo());
                    Log.e("Printer Info", mSunmiPrinter.getPrinterVersion());

                    printerType = Constants.SUNMI_PRINTER;
                } else {
                    printerType = Constants.NO_PRINTER;
                }

                if (mSunmiPrinter.isPrinterReady()) {
                    printerType = Constants.SUNMI_PRINTER;
                } else {
                    printerType = Constants.NO_PRINTER;
                }
            }, 300);
        } else if (printerStatus.equalsIgnoreCase(Constants.SUCCESS)) {
            //  PAX hardware, start initiate PAX printer
            if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                printerType = Constants.PAX_PRINTER;
            } else {
                printerType = Constants.NO_PRINTER;
            }
        } else {
            //  unexpected status received from PAX
            Toast.makeText(this, printerStatus, Toast.LENGTH_SHORT).show();
            printerType = Constants.NO_PRINTER;
        }
    }

    @Override
    protected void onDestroy(){
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER))
            mSunmiPrinter.disconnectPrinter();
        APICallback.hideProgressBar();
        super.onDestroy();
    }

    //  remove focus from edit text when clicked anywhere outside
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_MOVE) {
            isScrolling = true;
            return super.dispatchTouchEvent(event);
        }

        if (!isScrolling && event.getAction() == MotionEvent.ACTION_UP) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        } else {
            isScrolling = false;
        }
        return super.dispatchTouchEvent( event );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_ticket: {
                if (!(fm.findFragmentById(R.id.content_frame) instanceof HomepageFragment)) {
                    ll_history.setBackground(getResources().getDrawable(R.color.lighter_grey));
                    tv_history.setTextColor(getResources().getColor(R.color.kiple_blue));

                    ll_ticket.setBackground(getResources().getDrawable(R.color.kiple_blue));
                    tv_ticket.setTextColor(getResources().getColor(R.color.white));

                    replaceFragment(false, R.id.content_frame, new HomepageFragment(), true, HomepageFragment.class.getSimpleName());
                }

            }
            break;

            case R.id.ll_history: {
                if (!(fm.findFragmentById(R.id.content_frame) instanceof HistoryTransactionFragment)) {
                    ll_history.setBackground(getResources().getDrawable(R.color.kiple_blue));
                    tv_history.setTextColor(getResources().getColor(R.color.white));

                    ll_ticket.setBackground(getResources().getDrawable(R.color.lighter_grey));
                    tv_ticket.setTextColor(getResources().getColor(R.color.kiple_blue));

                    replaceFragment(false, R.id.content_frame, new HistoryTransactionFragment(), true, HistoryTransactionFragment.class.getSimpleName());
                }
            }
            break;

            case R.id.ll_scan:
                startScanner();
                break;
        }
    }

    private void startScanner() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt(getString(R.string.scanner_prompt));
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }

    private void processScan(String contents) {

        if(SessionManager.getInstance(this).isHybrid()) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("ticket_data", contents);
            params.put("service", SessionManager.getInstance(this).getActivationInfo().getIdentify());
            params.put("grace_period", SessionManager.getInstance(this).getActivationInfo().getGrace_period());
            params.put("pos_id", SessionManager.getInstance(this).getActivationInfo().getPos_id());

            APICall.getHybridTicket(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
        }else{
            HashMap<String, Object> params = new HashMap<>();
            params.put("user_id", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
            params.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
            params.put("ticket_data", contents);

            APICall.getValetTicket(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), params);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.TICKET_PAID_INT) {

        } else if (resultCode == Activity.RESULT_OK && requestCode == Constants.CLOCK_OUT_CODE) {
            //  log out user only here
            APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());
        } else {

            //  Code Scanning Response
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Log.d("AppActivity", "Cancelled scan");
                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Log.d("AppActivity", "Scanned");
//                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                    LogUtils.LOGI(this.getClass(), "Scanner result: " + result.getContents());
                    processScan(result.getContents());
                }
            }

            //  error response
            if (resultCode == Activity.RESULT_OK && requestCode == Constants.INVALID_TICKET_CODE) {
//            final Intent intent = new Intent(this, ContactUsActivity.class);
//            startActivity(intent);
            }
        }
    }

    @Override
    public void doFragmentChange(Fragment f, boolean history, String title) {
        replaceFragment(R.id.content_frame, f, history);
    }

    @Override
    public void doFragmentChange(boolean doAnimation, Fragment f, boolean history, String title) {
        // TODO Auto-generated method stub
        replaceFragment(doAnimation, R.id.content_frame, f, history, title);
    }

    @Override
    public void doFragmentChange(String animationFlag, Fragment f, boolean history, String title) {
        replaceFragment(animationFlag, R.id.content_frame, f, history, title);
    }

    @Override
    public void doLogout() {
        if (Utils.isUserSupervisor(SessionManager.getInstance(this).getActiveUserDetails().getData().getAccess_level())) {
            //  user is site manager, direct back to staff list page without logout
            //  display edit profile buttons
//                        Toast.makeText(this, "user is manager", Toast.LENGTH_SHORT).show();
            goToStaffList(Constants.MANAGER_LOGIN);
        } else {
            //  logout confirmation
            CustomAlerts.showResponseAlert(this, (dialog, which) -> {
                switch (which) {
                    case BUTTON_POSITIVE:
                        proceedLogout();
                        dialog.dismiss();
                        break;

                    case BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }, "", getString(R.string.confirm_logout));
        }
    }

    @Override
    public void hideBottomNavigation() {
        findViewById(R.id.ll_bottombar_buttons).setVisibility(View.GONE);
        findViewById(R.id.ll_scan).setVisibility(View.GONE);
    }

    @Override
    public void showBottomNavigation() {
        findViewById(R.id.ll_bottombar_buttons).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_scan).setVisibility(View.VISIBLE);
    }

    @Override
    public void setBottomIcon(int index) {

    }

    @Override
    public boolean getBottomStatus() {
        return false;
    }

    @Override
    public void changeTheme(int theme) {

    }

    private void proceedLogout() {
        //cancel service when logging out user
        TicketUpdateService.stopRepeatingService();

        //  user is normal valet staff, logout & go to staff list page
        //  hide edit profile buttons
        Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show();
//        APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());

        //  get transaction summary first before logging out
        HashMap<String, Object> param = new HashMap<>();
        param.put("site_id", SessionManager.getInstance(this).getActivationInfo().getSite_id());
        param.put("email", SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getEmail());
        param.put("datetime", Utils.getCurrentDateOrTime("yyyy-MM-dd"));
        param.put("logout_print", true);
        APICall.historySummary(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken(), param);
    }

    private void goToStaffList(String loginType) {
        //  show staff list
        final Intent intent = new Intent(this, StaffListActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.LOGIN_TYPE_BUNDLE, loginType); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
    }

    private void printSummary(SummaryResponse summaryResponse) {
        if (printerType.equalsIgnoreCase(Constants.SUNMI_PRINTER)) {
            try {
                //  print summary before executing logout
                IssueTicketModel issueTicketModel = null;
                PaymentReceiptModel paymentReceiptModel = null;

                ActivationResponse activationInfo = SessionManager.getInstance(this).getActivationInfo();
                SummaryModel summaryModel = new SummaryModel();
                summaryModel.setSite_name(activationInfo.getSite_name());

                String address = summaryResponse.getData().getSite_information().getStreet1() + ", " +
                        summaryResponse.getData().getSite_information().getStreet2() + ",\n" +
//                        cashResponse.getData().getSite_information().getPostcode() + " " +
                        summaryResponse.getData().getSite_information().getCity() + ", " +
                        summaryResponse.getData().getSite_information().getState();
                summaryModel.setSite_address(address);

                String[] period = summaryResponse.getData().getAuth_history().split(" - ");

                summaryModel.setStartDateTime(period[0].trim());
                summaryModel.setEndDateTime(period[1].trim());
                summaryModel.setStaff_name(SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());
                summaryModel.setCashArrayList(summaryResponse.getData().getTransactions().getCash());
                summaryModel.setGuestRoomArrayListArrayList(summaryResponse.getData().getTransactions().getGuest_room());
                summaryModel.setMasterAccountArrayListArrayList(summaryResponse.getData().getTransactions().getMaster_account());

                double cashAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getCash().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getCash().get(i).getType().toLowerCase().contains("void"))
                        cashAmount += Double.parseDouble(summaryResponse.getData().getTransactions().getCash().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getCash().get(i).getType().toLowerCase().contains("void"))
                        cashAmount -= Double.parseDouble(summaryResponse.getData().getTransactions().getCash().get(i).getAmount());
                }

                double guestRoomAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getGuest_room().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getGuest_room().get(i).getType().toLowerCase().contains("void"))
                        guestRoomAmount += Double.parseDouble(summaryResponse.getData().getTransactions().getGuest_room().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getGuest_room().get(i).getType().toLowerCase().contains("void"))
                        guestRoomAmount -= Double.parseDouble(summaryResponse.getData().getTransactions().getGuest_room().get(i).getAmount());
                }

                double masterAccAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getMaster_account().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getMaster_account().get(i).getType().toLowerCase().contains("void"))
                        masterAccAmount += Double.parseDouble(summaryResponse.getData().getTransactions().getMaster_account().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getMaster_account().get(i).getType().toLowerCase().contains("void"))
                        masterAccAmount -= Double.parseDouble(summaryResponse.getData().getTransactions().getMaster_account().get(i).getAmount());
                }

                double debitCreditAmount = 0;
                for (int i = 0; i < summaryResponse.getData().getTransactions().getDebit_credit().size(); i++) {
                    if (!summaryResponse.getData().getTransactions().getDebit_credit().get(i).getType().toLowerCase().contains("void"))
                        debitCreditAmount += Double.parseDouble(summaryResponse.getData().getTransactions().getDebit_credit().get(i).getAmount());
                    else if (summaryResponse.getData().getTransactions().getDebit_credit().get(i).getType().toLowerCase().contains("void"))
                        debitCreditAmount -= Double.parseDouble(summaryResponse.getData().getTransactions().getDebit_credit().get(i).getAmount());
                }

                summaryModel.setCashTotal(String.valueOf(cashAmount));
                summaryModel.setGuestRoomTotal(String.valueOf(guestRoomAmount));
                summaryModel.setMasterAccTotal(String.valueOf(masterAccAmount));
                summaryModel.setDebitCreditTotal(String.valueOf(debitCreditAmount));
                summaryModel.setGrandTotal(String.valueOf(cashAmount + guestRoomAmount + masterAccAmount + debitCreditAmount));
//                summaryModel.transaction_type = data.type
//                summaryModel.pos = activationInfo.pos_info1
//                summaryModel.qr_info = activationInfo.pos_info2
//
//                summaryModel.bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ic_kiplepark_text_logo_black), 280, 60, true)

                new PrintReceipts(mSunmiPrinter, this, issueTicketModel, paymentReceiptModel, summaryModel, "").execute("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (printerType.equalsIgnoreCase(Constants.PAX_PRINTER)) {
            View view = LayoutInflater.from(this).inflate(
                    R.layout.summary_receipt, null);
            LinearLayout v = view.findViewById(R.id.receipt);

            ActivationResponse activationInfo = SessionManager.getInstance(mContext).getActivationInfo();

            TextView tvSiteName = v.findViewById(R.id.site_name1);
            tvSiteName.setText(activationInfo.getSite_name());

            String address = summaryResponse.getData().getSite_information().getStreet1() + ", " +
                    summaryResponse.getData().getSite_information().getStreet2() + ",\n" +
                    summaryResponse.getData().getSite_information().getCity() + ", " +
                    summaryResponse.getData().getSite_information().getState();
            TextView tvSiteAddress = v.findViewById(R.id.site_address1);
            tvSiteAddress.setText(address);

            TextView tvStaffName = v.findViewById(R.id.staff_name);
            tvStaffName.setText(SessionManager.getInstance(this).getActiveUserInfo().getData().getUser().getUsername());

            String[] period = summaryResponse.getData().getAuth_history().split(" - ");

            TextView tvStartDateTime = v.findViewById(R.id.start_date);
            tvStartDateTime.setText(period[0].trim());

            TextView tvEndDateTime = v.findViewById(R.id.end_date);
            tvEndDateTime.setText(period[1].trim());

            //  cash list
            LinearLayout llSummaryCashList = v.findViewById(R.id.summary_cash_list);
            int cashMultiplier = 0;
            List<SummaryResponse.Cash> cashList = summaryResponse.getData().getTransactions().getCash();
            for (int i = 0; i < cashList.size(); i++) {
                View cashItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = cashItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(cashList.get(i).getTicketId() + " - " + cashList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (cashList.get(i).getType().contains("["))
                    cashMultiplier++;

                if (cashList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(cashList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(cashList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llSummaryCashList.addView(cashItem);
            }

            llSummaryCashList.getLayoutParams().height = Utils.getViewHeight(llSummaryCashList) + (cashMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double cashAmount = 0;
            for (int i = 0; i < cashList.size(); i++) {
                if (!cashList.get(i).getType().toLowerCase().contains("void"))
                    cashAmount += Double.parseDouble(cashList.get(i).getAmount());
                else if (cashList.get(i).getType().toLowerCase().contains("void"))
                    cashAmount -= Double.parseDouble(cashList.get(i).getAmount());
            }
            TextView tvCashTotal = v.findViewById(R.id.cash_total);
            tvCashTotal.setText(getString(R.string.rm) + Utils.formatBalance(cashAmount));

            //  guest room list
            LinearLayout llGuestroomList = v.findViewById(R.id.guestroom_list);
            int guestroomMultiplier = 0;
            List<SummaryResponse.Guest_room> guestroomList = summaryResponse.getData().getTransactions().getGuest_room();
            for (int i = 0; i < guestroomList.size(); i++) {
                View guestroomItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = guestroomItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(guestroomList.get(i).getTicketId() + " - " + guestroomList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (guestroomList.get(i).getType().contains("["))
                    guestroomMultiplier++;

                if (guestroomList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(guestroomList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(guestroomList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llSummaryCashList.addView(guestroomItem);
            }

            llGuestroomList.getLayoutParams().height = Utils.getViewHeight(llGuestroomList) + (guestroomMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double guestroomAmount = 0;
            for (int i = 0; i < guestroomList.size(); i++) {
                if (!guestroomList.get(i).getType().toLowerCase().contains("void"))
                    guestroomAmount += Double.parseDouble(guestroomList.get(i).getAmount());
                else if (guestroomList.get(i).getType().toLowerCase().contains("void"))
                    guestroomAmount -= Double.parseDouble(guestroomList.get(i).getAmount());
            }
            TextView tvGuestroomTotal = v.findViewById(R.id.guestroom_total);
            tvGuestroomTotal.setText(getString(R.string.rm) + Utils.formatBalance(guestroomAmount));

            //  master account list
            LinearLayout llMasterAccountList = v.findViewById(R.id.masteraccount_list);
            int masterAccountMultiplier = 0;
            List<SummaryResponse.Master_account> masterAccountList = summaryResponse.getData().getTransactions().getMaster_account();
            for (int i = 0; i < masterAccountList.size(); i++) {
                View masterAccountItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = masterAccountItem.findViewById(R.id.tvTxnID);
                tvTransactionId.setText(masterAccountList.get(i).getTicketId() + " - " + masterAccountList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (masterAccountList.get(i).getType().contains("["))
                    masterAccountMultiplier++;

                if (masterAccountList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionId.setText("- RM " + Utils.formatBalance(masterAccountList.get(i).getAmount()));
                else
                    tvTransactionId.setText(getString(R.string.rm) + Utils.formatBalance(masterAccountList.get(i).getAmount()));

                tvTransactionId.setTextSize(12F);

                llMasterAccountList.addView(masterAccountItem);
            }

            llMasterAccountList.getLayoutParams().height = Utils.getViewHeight(llMasterAccountList) + (masterAccountMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double masterAccountAmount = 0;
            for (int i = 0; i < guestroomList.size(); i++) {
                if (!guestroomList.get(i).getType().toLowerCase().contains("void"))
                    masterAccountAmount += Double.parseDouble(masterAccountList.get(i).getAmount());
                else if (guestroomList.get(i).getType().toLowerCase().contains("void"))
                    masterAccountAmount -= Double.parseDouble(masterAccountList.get(i).getAmount());
            }
            TextView tvMasterAccountTotal = v.findViewById(R.id.masteraccount_total);
            tvMasterAccountTotal.setText(getString(R.string.rm) + Utils.formatBalance(masterAccountAmount));

            //  debit credit list
            LinearLayout llDebitCreditList = v.findViewById(R.id.debit_credit_list);
            int debitCreditMultiplier = 0;
            List<SummaryResponse.Debit_credit> debitCreditList = summaryResponse.getData().getTransactions().getDebit_credit();
            for (int i = 0; i < debitCreditList.size(); i++) {
                View debitCreditItem = LayoutInflater.from(this).inflate(R.layout.item_history_summary_receipt, null);
                TextView tvTransactionId = debitCreditItem.findViewById(R.id.tvTxnID);
                TextView tvTransactionAmt = debitCreditItem.findViewById(R.id.tvTxnAmount);
                tvTransactionId.setText(debitCreditList.get(i).getTicketId() + " - " + debitCreditList.get(i).getType());
                tvTransactionId.setTextSize(12F);

                if (debitCreditList.get(i).getType().contains("["))
                    debitCreditMultiplier++;

                if (debitCreditList.get(i).getType().toLowerCase().contains("[Void]"))
                    tvTransactionAmt.setText("- RM " + Utils.formatBalance(debitCreditList.get(i).getAmount()));
                else
                    tvTransactionAmt.setText(getString(R.string.rm) + Utils.formatBalance(debitCreditList.get(i).getAmount()));

                tvTransactionAmt.setTextSize(12F);

                llDebitCreditList.addView(debitCreditItem);
            }

            llDebitCreditList.getLayoutParams().height = Utils.getViewHeight(llDebitCreditList) + (debitCreditMultiplier * 28);  //  28 = height of textview with default text size + parent layout top margin

            double debitCreditAmount = 0;
            for (int i = 0; i < debitCreditList.size(); i++) {
                if (!debitCreditList.get(i).getType().toLowerCase().contains("void"))
                    debitCreditAmount += Double.parseDouble(debitCreditList.get(i).getAmount());
                else if (guestroomList.get(i).getType().toLowerCase().contains("void"))
                    debitCreditAmount -= Double.parseDouble(debitCreditList.get(i).getAmount());
            }
            TextView debitCreditTotal = v.findViewById(R.id.debit_credit_total);
            debitCreditTotal.setText(getString(R.string.rm) + Utils.formatBalance(debitCreditAmount));

            double grandTotal = cashAmount + guestroomAmount + masterAccountAmount + debitCreditAmount;
            TextView tvGrandTotal = v.findViewById(R.id.grand_total);
            tvGrandTotal.setText(getString(R.string.rm) + Utils.formatBalance(grandTotal));

            new Thread(() -> {
                if (PAXPrinter.getInstance().init().equalsIgnoreCase(Constants.SUCCESS)) {
                    PAXPrinter.getInstance().printView(v);
                    PAXPrinter.getInstance().step(Constants.PAX_END_MARGIN);  //  spacing after printing
                    PAXPrinter.getInstance().start();
                }
            }).start();
        } else {
            Toast.makeText(this, getString(R.string.no_printer), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessCallBack(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case LOGOUT:
                //  logout success
                //  clear active user in SessionManager
                SessionManager.getInstance(this).clear();
                //  show staff list
                goToStaffList(Constants.STAFF_LOGIN);
                finish();
                break;

//            case GET_HYBRID_TICKET:
            case GET_VALET_TICKET:
                //  temp
                GetValetTicketResponse getValetTicketResponse = (GetValetTicketResponse) response.body();
                TicketListsResponse ticketListsResponse = new TicketListsResponse();

                if (getValetTicketResponse != null) {
                    ticketListsResponse.setTicketId(getValetTicketResponse.getTicketId());
                    ticketListsResponse.setSiteId(getValetTicketResponse.getSiteId());
                    ticketListsResponse.setTicketData(getValetTicketResponse.getTicketData());
                    ticketListsResponse.setEntryTime(getValetTicketResponse.getEntryTime());
                    ticketListsResponse.setReqTime(getValetTicketResponse.getReqTime());
                    ticketListsResponse.setAmount(getValetTicketResponse.getAmount());
                    ticketListsResponse.setUpfront_payment(getValetTicketResponse.getUpfront_payment());
                    ticketListsResponse.setFee(getValetTicketResponse.getFee());

                    final Intent intent = new Intent(this, ActivityCashPayment.class);
                    intent.putExtra(Constants.TICKET_INFO_BUNDLE, ticketListsResponse);
                    intent.putExtra("activity", "AppActivity");
                    this.startActivityForResult(intent, Constants.TICKET_PAID_INT);
                } else {
                    CustomAlerts.showOKAlert(this, (dialog, which) -> dialog.dismiss(),
                            getString(R.string.error), getString(R.string.ticketdata_error_msg));
                }
                break;

            case GET_HYBRID_TICKET:
                //  temp
                GetValetTicketResponse hybridTicketResponse = (GetValetTicketResponse) response.body();
                TicketListsResponse hybridPaymentResponse = new TicketListsResponse();

                switch (hybridTicketResponse.getKpCode()) {
                    case "KP_000":   //  ok
                        if (hybridTicketResponse != null) {
                            hybridPaymentResponse.setTicketId(hybridTicketResponse.getTicketId());
                            hybridPaymentResponse.setSiteId(hybridTicketResponse.getSiteId());
                            hybridPaymentResponse.setTicketData(hybridTicketResponse.getTicketData());
                            hybridPaymentResponse.setEntryTime(hybridTicketResponse.getEntryTime());
                            hybridPaymentResponse.setReqTime(hybridTicketResponse.getReqTime());
                            hybridPaymentResponse.setAmount(hybridTicketResponse.getAmount());

                            final Intent intent = new Intent(this, ActivityCashPayment.class);
                            intent.putExtra(Constants.TICKET_INFO_BUNDLE, hybridPaymentResponse);
                            intent.putExtra("activity", "AppActivity");
                            this.startActivityForResult(intent, Constants.TICKET_PAID_INT);
                        } else {
                            CustomAlerts.showOKAlert(this, (dialog, which) -> dialog.dismiss(),
                                    getString(R.string.error), getString(R.string.ticketdata_error_msg));
                        }
                        break;

                    case "KP_003":   //  ticket paid
                        final Intent ticketPaid = new Intent(this, ErrorActivity.class);
                        ticketPaid.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.TICKET_PAID);
                        startActivity(ticketPaid);
                        break;

                    case "KP_004":   //  grace period exceeded
                        final Intent graceExceeded = new Intent(this, ErrorActivity.class);
                        graceExceeded.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.TICKET_EXPIRED);
                        startActivity(graceExceeded);
                        break;
                }
                break;

            case HISTORY_SUMMARY:
                SummaryResponse summaryResponse = (SummaryResponse) response.body();

                if (summaryResponse != null) {
                    printSummary(summaryResponse);
                    //  proceed with logout after receipt printing executed
                    final Intent clockOut = new Intent(this, ErrorActivity.class);
                    clockOut.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.CLOCK_OUT);
                    startActivityForResult(clockOut, Constants.CLOCK_OUT_CODE);
//                    APICall.logout(this, this, Constants.AUTHORIZATION + SessionManager.getInstance(this).getActiveUserInfo().getData().getToken());
                } else {
                    Toast.makeText(this, "unable to get transaction summary info", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onError(Response response, @Nullable Constants.ApiFlags apiFlags) {
        APICallback.hideProgressBar();
        assert apiFlags != null;
        switch (apiFlags) {
            case LOGOUT:
                break;

            case GET_HYBRID_TICKET:
            case GET_VALET_TICKET:
                GetValetTicketResponse valetTicketResponse = (GetValetTicketResponse) response.body();
                Gson gson = new Gson();
                TypeAdapter<GetValetTicketResponse> adapter = gson.getAdapter(GetValetTicketResponse.class);
                try {
                    if (response.errorBody() != null)
                        valetTicketResponse = adapter.fromJson(response.errorBody().string());
                    if (valetTicketResponse != null) {
                        LogUtils.LOGI(this.getClass(), String.valueOf(valetTicketResponse) + "\n" + valetTicketResponse.getKpCode() + "\n" + valetTicketResponse.getKipleCodeDesc());
                        Intent systemError;
                        switch (valetTicketResponse.getKpCode()){
                            case "KP_003":
                                systemError = new Intent(this, ErrorActivity.class);
                                systemError.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.TICKET_PAID);
                                startActivityForResult(systemError, Constants.TICKET_PAID_CODE);
                                break;

                                default:
                                    systemError = new Intent(this, ErrorActivity.class);
                                    systemError.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.INVALID_TICKET);
                                    startActivityForResult(systemError, Constants.INVALID_TICKET_CODE);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                final Intent systemError = new Intent(this, ErrorActivity.class);
//                systemError.putExtra(Constants.ERROR_MSG_BUNDLE, Constants.INVALID_TICKET);
//                startActivityForResult(systemError, Constants.INVALID_TICKET_CODE);
                break;

            case HISTORY_SUMMARY:
                break;
        }
    }
}
